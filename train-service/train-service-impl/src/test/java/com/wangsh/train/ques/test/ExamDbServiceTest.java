package com.wangsh.train.ques.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.ques.pojo.AExamScoreDetail;
import com.wangsh.train.ques.service.IExamDbService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 考试的测试类
 * 
 * @author Wangsh
 */
@EnableAutoConfiguration
public class ExamDbServiceTest extends BaseTest
{
	@Resource
	private IExamDbService examDbService;

	/**
	 * 测试生成种类考试题的随机数
	 */
	@Test
	public void findOneExamScoreDetailService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 考试试卷概要Id */
		condMap.put("id", "1");
		/* 更新考试题目数量 */
		ApiResponse<AExamScoreDetail> apiResponse = this.examDbService.findOneExamScoreDetailService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("==返回结果:{}==",apiResponse.toJSON().toJSONString());
	}
}

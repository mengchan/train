package com.wangsh.train.ques.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.ques.pojo.AExamDesc;
import com.wangsh.train.ques.service.IExamOperService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 考试的测试类
 * 
 * @author Wangsh
 */
@EnableAutoConfiguration
public class ExamOperServiceTest extends BaseTest
{
	@Resource
	private IExamOperService examOperService;

	/**
	 * 测试生成种类考试题的随机数
	 */
	@Test
	public void updateOneExamDescService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 考试试卷概要Id */
		condMap.put("relaId", "5");
		/* 选择技术 */
		condMap.put("techIds", new String[] {"12","13","14"});
		/* 更新考试题目数量 */
		ApiResponse<Object> apiResponse = this.examOperService.updateOneExamDescService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("==返回结果:{}==",apiResponse.toJSON().toJSONString());
	}
	
	/**
	 * 系统随机生成题目
	 */
	@Test
	public void proccedOneExamDescService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 考试试卷概要Id */
		condMap.put("id", "3");
		ApiResponse<AExamDesc> apiResponse = this.examOperService.proccedOneExamDescService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("==返回结果:code:{},info:{}==",apiResponse.getCode() , apiResponse.getInfo());
	}
}

package com.wangsh.train.outer.test;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.BaseTest;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.outer.IServerOuterService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;

/**
 * demo模块的测试类
 * 
 * @author Wangsh
 */
@EnableAutoConfiguration
public class ServerOuterServiceTest extends BaseTest
{
	private String usersCenterOutUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url") ; 
	private String usersCenterOuterPriKey = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.out.prikey");
	
	@Resource
	private IServerOuterService serverOuterService;

	/**
	 * 测试查询所有的朝代
	 */
	@Test
	public void findCondListAdminsService()
	{
		JSONObject dataJSON = new JSONObject();
		dataJSON.put("id","");
		ApiResponse<Object> apiResponse = this.serverOuterService.serverVersionService(dataJSON);
		ConstatFinalUtil.SYS_LOGGER.info("--返回结果:{}",apiResponse.toJSON().toJSONString());
	}
}

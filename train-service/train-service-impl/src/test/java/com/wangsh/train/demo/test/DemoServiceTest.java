package com.wangsh.train.demo.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.common.util.SpringEmailUtil;
import com.wangsh.train.demo.pojo.ADemoDynasty;
import com.wangsh.train.demo.pojo.ADemoKing;
import com.wangsh.train.demo.pojo.ADemoKingEnum;
import com.wangsh.train.demo.service.IDemoDbService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * demo模块的测试类
 * 
 * @author Wangsh
 */
@EnableAutoConfiguration
public class DemoServiceTest extends BaseTest
{
	@Resource
	private IDemoDbService demoDbService;

	/**
	 * 测试查询所有的朝代
	 */
	@Test
	public void findCondListDynastyService()
	{
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setPageSize(5);
		pageInfoUtil.setCurrentPage(2);
		
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		//condMap.put("keyword", "朝");
		//condMap.put("status", "1");
		//condMap.put("stDate", new Date());
		//condMap.put("edDate", new Date());

		/* 查询数据库 */
		ApiResponse<ADemoDynasty> apiResponse = this.demoDbService.findCondListDynastyService(pageInfoUtil, condMap);
		//ApiResponse<ADemoDynasty> apiResponse = this.demoService.findCondListDynastyService(null, condMap);
		
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
		ConstatFinalUtil.SYS_LOGGER.info("pageInfoUtil:{}",pageInfoUtil.toJSON());
		
		/* 获取POJO */
		Map<String, List<ADemoDynasty>> dataList = apiResponse.getDataList() ; 
		List<ADemoDynasty> list = dataList.get(ApiResponseEnum.NAME_LIST.getName());
		/* 循环记录 */
		for (ADemoDynasty demoDynasty : list)
		{
			ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", demoDynasty.getId(), demoDynasty.toJSON());
		}
	}
	
	/**
	 * 测试查询一条的朝代
	 */
	@Test
	public void findOneDynastyService()
	{
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		/* 查询数据库 */
		ApiResponse<ADemoDynasty> apiResponse = this.demoDbService.findOneDynastyService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
		
		Map<String, ADemoDynasty> dataOne = apiResponse.getDataOne() ; 
		/* 查询单条记录 */
		ADemoDynasty demoDynasty = dataOne.get(ApiResponseEnum.NAME_ONE.getName());
		ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", demoDynasty.getId(), demoDynasty.toJSON());
	}
	
	/**
	 * 测试保存一条的朝代
	 */
	@Test
	public void saveOneDynastyService()
	{
		ADemoDynasty demoDynasty = new ADemoDynasty() ; 
		demoDynasty.setName("aa");
		demoDynasty.setContent("bb");
		demoDynasty.setAge(23);
		demoDynasty.setStYear(220);
		demoDynasty.setEdYear(240);
		demoDynasty.setStatus(Byte.valueOf("1"));
		demoDynasty.setCreateTime(new Date());
		demoDynasty.setUpdateTime(new Date());
		demoDynasty.setPubTime(new Date());
		/* 查询数据库 */
		ApiResponse<Object> apiResponse = this.demoDbService.saveOneDynastyService(demoDynasty);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
	}
	
	/**
	 * 测试更新一条的朝代
	 */
	@Test
	public void updateOneDynastyService()
	{
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		/* 查询数据库 */
		ApiResponse<ADemoDynasty> apiResponseDemo = this.demoDbService.findOneDynastyService(condMap);
		Map<String, ADemoDynasty> dataOne = apiResponseDemo.getDataOne() ; 
		/* 查询单条记录 */
		ADemoDynasty demoDynasty = dataOne.get(ApiResponseEnum.NAME_ONE.getName());
		ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", demoDynasty.getId(), demoDynasty.toJSON());
		
		demoDynasty.setName("c");
		demoDynasty.setContent("cc");
		demoDynasty.setAge(11);
		demoDynasty.setStYear(1);
		demoDynasty.setEdYear(1);
		demoDynasty.setStatus(Byte.valueOf("1"));
		demoDynasty.setCreateTime(new Date());
		demoDynasty.setUpdateTime(new Date());
		demoDynasty.setPubTime(new Date());
		
		ApiResponse<Object> apiResponse = this.demoDbService.updateOneDynastyService(demoDynasty);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
	}
	
	/**
	 * 测试查询所有的皇上
	 */
	@Test
	public void findCondListKingService()
	{
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setPageSize(5);
		pageInfoUtil.setCurrentPage(2);
		
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		//condMap.put("keyword", "朝");
		//condMap.put("status", "1");
		//condMap.put("stDate", new Date());
		//condMap.put("edDate", new Date());

		/* 查询数据库 */
		ApiResponse<ADemoKing> apiResponse = this.demoDbService.findCondListKingService(pageInfoUtil, condMap);
		//ApiResponse<ADemoKing> apiResponse = this.demoService.findCondListKingService(null, condMap);
		
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
		ConstatFinalUtil.SYS_LOGGER.info("pageInfoUtil:{}",pageInfoUtil.toJSON());
		
		/* 获取POJO */
		Map<String, List<ADemoKing>> dataList = apiResponse.getDataList() ; 
		List<ADemoKing> list = dataList.get(ApiResponseEnum.NAME_LIST.getName());
		/* 循环记录 */
		for (ADemoKing demoKing : list)
		{
			ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", demoKing.getId(), demoKing.toJSON());
		}
	}
	
	/**
	 * 测试查询一条的皇上
	 */
	@Test
	public void findOneKingService()
	{
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		/* 查询数据库 */
		ApiResponse<ADemoKing> apiResponse = this.demoDbService.findOneKingService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
		
		Map<String, ADemoKing> dataOne = apiResponse.getDataOne() ; 
		/* 查询单条记录 */
		ADemoKing demoKing = dataOne.get(ApiResponseEnum.NAME_ONE.getName());
		ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", demoKing.getId(), demoKing.toJSON());
		
		if(demoKing != null)
		{
			ADemoDynasty demoDynasty = demoKing.getDynasty() ; 
			ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", demoDynasty.getId(), demoDynasty.toJSON());
		}
	}
	
	/**
	 * 测试保存一条的皇上
	 */
	@Test
	public void saveOneKingService()
	{
		ADemoKing demoKing = new ADemoKing() ; 
		demoKing.setName("aa");
		demoKing.setContent("bb");
		demoKing.setMiaoHao("cc");
		demoKing.setNianHao("dd");
		demoKing.setStatus(ADemoKingEnum.STATUS_ENABLE.getStatus());
		demoKing.setCreateTime(new Date());
		demoKing.setUpdateTime(new Date());
		demoKing.setPubTime(new Date());
		/* 查询数据库 */
		ApiResponse<Object> apiResponse = this.demoDbService.saveOneKingService(demoKing);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
	}
	
	/**
	 * 测试更新一条的皇上
	 */
	@Test
	public void updateOneKingService()
	{
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "6");
		/* 查询数据库 */
		ApiResponse<ADemoKing> apiResponseDemo = this.demoDbService.findOneKingService(condMap);
		Map<String, ADemoKing> dataOne = apiResponseDemo.getDataOne() ; 
		/* 查询单条记录 */
		ADemoKing demoKing = dataOne.get(ApiResponseEnum.NAME_ONE.getName());
		ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", demoKing.getId(), demoKing.toJSON());
		
		demoKing.setName("c");
		demoKing.setContent("cc");
		demoKing.setStatus(Byte.valueOf("1"));
		demoKing.setCreateTime(new Date());
		demoKing.setUpdateTime(new Date());
		demoKing.setPubTime(new Date());
		
		ApiResponse<Object> apiResponse = this.demoDbService.updateOneKingService(demoKing);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
	}
	/**
	 * 测试邮箱发送
	 */
	@Test
	public void EmailSendTest() {
		SpringEmailUtil springEmailUtil = new SpringEmailUtil();
		boolean sendTextMail = springEmailUtil.sendTextMail("1293136894@.com", "测试", "测试");
		System.out.println(sendTextMail);
	}
}

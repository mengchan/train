package com.wangsh.train.outer.test;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.BaseTest;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.outer.IClientOuterService;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;

/**
 * demo模块的测试类
 * 
 * @author Wangsh
 */
@EnableAutoConfiguration
public class ClientOuterServiceTest extends BaseTest
{
	@Resource
	private IClientOuterService clientOuterService;

	/**
	 * 测试查询所有的朝代
	 */
	@Test
	public void regionListService()
	{
		JSONObject dataJSON = new JSONObject();
		dataJSON.put("id","1");
		JSONObject responseJSON = this.clientOuterService.regionListService(dataJSON);
		ConstatFinalUtil.SYS_LOGGER.info("--返回结果:{}",responseJSON.toJSONString());
	}
	
	/**
	 * 测试查询所有的朝代
	 */
	@Test
	public void regionBatchListService()
	{
		JSONObject dataJSON = new JSONObject();
		JSONArray idsArr = new JSONArray();
		idsArr.add("1");
		idsArr.add("3");
		idsArr.add("5");
		dataJSON.put("ids",idsArr);
		JSONObject responseJSON = this.clientOuterService.regionBatchOneService(dataJSON);
		ConstatFinalUtil.SYS_LOGGER.info("--返回结果:{}",responseJSON.toJSONString());
	}
	
	/**
	 * 查询分类列表
	 */
	@Test
	public void cateListService()
	{
		JSONObject dataJSON = new JSONObject();
		dataJSON.put("id", "0");
		dataJSON.put("cateType", "0");
		JSONObject responseJSON = this.clientOuterService.cateListService(dataJSON);
		ConstatFinalUtil.SYS_LOGGER.info("--返回结果:{}",responseJSON.toJSONString());
	}
	
	/**
	 * 请求自身服务的时间戳
	 */
	@Test
	public void selfCurrentDateService()
	{
		JSONObject dataJSON = new JSONObject();
		JSONObject responseJSON = this.clientOuterService.selfCurrentDateService(dataJSON);
		ConstatFinalUtil.SYS_LOGGER.info("--返回结果:{}",responseJSON.toJSONString());
	}
	
	/**
	 * 测试用户中心的接口
	 */
	@Test
	public void usersInfoService()
	{
		JSONObject dataReqJSON = new JSONObject();
		dataReqJSON.put("id", "1");
		/* 请求用户中心 */
		JSONObject responseJSON = this.clientOuterService.usersInfoService(dataReqJSON);
		ConstatFinalUtil.SYS_LOGGER.info("--返回结果:{}",responseJSON.toJSONString());
	}
}

package com.wangsh.train.users.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.users.pojo.AAdmins;
import com.wangsh.train.users.service.IUsersDbService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * demo模块的测试类
 * 
 * @author Wangsh
 */
@EnableAutoConfiguration
public class UsersServiceTest extends BaseTest
{
	@Resource
	private IUsersDbService usersDbService;

	/**
	 * 测试查询所有的朝代
	 */
	@Test
	public void findCondListAdminsService()
	{
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setPageSize(2);
		pageInfoUtil.setCurrentPage(1);
		
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		//condMap.put("keyword", "朝");
		//condMap.put("status", "1");
		//condMap.put("stDate", new Date());
		//condMap.put("edDate", new Date());

		/* 查询数据库 */
		ApiResponse<AAdmins> apiResponse = this.usersDbService.findCondListAdminsService(pageInfoUtil, condMap);
		//ApiResponse<AAdmins> apiResponse = this.demoService.findCondListAdminsService(null, condMap);
		
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
		ConstatFinalUtil.SYS_LOGGER.info("pageInfoUtil:{}",pageInfoUtil.toJSON());
		
		/* 获取POJO */
		Map<String, List<AAdmins>> dataList = apiResponse.getDataList() ; 
		List<AAdmins> list = dataList.get(ApiResponseEnum.NAME_LIST.getName());
		/* 循环记录 */
		for (AAdmins admins : list)
		{
			ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", admins.getId(), admins.toJSON());
		}
	}
	
	/**
	 * 测试查询一条的朝代
	 */
	@Test
	public void findOneAdminsService()
	{
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		/* 查询数据库 */
		ApiResponse<AAdmins> apiResponse = this.usersDbService.findOneAdminsService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
		
		Map<String, AAdmins> dataOne = apiResponse.getDataOne() ; 
		/* 查询单条记录 */
		AAdmins admins = dataOne.get(ApiResponseEnum.NAME_ONE.getName());
		ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", admins.getId(), admins.toJSON());
	}
	
	/**
	 * 测试保存一条的朝代
	 */
	@Test
	public void saveOneAdminsService()
	{
		AAdmins admins = new AAdmins() ; 
		admins.setEmail("22@22.com");
		admins.setContent("aa");
		admins.setSsoId(1);
		admins.setStatus(Byte.valueOf("1"));
		admins.setCreateTime(new Date());
		admins.setUpdateTime(new Date());
		admins.setLastLoginTime(new Date());
		/* 查询数据库 */
		ApiResponse<Object> apiResponse = this.usersDbService.saveOneAdminsService(admins);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
	}
	
	/**
	 * 测试更新一条的朝代
	 */
	@Test
	public void updateOneAdminsService()
	{
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		/* 查询数据库 */
		ApiResponse<AAdmins> apiResponseDemo = this.usersDbService.findOneAdminsService(condMap);
		Map<String, AAdmins> dataOne = apiResponseDemo.getDataOne() ; 
		/* 查询单条记录 */
		AAdmins admins = dataOne.get(ApiResponseEnum.NAME_ONE.getName());
		ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", admins.getId(), admins.toJSON());
		
		admins.setEmail(admins.getEmail() + "===");
		admins.setContent(admins.getContent() + "====");
		admins.setSsoId(admins.getSsoId() + 1);
		admins.setStatus(Byte.valueOf("1"));
		admins.setCreateTime(new Date());
		admins.setUpdateTime(new Date());
		admins.setLastLoginTime(new Date());
		
		ApiResponse<Object> apiResponse = this.usersDbService.updateOneAdminsService(admins);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
	}
}

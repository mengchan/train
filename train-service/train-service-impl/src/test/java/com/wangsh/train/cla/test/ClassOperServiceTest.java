package com.wangsh.train.cla.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.cla.service.IClassOperService;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.ConstatFinalUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 班级相关的测试类
 * 
 * @author TeaBig
 */
@EnableAutoConfiguration
public class ClassOperServiceTest extends BaseTest
{
	@Resource
	private IClassOperService classOperService;

	/**
	 * 统计课时
	 */
	@Test
	public void operStatDataService()
	{
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		ApiResponse<Object> operStatApiResponse = this.classOperService.operStatDataService(paramsMap);
		ConstatFinalUtil.SYS_LOGGER.info("==返回结果==:{}",operStatApiResponse.toJSON().toJSONString());
	}
}

package com.wangsh.train.cla.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.cla.service.IClassDbService;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 班级相关的测试类
 * @author TeaBig
 */
@EnableAutoConfiguration
public class ClassDbServiceTest extends BaseTest
{
	@Resource
	private IClassDbService classDbService;
	
	/**
	 * 查询多条班级记录
	 */
	@Test
	public void findCondListClassService()
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ;
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("extend", "true");
		/**
		 * 查询多条记录
		 */
		ApiResponse<AClass> apiResponse = this.classDbService.findCondListClassService(pageInfoUtil, condMap);
		ConstatFinalUtil.SYS_LOGGER.info("----{}",apiResponse.toJSON());
	}
}

package com.wangsh.train.ques.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.ques.service.IQuesOperService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * 模块的测试类
 * 
 * @author Wangsh
 */
@EnableAutoConfiguration
public class QuesOperServiceTest extends BaseTest
{
	@Resource
	private IQuesOperService quesOperService;
	
	/**
	 * 上传xlsx批量处理题库数据
	 */
	@Test
	public void insertBatchAskService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		try
		{
			/* 4_web前后端题库.xlsx */
			//FileInputStream fis = new FileInputStream("d:/1_Java初级题库.xlsx");
			//FileInputStream fis = new FileInputStream("d:/2_java中级题库.xlsx");
			//FileInputStream fis = new FileInputStream("d:/3_java高级题库.xlsx");
			//FileInputStream fis = new FileInputStream("d:/4_web前后端题库.xlsx");
			//FileInputStream fis = new FileInputStream("d:/5_JavaEE框架题库.xlsx"); 
			//FileInputStream fis = new FileInputStream("d:/6_大数据(一)题库.xlsx");
			//FileInputStream fis = new FileInputStream("d:/7_大数据(二)题库.xlsx");
			//FileInputStream fis = new FileInputStream("d:/8_大数据(三)题库.xlsx");
			FileInputStream fis = new FileInputStream("d:/9_大数据(四)题库.xlsx");
			condMap.put("is", fis);
			condMap.put("extendName", "xlsx");
			
			condMap.put("usersId", "2");
			condMap.put("usersClaId", "11");
			/**
			 * 上传文件是xlsx
			 */
			ApiResponse<Object> apiResponse = this.quesOperService.insertBatchAskService(condMap);
			ConstatFinalUtil.SYS_LOGGER.info("结果:{}",apiResponse.toJSON().toJSONString());
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
}

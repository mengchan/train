package com.wangsh.train.system.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.system.pojo.ASysFile;
import com.wangsh.train.system.service.ISystemDbService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 班级相关的测试类
 * @author TeaBig
 */
@EnableAutoConfiguration
public class SystemServiceTest extends BaseTest
{
	@Resource
	private ISystemDbService systemDbService;
	
	/**
	 * 查询多条系统文件记录
	 */
	@Test
	public void findCondListSysFileService()
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ;
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("extend", "true");
		/**
		 * 查询多条记录
		 */
		ApiResponse<ASysFile> apiResponse = this.systemDbService.findCondListSysFileService(pageInfoUtil, condMap);
		ConstatFinalUtil.SYS_LOGGER.info("----{}",apiResponse.toJSON());
	}
}

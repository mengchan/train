package com.wangsh.train.ques.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.ques.pojo.AAsk;
import com.wangsh.train.ques.service.IQuesDbService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 模块的测试类
 * 
 * @author Wangsh
 */
@EnableAutoConfiguration
public class QuesServiceTest extends BaseTest
{
	@Resource
	private IQuesDbService quesDbService;

	/**
	 * 测试查询所有的朝代
	 */
	@Test
	public void findCondListAskService()
	{
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setPageSize(5);
		pageInfoUtil.setCurrentPage(2);
		
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		//condMap.put("keyword", "朝");
		//condMap.put("status", "1");
		//condMap.put("stDate", new Date());
		//condMap.put("edDate", new Date());

		/* 查询数据库 */
		ApiResponse<AAsk> apiResponse = this.quesDbService.findCondListAskService(pageInfoUtil, condMap);
		//ApiResponse<AAsk> apiResponse = this.Service.findCondListAskService(null, condMap);
		
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
		ConstatFinalUtil.SYS_LOGGER.info("pageInfoUtil:{}",pageInfoUtil.toJSON());
		
		/* 获取POJO */
		Map<String, List<AAsk>> dataList = apiResponse.getDataList() ; 
		List<AAsk> list = dataList.get(ApiResponseEnum.NAME_LIST.getName());
		/* 循环记录 */
		for (AAsk Ask : list)
		{
			ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", Ask.getId(), Ask.toJSON());
		}
	}
	
	/**
	 * 测试查询一条的朝代
	 */
	@Test
	public void findOneAskService()
	{
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "2");
		/* 查询数据库 */
		ApiResponse<AAsk> apiResponse = this.quesDbService.findOneAskService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON().toJSONString());
		
		Map<String, AAsk> dataOne = apiResponse.getDataOne() ; 
		/* 查询单条记录 */
		AAsk Ask = dataOne.get(ApiResponseEnum.NAME_ONE.getName());
		ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", Ask.getId(), Ask.toJSON());
	}
	
	/**
	 * 测试保存一条的朝代
	 */
	@Test
	public void saveOneAskService()
	{
		AAsk Ask = new AAsk() ; 
		Ask.setName("aa");
		Ask.setContent("bb");
		Ask.setStatus(Byte.valueOf("1"));
		Ask.setCreateTime(new Date());
		Ask.setUpdateTime(new Date());
		Ask.setPubTime(new Date());
		/* 查询数据库 */
		ApiResponse<Object> apiResponse = this.quesDbService.saveOneAskService(Ask);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
	}
	
	/**
	 * 测试更新一条的朝代
	 */
	@Test
	public void updateOneAskService()
	{
		/* 查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "3");
		/* 查询数据库 */
		ApiResponse<AAsk> apiResponse = this.quesDbService.findOneAskService(condMap);
		Map<String, AAsk> dataOne = apiResponse.getDataOne() ; 
		/* 查询单条记录 */
		AAsk Ask = dataOne.get(ApiResponseEnum.NAME_ONE.getName());
		ConstatFinalUtil.SYS_LOGGER.info("id:{},记录:{}", Ask.getId(), Ask.toJSON());
		
		Ask.setName("c");
		Ask.setContent("cc");
		Ask.setStatus(Byte.valueOf("1"));
		Ask.setCreateTime(new Date());
		Ask.setUpdateTime(new Date());
		Ask.setPubTime(new Date());
		
		ApiResponse<Object> apiDbResponse = this.quesDbService.updateOneAskService(Ask);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}",apiDbResponse.getCode() , apiResponse.getInfo() , apiResponse.toJSON());
	}
}

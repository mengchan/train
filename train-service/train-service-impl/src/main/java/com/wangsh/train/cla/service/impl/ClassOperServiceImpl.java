package com.wangsh.train.cla.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.cla.pojo.AClassDataEnum;
import com.wangsh.train.cla.service.IClassDbService;
import com.wangsh.train.cla.service.IClassOperService;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.system.pojo.ASysTech;
import com.wangsh.train.system.service.ISystemDbService;

/**
 * 班级的操作的Service
 * 
 * @author TeaBig
 */
@Service("classOperService")
public class ClassOperServiceImpl extends BaseServiceImpl implements IClassOperService
{
	@Resource
	private IClassDbService classDbService;	
	@Resource
	private ISystemDbService systemDbService;

	@Override
	public ApiResponse<Object> saveOneClassService(Map<String, Object> paramsMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>() ; 
		/* 获取参数 */
		String regionId = paramsMap.get("regionId") + "" ; 
		String name = paramsMap.get("name") + "" ; 
		String address = paramsMap.get("address") + "" ; 
		String stTime = paramsMap.get("stTime") + "" ; 
		String edTime = paramsMap.get("edTime") + "" ; 
		String content = paramsMap.get("content") + "" ; 
		String claStatus = paramsMap.get("claStatus") + "" ; 
		String status = paramsMap.get("status") + "" ; 
		String pubTime = paramsMap.get("pubTime") + "" ; 
		
		/* 根据不同的请求跳转不同的页面 */
		AClass clas = new AClass();
		clas.setRegionId(Integer.valueOf(regionId));
		clas.setName(name);
		clas.setAddress(address);
		clas.setStTime(this.dateFormatUtil.strDateTime(stTime));
		clas.setEdTime(this.dateFormatUtil.strDateTime(edTime));
		clas.setContent(content);
		clas.setClaStatus(Byte.valueOf(claStatus));
		clas.setStatus(Byte.valueOf(status));
		clas.setPubTime(this.dateFormatUtil.strDateTime(pubTime));
		
		/* 更新用户 */
		clas.setUpdateTime(new Date());
		apiResponse = this.classDbService.saveOneClassService(clas);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneClassService(Map<String, Object> condMap)
	{
		return null;
	}

	@Override
	public ApiResponse<Object> operStatDataService(Map<String, Object> paramsMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 查询数据 */
		condMap.clear();
		/* 统计课时 */
		condMap.put("operType", "statTimeLong");
		condMap.put("status", AClassDataEnum.STATUS_ENABLE.getStatus() );
		ApiResponse<Map<String, Object>> statApiResponse = this.classDbService.statDataService(condMap);
		List<Map<String, Object>> statList = statApiResponse.getDataListJava();
		/* 循环Map */
		for (Map<String, Object> map : statList)
		{
			/* 获取数据 */
			Integer techId = Integer.valueOf(map.get("techId") + "");
			Long totalTimeLong = Long.valueOf(map.get("totalTimeLong") + "");
			
			/* 分别查询技术id,并且一一的赋值 */
			condMap.clear();
			condMap.put("id", techId);
			ApiResponse<ASysTech> techApiResponse = this.systemDbService.findOneSysTechService(condMap);
			/* 技术对象 */
			ASysTech tech = techApiResponse.getDataOneJava();
			/* 设置总时间 */
			tech.setClasTime(totalTimeLong);
			ApiResponse<Object> techUpdateApiResponse = this.systemDbService.updateOneSysTechService(tech);
			ConstatFinalUtil.SYS_LOGGER.info("更新技术列表结果:{}",techUpdateApiResponse.toJSON().toJSONString());
		}
		
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
}

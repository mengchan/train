package com.wangsh.train.job.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.pojo.JSONEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.job.dao.IAJobComPositionDao;
import com.wangsh.train.job.dao.IAJobCompanyDao;
import com.wangsh.train.job.dao.IAJobInterviewDao;
import com.wangsh.train.job.dao.IAJobOfferDao;
import com.wangsh.train.job.dao.IAJobResumeDao;
import com.wangsh.train.job.pojo.AJobComPosition;
import com.wangsh.train.job.pojo.AJobComPositionEnum;
import com.wangsh.train.job.pojo.AJobCompany;
import com.wangsh.train.job.pojo.AJobInterview;
import com.wangsh.train.job.pojo.AJobInterviewEnum;
import com.wangsh.train.job.pojo.AJobOffer;
import com.wangsh.train.job.pojo.AJobOfferEnum;
import com.wangsh.train.job.pojo.AJobResume;
import com.wangsh.train.job.pojo.AJobResumeEnum;
import com.wangsh.train.job.service.IJobDbService;
import com.wangsh.train.outer.IClientOuterService;
import com.wangsh.train.ques.pojo.AAskEnum;

/**
 * 工作模块的Service的实现类
 * 
 * @author TeaBig
 */
@Service("jobService")
public class JobDbServiceImpl extends BaseServiceImpl implements IJobDbService
{
	@Resource
	private IAJobCompanyDao jobCompanyDao;
	@Resource
	private IAJobComPositionDao jobComPositionDao;
	@Resource
	private IAJobInterviewDao jobInterviewDao;
	@Resource
	private IAJobOfferDao jobOfferDao;
	@Resource
	private IAJobResumeDao jobResumeDao;
	
	@Resource
	private IClientOuterService clientOuterService;
	
	@Override
	public ApiResponse<Object> saveOneJobCompanyService(AJobCompany jobCompany)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		jobCompany.setCreateTime(new Date());
		jobCompany.setUpdateTime(new Date());
		
		int res = this.jobCompanyDao.save(jobCompany);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), jobCompany.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), jobCompany);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneJobCompanyService(AJobCompany jobCompany)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		if(jobCompany.getSouStatus() == AAskEnum.STATUS_DRAFT.getStatus() 
				&& jobCompany.getStatus() == AAskEnum.STATUS_DRAFT.getStatus())
		{
			/* 状态由草稿,变成草稿,可以修改无数次 */
			jobCompany.setStatus(AAskEnum.STATUS_DRAFT.getStatus());
		}else if(jobCompany.getSouStatus() == AAskEnum.STATUS_DRAFT.getStatus() 
				&& jobCompany.getStatus() == AAskEnum.STATUS_PUBLISH.getStatus())
		{
			/* 状态由草稿,变成已发布 */
			jobCompany.setStatus(AAskEnum.STATUS_PUBLISH.getStatus());
		}else if(jobCompany.getSouStatus() == AAskEnum.STATUS_PUBLISH.getStatus()
				&& jobCompany.getStatus() == AAskEnum.STATUS_AUDITYES.getStatus())
		{
			/* 状态由发布,变成审核通过 */
			jobCompany.setStatus(AAskEnum.STATUS_AUDITYES.getStatus());
			/* 审核时间 */
			jobCompany.setAuditTime(new Date());
		}else if(jobCompany.getSouStatus() == AAskEnum.STATUS_PUBLISH.getStatus() 
				&& jobCompany.getStatus() == AAskEnum.STATUS_AUDITNO.getStatus())
		{
			/* 状态由审核通过,变成审核失败 */
			jobCompany.setStatus(AAskEnum.STATUS_AUDITNO.getStatus());
			/* 审核时间 */
			jobCompany.setAuditTime(new Date());
		}else if(jobCompany.getSouStatus() == AAskEnum.STATUS_AUDITNO.getStatus() 
				&& jobCompany.getStatus() == AAskEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核失败变成草稿 */
			jobCompany.setStatus(AAskEnum.STATUS_DRAFT.getStatus());
		}else if(jobCompany.getSouStatus() == AAskEnum.STATUS_AUDITYES.getStatus() 
				&& jobCompany.getStatus() == AAskEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核成功变成草稿 */
		}else if(jobCompany.getSouStatus() == AAskEnum.STATUS_AUDITYES.getStatus() 
				&& jobCompany.getStatus() == AAskEnum.STATUS_AUDITYES.getStatus())
		{
			/* 就是为了更新,randTime,全部放行 */
		}else
		{
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		int res = this.jobCompanyDao.update(jobCompany);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), jobCompany.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), jobCompany);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	public ApiResponse<Object> updateOneJobCompanyService(AJobCompany jobCompany,Map<String, Object> paramsMap) throws Exception
	{
		if(paramsMap.size() == 0 )
		{
			return this.updateOneJobCompanyService(jobCompany);
		}
		/* 先更新用户的信息 */
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		try
		{
			String fileName = paramsMap.get("fileName") + "";
			InputStream is = (InputStream) paramsMap.get("inputStream") ;
			
			/* 上传文件的相对路径 */
			String uploadFile = ConstatFinalUtil.CONFIG_JSON.getString("website.uploadFile");
			/* 存储的是网址的绝对路径 */
			String souPath = jobCompany.getLogoPath() ;
			/* 真实路径 */
			String truePath = ConstatFinalUtil.CONFIG_JSON.getString("website.truePath");
			/* 网站的相对路径 */
			String tarRelaPath = uploadFile + "/jobCompany/" + this.dateFormatUtil.dateStr(new Date()) + "/" + UUID.randomUUID().toString() + 
					fileName.substring(fileName.lastIndexOf("."), fileName.length());
			File tarTrueFile = new File(truePath + tarRelaPath);
			
			/* 创建父目录 */
			if(!tarTrueFile.getParentFile().exists())
			{
				tarTrueFile.getParentFile().mkdirs();
			}
			jobCompany.setLogoPath(tarRelaPath);
			/* 更新图片 */
			this.updateOneJobCompanyService(jobCompany);
			
			FileOutputStream fos = new FileOutputStream(tarTrueFile);
			/* 复制文件 */
			boolean flag = this.fileUtil.copyFile(is, fos);
			if(flag)
			{
				if(souPath != null && !"".equalsIgnoreCase(souPath))
				{
					/* 相对路径 */
					File souFile = new File(truePath + souPath);
					if(!souFile.delete())
					{
						ConstatFinalUtil.SYS_LOGGER.error("删除原始文件失败:原始文件路径:{}",souFile.getAbsolutePath());
					}
				}
			}
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("上传头像失败了,id:{}",jobCompany.getId() , e);
			throw e ; 
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneJobCompanyService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.jobCompanyDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AJobCompany> findCondListJobCompanyService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AJobCompany> apiResponse = new ApiResponse<AJobCompany>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AJobCompany> jobCompanyList = Collections.EMPTY_LIST;
		
		Map<String, List<AJobCompany>> dataList = new HashMap<String, List<AJobCompany>>();
			
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			jobCompanyList = this.jobCompanyDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			jobCompanyList = this.jobCompanyDao.findList(condMap) ; 
		}
		
		/* 查询扩展信息 */
		if("true".equalsIgnoreCase(condMap.get("extend") + "") && jobCompanyList.size() > 0 )
		{
			/* 批量查询 */
			JSONArray regionIdsArr = new JSONArray();
			for (Iterator iterator = jobCompanyList.iterator(); iterator.hasNext();)
			{
				AJobCompany jobCompany = (AJobCompany) iterator.next();
				String regionId = jobCompany.getRegionId() + ""; 
				if(!regionIdsArr.contains(jobCompany.getRegionId() + ""))
				{
					regionIdsArr.add(regionId);
				}
			}
			Map<String, JSONObject> regionMap = new HashMap<String, JSONObject>();
			/* 批量请求服务器 */
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ids",regionIdsArr);
			JSONObject responseJSON = this.clientOuterService.regionBatchOneService(dataJSON);
			if(responseJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseJSON.get("data");
				JSONArray regionResArr = (JSONArray) dataResJSON.get("list");
				for (Iterator iterator = regionResArr.iterator(); iterator.hasNext();)
				{
					JSONObject regionTempRegion = (JSONObject) iterator.next();
					regionMap.put(regionTempRegion.getString("id"), regionTempRegion);
				}
			}
			for (Iterator iterator = jobCompanyList.iterator(); iterator.hasNext();)
			{
				AJobCompany jobCompany = (AJobCompany) iterator.next();
				String regionId = jobCompany.getRegionId() + ""; 
				JSONObject regionJSON = regionMap.get(regionId);
				jobCompany.setRegionJSON(regionJSON);
			}
		}
		
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), jobCompanyList);
		
		apiResponse.setDataListJava(jobCompanyList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AJobCompany> findOneJobCompanyService(Map<String, Object> condMap)
	{
		ApiResponse<AJobCompany> apiResponse = new ApiResponse<AJobCompany>();
			
		Map<String, AJobCompany> data = new HashMap<String, AJobCompany>();
		AJobCompany jobCompany = this.jobCompanyDao.findOne(condMap);
		
		if("true".equalsIgnoreCase(condMap.get("extend") + ""))
		{
			/* 批量请求服务器 */
			JSONObject dataJSON = new JSONObject();
			JSONArray regionIdsArr = new JSONArray();
			regionIdsArr.add(jobCompany.getRegionId() + "");
			dataJSON.put("ids",regionIdsArr);
			JSONObject responseJSON = this.clientOuterService.regionBatchOneService(dataJSON);
			if(responseJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseJSON.get("data");
				JSONArray regionResArr = (JSONArray) dataResJSON.get("list");
				if(regionResArr.size() > 0 )
				{
					JSONObject regionJSON = (JSONObject) regionResArr.get(0);
					jobCompany.setRegionJSON(regionJSON);
				}
			}
		}
		
		data.put(ApiResponseEnum.NAME_ONE.getName(), jobCompany);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(jobCompany);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneComPositionService(AJobComPosition comPosition)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		comPosition.setCreateTime(new Date());
		comPosition.setUpdateTime(new Date());
		int res = this.jobComPositionDao.save(comPosition);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), comPosition.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), comPosition);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneComPositionService(AJobComPosition comPosition)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		if(comPosition.getSouStatus() == AJobComPositionEnum.STATUS_DRAFT.getStatus() 
				&& comPosition.getStatus() == AJobComPositionEnum.STATUS_DRAFT.getStatus())
		{
			/* 状态由草稿,变成草稿,可以修改无数次 */
			comPosition.setStatus(AJobComPositionEnum.STATUS_DRAFT.getStatus());
		}else if(comPosition.getSouStatus() == AJobComPositionEnum.STATUS_DRAFT.getStatus() 
				&& comPosition.getStatus() == AJobComPositionEnum.STATUS_PUBLISH.getStatus())
		{
			/* 状态由草稿,变成已发布 */
			comPosition.setStatus(AJobComPositionEnum.STATUS_PUBLISH.getStatus());
		}else if(comPosition.getSouStatus() == AJobComPositionEnum.STATUS_PUBLISH.getStatus()
				&& comPosition.getStatus() == AJobComPositionEnum.STATUS_AUDITYES.getStatus())
		{
			/* 状态由发布,变成审核通过 */
			comPosition.setStatus(AJobComPositionEnum.STATUS_AUDITYES.getStatus());
			/* 审核时间 */
			comPosition.setAuditTime(new Date());
		}else if(comPosition.getSouStatus() == AJobComPositionEnum.STATUS_PUBLISH.getStatus() 
				&& comPosition.getStatus() == AJobComPositionEnum.STATUS_AUDITNO.getStatus())
		{
			/* 状态由审核通过,变成审核失败 */
			comPosition.setStatus(AJobComPositionEnum.STATUS_AUDITNO.getStatus());
			/* 审核时间 */
			comPosition.setAuditTime(new Date());
		}else if(comPosition.getSouStatus() == AJobComPositionEnum.STATUS_AUDITNO.getStatus() 
				&& comPosition.getStatus() == AJobComPositionEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核失败变成草稿 */
			comPosition.setStatus(AJobComPositionEnum.STATUS_DRAFT.getStatus());
		}else if(comPosition.getSouStatus() == AJobComPositionEnum.STATUS_AUDITYES.getStatus() 
				&& comPosition.getStatus() == AJobComPositionEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核成功变成草稿 */
		}else if(comPosition.getSouStatus() == AJobComPositionEnum.STATUS_AUDITYES.getStatus() 
				&& comPosition.getStatus() == AJobComPositionEnum.STATUS_AUDITYES.getStatus())
		{
			/* 就是为了更新,randTime,全部放行 */
		}else
		{
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		comPosition.setUpdateTime(new Date());
		int res = this.jobComPositionDao.update(comPosition);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), comPosition.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), comPosition);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneComPositionService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.jobComPositionDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AJobComPosition> findCondListComPositionService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AJobComPosition> apiResponse = new ApiResponse<AJobComPosition>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AJobComPosition> jobComPositionList = Collections.EMPTY_LIST;
		
		Map<String, List<AJobComPosition>> dataList = new HashMap<String, List<AJobComPosition>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			jobComPositionList = this.jobComPositionDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			jobComPositionList = this.jobComPositionDao.findList(condMap) ; 
		}
		
		/* 查询扩展信息 */
		if("true".equalsIgnoreCase(condMap.get("extend") + "") && jobComPositionList.size() > 0 )
		{
			/* 批量查询 */
			JSONArray regionIdsArr = new JSONArray();
			for (Iterator iterator = jobComPositionList.iterator(); iterator.hasNext();)
			{
				AJobComPosition dataTemp = (AJobComPosition) iterator.next();
				String regionId = dataTemp.getRegionId() + ""; 
				if(!regionIdsArr.contains(dataTemp.getRegionId() + ""))
				{
					regionIdsArr.add(regionId);
				}
			}
			Map<String, JSONObject> regionMap = new HashMap<String, JSONObject>();
			/* 批量请求服务器 */
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ids",regionIdsArr);
			JSONObject responseJSON = this.clientOuterService.regionBatchOneService(dataJSON);
			if(responseJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseJSON.get("data");
				JSONArray regionResArr = (JSONArray) dataResJSON.get("list");
				for (Iterator iterator = regionResArr.iterator(); iterator.hasNext();)
				{
					JSONObject regionTempRegion = (JSONObject) iterator.next();
					regionMap.put(regionTempRegion.getString("id"), regionTempRegion);
				}
			}
			for (Iterator iterator = jobComPositionList.iterator(); iterator.hasNext();)
			{
				AJobComPosition dataTemp = (AJobComPosition) iterator.next();
				String regionId = dataTemp.getRegionId() + ""; 
				JSONObject regionJSON = regionMap.get(regionId);
				dataTemp.setRegionJSON(regionJSON);
			}
		}
		
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), jobComPositionList);
		
		apiResponse.setDataListJava(jobComPositionList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AJobComPosition> findOneComPositionService(Map<String, Object> condMap)
	{
		ApiResponse<AJobComPosition> apiResponse = new ApiResponse<AJobComPosition>();
			
		Map<String, AJobComPosition> data = new HashMap<String, AJobComPosition>();
		AJobComPosition comPosition = this.jobComPositionDao.findOne(condMap);
		
		if("true".equalsIgnoreCase(condMap.get("extend") + ""))
		{
			/* 批量请求服务器 */
			JSONObject dataJSON = new JSONObject();
			JSONArray regionIdsArr = new JSONArray();
			regionIdsArr.add(comPosition.getRegionId() + "");
			dataJSON.put("ids",regionIdsArr);
			JSONObject responseJSON = this.clientOuterService.regionBatchOneService(dataJSON);
			if(responseJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseJSON.get("data");
				JSONArray regionResArr = (JSONArray) dataResJSON.get("list");
				if(regionResArr.size() > 0 )
				{
					JSONObject regionJSON = (JSONObject) regionResArr.get(0);
					comPosition.setRegionJSON(regionJSON);
				}
			}
		}
		
		data.put(ApiResponseEnum.NAME_ONE.getName(), comPosition);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(comPosition);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneInterviewService(AJobInterview interview)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 设置askCreateId */
		interview.setAskCreateId(interview.getCreateId());
		
		interview.setCreateTime(new Date());
		interview.setUpdateTime(new Date());
		int res = this.jobInterviewDao.save(interview);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), interview.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), interview);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneInterviewService(AJobInterview interview)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		if(interview.getSouStatus() == AJobInterviewEnum.STATUS_DRAFT.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_DRAFT.getStatus())
		{
			/* 状态由草稿,变成草稿,可以修改无数次 */
			interview.setStatus(AJobInterviewEnum.STATUS_DRAFT.getStatus());
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_DRAFT.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_PUBLISH.getStatus())
		{
			/* 状态由草稿,变成已发布 */
			interview.setStatus(AJobInterviewEnum.STATUS_PUBLISH.getStatus());
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_PUBLISH.getStatus()
				&& interview.getStatus() == AJobInterviewEnum.STATUS_AUDITYES.getStatus())
		{
			/* 状态由发布,变成审核通过 */
			interview.setStatus(AJobInterviewEnum.STATUS_AUDITYES.getStatus());
			/* 审核时间 */
			interview.setAuditTime(new Date());
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_PUBLISH.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_AUDITNO.getStatus())
		{
			/* 状态由审核通过,变成审核失败 */
			interview.setStatus(AJobInterviewEnum.STATUS_AUDITNO.getStatus());
			/* 审核时间 */
			interview.setAuditTime(new Date());
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_AUDITNO.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核失败变成草稿 */
			interview.setStatus(AJobInterviewEnum.STATUS_DRAFT.getStatus());
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_AUDITYES.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核成功变成草稿 */
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_AUDITYES.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_AUDITYES.getStatus())
		{
			/* 就是为了更新,randTime,全部放行 */
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_AUDITYES.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_INSERTING.getStatus())
		{
			/* 审核成功变成录入中 */
			interview.setStatus(AJobInterviewEnum.STATUS_INSERTING.getStatus());
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_INSERTING.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_INSERTED.getStatus())
		{
			/* 录入中变成录入完成 */
			interview.setStatus(AJobInterviewEnum.STATUS_INSERTED.getStatus());
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_INSERTING.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_AUDITYES.getStatus())
		{
			/* 录入中变成录入审核完成 */
			interview.setStatus(AJobInterviewEnum.STATUS_AUDITYES.getStatus());
		}else if(interview.getSouStatus() == AJobInterviewEnum.STATUS_INSERTED.getStatus() 
				&& interview.getStatus() == AJobInterviewEnum.STATUS_CONFIRM.getStatus())
		{
			/* 录入完成确认 */
			interview.setStatus(AJobInterviewEnum.STATUS_CONFIRM.getStatus());
		}else
		{
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		interview.setUpdateTime(new Date());
		int res = this.jobInterviewDao.update(interview);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), interview.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), interview);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneInterviewService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.jobInterviewDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AJobInterview> findCondListInterviewService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AJobInterview> apiResponse = new ApiResponse<AJobInterview>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AJobInterview> jobInterviewList = Collections.EMPTY_LIST;
		
		Map<String, List<AJobInterview>> dataList = new HashMap<String, List<AJobInterview>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			jobInterviewList = this.jobInterviewDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			jobInterviewList = this.jobInterviewDao.findList(condMap) ; 
		}
		
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), jobInterviewList);
		
		apiResponse.setDataListJava(jobInterviewList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AJobInterview> findOneInterviewService(Map<String, Object> condMap)
	{
		ApiResponse<AJobInterview> apiResponse = new ApiResponse<AJobInterview>();
			
		Map<String, AJobInterview> data = new HashMap<String, AJobInterview>();
		AJobInterview interview = this.jobInterviewDao.findOne(condMap);
		
		data.put(ApiResponseEnum.NAME_ONE.getName(), interview);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(interview);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneOfferService(AJobOffer offer)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		offer.setCreateTime(new Date());
		offer.setUpdateTime(new Date());
		int res = this.jobOfferDao.save(offer);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), offer.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), offer);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneOfferService(AJobOffer offer)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		if(offer.getSouStatus() == AJobOfferEnum.STATUS_DRAFT.getStatus() 
				&& offer.getStatus() == AJobOfferEnum.STATUS_DRAFT.getStatus())
		{
			/* 状态由草稿,变成草稿,可以修改无数次 */
			offer.setStatus(AJobOfferEnum.STATUS_DRAFT.getStatus());
		}else if(offer.getSouStatus() == AJobOfferEnum.STATUS_DRAFT.getStatus() 
				&& offer.getStatus() == AJobOfferEnum.STATUS_PUBLISH.getStatus())
		{
			/* 状态由草稿,变成已发布 */
			offer.setStatus(AJobOfferEnum.STATUS_PUBLISH.getStatus());
		}else if(offer.getSouStatus() == AJobOfferEnum.STATUS_PUBLISH.getStatus()
				&& offer.getStatus() == AJobOfferEnum.STATUS_AUDITYES.getStatus())
		{
			/* 状态由发布,变成审核通过 */
			offer.setStatus(AJobOfferEnum.STATUS_AUDITYES.getStatus());
			/* 审核时间 */
			offer.setAuditTime(new Date());
		}else if(offer.getSouStatus() == AJobOfferEnum.STATUS_PUBLISH.getStatus() 
				&& offer.getStatus() == AJobOfferEnum.STATUS_AUDITNO.getStatus())
		{
			/* 状态由审核通过,变成审核失败 */
			offer.setStatus(AJobOfferEnum.STATUS_AUDITNO.getStatus());
			/* 审核时间 */
			offer.setAuditTime(new Date());
		}else if(offer.getSouStatus() == AJobOfferEnum.STATUS_AUDITNO.getStatus() 
				&& offer.getStatus() == AJobOfferEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核失败变成草稿 */
			offer.setStatus(AJobOfferEnum.STATUS_DRAFT.getStatus());
		}else if(offer.getSouStatus() == AJobOfferEnum.STATUS_AUDITYES.getStatus() 
				&& offer.getStatus() == AJobOfferEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核成功变成草稿 */
		}else if(offer.getSouStatus() == AJobOfferEnum.STATUS_AUDITYES.getStatus() 
				&& offer.getStatus() == AJobOfferEnum.STATUS_AUDITYES.getStatus())
		{
			/* 就是为了更新,randTime,全部放行 */
		}else
		{
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		offer.setUpdateTime(new Date());
		int res = this.jobOfferDao.update(offer);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), offer.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), offer);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneOfferService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.jobOfferDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AJobOffer> findCondListOfferService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AJobOffer> apiResponse = new ApiResponse<AJobOffer>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AJobOffer> jobOfferList = Collections.EMPTY_LIST;
		
		Map<String, List<AJobOffer>> dataList = new HashMap<String, List<AJobOffer>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			jobOfferList = this.jobOfferDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			jobOfferList = this.jobOfferDao.findList(condMap) ; 
		}
		
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), jobOfferList);
		
		apiResponse.setDataListJava(jobOfferList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AJobOffer> findOneOfferService(Map<String, Object> condMap)
	{
		ApiResponse<AJobOffer> apiResponse = new ApiResponse<AJobOffer>();
			
		Map<String, AJobOffer> data = new HashMap<String, AJobOffer>();
		AJobOffer offer = this.jobOfferDao.findOne(condMap);
		
		data.put(ApiResponseEnum.NAME_ONE.getName(), offer);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(offer);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneResumeService(AJobResume resume)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		resume.setCreateTime(new Date());
		resume.setUpdateTime(new Date());
		int res = this.jobResumeDao.save(resume);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), resume.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), resume);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneResumeService(AJobResume resume)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		if(resume.getSouStatus() == AJobResumeEnum.STATUS_DRAFT.getStatus() 
				&& resume.getStatus() == AJobResumeEnum.STATUS_DRAFT.getStatus())
		{
			/* 状态由草稿,变成草稿,可以修改无数次 */
			resume.setStatus(AJobResumeEnum.STATUS_DRAFT.getStatus());
		}else if(resume.getSouStatus() == AJobResumeEnum.STATUS_DRAFT.getStatus() 
				&& resume.getStatus() == AJobResumeEnum.STATUS_PUBLISH.getStatus())
		{
			/* 状态由草稿,变成已发布 */
			resume.setStatus(AJobResumeEnum.STATUS_PUBLISH.getStatus());
			/* 确认一下当前时间,判断提交简历是否按照提交 */
			resume.setTrueTime(new Date());
			if(resume.getMakeTime().getTime() - resume.getTrueTime().getTime() > 0 )
			{
				/* 按时提交 */
				resume.setOnTime(AJobResumeEnum.ONTIME_YES.getStatus());
			}else
			{
				/* 不按时提交 */
				resume.setOnTime(AJobResumeEnum.ONTIME_NO.getStatus());
			}
		}else if(resume.getSouStatus() == AJobResumeEnum.STATUS_PUBLISH.getStatus()
				&& resume.getStatus() == AJobResumeEnum.STATUS_AUDITYES.getStatus())
		{
			/* 状态由发布,变成审核通过 */
			resume.setStatus(AJobResumeEnum.STATUS_AUDITYES.getStatus());
			/* 审核时间 */
			resume.setAuditTime(new Date());
		}else if(resume.getSouStatus() == AJobResumeEnum.STATUS_PUBLISH.getStatus() 
				&& resume.getStatus() == AJobResumeEnum.STATUS_AUDITNO.getStatus())
		{
			/* 状态由审核通过,变成审核失败 */
			resume.setStatus(AJobResumeEnum.STATUS_AUDITNO.getStatus());
			/* 审核时间 */
			resume.setAuditTime(new Date());
		}else if(resume.getSouStatus() == AJobResumeEnum.STATUS_AUDITNO.getStatus() 
				&& resume.getStatus() == AJobResumeEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核失败变成草稿 */
			resume.setStatus(AJobResumeEnum.STATUS_DRAFT.getStatus());
		}else if(resume.getSouStatus() == AJobResumeEnum.STATUS_AUDITYES.getStatus() 
				&& resume.getStatus() == AJobResumeEnum.STATUS_AUDITYES.getStatus())
		{
			/* 就是为了更新,randTime,全部放行 */
		}else
		{
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		resume.setUpdateTime(new Date());
		int res = this.jobResumeDao.update(resume);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), resume.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), resume);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneResumeService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.jobResumeDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AJobResume> findCondListResumeService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AJobResume> apiResponse = new ApiResponse<AJobResume>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AJobResume> jobResumeList = Collections.EMPTY_LIST;
		
		Map<String, List<AJobResume>> dataList = new HashMap<String, List<AJobResume>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			jobResumeList = this.jobResumeDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			jobResumeList = this.jobResumeDao.findList(condMap) ; 
		}
		
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), jobResumeList);
		
		apiResponse.setDataListJava(jobResumeList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AJobResume> findOneResumeService(Map<String, Object> condMap)
	{
		ApiResponse<AJobResume> apiResponse = new ApiResponse<AJobResume>();
			
		Map<String, AJobResume> data = new HashMap<String, AJobResume>();
		AJobResume resume = this.jobResumeDao.findOne(condMap);
		
		data.put(ApiResponseEnum.NAME_ONE.getName(), resume);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(resume);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
}

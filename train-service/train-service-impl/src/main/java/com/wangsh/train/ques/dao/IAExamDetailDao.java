package com.wangsh.train.ques.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.ques.pojo.AExamDetail;

/**
 * 示例代码中考试明细的Dao
 * @author Wangsh
 */
@Mapper
public interface IAExamDetailDao extends IBaseDao<AExamDetail>
{
}

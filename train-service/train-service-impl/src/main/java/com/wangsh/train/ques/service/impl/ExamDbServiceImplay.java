package com.wangsh.train.ques.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.pojo.JSONEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.ques.dao.IAExamDescDao;
import com.wangsh.train.ques.dao.IAExamDetailDao;
import com.wangsh.train.ques.dao.IAExamScoreDescDao;
import com.wangsh.train.ques.dao.IAExamScoreDetailDao;
import com.wangsh.train.ques.pojo.AAnsSel;
import com.wangsh.train.ques.pojo.AAnsSelEnum;
import com.wangsh.train.ques.pojo.AAsk;
import com.wangsh.train.ques.pojo.AAskEnum;
import com.wangsh.train.ques.pojo.AExamDesc;
import com.wangsh.train.ques.pojo.AExamDescEnum;
import com.wangsh.train.ques.pojo.AExamDetail;
import com.wangsh.train.ques.pojo.AExamDetailEnum;
import com.wangsh.train.ques.pojo.AExamScoreDesc;
import com.wangsh.train.ques.pojo.AExamScoreDescEnum;
import com.wangsh.train.ques.pojo.AExamScoreDetail;
import com.wangsh.train.ques.pojo.AExamScoreDetailEnum;
import com.wangsh.train.ques.service.IExamDbService;
import com.wangsh.train.ques.service.IQuesDbService;

/**
 * Service实现类(考试代码)
 * 
 * @author Wangsh
 */
@Service("examDbService")
public class ExamDbServiceImplay extends BaseServiceImpl implements IExamDbService
{
	/**
	 * 拥有的Dao引用
	 */
	@Resource
	private IAExamDescDao examDescDao;
	@Resource
	private IAExamDetailDao examDetailDao;
	@Resource
	private IAExamScoreDescDao examScoreDescDao;
	@Resource
	private IAExamScoreDetailDao examScoreDetailDao;
	
	@Resource
	private IQuesDbService quesDbService;
	
	@Override
	public ApiResponse<Object> saveOneExamDescService(AExamDesc examDesc)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		examDesc.setCreateTime(new Date());
		examDesc.setUpdateTime(new Date());
		int res = this.examDescDao.save(examDesc);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), examDesc.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), examDesc);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneExamDescService(AExamDesc examDesc)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		if(examDesc.getSouStatus() == AExamDescEnum.STATUS_DRAFT.getStatus() 
				&& examDesc.getStatus() == AExamDescEnum.STATUS_DRAFT.getStatus())
		{
			/* 状态由草稿,变成草稿,可以修改无数次 */
			examDesc.setStatus(AExamDescEnum.STATUS_DRAFT.getStatus());
			/* 设置公共信息 */
		}else if(examDesc.getSouStatus() == AExamDescEnum.STATUS_DRAFT.getStatus() 
				&& examDesc.getStatus() == AExamDescEnum.STATUS_PUBLISH.getStatus())
		{
			/* 状态由草稿,变成已发布 */
			examDesc.setStatus(AExamDescEnum.STATUS_PUBLISH.getStatus());
		}else if(examDesc.getSouStatus() == AExamDescEnum.STATUS_PUBLISH.getStatus()
				&& examDesc.getStatus() == AExamDescEnum.STATUS_ING.getStatus())
		{
			/* 状态由发布,变成进行中 */
			examDesc.setStatus(AExamDescEnum.STATUS_ING.getStatus());
		}else if(examDesc.getSouStatus() == AExamDescEnum.STATUS_ING.getStatus() 
				&& examDesc.getStatus() == AExamDescEnum.STATUS_SCORE.getStatus())
		{
			/* 状态由进行中,变成改成绩 */
			examDesc.setStatus(AExamDescEnum.STATUS_SCORE.getStatus());
		}else if(examDesc.getSouStatus() == AExamDescEnum.STATUS_SCORE.getStatus() 
				&& examDesc.getStatus() == AExamDescEnum.STATUS_FINISH.getStatus())
		{
			/* 改成绩变成完成 */
			examDesc.setStatus(AExamDescEnum.STATUS_FINISH.getStatus());
			/* 设置公共信息 */
		}else
		{
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		examDesc.setUpdateTime(new Date());
		int res = this.examDescDao.update(examDesc);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), examDesc.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), examDesc);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneExamDescService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.examDescDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AExamDesc> findCondListExamDescService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AExamDesc> apiResponse = new ApiResponse<AExamDesc>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AExamDesc> ExamDescList = Collections.EMPTY_LIST;
		
		Map<String, List<AExamDesc>> dataList = new HashMap<String, List<AExamDesc>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			ExamDescList = this.examDescDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			ExamDescList = this.examDescDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), ExamDescList);
		
		apiResponse.setDataListJava(ExamDescList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AExamDesc> findOneExamDescService(Map<String, Object> condMap)
	{
		ApiResponse<AExamDesc> apiResponse = new ApiResponse<AExamDesc>();
		Map<String, AExamDesc> data = new HashMap<String, AExamDesc>();
		AExamDesc examDesc = this.examDescDao.findOne(condMap);
		/* 查询扩展信息 */
		if("true".equalsIgnoreCase(condMap.get("extend") + ""))
		{
			Map<String, Object> extMap = new HashMap<String, Object>();
			
			/*==选择题==*/
			extMap.clear();
			extMap.put("examId", examDesc.getId());
			extMap.put("askType", AExamDetailEnum.ASKTYPE_SELECT.getStatus() + "");
			extMap.put("status", AExamDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamDetail> examDetailSelApiResponse = this.findCondListExamDetailService(null, extMap);
			List<AExamDetail> selDetailList = examDetailSelApiResponse.getDataListJava() ;
			examDesc.setSelDetailList(selDetailList);
			
			/*==多选题==*/
			extMap.clear();
			extMap.put("examId", examDesc.getId());
			extMap.put("askType", AExamDetailEnum.ASKTYPE_MULSELECT.getStatus() + "");
			extMap.put("status", AExamDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamDetail> examDetailMultiSelApiResponse = this.findCondListExamDetailService(null, extMap);
			List<AExamDetail> multiSelDetailList = examDetailMultiSelApiResponse.getDataListJava() ;
			examDesc.setMultiSelDetailList(multiSelDetailList);
			
			/*==问答题==*/
			extMap.clear();
			extMap.put("examId", examDesc.getId());
			extMap.put("askType", AExamDetailEnum.ASKTYPE_QUES.getStatus() + "");
			extMap.put("status", AExamDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamDetail> examDetailaskApiResponse = this.findCondListExamDetailService(null, extMap);
			List<AExamDetail> askDetailList = examDetailaskApiResponse.getDataListJava() ;
			examDesc.setAskDetailList(askDetailList);
			
			/*==判断题==*/
			extMap.clear();
			extMap.put("examId", examDesc.getId());
			extMap.put("askType", AExamDetailEnum.ASKTYPE_JUDGE.getStatus() + "");
			extMap.put("status", AExamDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamDetail> examDetailjudeApiResponse = this.findCondListExamDetailService(null, extMap);
			List<AExamDetail> judeDetailList = examDetailjudeApiResponse.getDataListJava() ;
			examDesc.setJudeDetailList(judeDetailList);
			
			/*==编程题==*/
			extMap.clear();
			extMap.put("examId", examDesc.getId());
			extMap.put("askType", AExamDetailEnum.ASKTYPE_PROGRAM.getStatus() + "");
			extMap.put("status", AExamDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamDetail> examDetailproApiResponse = this.findCondListExamDetailService(null, extMap);
			List<AExamDetail> proDetailList = examDetailproApiResponse.getDataListJava() ;
			examDesc.setProDetailList(proDetailList);
			
			/*==面试题==*/
			extMap.clear();
			extMap.put("examId", examDesc.getId());
			extMap.put("askType", AExamDetailEnum.ASKTYPE_QUES.getStatus() + "");
			extMap.put("status", AExamDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamDetail> examDetailinterApiResponse = this.findCondListExamDetailService(null, extMap);
			List<AExamDetail> interDetailList = examDetailinterApiResponse.getDataListJava() ;
			examDesc.setInterDetailList(interDetailList);
		}
		data.put(ApiResponseEnum.NAME_ONE.getName(), examDesc);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(examDesc);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneExamDetailService(AExamDetail examDetail)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		examDetail.setCreateTime(new Date());
		examDetail.setUpdateTime(new Date());
		int res = this.examDetailDao.save(examDetail);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), examDetail.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), examDetail);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneExamDetailService(AExamDetail examDetail)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		examDetail.setUpdateTime(new Date());
		int res = this.examDetailDao.update(examDetail);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), examDetail.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), examDetail);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneExamDetailService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.examDetailDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AExamDetail> findCondListExamDetailService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AExamDetail> apiResponse = new ApiResponse<AExamDetail>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AExamDetail> ExamDetailList = Collections.EMPTY_LIST;
		
		Map<String, List<AExamDetail>> dataList = new HashMap<String, List<AExamDetail>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			ExamDetailList = this.examDetailDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			ExamDetailList = this.examDetailDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), ExamDetailList);
		
		apiResponse.setDataListJava(ExamDetailList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AExamDetail> findOneExamDetailService(Map<String, Object> condMap)
	{
		ApiResponse<AExamDetail> apiResponse = new ApiResponse<AExamDetail>();
			
		Map<String, AExamDetail> data = new HashMap<String, AExamDetail>();
		AExamDetail examDetail = this.examDetailDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), examDetail);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(examDetail);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneExamScoreDescService(AExamScoreDesc examScoreDesc)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 根据试卷查询相关信息 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", examScoreDesc.getExamId());
		ApiResponse<AExamDesc> examApiResponse = this.findOneExamDescService(condMap);
		AExamDesc examDesc = examApiResponse.getDataOneJava();
		if(examDesc.getStatus() != AExamDescEnum.STATUS_ING.getStatus())
		{
			/* 必须是进行中才能交卷;否则无法交卷 */
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		/* 用户id和试卷id,必须唯一 */
		condMap.clear();
		condMap.put("usersId", examScoreDesc.getUsersId());
		condMap.put("examId", examScoreDesc.getExamId());
		ApiResponse<AExamScoreDesc> examScoreDescApiResponse = this.findOneExamScoreDescService(condMap);
		AExamScoreDesc examScoreDescRes = examScoreDescApiResponse.getDataOneJava();
		if(examScoreDescRes != null)
		{
			/* 记录已经存在 */
			apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		/* 根据id查询试卷 */
		condMap.clear();
		condMap.put("id", examScoreDesc.getExamId());
		ApiResponse<AExamDesc> examDescApiResponse = this.findOneExamDescService(condMap);
		AExamDesc examDescRes = examDescApiResponse.getDataOneJava();
		if(examDescRes != null)
		{
			examScoreDesc.setName(examDescRes.getName());
			examScoreDesc.setScore(examDescRes.getScore());
			examScoreDesc.setCreateTime(examDescRes.getCreateTime());
			examScoreDesc.setContent(examDescRes.getContent());
			examScoreDesc.setClassId(examDescRes.getClassId());
		}
		
		examScoreDesc.setUpdateTime(new Date());
		examScoreDesc.setCommitTime(new Date());
		int res = this.examScoreDescDao.save(examScoreDesc);
		if (res > 0)
		{
			int totalScore = 0 ; 
			
			/* 用户交卷,要为用户计算成绩 */
			for (Iterator iterator = examScoreDesc.getExamScoreDetailList().iterator(); iterator.hasNext();)
			{
				AExamScoreDetail examScoreDetail = (AExamScoreDetail) iterator.next();
				
				/* 查询askId对应的问题 */
				String askId = examScoreDetail.getAskId() + ""; 
				condMap.clear();
				condMap.put("id", askId);
				ApiResponse<AAsk> askApiResponse = this.quesDbService.findOneAskService(condMap);
				/* 获取问题的对象 */
				AAsk ask = askApiResponse.getDataOneJava();
				
				examScoreDetail.setName(ask.getName());
				examScoreDetail.setAskType(ask.getAskType());
				
				examScoreDetail.setCorrType(AExamScoreDetailEnum.CORRTYPE_NO.getStatus());
				
				if(ask.getAskType() == AAskEnum.ASKTYPE_JUDGE.getStatus())
				{
					/* 去掉里面的逗号 */
					String answer = examScoreDetail.getAnswer().replace(",", "");
					/* 判断题 */
					if(ask.getAnswer().indexOf(answer) != -1)
					{
						/* 选择正确 */
						examScoreDetail.setCorrType(AExamScoreDetailEnum.CORRTYPE_YES.getStatus());
					}
				}else
				{
					/* 必须是单选题,多选题,判断题这三类题目才可以 */
					/* 假如说是错误的 */
					boolean multiFlag = true ; 
					/* if(ask.getAskType() == AAskEnum.ASKTYPE_SELECT.getStatus() || ask.getAskType() == AAskEnum.ASKTYPE_MULSELECT.getStatus() 
									|| ask.getAskType() == AAskEnum.ASKTYPE_JUDGE.getStatus()) */
					/* 判断用户的选项是否正确 */
					for (Iterator iterator2 = ask.getAnsSelList().iterator(); iterator2.hasNext();)
					{
						AAnsSel selTemp = (AAnsSel) iterator2.next();
						/* 此选项必须正确 */
						if(selTemp.getCorrType() == AAnsSelEnum.CORRTYPE_YES.getStatus())
						{
							/* 判断用户的选项是否正确 */
							if(ask.getAskType() == AAskEnum.ASKTYPE_SELECT.getStatus())
							{
								if(selTemp.getId() == examScoreDetail.getAnswerId())
								{
									/* 选择正确 */
									examScoreDetail.setCorrType(AExamScoreDetailEnum.CORRTYPE_YES.getStatus());
									break ; 
								}
							}else if(ask.getAskType() == AAskEnum.ASKTYPE_MULSELECT.getStatus())
							{
								/* 多选题;找不到,说明选择错误;直接退出,扣分 */
								if(examScoreDetail.getAnswer().indexOf(selTemp.getId() + ",") == -1)
								{
									/* 选择正确 */
									multiFlag = false ; 
									break ; 
								}
							}
							
							/* 多选,用户多选了,如何解决? */
						}else
						{
							/* 错误 */
							if(examScoreDetail.getAnswer().indexOf(selTemp.getId() + ",") != -1)
							{
								/* 选择正确 */
								multiFlag = false ; 
								break ; 
							}
						}
					}
					
					if(multiFlag && ask.getAskType() == AAskEnum.ASKTYPE_MULSELECT.getStatus())
					{
						/* 必须是多选,并且选择正确 */
						examScoreDetail.setCorrType(AExamScoreDetailEnum.CORRTYPE_YES.getStatus());
					}
				}
				
				if(examScoreDetail.getCorrType() == AExamScoreDetailEnum.CORRTYPE_YES.getStatus())
				{
					/* 正确,要算分 */
					examScoreDetail.setScore(ask.getScore());
					totalScore += ask.getScore() ; 
				}
				examScoreDetail.setStatus(AExamScoreDetailEnum.STATUS_ENABLE.getStatus());
				examScoreDetail.setExamScoreId(examScoreDesc.getId());
				examScoreDetail.setPubTime(new Date());
				this.saveOneExamScoreDetailService(examScoreDetail);
			}
			
			examScoreDesc.setScore(totalScore);
			
			examScoreDesc.setStatus(AExamScoreDescEnum.STATUS_SCORE.getStatus());
			/* 更新数据 */
			this.examScoreDescDao.update(examScoreDesc);
			
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), examScoreDesc.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), examScoreDesc);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneExamScoreDescService(AExamScoreDesc examScoreDesc)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		/* 先判断状态
		 * 状态:0:交卷,1:改成绩,2:完成;3:无效
		 *  */
		if(examScoreDesc.getSouStatus() == AExamScoreDescEnum.STATUS_COMMIT.getStatus() 
				&& examScoreDesc.getStatus() == AExamScoreDescEnum.STATUS_SCORE.getStatus())
		{
			/* 状态由交卷,变成改成绩,可以修改无数次 */
			examScoreDesc.setStatus(AExamScoreDescEnum.STATUS_SCORE.getStatus());
			/* 设置公共信息 */
		}else if(examScoreDesc.getSouStatus() == AExamScoreDescEnum.STATUS_SCORE.getStatus() 
				&& examScoreDesc.getStatus() == AExamScoreDescEnum.STATUS_FINISH.getStatus())
		{
			/* 状态由改成绩,变成完成 */
			examScoreDesc.setStatus(AExamScoreDescEnum.STATUS_FINISH.getStatus());
		}else if(examScoreDesc.getSouStatus() == AExamScoreDescEnum.STATUS_FINISH.getStatus()
				&& examScoreDesc.getStatus() == AExamScoreDescEnum.STATUS_INVALID.getStatus())
		{
			/* 状态由完成,变成无效 */
			examScoreDesc.setStatus(AExamScoreDescEnum.STATUS_INVALID.getStatus());
		}else
		{
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		/* 更新成绩明细的分数;还要计算分数 */
		int totalScore = examScoreDesc.getScore() ; 
		
		/* 用户交卷,要为用户计算成绩 */
		for (Iterator iterator = examScoreDesc.getExamScoreDetailList().iterator(); iterator.hasNext();)
		{
			AExamScoreDetail dataTemp = (AExamScoreDetail) iterator.next();
			int score = dataTemp.getScore() ;
			
			/* 查询id对应的问题 */
			String id = dataTemp.getId() + ""; 
			condMap.clear();
			condMap.put("id", id);
			ApiResponse<AExamScoreDetail> detailApiResponse = this.findOneExamScoreDetailService(condMap);
			/* 获取问题的对象 */
			AExamScoreDetail detail = detailApiResponse.getDataOneJava();
			/* 设置分数 */
			detail.setScore(score);
			/* 计算总分 */
			totalScore += detail.getScore() ; 
			this.examScoreDetailDao.update(detail);
		}
		
		examScoreDesc.setScore(totalScore);
		/* 更新数据 */
		this.examScoreDescDao.update(examScoreDesc);
		
		examScoreDesc.setUpdateTime(new Date());
		int res = this.examScoreDescDao.update(examScoreDesc);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), examScoreDesc.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), examScoreDesc);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneExamScoreDescService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.examScoreDescDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AExamScoreDesc> findCondListExamScoreDescService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AExamScoreDesc> apiResponse = new ApiResponse<AExamScoreDesc>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AExamScoreDesc> ExamScoreDescList = Collections.EMPTY_LIST;
		
		Map<String, List<AExamScoreDesc>> dataList = new HashMap<String, List<AExamScoreDesc>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			ExamScoreDescList = this.examScoreDescDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			ExamScoreDescList = this.examScoreDescDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), ExamScoreDescList);
		
		apiResponse.setDataListJava(ExamScoreDescList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AExamScoreDesc> findOneExamScoreDescService(Map<String, Object> condMap)
	{
		ApiResponse<AExamScoreDesc> apiResponse = new ApiResponse<AExamScoreDesc>();
		Map<String, AExamScoreDesc> data = new HashMap<String, AExamScoreDesc>();
		AExamScoreDesc examScoreDesc = this.examScoreDescDao.findOne(condMap);
		/* 查询扩展信息 */
		if("true".equalsIgnoreCase(condMap.get("extend") + ""))
		{
			Map<String, Object> extMap = new HashMap<String, Object>();
			
			/*==选择题==*/
			extMap.clear();
			extMap.put("examScoreId", examScoreDesc.getId());
			extMap.put("askType", AExamScoreDetailEnum.ASKTYPE_SELECT.getStatus() + "");
			extMap.put("status", AExamScoreDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamScoreDetail> examDetailSelApiResponse = this.findCondListExamScoreDetailService(null, extMap);
			List<AExamScoreDetail> selDetailList = examDetailSelApiResponse.getDataListJava() ;
			examScoreDesc.setSelDetailList(selDetailList);
			
			/*==多选题==*/
			extMap.clear();
			extMap.put("examScoreId", examScoreDesc.getId());
			extMap.put("askType", AExamScoreDetailEnum.ASKTYPE_MULSELECT.getStatus() + "");
			extMap.put("status", AExamScoreDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamScoreDetail> examDetailMultiSelApiResponse = this.findCondListExamScoreDetailService(null, extMap);
			List<AExamScoreDetail> multiSelDetailList = examDetailMultiSelApiResponse.getDataListJava() ;
			examScoreDesc.setMultiSelDetailList(multiSelDetailList);
			
			/*==问答题==*/
			extMap.clear();
			extMap.put("examScoreId", examScoreDesc.getId());
			extMap.put("askType", AExamScoreDetailEnum.ASKTYPE_QUES.getStatus() + "");
			extMap.put("status", AExamScoreDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamScoreDetail> examDetailaskApiResponse = this.findCondListExamScoreDetailService(null, extMap);
			List<AExamScoreDetail> askDetailList = examDetailaskApiResponse.getDataListJava() ;
			examScoreDesc.setAskDetailList(askDetailList);
			
			/*==判断题==*/
			extMap.clear();
			extMap.put("examScoreId", examScoreDesc.getId());
			extMap.put("askType", AExamScoreDetailEnum.ASKTYPE_JUDGE.getStatus() + "");
			extMap.put("status", AExamScoreDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamScoreDetail> examDetailjudeApiResponse = this.findCondListExamScoreDetailService(null, extMap);
			List<AExamScoreDetail> judeDetailList = examDetailjudeApiResponse.getDataListJava() ;
			examScoreDesc.setJudeDetailList(judeDetailList);
			
			/*==编程题==*/
			extMap.clear();
			extMap.put("examScoreId", examScoreDesc.getId());
			extMap.put("askType", AExamScoreDetailEnum.ASKTYPE_PROGRAM.getStatus() + "");
			extMap.put("status", AExamScoreDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamScoreDetail> examDetailproApiResponse = this.findCondListExamScoreDetailService(null, extMap);
			List<AExamScoreDetail> proDetailList = examDetailproApiResponse.getDataListJava() ;
			examScoreDesc.setProDetailList(proDetailList);
			
			/*==面试题==*/
			extMap.clear();
			extMap.put("examScoreId", examScoreDesc.getId());
			extMap.put("askType", AExamScoreDetailEnum.ASKTYPE_QUES.getStatus() + "");
			extMap.put("status", AExamScoreDetailEnum.STATUS_ENABLE.getStatus());
			/* 查询问题的数量 */
			ApiResponse<AExamScoreDetail> examDetailinterApiResponse = this.findCondListExamScoreDetailService(null, extMap);
			List<AExamScoreDetail> interDetailList = examDetailinterApiResponse.getDataListJava() ;
			examScoreDesc.setInterDetailList(interDetailList);
		}
		data.put(ApiResponseEnum.NAME_ONE.getName(), examScoreDesc);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(examScoreDesc);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneExamScoreDetailService(AExamScoreDetail examScoreDetail)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		examScoreDetail.setCreateTime(new Date());
		examScoreDetail.setUpdateTime(new Date());
		int res = this.examScoreDetailDao.save(examScoreDetail);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), examScoreDetail.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), examScoreDetail);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneExamScoreDetailService(AExamScoreDetail examScoreDetail)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		examScoreDetail.setUpdateTime(new Date());
		int res = this.examScoreDetailDao.update(examScoreDetail);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), examScoreDetail.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), examScoreDetail);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneExamScoreDetailService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.examScoreDetailDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AExamScoreDetail> findCondListExamScoreDetailService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AExamScoreDetail> apiResponse = new ApiResponse<AExamScoreDetail>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AExamScoreDetail> ExamScoreDetailList = Collections.EMPTY_LIST;
		
		Map<String, List<AExamScoreDetail>> dataList = new HashMap<String, List<AExamScoreDetail>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			ExamScoreDetailList = this.examScoreDetailDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			ExamScoreDetailList = this.examScoreDetailDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), ExamScoreDetailList);
		
		apiResponse.setDataListJava(ExamScoreDetailList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AExamScoreDetail> findOneExamScoreDetailService(Map<String, Object> condMap)
	{
		ApiResponse<AExamScoreDetail> apiResponse = new ApiResponse<AExamScoreDetail>();
		Map<String, AExamScoreDetail> data = new HashMap<String, AExamScoreDetail>();
		AExamScoreDetail examScoreDetail = this.examScoreDetailDao.findOne(condMap);
		/* 查询扩展信息 */
		if("true".equalsIgnoreCase(condMap.get("extend") + ""))
		{
			Map<String, Object> extMap = new HashMap<String, Object>();
			
		}
		data.put(ApiResponseEnum.NAME_ONE.getName(), examScoreDetail);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(examScoreDetail);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
}

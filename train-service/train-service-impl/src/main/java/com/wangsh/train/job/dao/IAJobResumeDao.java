package com.wangsh.train.job.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.job.pojo.AJobResume;

/**
 * 示例代码中公司简历的Dao
 * @author Wangsh
 */
@Mapper
public interface IAJobResumeDao extends IBaseDao<AJobResume>
{
}

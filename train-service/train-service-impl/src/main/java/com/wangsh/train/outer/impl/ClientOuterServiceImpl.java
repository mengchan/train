package com.wangsh.train.outer.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.outer.IClientOuterService;

/**
 * 请求外部的接口的实现类
 * 做为客户端请求外部的接口
 * @author TeaBig
 */
@Service("clientOuterService")
public class ClientOuterServiceImpl extends BaseServiceImpl implements IClientOuterService
{
	/**
	 * 用户中心请求的Url
	 */
	private String requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url") ; 
	private String prikey = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.out.prikey");
	
	/**
	 * 发送请求
	 * @param reqJSON
	 * @param method
	 * @return
	 */
	private JSONObject executeRequest(JSONObject dataReqJSON,String method)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--executeRequest--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>() ; 
		JSONObject reqJSON = new JSONObject();
		reqJSON.put("version", "1");
		reqJSON.put("method", method);
		reqJSON.put("pubKey", UUID.randomUUID().toString());
		reqJSON.put("time", System.currentTimeMillis() + "");
		
		/* 原字符串 */
		String souStr = reqJSON.getString("version") + 
				reqJSON.getString("pubKey") + reqJSON.getString("time") ; 
				
		/* 加密 */
		/* sha256,key加密 */
		HmacUtils hmacUtils = new HmacUtils(HmacAlgorithms.HMAC_SHA_256, prikey);
		/* 加密 */
		String encHou = hmacUtils.hmacHex(souStr);
		
		reqJSON.put("encrypt", encHou);
		reqJSON.put("data", dataReqJSON);
		
		/* 请求第三方 */
		/* 直接请求服务器 */
		/* 存储请求参数 */
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("json", reqJSON.toJSONString());
		
		Map<String,String> headerMap = new HashMap<String, String>();
		Map<String,String> responseMap = new HashMap<String, String>();
		/* 设置超时 */
		headerMap.put(ConstatFinalUtil.REQ_HEADER_IGNORE + "read_timeout", 10 * ConstatFinalUtil.SECOND + "");
		headerMap.put(ConstatFinalUtil.REQ_HEADER_IGNORE + "req_connect_timeout", 10 * ConstatFinalUtil.SECOND + "");
		String response = httpUtil.methodPost(requestUrl, headerMap,paramsMap,responseMap);
		JSONObject responseJSON = (JSONObject) JSON.parse(response);
		if(responseJSON.getString("code").equalsIgnoreCase(ApiResponseEnum.STATUS_SUCCESS.getStatus() + ""))
		{
			/* 成功 */
			JSONObject dataResJSON = (JSONObject) responseJSON.get("data");
			apiResponse.setDataOne(dataResJSON);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse.toJSON(); 
	}
	
	@Override
	public JSONObject regionBatchOneService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--regionBatchOne--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "regionBatchOne");
	}
	
	@Override
	public JSONObject regionListService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--regionList--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "regionList");
	}

	@Override
	public JSONObject regionOneService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--regionOne--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "regionOne");
	}
	
	@Override
	public JSONObject cateListService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--requestOuter--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "cateList");
	}
	
	@Override
	public JSONObject cateOneService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--cateOne--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "cateOne");
	}

	@Override
	public JSONObject cateBatchOneService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--cateBatchOne--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "cateBatchOne");
	}

	@Override
	public JSONObject verifyAdminsTokenService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--verifyAdminsToken--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "verifyAdminsToken");
	}
	
	@Override
	public JSONObject verifyUsersTokenService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--verifyUsersToken--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "verifyUsersToken");
	}

	@Override
	public JSONObject usersInfoService(JSONObject dataJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersInfo--");
		requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.outer.url");
		return this.executeRequest(dataJSON, "usersInfo");
	}

	@Override
	public JSONObject selfCurrentDateService(JSONObject dataJSON)
	{
		this.requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("self.outer.url") ; 
		return this.executeRequest(dataJSON, "currentDate");
	}

	@Override
	public JSONObject selfFindOneAskService(JSONObject dataJSON)
	{
		this.requestUrl = ConstatFinalUtil.CONFIG_JSON.getString("self.outer.url") ; 
		return this.executeRequest(dataJSON, "findOneAsk");
	}
}

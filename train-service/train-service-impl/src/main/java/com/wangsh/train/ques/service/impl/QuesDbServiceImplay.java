package com.wangsh.train.ques.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.pojo.JSONEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.ques.dao.IAAnsSelDao;
import com.wangsh.train.ques.dao.IAAskDao;
import com.wangsh.train.ques.pojo.AAnsSel;
import com.wangsh.train.ques.pojo.AAnsSelEnum;
import com.wangsh.train.ques.pojo.AAsk;
import com.wangsh.train.ques.pojo.AAskEnum;
import com.wangsh.train.ques.service.IQuesDbService;
import com.wangsh.train.system.pojo.ASysPro;

/**
 * Service实现类(示例代码)
 * 
 * @author Wangsh
 */
@Service("quesDbService")
public class QuesDbServiceImplay extends BaseServiceImpl implements IQuesDbService
{
	/**
	 * 拥有的Dao引用
	 */
	@Resource
	private IAAskDao AskDao;
	@Resource
	private IAAnsSelDao ansSelDao;
	
	@Override
	public ApiResponse<Object> saveOneAskService(AAsk ask)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		Map<String, Object> condMap = new HashMap<String, Object>();
		/*  判断名称是否存在 */
		condMap.clear();
		condMap.put("name", ask.getName());
		condMap.put("askType", ask.getAskType());
		/* 查询到的结果 */
		ApiResponse<AAsk> apiResponseRes = this.findOneAskService(condMap);
		AAsk askRes = apiResponseRes.getDataOneJava();
		if(askRes != null)
		{
			/* 如果结果存在 */
			/* 说明找到了记录,直接返回 */
			apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
			return apiResponse; 
		}
		
		/* 名称太长，存储到content字段中 */
		if(ask.getName().length() > 255)
		{
			ask.setName(ask.getName().substring(0, 255));
			ask.setContent(ask.getName());
		}
		
		/* 设置公共信息 */
		commonAskServiceUtil(ask);
		
		ask.setCreateTime(new Date());
		ask.setUpdateTime(new Date());
		ask.setRandTime(new Date());
		int res = this.AskDao.save(ask);
		if (res > 0)
		{
			/* 保存问题成功,要保存答案 */
			for (Iterator iterator = ask.getAnsSelList().iterator(); iterator.hasNext();)
			{
				AAnsSel ansSelTemp = (AAnsSel) iterator.next();
				
				/* 设置问题Id */
				ansSelTemp.setAskId(ask.getId());
				
				/* 保存问题 */
				this.saveOneAnsSelService(ansSelTemp);
			}
			
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), ask.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), ask);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	/**
	 * 设置问题的公共信息
	 * @param ask
	 */
	private void commonAskServiceUtil(AAsk ask)
	{
		/* 设置分数 */
		ASysPro quesSelSysPro = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("ques_sel");
		ASysPro quesAskSysPro = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("ques_ask");
		ASysPro quesProSysPro = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("ques_pro");
		ASysPro quesJudgeScoreSysPro = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("ques_judge_score");
		ASysPro quesMultiScoreSysPro = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("ques_multiSel_score");
		
		if(ask.getAskType() == AAskEnum.ASKTYPE_SELECT.getStatus())
		{
			/* 选择题 */
			ask.setScore(Integer.valueOf(quesSelSysPro.getVals()));
		}else if(ask.getAskType() == AAskEnum.ASKTYPE_QUES.getStatus())
		{
			/* 问答题 */
			ask.setScore(Integer.valueOf(quesAskSysPro.getVals()));
		}else if(ask.getAskType() == AAskEnum.ASKTYPE_PROGRAM.getStatus())
		{
			/* 编程题 */
			ask.setScore(Integer.valueOf(quesProSysPro.getVals()));
		}else if(ask.getAskType() == AAskEnum.ASKTYPE_JUDGE.getStatus())
		{
			/* 判断题 */
			ask.setScore(Integer.valueOf(quesJudgeScoreSysPro.getVals()));
		}else if(ask.getAskType() == AAskEnum.ASKTYPE_MULSELECT.getStatus())
		{
			/* 判断题 */
			ask.setScore(Integer.valueOf(quesMultiScoreSysPro.getVals()));
		}
	}

	@Override
	public ApiResponse<Object> updateOneAskService(AAsk ask)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		if(ask.getSouStatus() == AAskEnum.STATUS_DRAFT.getStatus() 
				&& ask.getStatus() == AAskEnum.STATUS_DRAFT.getStatus())
		{
			/* 状态由草稿,变成草稿,可以修改无数次 */
			ask.setStatus(AAskEnum.STATUS_DRAFT.getStatus());
			/* 设置公共信息 */
			commonAskServiceUtil(ask);
		}else if(ask.getSouStatus() == AAskEnum.STATUS_DRAFT.getStatus() 
				&& ask.getStatus() == AAskEnum.STATUS_PUBLISH.getStatus())
		{
			/* 状态由草稿,变成已发布 */
			ask.setStatus(AAskEnum.STATUS_PUBLISH.getStatus());
			/* 设置公共信息 */
			commonAskServiceUtil(ask);
		}else if(ask.getSouStatus() == AAskEnum.STATUS_PUBLISH.getStatus()
				&& ask.getStatus() == AAskEnum.STATUS_AUDITYES.getStatus())
		{
			/* 状态由发布,变成审核通过 */
			ask.setStatus(AAskEnum.STATUS_AUDITYES.getStatus());
			/* 审核时间 */
			ask.setAuditTime(new Date());
		}else if(ask.getSouStatus() == AAskEnum.STATUS_PUBLISH.getStatus() 
				&& ask.getStatus() == AAskEnum.STATUS_AUDITNO.getStatus())
		{
			/* 状态由审核通过,变成审核失败 */
			ask.setStatus(AAskEnum.STATUS_AUDITNO.getStatus());
			/* 审核时间 */
			ask.setAuditTime(new Date());
		}else if(ask.getSouStatus() == AAskEnum.STATUS_AUDITNO.getStatus() 
				&& ask.getStatus() == AAskEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核失败变成草稿 */
			ask.setStatus(AAskEnum.STATUS_DRAFT.getStatus());
			/* 设置公共信息 */
			commonAskServiceUtil(ask);
		}else if(ask.getSouStatus() == AAskEnum.STATUS_AUDITYES.getStatus() 
				&& ask.getStatus() == AAskEnum.STATUS_DRAFT.getStatus())
		{
			/* 审核成功变成草稿 */
			ask.setStatus(AAskEnum.STATUS_DRAFT.getStatus());
			/* 设置公共信息 */
			commonAskServiceUtil(ask);
		}else if(ask.getSouStatus() == AAskEnum.STATUS_AUDITYES.getStatus() 
				&& ask.getStatus() == AAskEnum.STATUS_AUDITYES.getStatus())
		{
			/* 就是为了更新,randTime,全部放行 */
		}else
		{
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		/*  判断名称是否存在 */
		condMap.clear();
		condMap.put("name", ask.getName());
		condMap.put("askType", ask.getAskType());
		/* 查询到的结果 */
		ApiResponse<AAsk> apiResponseRes = this.findOneAskService(condMap);
		AAsk askRes = apiResponseRes.getDataOneJava();
		if(askRes != null && askRes.getId() != ask.getId())
		{
			/* 如果结果存在 */
			/* 说明找到了记录,直接返回 */
			apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
			return apiResponse; 
		}
		
		ask.setUpdateTime(new Date());
		ask.setRandTime(new Date());
		int res = this.AskDao.update(ask);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), ask.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), ask);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneAskService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.AskDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AAsk> findCondListAskService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AAsk> apiResponse = new ApiResponse<AAsk>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AAsk> AskList = Collections.EMPTY_LIST;
		
		Map<String, List<AAsk>> dataList = new HashMap<String, List<AAsk>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			AskList = this.AskDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			AskList = this.AskDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), AskList);
		
		apiResponse.setDataListJava(AskList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AAsk> findOneAskService(Map<String, Object> condMap)
	{
		ApiResponse<AAsk> apiResponse = new ApiResponse<AAsk>();
			
		Map<String, AAsk> data = new HashMap<String, AAsk>();
		AAsk ask = this.AskDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), ask);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(ask);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneAnsSelService(AAnsSel ansSel)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		ansSel.setCreateTime(new Date());
		ansSel.setUpdateTime(new Date());
		int res = this.ansSelDao.save(ansSel);
		if (res > 0)
		{
			/* 更新选项序号开始 */
			updateBatchAnsSelService(ansSel);
			
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), ansSel.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), ansSel);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	/**
	 * 更新选项序号
	 * @param ansSel
	 */
	private void updateBatchAnsSelService(AAnsSel ansSel)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		if(ansSel.getCorrType() == AAnsSelEnum.CORRTYPE_YES.getStatus())
		{
			/* 设置正确答案 */
			condMap.clear();
			condMap.put("id", ansSel.getAskId());
			ApiResponse<AAsk> askApiResponse = this.findOneAskService(condMap);
			AAsk ask = askApiResponse.getDataOneJava();
			if(ask.getAskType() == AAskEnum.ASKTYPE_SELECT.getStatus())
			{
				/* 正确答案 */
				ask.setAnswer(ansSel.getSn());
				ask.setUpdateTime(new Date());
				/* 更新问题 */
				this.updateOneAskService(ask);
			}else
			{
				return ; 
			}
		}
			
		/* ====更新选项序号开始==== */
		/* 查询出当前问题对应的所有选项 */
		condMap.clear();
		condMap.put("askId", ansSel.getAskId());
		condMap.put("orderBy", "pubTimeAsc");
		ApiResponse<AAnsSel> ansSelApiResponse = this.findCondListAnsSelService(null, condMap);
		List<AAnsSel> ansSelList = ansSelApiResponse.getDataListJava();
		char startCh = 'A';
		for (Iterator iterator = ansSelList.iterator(); iterator.hasNext();)
		{
			AAnsSel ansSelTemp = (AAnsSel) iterator.next();
			if(ansSel.getCorrType() == AAnsSelEnum.CORRTYPE_YES.getStatus())
			{
				/* 如果当前的是正确的,其实的都是错误的 */
				if(ansSel.getId() != ansSelTemp.getId())
				{
					/* 设置为错误 */
					ansSelTemp.setCorrType(AAnsSelEnum.CORRTYPE_NO.getStatus());
				}
			}
			/* 更新序号 */
			ansSelTemp.setSn(startCh + "");
			ansSelTemp.setUpdateTime(new Date());
			/* 更新序号 */
			this.ansSelDao.update(ansSelTemp);
			
			startCh ++ ; 
		}
		/* ====更新选项序号结束==== */
	}

	@Override
	public ApiResponse<Object> updateOneAnsSelService(AAnsSel ansSel)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		ansSel.setUpdateTime(new Date());
		int res = this.ansSelDao.update(ansSel);
		if (res > 0)
		{
			/* 更新选项序号开始 */
			updateBatchAnsSelService(ansSel);
			
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), ansSel.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), ansSel);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneAnsSelService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.ansSelDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AAnsSel> findCondListAnsSelService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AAnsSel> apiResponse = new ApiResponse<AAnsSel>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AAnsSel> AnsSelList = Collections.EMPTY_LIST;
		
		Map<String, List<AAnsSel>> dataList = new HashMap<String, List<AAnsSel>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			AnsSelList = this.ansSelDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			AnsSelList = this.ansSelDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), AnsSelList);
		
		apiResponse.setDataListJava(AnsSelList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AAnsSel> findOneAnsSelService(Map<String, Object> condMap)
	{
		ApiResponse<AAnsSel> apiResponse = new ApiResponse<AAnsSel>();
			
		Map<String, AAnsSel> data = new HashMap<String, AAnsSel>();
		AAnsSel ansSel = this.ansSelDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), ansSel);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(ansSel);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
}

package com.wangsh.train.cla.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.cla.pojo.AClassData;
import com.wangsh.train.common.dao.IBaseDao;

/**
 * 示例代码中课程资料的Dao
 * 
 * @author Wangsh
 */
@Mapper
public interface IAClassDataDao extends IBaseDao<AClassData>
{
}

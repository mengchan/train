package com.wangsh.train.ques.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.ques.pojo.AExamDesc;

/**
 * 示例代码中问题的Dao
 * @author Wangsh
 */
@Mapper
public interface IAExamDescDao extends IBaseDao<AExamDesc>
{
}

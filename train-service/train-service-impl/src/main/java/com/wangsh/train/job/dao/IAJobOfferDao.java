package com.wangsh.train.job.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.job.pojo.AJobOffer;

/**
 * 示例代码中公司offer的Dao
 * @author Wangsh
 */
@Mapper
public interface IAJobOfferDao extends IBaseDao<AJobOffer>
{
}

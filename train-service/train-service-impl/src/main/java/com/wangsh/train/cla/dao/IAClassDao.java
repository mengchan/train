package com.wangsh.train.cla.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.common.dao.IBaseDao;

/**
 * 班级的Dao
 * @author TeaBig
 */
@Mapper
public interface IAClassDao extends IBaseDao<AClass>
{
}

package com.wangsh.train.users.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.users.pojo.AUsersClass;

/**
 * 用户班级的Dao
 * @author TeaBig
 */
@Mapper
public interface IAUsersClassDao extends IBaseDao<AUsersClass>
{
	/**
	 * 批量更新
	 */
	int updateBatch(Map<String, Object> condMap);
}

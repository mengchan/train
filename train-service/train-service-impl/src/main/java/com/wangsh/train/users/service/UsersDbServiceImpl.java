package com.wangsh.train.users.service;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.pojo.JSONEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.system.pojo.ASysPro;
import com.wangsh.train.users.dao.IAAdminsDao;
import com.wangsh.train.users.dao.IAUsersCateDao;
import com.wangsh.train.users.dao.IAUsersClassDao;
import com.wangsh.train.users.dao.IAUsersDao;
import com.wangsh.train.users.pojo.AAdmins;
import com.wangsh.train.users.pojo.AUsers;
import com.wangsh.train.users.pojo.AUsersCate;
import com.wangsh.train.users.pojo.AUsersCateEnum;
import com.wangsh.train.users.pojo.AUsersClass;
import com.wangsh.train.users.pojo.AUsersClassEnum;

/**
 * 用户模块的Service的实现类
 * 
 * @author TeaBig
 */
@Service("usersService")
public class UsersDbServiceImpl extends BaseServiceImpl implements IUsersDbService
{
	@Resource
	private IAAdminsDao adminsDao;
	@Resource
	private IAUsersDao usersDao;
	@Resource
	private IAUsersClassDao usersClassDao;
	@Resource
	private IAUsersCateDao usersCateDao;
	
	@Override
	public ApiResponse<Object> saveOneAdminsService(AAdmins admins)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		int res = this.adminsDao.save(admins);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), admins.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), admins);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneAdminsService(AAdmins admins)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.adminsDao.update(admins);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), admins.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), admins);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneAdminsService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.adminsDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AAdmins> findCondListAdminsService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AAdmins> apiResponse = new ApiResponse<AAdmins>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AAdmins> adminsList = Collections.EMPTY_LIST;
		
		Map<String, List<AAdmins>> dataList = new HashMap<String, List<AAdmins>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			adminsList = this.adminsDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			adminsList = this.adminsDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), adminsList);
		
		apiResponse.setDataListJava(adminsList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AAdmins> findOneAdminsService(Map<String, Object> condMap)
	{
		ApiResponse<AAdmins> apiResponse = new ApiResponse<AAdmins>();
			
		Map<String, AAdmins> data = new HashMap<String, AAdmins>();
		AAdmins admins = this.adminsDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), admins);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(admins);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneUsersService(AUsers users)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.usersDao.save(users);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), users.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), users);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneUsersService(AUsers users)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.usersDao.update(users);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), users.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), users);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneUsersService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.usersDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AUsers> findCondListUsersService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AUsers> apiResponse = new ApiResponse<AUsers>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AUsers> usersList = Collections.EMPTY_LIST;
		
		Map<String, List<AUsers>> dataList = new HashMap<String, List<AUsers>>();
			
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			usersList = this.usersDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			usersList = this.usersDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), usersList);
		
		apiResponse.setDataListJava(usersList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AUsers> findOneUsersService(Map<String, Object> condMap)
	{
		ApiResponse<AUsers> apiResponse = new ApiResponse<AUsers>();
			
		Map<String, AUsers> data = new HashMap<String, AUsers>();
		AUsers users = this.usersDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), users);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(users);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneUsersClassService(AUsersClass usersClass)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		usersClass.setCreateTime(new Date());
		usersClass.setUpdateTime(new Date());
		
		/**
		 * 根据用户id和班级id查询是否重复
		 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("usersId", usersClass.getUsersId());
		condMap.put("classId", usersClass.getClassId());
		
		ApiResponse<AUsersClass> usersClassResponse = this.findOneUsersClassService(condMap);
		AUsersClass usersRes = usersClassResponse.getDataOneJava() ; 
		if(usersRes != null)
		{
			/* 如果分页记录大于1,意味着重复 */
			apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		/* 如果当前为默认值 */
		if(usersClass.getDefType() == AUsersClassEnum.DEFTYPE_YES.getStatus())
		{
			/* 将班级的所有状态更新为非默认 */
			condMap.clear();
			condMap.put("operType", "defTypeBatch");
			condMap.put("defType", AUsersClassEnum.DEFTYPE_NO.getStatus());
			condMap.put("usersId", usersClass.getUsersId());
			apiResponse = this.updateBatchUsersClassService(condMap);
		}
		
		int res = this.usersClassDao.save(usersClass);
		if (res > 0)
		{
			/* 如果当前为默认值 */
			if(usersClass.getDefType() == AUsersClassEnum.DEFTYPE_YES.getStatus())
			{
				/* 更新用户表中的班级Id */
				/* 查询用户 */
				condMap.clear();
				condMap.put("id", usersClass.getUsersId());
				ApiResponse<AUsers> usersResponse = this.findOneUsersService(condMap);
				AUsers users = usersResponse.getDataOneJava(); 
				users.setUsersClassId(usersClass.getId());
				users.setClassId(usersClass.getClassId());
				users.setUpdateTime(new Date());
				this.updateOneUsersService(users);
			}
			
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), usersClass.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), usersClass);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneUsersClassService(AUsersClass usersClass)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
//		if(usersClass.getStatus() == AUsersClassEnum.STATUS_DISABLE.getStatus())
//		{
//			/* 用户班级只有状态为启用的时候 */
//			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
//			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
//			return apiResponse ;
//		}
		
		/**
		 * 根据用户id和班级id查询是否重复
		 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("usersId", usersClass.getUsersId());
		condMap.put("classId", usersClass.getClassId());
		
		ApiResponse<AUsersClass> usersClassResponse = this.findOneUsersClassService(condMap);
		AUsersClass usersRes = usersClassResponse.getDataOneJava() ; 
		if(usersRes != null && usersRes.getId() != usersClass.getId())
		{
			/* 如果分页记录大于1,意味着重复 */
			apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		/* 如果当前为默认值 */
		if(usersClass.getDefType() == AUsersClassEnum.DEFTYPE_YES.getStatus())
		{
			/* 将班级的所有状态更新为非默认 */
			condMap.clear();
			condMap.put("operType", "defTypeBatch");
			condMap.put("defType", AUsersClassEnum.DEFTYPE_NO.getStatus());
			condMap.put("usersId", usersClass.getUsersId());
			apiResponse = this.updateBatchUsersClassService(condMap);
		}
		
		int res = this.usersClassDao.update(usersClass);
		if (res > 0)
		{
			/* 如果当前为默认值 */
			if(usersClass.getDefType() == AUsersClassEnum.DEFTYPE_YES.getStatus())
			{
				/* 更新用户表中的班级Id */
				/* 查询用户 */
				condMap.clear();
				condMap.put("id", usersClass.getUsersId());
				ApiResponse<AUsers> usersResponse = this.findOneUsersService(condMap);
				AUsers users = usersResponse.getDataOneJava(); 
				users.setUsersClassId(usersClass.getId());
				users.setClassId(usersClass.getClassId());
				users.setUpdateTime(new Date());
				this.updateOneUsersService(users);
			}
			
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), usersClass.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), usersClass);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	@Override
	public ApiResponse<Object> updateBatchUsersClassService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		int res = this.usersClassDao.updateBatch(condMap) ;
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneUsersClassService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.usersClassDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AUsersClass> findCondListUsersClassService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AUsersClass> apiResponse = new ApiResponse<AUsersClass>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AUsersClass> usersClassList = Collections.EMPTY_LIST;
		
		Map<String, List<AUsersClass>> dataList = new HashMap<String, List<AUsersClass>>();
			
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			usersClassList = this.usersClassDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			usersClassList = this.usersClassDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), usersClassList);
		
		apiResponse.setDataListJava(usersClassList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AUsersClass> findOneUsersClassService(Map<String, Object> condMap)
	{
		ApiResponse<AUsersClass> apiResponse = new ApiResponse<AUsersClass>();
			
		Map<String, AUsersClass> data = new HashMap<String, AUsersClass>();
		AUsersClass usersClass = this.usersClassDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), usersClass);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(usersClass);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneUsersCateService(AUsersCate usersCate)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		usersCate.setCreateTime(new Date());
		
		ASysPro stuDaySys = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("usersCate_template_id_stuDay");
		ASysPro stuWeekSys = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("usersCate_template_id_week");
		ASysPro stuMonthSys = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("usersCate_template_id_month");
		
		/* 根据日期和模板id查询,当天的日报只能填写一个 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("cateTempId", usersCate.getCateTempId());
		condMap.put("usersClaId", usersCate.getUsersClaId());
		/* 日期 */
		Date currDate = usersCate.getCurrDate() ; 
		Calendar calendar = Calendar.getInstance() ; 
		calendar.setTime(currDate);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		/* 默认按照天 */
		Date currStDate = new Date() ; 
		calendar.add(Calendar.DATE, 1);
		Date currEdDate = new Date() ; 
		if(usersCate.getCateTempId() == Integer.valueOf(stuDaySys.getVals()))
		{
			currStDate = calendar.getTime() ; 
			calendar.add(Calendar.DATE, 1);
			currEdDate = calendar.getTime() ; 
		}else if(usersCate.getCateTempId() == Integer.valueOf(stuWeekSys.getVals()))
		{
			calendar.set(Calendar.DAY_OF_WEEK, 1);
			currStDate = calendar.getTime() ; 
			calendar.add(Calendar.DATE, 7);
			currEdDate = calendar.getTime() ; 
		}else if(usersCate.getCateTempId() == Integer.valueOf(stuMonthSys.getVals()))
		{
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			currStDate = calendar.getTime() ; 
			calendar.add(Calendar.MONTH, 1);
			currEdDate = calendar.getTime() ; 
		}
		condMap.put("currStDate", this.dateFormatUtil.dateTimeStr(currStDate));
		condMap.put("currEdDate", this.dateFormatUtil.dateTimeStr(currEdDate));
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		this.findCondListUsersCateService(pageInfoUtil, condMap) ;
		if(pageInfoUtil.getTotalRecord() > 0)
		{
			/* 说明已经找到了 */
			apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		int res = this.usersCateDao.save(usersCate);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), usersCate.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), usersCate);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneUsersCateService(AUsersCate usersCate)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		ASysPro stuDaySys = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("usersCate_template_id_stuDay");
		ASysPro stuWeekSys = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("usersCate_template_id_week");
		ASysPro stuMonthSys = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("usersCate_template_id_month");
		
		/* 根据日期和模板id查询,当天的日报只能填写一个 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("cateTempId", usersCate.getCateTempId());
		condMap.put("usersClaId", usersCate.getUsersClaId());
		/* 日期 */
		Date currDate = usersCate.getCurrDate() ; 
		Calendar calendar = Calendar.getInstance() ; 
		calendar.setTime(currDate);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		/* 默认按照天 */
		Date currStDate = new Date() ; 
		calendar.add(Calendar.DATE, 1);
		Date currEdDate = new Date() ; 
		if(usersCate.getCateTempId() == Integer.valueOf(stuDaySys.getVals()))
		{
			currStDate = calendar.getTime() ; 
			calendar.add(Calendar.DATE, 1);
			currEdDate = calendar.getTime() ; 
		}else if(usersCate.getCateTempId() == Integer.valueOf(stuWeekSys.getVals()))
		{
			calendar.set(Calendar.DAY_OF_WEEK, 1);
			currStDate = calendar.getTime() ; 
			calendar.add(Calendar.DATE, 7);
			currEdDate = calendar.getTime() ; 
		}else if(usersCate.getCateTempId() == Integer.valueOf(stuMonthSys.getVals()))
		{
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			currStDate = calendar.getTime() ; 
			calendar.add(Calendar.MONTH, 1);
			currEdDate = calendar.getTime() ; 
		}
		condMap.put("currStDate", this.dateFormatUtil.dateTimeStr(currStDate));
		condMap.put("currEdDate", this.dateFormatUtil.dateTimeStr(currEdDate));
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		ApiResponse<AUsersCate> apiRes = this.findCondListUsersCateService(pageInfoUtil, condMap) ;
		List<AUsersCate> usersCateResList = apiRes.getDataListJava() ; 
		if(usersCateResList.size() > 0)
		{
			AUsersCate usersCateRes = usersCateResList.get(0);
			if(usersCateRes.getId() != usersCate.getId())
			{
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put(ApiResponseEnum.NAME_ONE.getName(), usersCate);
				apiResponse.setDataOne(dataMap);
				
				/* 说明已经找到了 */
				apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
				apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
				return apiResponse ; 
			}
		}
		
		/*---------检查状态------*/
		if(usersCate.getSouStatus() == AUsersCateEnum.STATUS_ENABLE.getStatus())
		{
			/* 状态非法 */
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), usersCate);
			apiResponse.setDataOne(dataMap);
			
			/* 说明已经找到了 */
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}
		
		int res = this.usersCateDao.update(usersCate);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), usersCate.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), usersCate);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	@Override
	public ApiResponse<Object> deleteOneUsersCateService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.usersCateDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AUsersCate> findCondListUsersCateService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AUsersCate> apiResponse = new ApiResponse<AUsersCate>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AUsersCate> usersCateList = Collections.EMPTY_LIST;
		
		Map<String, List<AUsersCate>> dataList = new HashMap<String, List<AUsersCate>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			usersCateList = this.usersCateDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			usersCateList = this.usersCateDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), usersCateList);
		
		apiResponse.setDataListJava(usersCateList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AUsersCate> findOneUsersCateService(Map<String, Object> condMap)
	{
		ApiResponse<AUsersCate> apiResponse = new ApiResponse<AUsersCate>();
			
		Map<String, AUsersCate> data = new HashMap<String, AUsersCate>();
		AUsersCate usersCate = this.usersCateDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), usersCate);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(usersCate);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
}

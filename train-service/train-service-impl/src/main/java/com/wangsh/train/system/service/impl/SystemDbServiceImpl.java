package com.wangsh.train.system.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.pojo.JSONEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.outer.IClientOuterService;
import com.wangsh.train.system.dao.IASysFileDao;
import com.wangsh.train.system.dao.IASysProDao;
import com.wangsh.train.system.dao.IASysTechDao;
import com.wangsh.train.system.pojo.ASysFile;
import com.wangsh.train.system.pojo.ASysFileEnum;
import com.wangsh.train.system.pojo.ASysPro;
import com.wangsh.train.system.pojo.ASysProEnum;
import com.wangsh.train.system.pojo.ASysTech;
import com.wangsh.train.system.service.ISystemDbService;

/**
 * 系统的Service
 * 
 * @author TeaBig
 */
@Service("systemService")
public class SystemDbServiceImpl extends BaseServiceImpl implements ISystemDbService
{
	@Resource
	private IASysFileDao sysFileDao;
	@Resource
	private IASysProDao sysProDao;
	@Resource
	private IASysTechDao sysTechDao;
	@Resource
	private IClientOuterService clientOuterService; 
	
	/**
	 * 初始化操作
	 */
	@PostConstruct
	public ApiResponse<Object> reloadSystem()
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>() ; 
		/* 查询所有启用的共享变量,系统配置 */
		Map<String, Object> condMap = new HashMap<String, Object>() ; 
		condMap.put("status", ASysProEnum.STATUS_ENABLE.getStatus());
		ApiResponse<ASysPro> response = this.findCondListSysProService(null, condMap);
		List<ASysPro> sysProList = response.getDataListJava() ; 
		/* 循环放到Map中 */
		for (Iterator iterator = sysProList.iterator(); iterator.hasNext();)
		{
			ASysPro sysPro = (ASysPro) iterator.next();
			ConstatFinalUtil.SYS_PRO_MAP.put(sysPro.getEngName(), sysPro);
		}
		ConstatFinalUtil.SYS_LOGGER.info("==初始化完成==变量:{}",ConstatFinalUtil.SYS_PRO_MAP);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	@Override
	public ApiResponse<Object> saveOneSysFileService(ASysFile sysFile)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		sysFile.setCreateTime(new Date());
		sysFile.setUpdateTime(new Date());
		int res = this.sysFileDao.save(sysFile);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), sysFile.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), sysFile);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	public ApiResponse<Object> updateOneSysFileService(ASysFile sysFile)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		sysFile.setUpdateTime(new Date());
		int res = this.sysFileDao.update(sysFile);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), sysFile.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), sysFile);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	@Override
	public ApiResponse<Object> updateOneSysFileService(ASysFile sysFile,Map<String, Object> paramsMap) throws Exception
	{
		if(paramsMap.size() == 0 )
		{
			return this.updateOneSysFileService(sysFile);
		}
		
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
			
		try
		{
			String fileName = paramsMap.get("fileName") + "";
			InputStream is = (InputStream) paramsMap.get("inputStream") ;
			String fileSize = paramsMap.get("fileSize") + "";
			
			/* 上传文件的相对路径 */
			String uploadFile = ConstatFinalUtil.CONFIG_JSON.getString("website.uploadFile");
			/* 存储的是网址的绝对路径 */
			String souPath = sysFile.getFilePath() ;
			/* 真实路径 */
			String truePath = ConstatFinalUtil.CONFIG_JSON.getString("website.truePath");
			String midPath = "sysFile" ;
			if(sysFile.getFileRelaType() == ASysFileEnum.FILERELATYPE_USERSCATE.getStatus())
			{
				/* 读取usersCate的系统变量 */
				ASysPro usersCateSys = (ASysPro) ConstatFinalUtil.SYS_PRO_MAP.get("uploadPath_cate") ;
				midPath = usersCateSys.getVals() ; 
			}
			/* 网站的相对路径,
			 * 路径是写活的
			 *  */
			String tarRelaPath = uploadFile + "/" + midPath + "/" +  this.dateFormatUtil.dateStr(new Date()) + "/" + UUID.randomUUID().toString() + 
					fileName.substring(fileName.lastIndexOf("."), fileName.length());
			File tarTrueFile = new File(truePath + tarRelaPath);
			
			/* 创建父目录 */
			if(!tarTrueFile.getParentFile().exists())
			{
				tarTrueFile.getParentFile().mkdirs();
			}
			sysFile.setName(fileName);
			sysFile.setFileSize(fileSize);
			sysFile.setFilePath(tarRelaPath);
			
			FileOutputStream fos = new FileOutputStream(tarTrueFile);
			/* 复制文件 */
			boolean flag = this.fileUtil.copyFile(is, fos);
			if(flag)
			{
				if(souPath != null && !"".equalsIgnoreCase(souPath))
				{
					/* 相对路径 */
					File souFile = new File(truePath + souPath);
					if(!souFile.delete())
					{
						ConstatFinalUtil.SYS_LOGGER.error("删除原始文件失败:原始文件路径:{}",souFile.getAbsolutePath());
					}
				}
			}
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			/* 更新记录 */
			this.updateOneSysFileService(sysFile);
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("上传头像失败了,id:{}",sysFile.getId() , e);
			throw e ; 
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneSysFileService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.sysFileDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<ASysFile> findCondListSysFileService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<ASysFile> apiResponse = new ApiResponse<ASysFile>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<ASysFile> sysFileList = Collections.EMPTY_LIST;
		
		Map<String, List<ASysFile>> dataList = new HashMap<String, List<ASysFile>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			sysFileList = this.sysFileDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			sysFileList = this.sysFileDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), sysFileList);
		
		apiResponse.setDataListJava(sysFileList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<ASysFile> findOneSysFileService(Map<String, Object> condMap)
	{
		ApiResponse<ASysFile> apiResponse = new ApiResponse<ASysFile>();
			
		Map<String, ASysFile> data = new HashMap<String, ASysFile>();
		ASysFile sysFile = this.sysFileDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), sysFile);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(sysFile);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	/* 系统配置操作开始 */
	@Override
	public ApiResponse<Object> saveOneSysProService(ASysPro sysPro)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		sysPro.setCreateTime(new Date());
		sysPro.setUpdateTime(new Date());
		int res = this.sysProDao.save(sysPro);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), sysPro.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), sysPro);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneSysProService(ASysPro sysPro)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		int res = this.sysProDao.update(sysPro);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), sysPro.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), sysPro);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneSysProService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.sysProDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<ASysPro> findCondListSysProService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<ASysPro> apiResponse = new ApiResponse<ASysPro>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<ASysPro> SysProList = Collections.EMPTY_LIST;
		
		Map<String, List<ASysPro>> dataList = new HashMap<String, List<ASysPro>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			SysProList = this.sysProDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			SysProList = this.sysProDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), SysProList);
		
		apiResponse.setDataListJava(SysProList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<ASysPro> findOneSysProService(Map<String, Object> condMap)
	{
		ApiResponse<ASysPro> apiResponse = new ApiResponse<ASysPro>();
			
		Map<String, ASysPro> data = new HashMap<String, ASysPro>();
		ASysPro SysPro = this.sysProDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), SysPro);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(SysPro);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneSysTechService(ASysTech sysTech)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		sysTech.setCreateTime(new Date());
		sysTech.setUpdateTime(new Date());
		int res = this.sysTechDao.save(sysTech);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), sysTech.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), sysTech);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	public ApiResponse<Object> updateOneSysTechService(ASysTech sysTech)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		sysTech.setUpdateTime(new Date());
		int res = this.sysTechDao.update(sysTech);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), sysTech.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), sysTech);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	@Override
	public ApiResponse<Object> deleteOneSysTechService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.sysTechDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<ASysTech> findCondListSysTechService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<ASysTech> apiResponse = new ApiResponse<ASysTech>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<ASysTech> sysTechList = Collections.EMPTY_LIST;
		
		Map<String, List<ASysTech>> dataList = new HashMap<String, List<ASysTech>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			sysTechList = this.sysTechDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			sysTechList = this.sysTechDao.findList(condMap) ; 
		}
		
		/* 查询扩展信息 */
		if("true".equalsIgnoreCase(condMap.get("extend") + "") && sysTechList.size() > 0 )
		{
			/* 批量查询 */
			JSONArray cateIdsArr = new JSONArray();
			for (Iterator iterator = sysTechList.iterator(); iterator.hasNext();)
			{
				ASysTech cla = (ASysTech) iterator.next();
				String cateId = cla.getCateId() + "" ; 
				if(!cateIdsArr.contains(cateId))
				{
					cateIdsArr.add(cateId);
				}
			}
			Map<String, JSONObject> regionMap = new HashMap<String, JSONObject>();
			/* 批量请求服务器 */
			JSONObject dataJSON = new JSONObject();
			
			Map<String, JSONObject> cateMap = new HashMap<String, JSONObject>();
			/* 批量请求服务器 */
			JSONObject dataCateJSON = new JSONObject();
			dataCateJSON.put("ids",cateIdsArr);
			JSONObject responseCateJSON = this.clientOuterService.cateBatchOneService(dataCateJSON);
			if(responseCateJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseCateJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseCateJSON.get("data");
				JSONArray cateResArr = (JSONArray) dataResJSON.get("list");
				for (Iterator iterator = cateResArr.iterator(); iterator.hasNext();)
				{
					JSONObject cateTempCate = (JSONObject) iterator.next();
					cateMap.put(cateTempCate.getString("id"), cateTempCate);
				}
			}
			
			for (Iterator iterator = sysTechList.iterator(); iterator.hasNext();)
			{
				ASysTech cla = (ASysTech) iterator.next();
				String cateId = cla.getCateId() + "" ; 
				JSONObject cateJSON = cateMap.get(cateId);
				cla.setCateJSON(cateJSON);
			}
		}
		
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), sysTechList);
		
		apiResponse.setDataListJava(sysTechList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<ASysTech> findOneSysTechService(Map<String, Object> condMap)
	{
		ApiResponse<ASysTech> apiResponse = new ApiResponse<ASysTech>();
			
		Map<String, ASysTech> data = new HashMap<String, ASysTech>();
		ASysTech sysTech = this.sysTechDao.findOne(condMap);
		
		if("true".equalsIgnoreCase(condMap.get("extend") + ""))
		{
			/* 批量请求服务器 */
			JSONObject dataJSON = new JSONObject();
			JSONArray cateIdsArr = new JSONArray();
			cateIdsArr.add(sysTech.getCateId() + "");
			dataJSON.put("ids",cateIdsArr);
			JSONObject responseJSON = this.clientOuterService.cateBatchOneService(dataJSON);
			if(responseJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseJSON.get("data");
				JSONArray cateResArr = (JSONArray) dataResJSON.get("list");
				if(cateResArr.size() > 0 )
				{
					JSONObject cateJSON = (JSONObject) cateResArr.get(0);
					sysTech.setCateJSON(cateJSON);
				}
			}
		}
		
		data.put(ApiResponseEnum.NAME_ONE.getName(), sysTech);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(sysTech);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
}

package com.wangsh.train.demo.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.pojo.JSONEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.demo.dao.IADemoDynastyDao;
import com.wangsh.train.demo.dao.IADemoKingDao;
import com.wangsh.train.demo.pojo.ADemoDynasty;
import com.wangsh.train.demo.pojo.ADemoKing;
import com.wangsh.train.demo.service.IDemoDbService;

/**
 * 数据模块的实现类
 * 
 * @author Wangsh
 * 
 */
@Service("demoService")
public class DemoDbServiceImplay implements IDemoDbService
{
	@Resource
	private IADemoDynastyDao demoDynastyDao;
	@Resource
	private IADemoKingDao demoKingDao;
	
	@Override
	public ApiResponse<Object> saveOneDynastyService(ADemoDynasty dynasty)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		dynasty.setAge(dynasty.getEdYear() - dynasty.getStYear());
		
		dynasty.setCreateTime(new Date());
		dynasty.setUpdateTime(new Date());
		int res = this.demoDynastyDao.save(dynasty);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), dynasty.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), dynasty);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneDynastyService(ADemoDynasty dynasty)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		dynasty.setAge(dynasty.getEdYear() - dynasty.getStYear());
		
		dynasty.setUpdateTime(new Date());
		int res = this.demoDynastyDao.update(dynasty);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), dynasty.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), dynasty);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneDynastyService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.demoDynastyDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<ADemoDynasty> findCondListDynastyService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<ADemoDynasty> apiResponse = new ApiResponse<ADemoDynasty>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<ADemoDynasty> demoDynastyList = Collections.EMPTY_LIST;
		
		Map<String, List<ADemoDynasty>> dataList = new HashMap<String, List<ADemoDynasty>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			demoDynastyList = this.demoDynastyDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			demoDynastyList = this.demoDynastyDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), demoDynastyList);
		
		apiResponse.setDataListJava(demoDynastyList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<ADemoDynasty> findOneDynastyService(Map<String, Object> condMap)
	{
		ApiResponse<ADemoDynasty> apiResponse = new ApiResponse<ADemoDynasty>();
			
		Map<String, ADemoDynasty> data = new HashMap<String, ADemoDynasty>();
		ADemoDynasty dynasty = this.demoDynastyDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), dynasty);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(dynasty);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneKingService(ADemoKing king)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		king.setCreateTime(new Date());
		king.setUpdateTime(new Date());
		int res = this.demoKingDao.save(king);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), king.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), king);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneKingService(ADemoKing king)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		king.setUpdateTime(new Date());
		int res = this.demoKingDao.update(king);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), king.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), king);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneKingService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.demoKingDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<ADemoKing> findCondListKingService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<ADemoKing> apiResponse = new ApiResponse<ADemoKing>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<ADemoKing> demoKingList = Collections.EMPTY_LIST;
		
		Map<String, List<ADemoKing>> dataList = new HashMap<String, List<ADemoKing>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			demoKingList = this.demoKingDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			demoKingList = this.demoKingDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), demoKingList);
		
		apiResponse.setDataListJava(demoKingList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<ADemoKing> findOneKingService(Map<String, Object> condMap)
	{
		ApiResponse<ADemoKing> apiResponse = new ApiResponse<ADemoKing>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		
		Map<String, ADemoKing> data = new HashMap<String, ADemoKing>();
		ADemoKing king = this.demoKingDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), king);
		
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(king);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
}

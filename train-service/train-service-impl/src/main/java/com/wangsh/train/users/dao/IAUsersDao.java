package com.wangsh.train.users.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.users.pojo.AUsers;

/**
 * 用户的Dao
 * @author TeaBig
 */
@Mapper
public interface IAUsersDao extends IBaseDao<AUsers>
{
}

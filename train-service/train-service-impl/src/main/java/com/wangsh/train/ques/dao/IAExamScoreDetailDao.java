package com.wangsh.train.ques.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.ques.pojo.AExamScoreDetail;

/**
 * AExamScoreDetail的Dao
 * @author Wangsh
 */
@Mapper
public interface IAExamScoreDetailDao extends IBaseDao<AExamScoreDetail>
{
}

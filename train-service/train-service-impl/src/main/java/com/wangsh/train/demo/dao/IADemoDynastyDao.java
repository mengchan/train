package com.wangsh.train.demo.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.demo.pojo.ADemoDynasty;

/**
 * 示例代码中朝代的Dao
 * @author Wangsh
 */
@Mapper
public interface IADemoDynastyDao extends IBaseDao<ADemoDynasty>
{
}

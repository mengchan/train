package com.wangsh.train.demo.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.demo.pojo.ADemoKing;

/**
 * 示例代码中皇上的Dao
 * @author Wangsh
 */
@Mapper
public interface IADemoKingDao extends IBaseDao<ADemoKing>
{
}

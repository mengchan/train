package com.wangsh.train.job.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.job.pojo.AJobInterview;

/**
 * 示例代码中面试的Dao
 * @author Wangsh
 */
@Mapper
public interface IAJobInterviewDao extends IBaseDao<AJobInterview>
{
}

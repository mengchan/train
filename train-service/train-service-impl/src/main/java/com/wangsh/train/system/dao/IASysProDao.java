package com.wangsh.train.system.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.system.pojo.ASysPro;

/**
 * 系统配置的Dao
 */
@Mapper
public interface IASysProDao extends IBaseDao<ASysPro>
{

}

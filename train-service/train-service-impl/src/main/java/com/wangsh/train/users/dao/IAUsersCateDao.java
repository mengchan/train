package com.wangsh.train.users.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.users.pojo.AUsersCate;

/**
 * 用户分类(模板)的Dao
 * @author TeaBig
 */
@Mapper
public interface IAUsersCateDao extends IBaseDao<AUsersCate>
{
}

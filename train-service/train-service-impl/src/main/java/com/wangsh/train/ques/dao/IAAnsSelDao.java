package com.wangsh.train.ques.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.ques.pojo.AAnsSel;

/**
 * 示例代码中选项的Dao
 * @author Wangsh
 */
@Mapper
public interface IAAnsSelDao extends IBaseDao<AAnsSel>
{
}

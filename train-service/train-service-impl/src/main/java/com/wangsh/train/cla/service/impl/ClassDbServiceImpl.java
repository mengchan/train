package com.wangsh.train.cla.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wangsh.train.cla.dao.IAClassDao;
import com.wangsh.train.cla.dao.IAClassDataDao;
import com.wangsh.train.cla.dao.IACodeDetailDao;
import com.wangsh.train.cla.dao.IACodeStatDao;
import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.cla.pojo.AClassData;
import com.wangsh.train.cla.pojo.ACodeDetail;
import com.wangsh.train.cla.pojo.ACodeDetailEnum;
import com.wangsh.train.cla.pojo.ACodeStat;
import com.wangsh.train.cla.pojo.ACodeStatEnum;
import com.wangsh.train.cla.service.IClassDbService;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.pojo.JSONEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.outer.IClientOuterService;

/**
 * 班级的Service
 * @author TeaBig
 */
@Service("classService")
public class ClassDbServiceImpl extends BaseServiceImpl implements IClassDbService
{
	@Resource
	private IAClassDao clasDao;
	@Resource
	private IACodeStatDao codeStatDao;
	@Resource
	private IACodeDetailDao codeDetailDao;
	@Resource
	private IAClassDataDao classDataDao;
	@Resource
	private IClientOuterService clientOuterService; 
	
	@Override
	public ApiResponse<Object> saveOneClassService(AClass clas)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.clasDao.save(clas);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), clas.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), clas);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneClassService(AClass clas)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.clasDao.update(clas);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), clas.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), clas);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneClassService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.clasDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AClass> findCondListClassService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AClass> apiResponse = new ApiResponse<AClass>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AClass> clasList = Collections.EMPTY_LIST;
		
		Map<String, List<AClass>> dataList = new HashMap<String, List<AClass>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			clasList = this.clasDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			clasList = this.clasDao.findList(condMap) ; 
		}
		
		/* 查询扩展信息 */
		if("true".equalsIgnoreCase(condMap.get("extend") + "") && clasList.size() > 0 )
		{
			/* 批量查询 */
			JSONArray regionIdsArr = new JSONArray();
			JSONArray cateIdsArr = new JSONArray();
			for (Iterator iterator = clasList.iterator(); iterator.hasNext();)
			{
				AClass cla = (AClass) iterator.next();
				String regionId = cla.getRegionId() + ""; 
				if(!regionIdsArr.contains(regionId))
				{
					regionIdsArr.add(regionId);
				}
				String cateId = cla.getCateId() + "" ; 
				if(!cateIdsArr.contains(cateId))
				{
					cateIdsArr.add(cateId);
				}
			}
			Map<String, JSONObject> regionMap = new HashMap<String, JSONObject>();
			/* 批量请求服务器 */
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ids",regionIdsArr);
			JSONObject responseJSON = this.clientOuterService.regionBatchOneService(dataJSON);
			if(responseJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseJSON.get("data");
				JSONArray regionResArr = (JSONArray) dataResJSON.get("list");
				for (Iterator iterator = regionResArr.iterator(); iterator.hasNext();)
				{
					JSONObject regionTempRegion = (JSONObject) iterator.next();
					regionMap.put(regionTempRegion.getString("id"), regionTempRegion);
				}
			}
			
			Map<String, JSONObject> cateMap = new HashMap<String, JSONObject>();
			/* 批量请求服务器 */
			JSONObject dataCateJSON = new JSONObject();
			dataCateJSON.put("ids",cateIdsArr);
			JSONObject responseCateJSON = this.clientOuterService.cateBatchOneService(dataCateJSON);
			if(responseCateJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseCateJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseCateJSON.get("data");
				JSONArray cateResArr = (JSONArray) dataResJSON.get("list");
				for (Iterator iterator = cateResArr.iterator(); iterator.hasNext();)
				{
					JSONObject cateTempCate = (JSONObject) iterator.next();
					cateMap.put(cateTempCate.getString("id"), cateTempCate);
				}
			}
			
			for (Iterator iterator = clasList.iterator(); iterator.hasNext();)
			{
				AClass cla = (AClass) iterator.next();
				String regionId = cla.getRegionId() + ""; 
				JSONObject regionJSON = regionMap.get(regionId);
				cla.setRegionJSON(regionJSON);
				
				String cateId = cla.getCateId() + "" ; 
				JSONObject cateJSON = cateMap.get(cateId);
				cla.setCateJSON(cateJSON);
			}
		}
		
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), clasList);
		
		apiResponse.setDataListJava(clasList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AClass> findOneClassService(Map<String, Object> condMap)
	{
		ApiResponse<AClass> apiResponse = new ApiResponse<AClass>();
			
		Map<String, AClass> data = new HashMap<String, AClass>();
		AClass clas = this.clasDao.findOne(condMap);
		
		if("true".equalsIgnoreCase(condMap.get("extend") + ""))
		{
			/* 批量请求服务器 */
			JSONObject dataJSON = new JSONObject();
			JSONArray regionIdsArr = new JSONArray();
			regionIdsArr.add(clas.getRegionId() + "");
			dataJSON.put("ids",regionIdsArr);
			JSONObject responseJSON = this.clientOuterService.regionBatchOneService(dataJSON);
			if(responseJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseJSON.get("data");
				JSONArray regionResArr = (JSONArray) dataResJSON.get("list");
				if(regionResArr.size() > 0 )
				{
					JSONObject regionJSON = (JSONObject) regionResArr.get(0);
					clas.setRegionJSON(regionJSON);
				}
			}
			
			JSONArray cateIdsArr = new JSONArray();
			cateIdsArr.add(clas.getCateId() + "");
			dataJSON.put("ids",cateIdsArr);
			JSONObject responseCateJSON = this.clientOuterService.cateBatchOneService(dataJSON);
			if(responseCateJSON != null && ApiResponseEnum.STATUS_SUCCESS.getStatus() == responseCateJSON.getByte("code"))
			{
				JSONObject dataResJSON = (JSONObject) responseCateJSON.get("data");
				JSONArray cateResArr = (JSONArray) dataResJSON.get("list");
				if(cateResArr.size() > 0 )
				{
					JSONObject cateJSON = (JSONObject) cateResArr.get(0);
					clas.setCateJSON(cateJSON);
				}
			}
		}

		data.put(ApiResponseEnum.NAME_ONE.getName(), clas);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(clas);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneCodeStatService(ACodeStat codeStat)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		codeStat.setCreateTime(new Date());
		codeStat.setUpdateTime(new Date());
		
		int res = this.codeStatDao.save(codeStat);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), codeStat.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), codeStat);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneCodeStatService(ACodeStat codeStat)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		codeStat.setUpdateTime(new Date());
		int res = this.codeStatDao.update(codeStat);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), codeStat.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), codeStat);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	public ApiResponse<Object> updateOneCodeStatService(ACodeStat codeStat,Map<String, Object> paramsMap) throws Exception
	{
		if(paramsMap.size() == 0 )
		{
			return this.updateOneCodeStatService(codeStat);
		}
		/* 先更新用户的信息 */
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		if(codeStat.getCodeType()== ACodeStatEnum.CODETYPE_ED.getStatus())
		{
			/* 直接返回 */
			apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ;
		}
		try
		{
			/* 统计中 */
			codeStat.setCodeType(ACodeStatEnum.CODETYPE_ING.getStatus());
			String fileName = paramsMap.get("fileName") + "";
			InputStream is = (InputStream) paramsMap.get("inputStream") ;
			String sizeZip = paramsMap.get("fileSize") + "";
			
			/* 上传文件的相对路径 */
			String uploadFile = ConstatFinalUtil.CONFIG_JSON.getString("website.uploadFile");
			/* 存储的是网址的绝对路径 */
			String souPath = codeStat.getFilePath() ;
			/* 真实路径 */
			String truePath = ConstatFinalUtil.CONFIG_JSON.getString("website.truePath");
			/* 网站的相对路径 */
			String tarRelaPath = uploadFile + "/codeStat/" + this.dateFormatUtil.dateStr(new Date()) + "/" + UUID.randomUUID().toString() + 
					fileName.substring(fileName.lastIndexOf("."), fileName.length());
			File tarTrueFile = new File(truePath + tarRelaPath);
			
			/* 创建父目录 */
			if(!tarTrueFile.getParentFile().exists())
			{
				tarTrueFile.getParentFile().mkdirs();
			}
			codeStat.setFilePath(tarRelaPath);
			FileOutputStream fos = new FileOutputStream(tarTrueFile);
			/* 复制文件 */
			boolean flag = this.fileUtil.copyFile(is, fos);
			if(flag)
			{
				if(souPath != null && !"".equalsIgnoreCase(souPath))
				{
					/* 相对路径 */
					File souFile = new File(truePath + souPath);
					if(!souFile.delete())
					{
						ConstatFinalUtil.SYS_LOGGER.error("删除原始文件失败:原始文件路径:{}",souFile.getAbsolutePath());
					}
				}
			}
			
			/*-----------处理压缩文件中的文件内容--------*/
			if(tarTrueFile.getName().endsWith("zip"))
			{
				int totalCountTotal = 0 ; 
				int codeCountTotal = 0 ; 
				int singleCommentCountTotal = 0 ; 
				int multiCommentCountTotal = 0 ; 
				int blankCountTotal = 0 ;  
				long fileSizeTotal = 0 ; 
				File tarZipFile = new File(tarTrueFile.getAbsolutePath() + "-tar");
				List<Map<String, Object>> javaFileList = this.fileUtil.codeStatJavaZipFile(tarTrueFile, tarZipFile);
				for (Iterator iterator = javaFileList.iterator(); iterator.hasNext();)
				{
					Map<String, Object> mapTemp = (Map<String, Object>) iterator.next();
					/* 获取一些变量 */
					String name = mapTemp.get("name") + "" ; 
					if(mapTemp.get("totalCount") == null)
					{
						/* 文件夹不统计 */
						continue ; 
					}
					int totalCount = Integer.valueOf(mapTemp.get("totalCount") + "") ; 
					int codeCount = Integer.valueOf(mapTemp.get("codeCount") + "") ; 
					int singleCommentCount = Integer.valueOf(mapTemp.get("singleCommentCount") + "") ; 
					int multiCommentCount = Integer.valueOf(mapTemp.get("multiCommentCount") + "") ; 
					int blankCount = Integer.valueOf(mapTemp.get("blankCount") + "") ; 
					boolean equals = Boolean.valueOf(mapTemp.get("equals") + "") ; 
					String fileRelaName = mapTemp.get("fileRelaName") + "" ; 
					String extendName = mapTemp.get("extendName") + "" ; 
					String fileSize = mapTemp.get("fileSize") + "" ; 
					
					totalCountTotal = totalCountTotal + totalCount ; 
					codeCountTotal = codeCountTotal + codeCount ; 
					singleCommentCountTotal = singleCommentCountTotal + singleCommentCount ; 
					multiCommentCountTotal = multiCommentCountTotal + multiCommentCount ; 
					blankCountTotal = blankCountTotal + blankCount ;  
					fileSizeTotal = fileSizeTotal + Long.valueOf(fileSize) ; 
					
					ACodeDetail codeDetail = new ACodeDetail() ;
					codeDetail.setName(name);
					codeDetail.setOriName(name);
					codeDetail.setFilePath(fileRelaName);
					/* 文件的Md5值木有获取 */
					codeDetail.setCodeStatId(codeStat.getId());
					codeDetail.setTotalCount(totalCount);
					codeDetail.setCodeCount(codeCount);
					codeDetail.setSinComCount(singleCommentCount);
					codeDetail.setMulComCount(multiCommentCount);
					codeDetail.setBlankCount(blankCount);
					codeDetail.setFileSize(Long.valueOf(fileSize));
					codeDetail.setStatus(ACodeDetailEnum.STATUS_ENABLE.getStatus());
					if(equals)
					{
						codeDetail.setEqualsFlag(ACodeDetailEnum.EQUALSFLAG_TRUE.getStatus());
					}else
					{
						codeDetail.setEqualsFlag(ACodeDetailEnum.EQUALSFLAG_FALSE.getStatus());
					}
					/* 文件类型 */
					if("java".equalsIgnoreCase(extendName.toLowerCase()))
					{
						/* java文件 */
						codeDetail.setFileType(ACodeDetailEnum.FILETYPE_JAVA.getStatus());
					}else if("py".equalsIgnoreCase(extendName.toLowerCase()))
					{
						/* java文件 */
						codeDetail.setFileType(ACodeDetailEnum.FILETYPE_PYTHON.getStatus());
					}else if("scala".equalsIgnoreCase(extendName.toLowerCase()))
					{
						/* java文件 */
						codeDetail.setFileType(ACodeDetailEnum.FILETYPE_SCALA.getStatus());
					}
					
					codeDetail.setPubTime(new Date());
					this.saveOneCodeDetailService(codeDetail);
				}
				
				codeStat.setOriName(fileName);
				codeStat.setTotalCount(totalCountTotal);
				codeStat.setCodeCount(codeCountTotal);
				codeStat.setSinComCount(singleCommentCountTotal);
				codeStat.setMulComCount(multiCommentCountTotal);
				codeStat.setBlankCount(blankCountTotal);
				codeStat.setFileSize(Long.valueOf(sizeZip));
				codeStat.setTotalFileSize(Long.valueOf(fileSizeTotal));
				/* 统计中 */
				codeStat.setCodeType(ACodeStatEnum.CODETYPE_ED.getStatus());
				apiResponse = this.updateOneCodeStatService(codeStat);
			}else
			{
				apiResponse.setCode(ApiResponseEnum.INFO_ERROR_FORMAT.getStatus());
			}
			
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("上传头像失败了,id:{}",codeStat.getId() , e);
			throw e ; 
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneCodeStatService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.codeStatDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<ACodeStat> findCondListCodeStatService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<ACodeStat> apiResponse = new ApiResponse<ACodeStat>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<ACodeStat> codeStatList = Collections.EMPTY_LIST;
		
		Map<String, List<ACodeStat>> dataList = new HashMap<String, List<ACodeStat>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			codeStatList = this.codeStatDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			codeStatList = this.codeStatDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), codeStatList);
		
		apiResponse.setDataListJava(codeStatList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<ACodeStat> findOneCodeStatService(Map<String, Object> condMap)
	{
		ApiResponse<ACodeStat> apiResponse = new ApiResponse<ACodeStat>();
			
		Map<String, ACodeStat> data = new HashMap<String, ACodeStat>();
		ACodeStat codeStat = this.codeStatDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), codeStat);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(codeStat);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneCodeDetailService(ACodeDetail codeDetail)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		codeDetail.setCreateTime(new Date());
		codeDetail.setUpdateTime(new Date());
		int res = this.codeDetailDao.save(codeDetail);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), codeDetail.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), codeDetail);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneCodeDetailService(ACodeDetail codeDetail)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		codeDetail.setUpdateTime(new Date());
		int res = this.codeDetailDao.update(codeDetail);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), codeDetail.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), codeDetail);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneCodeDetailService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.codeDetailDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<ACodeDetail> findCondListCodeDetailService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<ACodeDetail> apiResponse = new ApiResponse<ACodeDetail>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<ACodeDetail> codeDetailList = Collections.EMPTY_LIST;
		
		Map<String, List<ACodeDetail>> dataList = new HashMap<String, List<ACodeDetail>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			codeDetailList = this.codeDetailDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			codeDetailList = this.codeDetailDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), codeDetailList);
		
		apiResponse.setDataListJava(codeDetailList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<ACodeDetail> findOneCodeDetailService(Map<String, Object> condMap)
	{
		ApiResponse<ACodeDetail> apiResponse = new ApiResponse<ACodeDetail>();
			
		Map<String, ACodeDetail> data = new HashMap<String, ACodeDetail>();
		ACodeDetail codeDetail = this.codeDetailDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), codeDetail);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(codeDetail);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}
	
	@Override
	public ApiResponse<Object> saveOneDataService(AClassData data)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		data.setCreateTime(new Date());
		data.setUpdateTime(new Date());
		int res = this.classDataDao.save(data);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), data.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), data);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneDataService(AClassData data)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		data.setUpdateTime(new Date());
		int res = this.classDataDao.update(data);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), data.getId());
			dataMap.put(ApiResponseEnum.NAME_ONE.getName(), data);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneDataService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.classDataDao.delete(condMap);
		if (res > 0)
		{
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AClassData> findCondListDataService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		ApiResponse<AClassData> apiResponse = new ApiResponse<AClassData>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AClassData> demoDataList = Collections.EMPTY_LIST;
		
		Map<String, List<AClassData>> dataList = new HashMap<String, List<AClassData>>();
		/* 对关键字进行模糊匹配 */
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%") ; 
		}
		
		/* 设置分页相关信息 */
		if(pageInfoUtil != null)
		{
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(),pageInfoUtil.getPageSize());
			demoDataList = this.classDataDao.findList(condMap) ; 
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		}else
		{
			demoDataList = this.classDataDao.findList(condMap) ; 
		}
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), demoDataList);
		
		apiResponse.setDataListJava(demoDataList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}

	@Override
	public ApiResponse<AClassData> findOneDataService(Map<String, Object> condMap)
	{
		ApiResponse<AClassData> apiResponse = new ApiResponse<AClassData>();
			
		Map<String, AClassData> dataMap = new HashMap<String, AClassData>();
		AClassData data = this.classDataDao.findOne(condMap);
		dataMap.put(ApiResponseEnum.NAME_ONE.getName(), data);
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(dataMap);
		apiResponse.setDataOneJava(data);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse ; 
	}

	@Override
	public ApiResponse<Map<String, Object>> statDataService(Map<String, Object> condMap)
	{
		ApiResponse<Map<String, Object>> apiResponse = new ApiResponse<Map<String, Object>>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		
		List<Map<String, Object>> codeDetailList = Collections.EMPTY_LIST;
		
		Map<String, List<Map<String, Object>>> dataList = new HashMap<String, List<Map<String, Object>>>();
		
		codeDetailList = this.classDataDao.statMap(condMap) ; 
		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), codeDetailList);
		
		apiResponse.setDataListJava(codeDetailList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""),Collections.EMPTY_MAP);
		return apiResponse; 
	}
}

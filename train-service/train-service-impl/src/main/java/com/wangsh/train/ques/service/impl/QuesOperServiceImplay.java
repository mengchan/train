package com.wangsh.train.ques.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.ques.pojo.AAnsSel;
import com.wangsh.train.ques.pojo.AAnsSelEnum;
import com.wangsh.train.ques.pojo.AAsk;
import com.wangsh.train.ques.pojo.AAskEnum;
import com.wangsh.train.ques.service.IQuesDbService;
import com.wangsh.train.ques.service.IQuesOperService;

/**
 * Service实现类(问题代码)
 * 
 * @author Wangsh
 */
@Service("quesOperService")
public class QuesOperServiceImplay extends BaseServiceImpl implements IQuesOperService
{
	@Resource
	private IQuesDbService quesDbService;
	
	@Override
	public ApiResponse<Object> insertBatchAskService(Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 获取输入流 */
		InputStream is = (InputStream) condMap.get("is");
		String extendName = condMap.get("extendName") + "";
		try
		{
			/* 读取sheet的内容 */
			Map<String, List> excelDataMap = this.poiUtil.readExcel(is, extendName);
			//ConstatFinalUtil.SYS_LOGGER.info("==resultMap==>" , excelDataMap.size());
			
			/* 单选题 */
			List selList = excelDataMap.get("单选");
			proccedAskSel(selList,condMap);
			
			/* 多选题 */
			selList = excelDataMap.get("多选");
			proccedAskSel(selList,condMap);
			
			/* 判断题 */
			selList = excelDataMap.get("判断");
			proccedAskJUDGE(selList,condMap);
			
			/* 简答题 */
			selList = excelDataMap.get("简答");
			proccedAskQues(selList,condMap);
			
			/* 论述题 */
			selList = excelDataMap.get("论述");
			proccedAskQues(selList,condMap);
			
			/* 综合题 */
			selList = excelDataMap.get("综合");
			if(selList != null)
			{
				proccedAskQues(selList,condMap);
			}
			
			/* 编程 */
			selList = excelDataMap.get("编程");
			if(selList != null)
			{
				proccedAskProgram(selList,condMap);
			}
			
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} catch (IOException e)
		{
			apiResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			ConstatFinalUtil.SYS_LOGGER.error("导入题库文件失败了;",e);
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	/**
	 * 编程题
	 * @param selList
	 * @param condMap
	 */
	private ApiResponse<Object> proccedAskProgram(List selList, Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponseResult = new ApiResponse<Object>();
		int totalCount = 0 ; 
		int succedCount = 0 ; 
		int failedCount = 0 ; 
		/* 考试概要id */
		String usersId = condMap.get("usersId") + "";
		String usersClaId = condMap.get("usersClaId") + "";
		
		List<AAsk> resultList = new ArrayList<AAsk>() ;
		for (Iterator iterator = selList.iterator(); iterator.hasNext();)
		{
			Map<String, Object> rowMap = (Map<String, Object>) iterator.next();
			AAsk ask = new AAsk();
			
			ask.setUsersId(Integer.valueOf(usersId));
			ask.setUsersClaId(Integer.valueOf(usersClaId));
			
			String souName = rowMap.get("题目") + ""; 
			souName = souName.trim() ; 
			String hardStar = rowMap.get("难易度") + "";
			hardStar = hardStar.trim() ; 
			
			ConstatFinalUtil.SYS_LOGGER.info("==解析后:{}==",rowMap);
			/* 处理答案 */
			String answer = rowMap.get("答案") + "";
			answer = answer.trim() ;
			ask.setAnswer(answer);
			
			if("难".equalsIgnoreCase(hardStar))
			{
				hardStar = "5";
			}else if("中".equalsIgnoreCase(hardStar))
			{
				hardStar = "3";
			}else if("易".equalsIgnoreCase(hardStar))
			{
				hardStar = "1";
			}else
			{
				hardStar = "0";
			}
			
			/* 开始赋值 */
			ask.setName(souName);
			ask.setHardStar(Byte.valueOf(hardStar));
			
			ask.setAskType(AAskEnum.ASKTYPE_PROGRAM.getStatus());
			ask.setSouType(AAskEnum.SOUTYPE_INSERT.getStatus());
			
			ask.setStatus(AAskEnum.STATUS_PUBLISH.getStatus());
			ask.setPubTime(new Date());
			
			/* 保存数据 */
			apiResponseResult = this.quesDbService.saveOneAskService(ask);
			totalCount ++ ; 
			if(apiResponseResult.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
			{
				succedCount ++ ; 
			}else
			{
				failedCount ++ ; 
			}
		}
		apiResponseResult.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		apiResponseResult.setInfo("总条数:" + totalCount + ";成功:" + succedCount + ";失败:" + failedCount, Collections.EMPTY_MAP);
		ConstatFinalUtil.SYS_LOGGER.info("插入问答题结果:{}",apiResponseResult.toJSON().toJSONString());
		return apiResponseResult ; 
	}
	
	/**
	 * 处理判断题
	 * @param selList
	 * @param condMap
	 */
	private ApiResponse<Object> proccedAskJUDGE(List selList, Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponseResult = new ApiResponse<Object>();
		int totalCount = 0 ; 
		int succedCount = 0 ; 
		int failedCount = 0 ; 
		/* 考试概要id */
		String usersId = condMap.get("usersId") + "";
		String usersClaId = condMap.get("usersClaId") + "";
		
		List<AAsk> resultList = new ArrayList<AAsk>() ;
		for (Iterator iterator = selList.iterator(); iterator.hasNext();)
		{
			Map<String, Object> rowMap = (Map<String, Object>) iterator.next();
			AAsk ask = new AAsk();
			
			ask.setUsersId(Integer.valueOf(usersId));
			ask.setUsersClaId(Integer.valueOf(usersClaId));
			
			String souName = rowMap.get("题目") + ""; 
			souName = souName.trim() ; 
			String hardStar = rowMap.get("难易度") + "";
			hardStar = hardStar.trim() ; 
			
			/* 处理答案 */
			String answer = rowMap.get("答案") + "";
			answer = answer.trim() ;
			if("对".equalsIgnoreCase(answer))
			{
				answer = "0";
			}else if("错".equalsIgnoreCase(answer))
			{
				answer = "1";
			}
			ask.setAnswer(answer);
			
			if("难".equalsIgnoreCase(hardStar))
			{
				hardStar = "5";
			}else if("中".equalsIgnoreCase(hardStar))
			{
				hardStar = "3";
			}else if("易".equalsIgnoreCase(hardStar))
			{
				hardStar = "1";
			}else
			{
				hardStar = "0";
			}
			
			/* 开始赋值 */
			ask.setName(souName);
			ask.setHardStar(Byte.valueOf(hardStar));
			
			ask.setAskType(AAskEnum.ASKTYPE_JUDGE.getStatus());
			ask.setSouType(AAskEnum.SOUTYPE_INSERT.getStatus());
			
			ask.setStatus(AAskEnum.STATUS_PUBLISH.getStatus());
			ask.setPubTime(new Date());
			
			/* 保存数据 */
			apiResponseResult = this.quesDbService.saveOneAskService(ask);
			totalCount ++ ; 
			if(apiResponseResult.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
			{
				succedCount ++ ; 
			}else
			{
				failedCount ++ ; 
			}
		}
		apiResponseResult.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		apiResponseResult.setInfo("总条数:" + totalCount + ";成功:" + succedCount + ";失败:" + failedCount, Collections.EMPTY_MAP);
		ConstatFinalUtil.SYS_LOGGER.info("插入问答题结果:{}",apiResponseResult.toJSON().toJSONString());
		return apiResponseResult ; 
	}

	/**
	 * 处理简单题
	 * @param selList
	 * @param condMap
	 */
	private ApiResponse<Object> proccedAskQues(List selList, Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponseResult = new ApiResponse<Object>();
		int totalCount = 0 ; 
		int succedCount = 0 ; 
		int failedCount = 0 ; 
		/* 考试概要id */
		String usersId = condMap.get("usersId") + "";
		String usersClaId = condMap.get("usersClaId") + "";
		
		List<AAsk> resultList = new ArrayList<AAsk>() ;
		for (Iterator iterator = selList.iterator(); iterator.hasNext();)
		{
			Map<String, Object> rowMap = (Map<String, Object>) iterator.next();
			AAsk ask = new AAsk();
			
			ask.setUsersId(Integer.valueOf(usersId));
			ask.setUsersClaId(Integer.valueOf(usersClaId));
			
			String souName = rowMap.get("题目") + ""; 
			souName = souName.trim() ; 
			String hardStar = rowMap.get("难易度") + "";
			hardStar = hardStar.trim() ; 
			
			ConstatFinalUtil.SYS_LOGGER.info("==解析后:{}==",rowMap);
			/* 处理答案 */
			String answer = rowMap.get("答案") + "";
			answer = answer.trim() ;
			ask.setAnswer(answer);
			
			if("难".equalsIgnoreCase(hardStar))
			{
				hardStar = "5";
			}else if("中".equalsIgnoreCase(hardStar))
			{
				hardStar = "3";
			}else if("易".equalsIgnoreCase(hardStar))
			{
				hardStar = "1";
			}else
			{
				hardStar = "0";
			}
			
			/* 开始赋值 */
			ask.setName(souName);
			ask.setHardStar(Byte.valueOf(hardStar));
			
			ask.setAskType(AAskEnum.ASKTYPE_QUES.getStatus());
			ask.setSouType(AAskEnum.SOUTYPE_INSERT.getStatus());
			
			ask.setStatus(AAskEnum.STATUS_PUBLISH.getStatus());
			ask.setPubTime(new Date());
			
			/* 保存数据 */
			apiResponseResult = this.quesDbService.saveOneAskService(ask);
			totalCount ++ ; 
			if(apiResponseResult.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
			{
				succedCount ++ ; 
			}else
			{
				failedCount ++ ; 
			}
		}
		apiResponseResult.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		apiResponseResult.setInfo("总条数:" + totalCount + ";成功:" + succedCount + ";失败:" + failedCount, Collections.EMPTY_MAP);
		ConstatFinalUtil.SYS_LOGGER.info("插入问答题结果:{}",apiResponseResult.toJSON().toJSONString());
		return apiResponseResult ; 
	}

	/**
	 * 处理选择题
	 * @param excelDataMap
	 */
	private ApiResponse<Object> proccedAskSel(List<Map> selList,Map<String, Object> condMap)
	{
		ApiResponse<Object> apiResponseResult = new ApiResponse<Object>();
		int totalCount = 0 ; 
		int succedCount = 0 ; 
		int failedCount = 0 ; 
		/* 考试概要id */
		String usersId = condMap.get("usersId") + "";
		String usersClaId = condMap.get("usersClaId") + "";
		
		List<AAsk> resultList = new ArrayList<AAsk>() ;
		for (Iterator iterator = selList.iterator(); iterator.hasNext();)
		{
			Map<String, Object> rowMap = (Map<String, Object>) iterator.next();
			AAsk ask = new AAsk();
			
			ask.setUsersId(Integer.valueOf(usersId));
			ask.setUsersClaId(Integer.valueOf(usersClaId));
			
			String souName = rowMap.get("题目") + ""; 
			souName = souName.trim() ; 
			if("".equalsIgnoreCase(souName))
			{
				continue ; 
			}
			/* 拆分出问题和答案 */
			String[] souNames = souName.split("\n");
			
			List<String> souNamesList = new ArrayList<String>();
			for (int i = 0; i < souNames.length; i++)
			{
				String temp = souNames[i];
				souNamesList.add(temp);
			}
			String nameTemp = souNames[0];
			/* 处理答案在一行的问题 */
			if(souNamesList.size() <= 2)
			{
				/* 移除最后一个元素 */
				souNamesList.remove(souNamesList.size() - 1);
				/* 答案有两行 */
				String[] sels = souNames[1].split("   ");
				/* 如果拆分失败,再换一种切分方法 */
				if(sels.length == 1)
				{
					sels = souNames[1].split("  ");
				}
				if(sels.length == 1)
				{
					sels = souNames[1].split(" ");
				}
				for (int i = 0; i < sels.length; i++)
				{
					String temp = sels[i];
					souNamesList.add(temp);
				}
			}
			
			String hardStar = rowMap.get("难易度") + "";
			hardStar = hardStar.trim() ; 
			
			ConstatFinalUtil.SYS_LOGGER.info("==解析后:{}==",rowMap);
			String xuHao = rowMap.get("序号") + "";
			if(45 == Double.valueOf(xuHao).intValue())
			{
				System.out.println("====");
			}
			List<AAnsSel> ansSelList = new ArrayList<AAnsSel>();
			
			/* 把所有的答案放到一个map中.
			 * 键是选项,值是答案的内容 */
			Map<String, AAnsSel> selMap = new HashMap<String, AAnsSel>();
			for (int i = 1; i < souNamesList.size(); i++)
			{
				String temp = souNamesList.get(i);
				if("".equalsIgnoreCase(temp.trim()))
				{
					continue ; 
				}
				/* 去掉前后空格 */
				temp = temp.trim() ; 
				/* 选项 */
				String selKeyTemp = temp.substring(0, 2).replace(".", "").trim();
				String selValTemp = temp.substring(2, temp.length()).trim();
				
				AAnsSel ansSel = new AAnsSel();
				
				ansSel.setName(selValTemp);
				ansSel.setSn(selKeyTemp);
				
				ansSel.setStatus(AAnsSelEnum.STATUS_ENABLE.getStatus());
				ansSel.setCorrType(AAnsSelEnum.CORRTYPE_NO.getStatus());
				
				ansSel.setPubTime(new Date());
				
				/* 添加到集合中 */
				ansSelList.add(ansSel);
				
				/* 存储到map中 */
				selMap.put(selKeyTemp, ansSel);
			}
			/* 处理答案 */
			String answer = rowMap.get("答案") + "";
			answer = answer.trim() ; 
			for (int i = 0; i < answer.length(); i++)
			{
				String charStr = answer.charAt(i) + "" ; 
				/* 获取正确答案 */
				AAnsSel ansSel = selMap.get(charStr);
				ansSel.setCorrType(AAnsSelEnum.CORRTYPE_YES.getStatus());
			}
			if("难".equalsIgnoreCase(hardStar))
			{
				hardStar = "5";
			}else if("中".equalsIgnoreCase(hardStar))
			{
				hardStar = "3";
			}else if("易".equalsIgnoreCase(hardStar))
			{
				hardStar = "1";
			}else
			{
				hardStar = "0";
			}
			
			/* 开始赋值 */
			ask.setName(nameTemp);
			ask.setHardStar(Byte.valueOf(hardStar));
			
			if(answer.length() == 1)
			{
				/* 单选 */
				ask.setAskType(AAskEnum.ASKTYPE_SELECT.getStatus());
			}else
			{
				/* 多选 */
				ask.setAskType(AAskEnum.ASKTYPE_MULSELECT.getStatus());
			}
			ask.setSouType(AAskEnum.SOUTYPE_INSERT.getStatus());
			
			ask.setStatus(AAskEnum.STATUS_PUBLISH.getStatus());
			ask.setPubTime(new Date());
			
			/* 保存答案 */
			ask.setAnsSelList(ansSelList);
			
			/* 保存数据 */
			apiResponseResult = this.quesDbService.saveOneAskService(ask);
			totalCount ++ ; 
			if(apiResponseResult.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
			{
				succedCount ++ ; 
			}else
			{
				failedCount ++ ; 
			}
		}
		apiResponseResult.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		apiResponseResult.setInfo("总条数:" + totalCount + ";成功:" + succedCount + ";失败:" + failedCount, Collections.EMPTY_MAP);
		ConstatFinalUtil.SYS_LOGGER.info("插入单选题结果:{}",apiResponseResult.toJSON().toJSONString());
		return apiResponseResult ; 
	}
}

package com.wangsh.train.outer.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.service.BaseServiceImpl;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.outer.IClientOuterService;
import com.wangsh.train.outer.IServerOuterService;
import com.wangsh.train.ques.pojo.AAnsSel;
import com.wangsh.train.ques.pojo.AAsk;
import com.wangsh.train.ques.service.IExamDbService;
import com.wangsh.train.ques.service.IQuesDbService;
import com.wangsh.train.system.service.ISystemDbService;
import com.wangsh.train.users.service.IUsersDbService;

/**
 * 第三方请求当前服务器(当前服务器是服务端)
 * 
 * @author wangsh
 * @param <T>
 */
@Service("serverOuterService")
public class ServerOuterServiceImpl<T> extends BaseServiceImpl implements IServerOuterService
{
	@Resource
	private ISystemDbService systemDbService;
	@Resource
	private IUsersDbService usersDbService;
	@Resource
	private IExamDbService examDbService;
	@Resource
	private IQuesDbService quesDbService;

	@Resource
	private IClientOuterService clientOuterService;
	
	/**
	 * 验证加密的字符串
	 * 
	 * 加密参数;sha256(version+pubKey+私钥)
	 * 
	 * @return
	 */
	private ApiResponse<Object> verifyEncryptStr(JSONObject reqJSON)
	{
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		String version = reqJSON.getString("version");
		String pubKey = reqJSON.getString("pubKey");
		String time = reqJSON.getString("time");
		String encryptStr = reqJSON.getString("encrypt");

		/*
		 * 为了安全,私钥不能从请求中获取,读取本地的配置文件 私钥:客户端和服务器端同时存储到配置文件中
		 */
		// priKey=abc
		String priKey = ConstatFinalUtil.CONFIG_JSON.getString("usersCenter.out.prikey");

		//String encHou = DigestUtils.sha256Hex(version + pubKey + priKey);
		/* 加密源字符串 */
		String souStr = version + pubKey + time ; 
		/* sha256,key加密 */
		HmacUtils hmacUtils = new HmacUtils(HmacAlgorithms.HMAC_SHA_256, priKey);
		/* 加密 */
		String encHou = hmacUtils.hmacHex(souStr);
		if (encHou.equalsIgnoreCase(encryptStr)
				|| "0".equalsIgnoreCase(ConstatFinalUtil.CONFIG_JSON.getString("version")))
		{
			/* 成功 */
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else
		{
			/* 请求非法,您在恶意攻击 */
			apiResponse.setCode(ApiResponseEnum.INFO_JSON_ATTACK.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> serverVersionService(JSONObject reqJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--clientVersionService--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		String version = reqJSON.getString("version");
		String method = reqJSON.getString("method");
		if ("currentDate".equalsIgnoreCase(method))
		{
			if ("1".equalsIgnoreCase(version))
			{
				apiResponse = this.currentDate(reqJSON);
			} else
			{
				apiResponse.setCode(ApiResponseEnum.INFO_JSON_VERSION_ERROR.getStatus());
			}
		}else if ("findOneAsk".equalsIgnoreCase(method))
		{
			if ("1".equalsIgnoreCase(version))
			{
				apiResponse = this.findOneAsk(reqJSON);
			} else
			{
				apiResponse.setCode(ApiResponseEnum.INFO_JSON_VERSION_ERROR.getStatus());
			}
		} 
		
		if(apiResponse.getInfo() == null || "".equalsIgnoreCase(apiResponse.getInfo()))
		{
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		}
		/* 提示信息 */
		return apiResponse;
	}

	/**
	 * 返回当前时间
	 * 
	 * @param reqJSON
	 * @return
	 */
	public ApiResponse<Object> currentDate(JSONObject reqJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--currentDate--");
		/* 验证请求的合法性 */
		ApiResponse<Object> apiResponse = this.verifyEncryptStr(reqJSON);
		if (apiResponse.getCode() != ApiResponseEnum.STATUS_SUCCESS.getStatus())
		{
			return apiResponse;
		}
		/* 存储多个结果,Map存储 */
		Map<String, Object> dataMapJava = new HashMap<String, Object>();
		apiResponse.setDataMapJava(dataMapJava);

		Map<String, Object> dataOneMap = new HashMap<String, Object>();
		apiResponse.setDataOne(dataOneMap);

		// 接收参数
		JSONObject dataReqJSON = (JSONObject) reqJSON.get("data");
		
		dataOneMap.put("time", this.dateFormatUtil.dateTimeStr(new Date()));

		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		/* 提示信息 */
		return apiResponse;
	}
	
	/**
	 * 返回当前时间
	 * 
	 * @param reqJSON
	 * @return
	 */
	public ApiResponse<Object> findOneAsk(JSONObject reqJSON)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--findOneAsk--");
		Map<String,Object> condMap = new HashMap<String, Object>();
		
		/* 验证请求的合法性 */
		ApiResponse<Object> apiResponse = this.verifyEncryptStr(reqJSON);
		if (apiResponse.getCode() != ApiResponseEnum.STATUS_SUCCESS.getStatus())
		{
			return apiResponse;
		}
		/* 存储多个结果,Map存储 */
		Map<String, Object> dataMapJava = new HashMap<String, Object>();
		apiResponse.setDataMapJava(dataMapJava);

		Map<String, Object> dataOneMap = new HashMap<String, Object>();
		apiResponse.setDataOne(dataOneMap);

		// 接收参数
		JSONObject dataReqJSON = (JSONObject) reqJSON.get("data");
		/* 问题Id */
		String id = dataReqJSON.getString("id");
		
		/* 根据id查询问题 */
		condMap.put("id", id);
		ApiResponse<AAsk> askApiResponse = this.quesDbService.findOneAskService(condMap);
		AAsk askRes = askApiResponse.getDataOneJava();
		
		List<AAnsSel> ansSelList = askRes.getAnsSelList() ;
		Collections.shuffle(ansSelList);
		askRes.setAnsSelList(ansSelList);
		
		/* 存储数据 */
		dataMapJava.put(ApiResponseEnum.NAME_ONE.getName(), askRes);
		/* 设置分页信息 */
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(askApiResponse.getCode() + ""),
				Collections.EMPTY_MAP);
		
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		/* 提示信息 */
		return apiResponse;
	}
}

package com.wangsh.train.system.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.system.pojo.ASysFile;

/**
 * 系统文件的Dao
 * @author TeaBig
 */
@Mapper
public interface IASysFileDao extends IBaseDao<ASysFile>
{
	
}

package com.wangsh.train.job.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.job.pojo.AJobCompany;

/**
 * 公司的的Dao
 * @author TeaBig
 */
@Mapper
public interface IAJobCompanyDao extends IBaseDao<AJobCompany>
{
}

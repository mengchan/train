package com.wangsh.train.job.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.job.pojo.AJobComPosition;

/**
 * 示例代码中公司职位的Dao
 * @author Wangsh
 */
@Mapper
public interface IAJobComPositionDao extends IBaseDao<AJobComPosition>
{
}

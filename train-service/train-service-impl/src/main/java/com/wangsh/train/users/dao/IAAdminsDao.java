package com.wangsh.train.users.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.users.pojo.AAdmins;

/**
 * 管理员的Dao
 * @author TeaBig
 */
@Mapper
public interface IAAdminsDao extends IBaseDao<AAdmins>
{
}

package com.wangsh.train.system.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.common.dao.IBaseDao;
import com.wangsh.train.system.pojo.ASysTech;

/**
 * 系统技术的Dao
 * @author TeaBig
 */
@Mapper
public interface IASysTechDao extends IBaseDao<ASysTech>
{
	
}

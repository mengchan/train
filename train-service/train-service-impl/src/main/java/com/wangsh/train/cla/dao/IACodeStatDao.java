package com.wangsh.train.cla.dao;

import org.apache.ibatis.annotations.Mapper;

import com.wangsh.train.cla.pojo.ACodeStat;
import com.wangsh.train.common.dao.IBaseDao;

/**
 * 代码统计的Dao
 * @author TeaBig
 */
@Mapper
public interface IACodeStatDao extends IBaseDao<ACodeStat>
{
}

package com.wangsh.train.ques.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.system.pojo.ASysTech;
import com.wangsh.train.users.pojo.AUsers;
import com.wangsh.train.users.pojo.AUsersClass;

/**
 * 示例代码中的问题表
 * ~类名为表名,去掉下划线;
 * ~下划线隔开的单词要采用驼峰标识
 * @author Wangsh
 */
public class AAsk extends BasePojo<AAsk>
{
	private int id;
	private int usersId;
	private int usersClaId;
	private int ansUsersId;
	private int interviewId;
	private int techId ; 
	private String name;
	private String answer;
	private byte hardStar;
	private int imtCount;
	private int score;
	private byte askType;
	private byte souType;
	private String examContent;
	private String content;
	private String auditContent ; 
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	private Date auditTime ; 
	private Date randTime ; 
	
	/*--字符串表示--*/
	private String statusStr ; 
	private String askTypeStr ; 
	private String souTypeStr ; 
	private byte souStatus ; 
	private String souStatusStr ; 
	/*--关联对象--*/
	private AUsers users ; 
	private AUsersClass usersClass ; 
	private AUsers ansUsers ;
	private ASysTech tech ;
	/* 集合:选项列表 */
	private List<AAnsSel> ansSelList = new ArrayList<AAnsSel>();
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_ENABLE-1,值:启用
	 */
	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AAskEnum[] enums = AAskEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAskEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_DRAFT,值:启用
	 */
	public Map<String, String> getEnums()
	{
		// 根据状态值获取字符串描述
		AAskEnum[] enums = AAskEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAskEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(enumTemp.toString(), enumTemp.getStatus() + "");
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		AAskEnum[] enums = AAskEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAskEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AAskEnum[] enums = AAskEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAskEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getSouStatusStr()
	{
		// 根据状态值获取字符串描述
		AAskEnum[] enums = AAskEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAskEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getSouStatus())
				{
					this.souStatusStr = enumTemp.getName();
					break;
				}
			}
		}
		return souStatusStr;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getAskTypeStr()
	{
		// 根据状态值获取字符串描述
		AAskEnum[] enums = AAskEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAskEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("ASKTYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getAskType())
				{
					this.askTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return askTypeStr;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getSouTypeStr()
	{
		// 根据状态值获取字符串描述
		AAskEnum[] enums = AAskEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAskEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("SOUTYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getSouType())
				{
					this.souTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return souTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersId()
	{
		return usersId;
	}

	public void setUsersId(int usersId)
	{
		this.usersId = usersId;
	}

	public int getUsersClaId()
	{
		return usersClaId;
	}

	public void setUsersClaId(int usersClaId)
	{
		this.usersClaId = usersClaId;
	}

	public int getAnsUsersId()
	{
		return ansUsersId;
	}

	public void setAnsUserId(int ansUsersId)
	{
		this.ansUsersId = ansUsersId;
	}

	public int getInterviewId()
	{
		return interviewId;
	}

	public void setInterviewId(int interviewId)
	{
		this.interviewId = interviewId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAnswer()
	{
		return answer;
	}

	public void setAnswer(String answer)
	{
		this.answer = answer;
	}

	public byte getHardStar()
	{
		return hardStar;
	}

	public void setHardStar(byte hardStar)
	{
		this.hardStar = hardStar;
	}

	public int getImtCount()
	{
		return imtCount;
	}

	public void setImtCount(int imtCount)
	{
		this.imtCount = imtCount;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}

	public byte getAskType()
	{
		return askType;
	}

	public void setAskType(byte askType)
	{
		this.askType = askType;
	}

	public byte getSouType()
	{
		return souType;
	}

	public void setSouType(byte souType)
	{
		this.souType = souType;
	}

	public String getExamContent()
	{
		return examContent;
	}

	public void setExamContent(String examContent)
	{
		this.examContent = examContent;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public void setEnumsMap(Map<String, String> enumsMap)
	{
		this.enumsMap = enumsMap;
	}

	public AUsers getUsers()
	{
		return users;
	}

	public void setUsers(AUsers users)
	{
		this.users = users;
	}

	public AUsersClass getUsersClass()
	{
		return usersClass;
	}

	public void setUsersClass(AUsersClass usersClass)
	{
		this.usersClass = usersClass;
	}

	public AUsers getAnsUsers()
	{
		return ansUsers;
	}

	public void setAnsUsers(AUsers ansUsers)
	{
		this.ansUsers = ansUsers;
	}

	public void setAnsUsersId(int ansUsersId)
	{
		this.ansUsersId = ansUsersId;
	}

	public int getTechId()
	{
		return techId;
	}

	public void setTechId(int techId)
	{
		this.techId = techId;
	}

	public byte getSouStatus()
	{
		return souStatus;
	}

	public void setSouStatus(byte souStatus)
	{
		this.souStatus = souStatus;
	}

	public String getAuditContent()
	{
		return auditContent;
	}

	public void setAuditContent(String auditContent)
	{
		this.auditContent = auditContent;
	}

	public Date getAuditTime()
	{
		return auditTime;
	}

	public void setAuditTime(Date auditTime)
	{
		this.auditTime = auditTime;
	}

	public List<AAnsSel> getAnsSelList()
	{
		return ansSelList;
	}

	public void setAnsSelList(List<AAnsSel> ansSelList)
	{
		this.ansSelList = ansSelList;
	}

	public Date getRandTime()
	{
		return randTime;
	}

	public void setRandTime(Date randTime)
	{
		this.randTime = randTime;
	}

	public ASysTech getTech()
	{
		return tech;
	}

	public void setTech(ASysTech tech)
	{
		this.tech = tech;
	}
}

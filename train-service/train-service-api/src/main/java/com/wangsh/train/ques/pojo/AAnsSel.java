package com.wangsh.train.ques.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;

/**
 * 示例代码中的选项表
 * ~类名为表名,去掉下划线;
 * ~下划线隔开的单词要采用驼峰标识
 * @author Wangsh
 */
public class AAnsSel extends BasePojo<AAnsSel>
{
	private int id;
	private int askId;
	private String name;
	private String sn;
	private byte corrType;
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--字符串表示--*/
	private String statusStr ; 
	private String corrTypeStr ; 
	/*--关联对象--*/
	private AAsk ask ; 
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_ENABLE-1,值:启用
	 */
	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AAnsSelEnum[] enums = AAnsSelEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAnsSelEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		AAnsSelEnum[] enums = AAnsSelEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAnsSelEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AAnsSelEnum[] enums = AAnsSelEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAnsSelEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getCorrTypeStr()
	{
		// 根据状态值获取字符串描述
		AAnsSelEnum[] enums = AAnsSelEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAnsSelEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("CORRTYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getCorrType())
				{
					this.corrTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return corrTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getAskId()
	{
		return askId;
	}

	public void setAskId(int askId)
	{
		this.askId = askId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSn()
	{
		return sn;
	}

	public void setSn(String sn)
	{
		this.sn = sn;
	}

	public byte getCorrType()
	{
		return corrType;
	}

	public void setCorrType(byte corrType)
	{
		this.corrType = corrType;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public AAsk getAsk()
	{
		return ask;
	}

	public void setAsk(AAsk ask)
	{
		this.ask = ask;
	}

	public void setEnumsMap(Map<String, String> enumsMap)
	{
		this.enumsMap = enumsMap;
	}
}

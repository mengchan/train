package com.wangsh.train.ques.pojo;

/**
 * 	朝代的枚举
 * 
 * @author wangsh
 */
public enum AExamScoreDescEnum
{
	/* 枚举中不能包含中划线 */
	/* 状态:0:交卷,1:改成绩,2:完成;3:无效 */
	STATUS_COMMIT(Byte.valueOf("0"), "交卷"), 
	STATUS_SCORE(Byte.valueOf("1"), "改成绩"),
	STATUS_FINISH(Byte.valueOf("2"), "完成"),
	STATUS_INVALID(Byte.valueOf("3"), "无效"),
	
	;

	private byte status;
	private String name;

	private AExamScoreDescEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

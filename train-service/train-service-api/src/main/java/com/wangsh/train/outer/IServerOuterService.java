package com.wangsh.train.outer;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.pojo.ApiResponse;

/**
 * 当前服务是服务端(其它服务请求当前服务)
 * @author Wangsh
 */
public interface IServerOuterService
{
	/**
	 * 服务端版本的请求
	 * @param reqJSON
	 * @return
	 */
	ApiResponse<Object> serverVersionService(JSONObject reqJSON);
}

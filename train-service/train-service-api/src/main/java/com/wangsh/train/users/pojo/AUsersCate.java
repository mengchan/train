package com.wangsh.train.users.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.common.pojo.BasePojo;

/**
 * 用户分类的Pojo
 * @author TeaBig
 */
public class AUsersCate extends BasePojo<AUsersCate>
{
	private int id;
	private int usersClaId;
	private int cateTempId;
	private Date currDate;
	private String fileUrl;
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--关联对象表示--*/
	private AUsersClass usersClass ;

	/*--字符串表示--*/
	private byte souStatus ; 
	private String statusStr;
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AUsersCateEnum[] enums = AUsersCateEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersCateEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AUsersCateEnum[] enums = AUsersCateEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersCateEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersClaId()
	{
		return usersClaId;
	}

	public void setUsersClaId(int usersClaId)
	{
		this.usersClaId = usersClaId;
	}

	public int getCateTempId()
	{
		return cateTempId;
	}

	public void setCateTempId(int cateTempId)
	{
		this.cateTempId = cateTempId;
	}

	public Date getCurrDate()
	{
		return currDate;
	}

	public void setCurrDate(Date currDate)
	{
		this.currDate = currDate;
	}

	public String getFileUrl()
	{
		return fileUrl;
	}

	public void setFileUrl(String fileUrl)
	{
		this.fileUrl = fileUrl;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public AUsersClass getUsersClass()
	{
		return usersClass;
	}

	public void setUsersClass(AUsersClass usersClass)
	{
		this.usersClass = usersClass;
	}

	public void setStatusStr(String statusStr)
	{
		this.statusStr = statusStr;
	}

	public byte getSouStatus()
	{
		return souStatus;
	}

	public void setSouStatus(byte souStatus)
	{
		this.souStatus = souStatus;
	}
}

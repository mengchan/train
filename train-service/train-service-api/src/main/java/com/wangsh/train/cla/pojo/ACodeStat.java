package com.wangsh.train.cla.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.users.pojo.AUsers;
import com.wangsh.train.users.pojo.AUsersClass;

/**
 * 代码统计的POJO
 */
public class ACodeStat extends BasePojo<ACodeStat>
{
	private int id;
	private int usersId;
	private int usersClaId;
	private String name;
	private String oriName ;
	private String filePath;
	private int totalCount;
	private int sinComCount;
	private int mulComCount;
	private int blankCount;
	private int codeCount;
	private long fileSize ; 
	private long totalFileSize ;
	private String content;
	private byte codeType ;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--关联对象表示--*/
	private AUsers users ; 
	private AUsersClass usersClass ; 
	
	/*--字符串表示--*/
	private String statusStr ; 
	private String codeTypeStr ; 
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		ACodeStatEnum[] enums = ACodeStatEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ACodeStatEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			if(key.lastIndexOf("_") != -1)
			{
				key = key.substring(0,key.lastIndexOf("_")) ; 
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		ACodeStatEnum[] enums = ACodeStatEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ACodeStatEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getCodeTypeStr()
	{
		// 根据状态值获取字符串描述
		ACodeStatEnum[] enums = ACodeStatEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ACodeStatEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("CODETYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getCodeType())
				{
					this.codeTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return codeTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersId()
	{
		return usersId;
	}

	public void setUsersId(int usersId)
	{
		this.usersId = usersId;
	}

	public int getUsersClaId()
	{
		return usersClaId;
	}

	public void setUsersClaId(int usersClaId)
	{
		this.usersClaId = usersClaId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getFilePath()
	{
		return filePath;
	}

	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
	}

	public int getTotalCount()
	{
		return totalCount;
	}

	public void setTotalCount(int totalCount)
	{
		this.totalCount = totalCount;
	}

	public int getSinComCount()
	{
		return sinComCount;
	}

	public void setSinComCount(int sinComCount)
	{
		this.sinComCount = sinComCount;
	}

	public int getMulComCount()
	{
		return mulComCount;
	}

	public void setMulComCount(int mulComCount)
	{
		this.mulComCount = mulComCount;
	}

	public int getBlankCount()
	{
		return blankCount;
	}

	public void setBlankCount(int blankCount)
	{
		this.blankCount = blankCount;
	}

	public int getCodeCount()
	{
		return codeCount;
	}

	public void setCodeCount(int codeCount)
	{
		this.codeCount = codeCount;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public String getOriName()
	{
		return oriName;
	}

	public void setOriName(String oriName)
	{
		this.oriName = oriName;
	}

	public byte getCodeType()
	{
		return codeType;
	}

	public void setCodeType(byte codeType)
	{
		this.codeType = codeType;
	}

	public long getFileSize()
	{
		return fileSize;
	}

	public void setFileSize(long size)
	{
		this.fileSize = size;
	}

	public long getTotalFileSize()
	{
		return totalFileSize;
	}

	public void setTotalFileSize(long totalFileSize)
	{
		this.totalFileSize = totalFileSize;
	}

	public AUsers getUsers()
	{
		return users;
	}

	public void setUsers(AUsers users)
	{
		this.users = users;
	}

	public AUsersClass getUsersClass()
	{
		return usersClass;
	}

	public void setUsersClass(AUsersClass usersClass)
	{
		this.usersClass = usersClass;
	}
}

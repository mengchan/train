package com.wangsh.train.job.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.job.pojo.AJobComPosition;
import com.wangsh.train.job.pojo.AJobCompany;
import com.wangsh.train.job.pojo.AJobInterview;
import com.wangsh.train.job.pojo.AJobOffer;
import com.wangsh.train.job.pojo.AJobResume;

/**
 * 工作模块的Service
 * @author TeaBig
 */
public interface IJobDbService
{
	/*----- 公司模块管理开始 -----*/
	/**
	 * 添加一条公司
	 * 
	 * @param jobCompany
	 * @return
	 */
	ApiResponse<Object> saveOneJobCompanyService(AJobCompany jobCompany);

	/**
	 * 更新一条公司
	 * 
	 * @param jobCompany
	 * @return
	 */
	ApiResponse<Object> updateOneJobCompanyService(AJobCompany jobCompany);
	
	/**
	 * 更新一条公司,更新logo
	 * @param jobCompany
	 * @param paramsMap
	 * @return
	 * @throws Exception
	 */
	ApiResponse<Object> updateOneJobCompanyService(AJobCompany jobCompany,Map<String, Object> paramsMap) throws Exception ; 

	/**
	 * 删除一条公司
	 * 
	 * @param jobCompany
	 * @return
	 */
	ApiResponse<Object> deleteOneJobCompanyService(Map<String, Object> condMap);

	/**
	 * 查询多条公司
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobCompany> findCondListJobCompanyService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条公司
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobCompany> findOneJobCompanyService(Map<String, Object> condMap);
	/*----- 公司模块管理结束 -----*/
	
	/*----- 公司职位模块管理开始 -----*/
	/**
	 * 添加一条公司职位
	 * 
	 * @param jobComPosition
	 * @return
	 */
	ApiResponse<Object> saveOneComPositionService(AJobComPosition comPosition);

	/**
	 * 更新一条公司职位
	 * 
	 * @param jobComPosition
	 * @return
	 */
	ApiResponse<Object> updateOneComPositionService(AJobComPosition comPosition);

	/**
	 * 删除一条公司职位
	 * 
	 * @param jobComPosition
	 * @return
	 */
	ApiResponse<Object> deleteOneComPositionService(Map<String, Object> condMap);

	/**
	 * 查询多条公司职位
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobComPosition> findCondListComPositionService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条公司职位
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobComPosition> findOneComPositionService(Map<String, Object> condMap);
	/*----- 公司职位模块管理结束 -----*/
	
	/*----- 面试模块管理开始 -----*/
	/**
	 * 添加一条面试
	 * 
	 * @param jobInterview
	 * @return
	 */
	ApiResponse<Object> saveOneInterviewService(AJobInterview interview);

	/**
	 * 更新一条面试
	 * 
	 * @param jobInterview
	 * @return
	 */
	ApiResponse<Object> updateOneInterviewService(AJobInterview interview);

	/**
	 * 删除一条面试
	 * 
	 * @param jobInterview
	 * @return
	 */
	ApiResponse<Object> deleteOneInterviewService(Map<String, Object> condMap);

	/**
	 * 查询多条面试
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobInterview> findCondListInterviewService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条面试
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobInterview> findOneInterviewService(Map<String, Object> condMap);
	/*----- 面试模块管理结束 -----*/
	
	/*----- 公司offer模块管理开始 -----*/
	/**
	 * 添加一条公司offer
	 * 
	 * @param jobOffer
	 * @return
	 */
	ApiResponse<Object> saveOneOfferService(AJobOffer offer);

	/**
	 * 更新一条公司offer
	 * 
	 * @param jobOffer
	 * @return
	 */
	ApiResponse<Object> updateOneOfferService(AJobOffer offer);

	/**
	 * 删除一条公司offer
	 * 
	 * @param jobOffer
	 * @return
	 */
	ApiResponse<Object> deleteOneOfferService(Map<String, Object> condMap);

	/**
	 * 查询多条公司offer
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobOffer> findCondListOfferService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条公司offer
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobOffer> findOneOfferService(Map<String, Object> condMap);
	/*----- 公司offer模块管理结束 -----*/
	
	/*----- 简历模块管理开始 -----*/
	/**
	 * 添加一条简历
	 * 
	 * @param jobResume
	 * @return
	 */
	ApiResponse<Object> saveOneResumeService(AJobResume resume);

	/**
	 * 更新一条简历
	 * 
	 * @param jobResume
	 * @return
	 */
	ApiResponse<Object> updateOneResumeService(AJobResume resume);

	/**
	 * 删除一条简历
	 * 
	 * @param jobResume
	 * @return
	 */
	ApiResponse<Object> deleteOneResumeService(Map<String, Object> condMap);

	/**
	 * 查询多条简历
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobResume> findCondListResumeService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条简历
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AJobResume> findOneResumeService(Map<String, Object> condMap);
	/*----- 简历模块管理结束 -----*/
}

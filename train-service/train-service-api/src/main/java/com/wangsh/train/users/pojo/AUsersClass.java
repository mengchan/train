package com.wangsh.train.users.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.common.pojo.BasePojo;

/**
 * 用户班级的Pojo
 * @author TeaBig
 */
public class AUsersClass extends BasePojo<AUsersClass>
{
	private int id;
	private int usersId;
	private int classId;
	private String content;
	private byte defType ; 
	private byte classType;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--关联对象表示--*/
	private AUsers users ;
	private AClass cla ; 

	/*--字符串表示--*/
	private String statusStr;
	private String classTypeStr ;
	private String defTypeStr;
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AUsersClassEnum[] enums = AUsersClassEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersClassEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AUsersClassEnum[] enums = AUsersClassEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersClassEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getClassTypeStr()
	{
		// 根据状态值获取字符串描述
		AUsersClassEnum[] enums = AUsersClassEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersClassEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("CLASSTYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getClassType())
				{
					this.classTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return classTypeStr;
	}
	
	public String getDefTypeStr()
	{
		// 根据状态值获取字符串描述
		AUsersClassEnum[] enums = AUsersClassEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersClassEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("DEFTYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getDefType())
				{
					this.defTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return defTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersId()
	{
		return usersId;
	}

	public void setUsersId(int usersId)
	{
		this.usersId = usersId;
	}

	public int getClassId()
	{
		return classId;
	}

	public void setClassId(int classId)
	{
		this.classId = classId;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getClassType()
	{
		return classType;
	}

	public void setClassType(byte classType)
	{
		this.classType = classType;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public AUsers getUsers()
	{
		return users;
	}

	public void setUsers(AUsers users)
	{
		this.users = users;
	}

	public AClass getCla()
	{
		return cla;
	}

	public void setCla(AClass cla)
	{
		this.cla = cla;
	}

	public byte getDefType()
	{
		return defType;
	}

	public void setDefType(byte defType)
	{
		this.defType = defType;
	}
}

package com.wangsh.train.cla.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;

/**
 * 班级的Service
 * 
 * @author TeaBig
 */
public interface IClassOperService
{
	/*----- 班级模块管理开始 -----*/
	/**
	 * 添加一条班级
	 * 
	 * @param paramsMap
	 * @return
	 */
	ApiResponse<Object> saveOneClassService(Map<String, Object> paramsMap);

	/**
	 * 更新一条班级
	 * 
	 * @param paramsMap
	 * @return
	 */
	ApiResponse<Object> updateOneClassService(Map<String, Object> paramsMap);
	/*----- 班级模块管理结束 -----*/
	
	/**
	 * 统计课程资料
	 * @return
	 */
	ApiResponse<Object> operStatDataService(Map<String, Object> paramsMap);
}

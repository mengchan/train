package com.wangsh.train.cla.pojo;

/**
 * 	课程资料的枚举
 * @author wangsh
 *
 */
public enum AClassDataEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用");
	
	private byte status;
	private String name;

	private AClassDataEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

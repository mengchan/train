package com.wangsh.train.cla.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;

/**
 * 代码统计的POJO
 */
public class ACodeDetail extends BasePojo<ACodeDetail>
{
	private int id;
	private int codeStatId;
	private String name;
	private String oriName ;
	private String filePath;
	private int totalCount;
	private int sinComCount;
	private int mulComCount;
	private int blankCount;
	private int codeCount;
	private String fileMd5;
	private byte fileType;
	private byte equalsFlag ;
	private long fileSize ; 
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--关联对象表示--*/
	private ACodeStat codeStat ;
	
	/*--字符串表示--*/
	private String statusStr ; 
	private String equalsFlagStr ; 
	private String fileTypeStr ; 
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		ACodeStatEnum[] enums = ACodeStatEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ACodeStatEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			if(key.lastIndexOf("_") != -1)
			{
				key = key.substring(0,key.lastIndexOf("_")) ; 
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		ACodeDetailEnum[] enums = ACodeDetailEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ACodeDetailEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getFileTypeStr()
	{
		// 根据状态值获取字符串描述
		ACodeDetailEnum[] enums = ACodeDetailEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ACodeDetailEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("FILETYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getFileType())
				{
					this.fileTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return fileTypeStr;
	}
	
	public String getEqualsFlagStr()
	{
		// 根据状态值获取字符串描述
		ACodeDetailEnum[] enums = ACodeDetailEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ACodeDetailEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("EQUALSFLAG_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getEqualsFlag())
				{
					this.equalsFlagStr = enumTemp.getName();
					break;
				}
			}
		}
		return equalsFlagStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getCodeStatId()
	{
		return codeStatId;
	}

	public void setCodeStatId(int codeStatId)
	{
		this.codeStatId = codeStatId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getFilePath()
	{
		return filePath;
	}

	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
	}

	public int getTotalCount()
	{
		return totalCount;
	}

	public void setTotalCount(int totalCount)
	{
		this.totalCount = totalCount;
	}

	public int getSinComCount()
	{
		return sinComCount;
	}

	public void setSinComCount(int sinComCount)
	{
		this.sinComCount = sinComCount;
	}

	public int getMulComCount()
	{
		return mulComCount;
	}

	public void setMulComCount(int mulComCount)
	{
		this.mulComCount = mulComCount;
	}

	public int getBlankCount()
	{
		return blankCount;
	}

	public void setBlankCount(int blankCount)
	{
		this.blankCount = blankCount;
	}

	public int getCodeCount()
	{
		return codeCount;
	}

	public void setCodeCount(int codeCount)
	{
		this.codeCount = codeCount;
	}

	public String getFileMd5()
	{
		return fileMd5;
	}

	public void setFileMd5(String fileMd5)
	{
		this.fileMd5 = fileMd5;
	}

	public byte getFileType()
	{
		return fileType;
	}

	public void setFileType(byte fileType)
	{
		this.fileType = fileType;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public void setStatusStr(String statusStr)
	{
		this.statusStr = statusStr;
	}

	public byte getEqualsFlag()
	{
		return equalsFlag;
	}

	public void setEqualsFlag(byte equalsFlag)
	{
		this.equalsFlag = equalsFlag;
	}

	public String getOriName()
	{
		return oriName;
	}

	public void setOriName(String oriName)
	{
		this.oriName = oriName;
	}

	public long getFileSize()
	{
		return fileSize;
	}

	public void setFileSize(long fileSize)
	{
		this.fileSize = fileSize;
	}

	public ACodeStat getCodeStat()
	{
		return codeStat;
	}

	public void setCodeStat(ACodeStat codeStat)
	{
		this.codeStat = codeStat;
	}
}

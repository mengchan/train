package com.wangsh.train.users.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.users.pojo.AAdmins;
import com.wangsh.train.users.pojo.AUsers;
import com.wangsh.train.users.pojo.AUsersCate;
import com.wangsh.train.users.pojo.AUsersClass;

/**
 * 用户模块的Service
 * @author TeaBig
 */
public interface IUsersDbService
{
	/*----- 管理员模块管理开始 -----*/
	/**
	 * 添加一条管理员
	 * 
	 * @param admins
	 * @return
	 */
	ApiResponse<Object> saveOneAdminsService(AAdmins admins);

	/**
	 * 更新一条管理员
	 * 
	 * @param admins
	 * @return
	 */
	ApiResponse<Object> updateOneAdminsService(AAdmins admins);

	/**
	 * 删除一条管理员
	 * 
	 * @param admins
	 * @return
	 */
	ApiResponse<Object> deleteOneAdminsService(Map<String, Object> condMap);

	/**
	 * 查询多条管理员
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AAdmins> findCondListAdminsService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条管理员
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AAdmins> findOneAdminsService(Map<String, Object> condMap);
	/*----- 管理员模块管理结束 -----*/
	
	/*----- 用户模块管理开始 -----*/
	/**
	 * 添加一条用户
	 * 
	 * @param users
	 * @return
	 */
	ApiResponse<Object> saveOneUsersService(AUsers users);

	/**
	 * 更新一条用户
	 * 
	 * @param users
	 * @return
	 */
	ApiResponse<Object> updateOneUsersService(AUsers users);

	/**
	 * 删除一条用户
	 * 
	 * @param users
	 * @return
	 */
	ApiResponse<Object> deleteOneUsersService(Map<String, Object> condMap);

	/**
	 * 查询多条用户
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AUsers> findCondListUsersService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条用户
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AUsers> findOneUsersService(Map<String, Object> condMap);
	/*----- 用户模块管理结束 -----*/
	
	/*----- 用户班级模块管理开始 -----*/
	/**
	 * 添加一条用户班级
	 * 
	 * @param usersClass
	 * @return
	 */
	ApiResponse<Object> saveOneUsersClassService(AUsersClass usersClass);

	/**
	 * 更新一条用户班级
	 * 
	 * @param usersClass
	 * @return
	 */
	ApiResponse<Object> updateOneUsersClassService(AUsersClass usersClass);
	
	/**
	 * 批量更新用户班级
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> updateBatchUsersClassService(Map<String, Object> condMap);

	/**
	 * 删除一条用户班级
	 * 
	 * @param usersClass
	 * @return
	 */
	ApiResponse<Object> deleteOneUsersClassService(Map<String, Object> condMap);

	/**
	 * 查询多条用户班级
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AUsersClass> findCondListUsersClassService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条用户班级
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AUsersClass> findOneUsersClassService(Map<String, Object> condMap);
	/*----- 用户班级模块管理结束 -----*/
	
	/*----- 用户分类模块管理开始 -----*/
	/**
	 * 添加一条用户分类
	 * 
	 * @param usersCate
	 * @return
	 */
	ApiResponse<Object> saveOneUsersCateService(AUsersCate usersCate);

	/**
	 * 更新一条用户分类
	 * 
	 * @param usersCate
	 * @return
	 */
	ApiResponse<Object> updateOneUsersCateService(AUsersCate usersCate);
	
	/**
	 * 删除一条用户分类
	 * 
	 * @param usersCate
	 * @return
	 */
	ApiResponse<Object> deleteOneUsersCateService(Map<String, Object> condMap);

	/**
	 * 查询多条用户分类
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AUsersCate> findCondListUsersCateService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条用户分类
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AUsersCate> findOneUsersCateService(Map<String, Object> condMap);
	/*----- 用户分类模块管理结束 -----*/
}

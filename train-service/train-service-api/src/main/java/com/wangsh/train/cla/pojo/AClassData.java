package com.wangsh.train.cla.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.system.pojo.ASysTech;

/**
 * 课程资料
 * ~类名为表名,去掉下划线; ~下划线隔开的单词要采用驼峰标识
 * 
 * @author Wangsh
 */
public class AClassData extends BasePojo<AClassData>
{
	private int id;
	private int techId;
	private String name;
	private int viewNum;
	private int upNum;
	private int downNum;
	private String videoPath;
	private String videoCode;
	private int timeLong ; 
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;

	/*--字符串表示--*/
	private String statusStr;
	/*--对象表示--*/
	private ASysTech tech ; 
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AClassDataEnum[] enums = AClassDataEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AClassDataEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AClassDataEnum[] enums = AClassDataEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AClassDataEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getTechId()
	{
		return techId;
	}

	public void setTechId(int techId)
	{
		this.techId = techId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getViewNum()
	{
		return viewNum;
	}

	public void setViewNum(int viewNum)
	{
		this.viewNum = viewNum;
	}

	public int getUpNum()
	{
		return upNum;
	}

	public void setUpNum(int upNum)
	{
		this.upNum = upNum;
	}

	public int getDownNum()
	{
		return downNum;
	}

	public void setDownNum(int downNum)
	{
		this.downNum = downNum;
	}

	public String getVideoPath()
	{
		return videoPath;
	}

	public void setVideoPath(String videoPath)
	{
		this.videoPath = videoPath;
	}

	public String getVideoCode()
	{
		return videoCode;
	}

	public void setVideoCode(String videoCode)
	{
		this.videoCode = videoCode;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public void setEnumsMap(Map<String, String> enumsMap)
	{
		this.enumsMap = enumsMap;
	}

	public ASysTech getTech()
	{
		return tech;
	}

	public void setTech(ASysTech tech)
	{
		this.tech = tech;
	}

	public int getTimeLong()
	{
		return timeLong;
	}

	public void setTimeLong(int timeLong)
	{
		this.timeLong = timeLong;
	}

}

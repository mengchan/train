package com.wangsh.train.ques.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.ques.pojo.AExamDesc;
import com.wangsh.train.ques.pojo.AExamDetail;
import com.wangsh.train.ques.pojo.AExamScoreDesc;
import com.wangsh.train.ques.pojo.AExamScoreDetail;

/**
 * Service接口(考试代码)
 * 此接口中只存储数据库表的CRUD
 * @author Wangsh
 */
public interface IExamDbService
{
	/*----- 考试概要模块管理开始 -----*/
	/**
	 * 添加一条考试概要
	 * 
	 * @param ExamDesc	考试概要的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> saveOneExamDescService(AExamDesc examDesc);

	/**
	 * 更新一条考试概要
	 * 
	 * @param ExamDesc	考试概要的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> updateOneExamDescService(AExamDesc examDesc);

	/**
	 * 删除一条考试概要
	 * 
	 * @param condMap	删除的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> deleteOneExamDescService(Map<String, Object> condMap);

	/**
	 * 查询多条考试概要
	 * 
	 * @param pageInfoUtil	封装的分页对象;{{@link PageInfoUtil}
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AExamDesc
	 */
	ApiResponse<AExamDesc> findCondListExamDescService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条考试概要
	 * 
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AExamDesc
	 */
	ApiResponse<AExamDesc> findOneExamDescService(Map<String, Object> condMap);
	/*----- 考试概要模块管理结束 -----*/
	
	/*----- 考试明细模块管理开始 -----*/
	/**
	 * 添加一条考试明细
	 * 
	 * @param ExamDetail	考试明细的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> saveOneExamDetailService(AExamDetail examDetail);

	/**
	 * 更新一条考试明细
	 * 
	 * @param ExamDetail	考试明细的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> updateOneExamDetailService(AExamDetail examDetail);

	/**
	 * 删除一条考试明细
	 * 
	 * @param condMap	删除的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> deleteOneExamDetailService(Map<String, Object> condMap);

	/**
	 * 查询多条考试明细
	 * 
	 * @param pageInfoUtil	封装的分页对象;{{@link PageInfoUtil}
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AExamDetail
	 */
	ApiResponse<AExamDetail> findCondListExamDetailService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条考试明细
	 * 
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AExamDetail
	 */
	ApiResponse<AExamDetail> findOneExamDetailService(Map<String, Object> condMap);
	/*----- 考试明细模块管理结束 -----*/
	
	/*----- 分数概要模块管理开始 -----*/
	/**
	 * 添加一条分数概要
	 * 
	 * @param ExamScoreDesc	分数概要的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> saveOneExamScoreDescService(AExamScoreDesc examScoreDesc);

	/**
	 * 更新一条分数概要
	 * 
	 * @param ExamScoreDesc	分数概要的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> updateOneExamScoreDescService(AExamScoreDesc examScoreDesc);

	/**
	 * 删除一条分数概要
	 * 
	 * @param condMap	删除的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> deleteOneExamScoreDescService(Map<String, Object> condMap);

	/**
	 * 查询多条分数概要
	 * 
	 * @param pageInfoUtil	封装的分页对象;{{@link PageInfoUtil}
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AExamScoreDesc
	 */
	ApiResponse<AExamScoreDesc> findCondListExamScoreDescService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条分数概要
	 * 
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AExamScoreDesc
	 */
	ApiResponse<AExamScoreDesc> findOneExamScoreDescService(Map<String, Object> condMap);
	/*----- 分数概要模块管理结束 -----*/
	
	/*----- 分数明细模块管理开始 -----*/
	/**
	 * 添加一条分数明细
	 * 
	 * @param ExamScoreDetail	分数明细的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> saveOneExamScoreDetailService(AExamScoreDetail examScoreDetail);

	/**
	 * 更新一条分数明细
	 * 
	 * @param ExamScoreDetail	分数明细的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> updateOneExamScoreDetailService(AExamScoreDetail examScoreDetail);

	/**
	 * 删除一条分数明细
	 * 
	 * @param condMap	删除的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> deleteOneExamScoreDetailService(Map<String, Object> condMap);

	/**
	 * 查询多条分数明细
	 * 
	 * @param pageInfoUtil	封装的分页对象;{{@link PageInfoUtil}
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AExamScoreDetail
	 */
	ApiResponse<AExamScoreDetail> findCondListExamScoreDetailService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条分数明细
	 * 
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AExamScoreDetail
	 */
	ApiResponse<AExamScoreDetail> findOneExamScoreDetailService(Map<String, Object> condMap);
	/*----- 分数明细模块管理结束 -----*/
}

package com.wangsh.train.users.pojo;

/**
 * 	用户的枚举
 * @author wangsh
 *
 */
public enum AUsersClassEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	/* 类型:0:正常,1:降班,2:升班 */
	CLASSTYPE_DOWN(Byte.valueOf("0"), "降班"),
	CLASSTYPE_NORMAL(Byte.valueOf("1"), "正常"),
	CLASSTYPE_UP(Byte.valueOf("2"), "升班"),
	
	/* 是否默认:0:否,1:是 */
	DEFTYPE_NO(Byte.valueOf("0"), "否"),
	DEFTYPE_YES(Byte.valueOf("1"), "是"),
	;
	
	private byte status;
	private String name;

	private AUsersClassEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

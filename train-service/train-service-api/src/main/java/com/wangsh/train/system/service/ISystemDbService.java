package com.wangsh.train.system.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.system.pojo.ASysFile;
import com.wangsh.train.system.pojo.ASysPro;
import com.wangsh.train.system.pojo.ASysTech;

/**
 * 系统相关的模块Service
 * @author TeaBig
 */
public interface ISystemDbService
{
	/**
	 * 重新加载,或者初始化
	 */
	ApiResponse<Object> reloadSystem() ;
	
	/*----- 系统文件模块管理开始 -----*/
	/**
	 * 添加一条系统文件
	 * 
	 * @param sysFile
	 * @return
	 */
	ApiResponse<Object> saveOneSysFileService(ASysFile sysFile);
	
	/**
	 * 更新一条系统文件
	 * @param sysFile
	 * @return
	 */
	ApiResponse<Object> updateOneSysFileService(ASysFile sysFile);
	
	/**
	 * 更新一条系统文件,上传文件
	 * 
	 * @param sysFile
	 * @return
	 */
	ApiResponse<Object> updateOneSysFileService(ASysFile sysFile,Map<String, Object> paramsMap) throws Exception;

	/**
	 * 删除一条系统文件
	 * 
	 * @param sysFile
	 * @return
	 */
	ApiResponse<Object> deleteOneSysFileService(Map<String, Object> condMap);

	/**
	 * 查询多条系统文件
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ASysFile> findCondListSysFileService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条系统文件
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ASysFile> findOneSysFileService(Map<String, Object> condMap);
	/*----- 系统文件模块管理结束 -----*/
	
	/* == 系统配置操作开始 == */
	/**
	 * 添加一条系统配置
	 * 
	 * @param SysPro
	 * @return
	 */
	ApiResponse<Object> saveOneSysProService(ASysPro ASysPro);

	/**
	 * 更新一条系统配置
	 * 
	 * @param SysPro
	 * @return
	 */
	ApiResponse<Object> updateOneSysProService(ASysPro ASysPro);

	/**
	 * 删除一条系统配置
	 * 
	 * @param SysPro
	 * @return
	 */
	ApiResponse<Object> deleteOneSysProService(Map<String, Object> condMap);

	/**
	 * 查询多条系统配置
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ASysPro> findCondListSysProService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条系统配置
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ASysPro> findOneSysProService(Map<String, Object> condMap);
	/* == 系统配置操作结束 == */
	
	/*----- 系统技术模块管理开始 -----*/
	/**
	 * 添加一条系统技术
	 * 
	 * @param sysTech
	 * @return
	 */
	ApiResponse<Object> saveOneSysTechService(ASysTech sysTech);
	
	/**
	 * 更新一条系统技术
	 * @param sysTech
	 * @return
	 */
	ApiResponse<Object> updateOneSysTechService(ASysTech sysTech);
	
	/**
	 * 删除一条系统技术
	 * 
	 * @param sysTech
	 * @return
	 */
	ApiResponse<Object> deleteOneSysTechService(Map<String, Object> condMap);

	/**
	 * 查询多条系统技术
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ASysTech> findCondListSysTechService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条系统技术
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ASysTech> findOneSysTechService(Map<String, Object> condMap);
	/*----- 系统技术模块管理结束 -----*/
}

package com.wangsh.train.job.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.system.pojo.ASysPro;
import com.wangsh.train.users.pojo.AUsers;

/**
 * 示例代码中的公司职位表 ~类名为表名,去掉下划线; ~下划线隔开的单词要采用驼峰标识
 * 
 * @author Wangsh
 */
public class AJobComPosition extends BasePojo<AJobComPosition>
{
	private int id;
	private int createId ; 
	private int companyId;
	private int regionId;
	private int cateId;
	private int educId;
	private String name;
	private int wages;
	private int experSt;
	private int experEd;
	private String address;
	private int upNum;
	private int downNum;
	private String auditContent;
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	private Date auditTime ; 

	/*--字符串表示--*/
	private String statusStr;
	private byte souStatus ; 
	/*--对象表示--*/
	private AUsers create ; 
	private AJobCompany company ; 
	private ASysPro catePro ; 
	private ASysPro educPro ;
	/* 关联的对象 */
	private JSONObject regionJSON ; 
	
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AJobComPositionEnum[] enums = AJobComPositionEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobComPositionEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_DRAFT,值:启用
	 */
	public Map<String, String> getEnums()
	{
		// 根据状态值获取字符串描述
		AJobComPositionEnum[] enums = AJobComPositionEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobComPositionEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(enumTemp.toString(), enumTemp.getStatus() + "");
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		AJobComPositionEnum[] enums = AJobComPositionEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobComPositionEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AJobComPositionEnum[] enums = AJobComPositionEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobComPositionEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getCompanyId()
	{
		return companyId;
	}

	public void setCompanyId(int companyId)
	{
		this.companyId = companyId;
	}

	public int getRegionId()
	{
		return regionId;
	}

	public void setRegionId(int regionId)
	{
		this.regionId = regionId;
	}

	public int getCateId()
	{
		return cateId;
	}

	public void setCateId(int cateId)
	{
		this.cateId = cateId;
	}

	public int getEducId()
	{
		return educId;
	}

	public void setEducId(int educId)
	{
		this.educId = educId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getWages()
	{
		return wages;
	}

	public void setWages(int wages)
	{
		this.wages = wages;
	}

	public int getExperSt()
	{
		return experSt;
	}

	public void setExperSt(int experSt)
	{
		this.experSt = experSt;
	}

	public int getExperEd()
	{
		return experEd;
	}

	public void setExperEd(int experEd)
	{
		this.experEd = experEd;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public int getUpNum()
	{
		return upNum;
	}

	public void setUpNum(int upNum)
	{
		this.upNum = upNum;
	}

	public int getDownNum()
	{
		return downNum;
	}

	public void setDownNum(int downNum)
	{
		this.downNum = downNum;
	}

	public String getAuditContent()
	{
		return auditContent;
	}

	public void setAuditContent(String auditContent)
	{
		this.auditContent = auditContent;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public ASysPro getCatePro()
	{
		return catePro;
	}

	public void setCatePro(ASysPro catePro)
	{
		this.catePro = catePro;
	}

	public ASysPro getEducPro()
	{
		return educPro;
	}

	public void setEducPro(ASysPro educPro)
	{
		this.educPro = educPro;
	}

	public AJobCompany getCompany()
	{
		return company;
	}

	public void setCompany(AJobCompany company)
	{
		this.company = company;
	}

	public JSONObject getRegionJSON()
	{
		return regionJSON;
	}

	public void setRegionJSON(JSONObject regionJSON)
	{
		this.regionJSON = regionJSON;
	}

	public int getCreateId()
	{
		return createId;
	}

	public void setCreateId(int createId)
	{
		this.createId = createId;
	}

	public AUsers getCreate()
	{
		return create;
	}

	public void setCreate(AUsers create)
	{
		this.create = create;
	}

	public Date getAuditTime()
	{
		return auditTime;
	}

	public void setAuditTime(Date auditTime)
	{
		this.auditTime = auditTime;
	}

	public byte getSouStatus()
	{
		return souStatus;
	}

	public void setSouStatus(byte souStatus)
	{
		this.souStatus = souStatus;
	}
}

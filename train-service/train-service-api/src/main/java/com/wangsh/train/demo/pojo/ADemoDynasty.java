package com.wangsh.train.demo.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;

/**
 * 示例代码中的朝代表
 * ~类名为表名,去掉下划线;
 * ~下划线隔开的单词要采用驼峰标识
 * @author Wangsh
 */
public class ADemoDynasty extends BasePojo<ADemoDynasty>
{
	private int id;
	private String name;
	private String content;
	private String capital;
	private int stYear;
	private int edYear;
	private int age;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--字符串表示--*/
	private String statusStr ; 
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		ADemoDynastyEnum[] enums = ADemoDynastyEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ADemoDynastyEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			if(key.lastIndexOf("_") != -1)
			{
				key = key.substring(0,key.lastIndexOf("_")) ; 
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		ADemoDynastyEnum[] enums = ADemoDynastyEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ADemoDynastyEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getCapital()
	{
		return capital;
	}

	public void setCapital(String capital)
	{
		this.capital = capital;
	}

	public int getStYear()
	{
		return stYear;
	}

	public void setStYear(int stYear)
	{
		this.stYear = stYear;
	}

	public int getEdYear()
	{
		return edYear;
	}

	public void setEdYear(int edYear)
	{
		this.edYear = edYear;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}
}

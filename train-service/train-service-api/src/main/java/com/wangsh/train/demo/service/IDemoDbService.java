package com.wangsh.train.demo.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.demo.pojo.ADemoDynasty;
import com.wangsh.train.demo.pojo.ADemoKing;

/**
 * 系统模块的主要service
 * @author Wangsh
 */
public interface IDemoDbService
{
	/*----- 朝代模块管理开始 -----*/
	/**
	 * 添加一条朝代
	 * 
	 * @param demoDynasty
	 * @return
	 */
	ApiResponse<Object> saveOneDynastyService(ADemoDynasty dynasty);

	/**
	 * 更新一条朝代
	 * 
	 * @param demoDynasty
	 * @return
	 */
	ApiResponse<Object> updateOneDynastyService(ADemoDynasty dynasty);

	/**
	 * 删除一条朝代
	 * 
	 * @param demoDynasty
	 * @return
	 */
	ApiResponse<Object> deleteOneDynastyService(Map<String, Object> condMap);

	/**
	 * 查询多条朝代
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ADemoDynasty> findCondListDynastyService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条朝代
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ADemoDynasty> findOneDynastyService(Map<String, Object> condMap);
	/*----- 朝代模块管理结束 -----*/
	
	/*----- 皇上模块管理开始 -----*/
	/**
	 * 添加一条皇上
	 * 
	 * @param demoKing
	 * @return
	 */
	ApiResponse<Object> saveOneKingService(ADemoKing king);

	/**
	 * 更新一条皇上
	 * 
	 * @param demoKing
	 * @return
	 */
	ApiResponse<Object> updateOneKingService(ADemoKing king);

	/**
	 * 删除一条皇上
	 * 
	 * @param demoKing
	 * @return
	 */
	ApiResponse<Object> deleteOneKingService(Map<String, Object> condMap);

	/**
	 * 查询多条皇上
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ADemoKing> findCondListKingService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条皇上
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ADemoKing> findOneKingService(Map<String, Object> condMap);
	/*----- 皇上模块管理结束 -----*/
}

package com.wangsh.train.demo.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;

/**
 * 示例代码中的皇上表
 * 
 * @author Wangsh
 */
public class ADemoKing extends BasePojo<ADemoKing>
{

	private int id;
	private int dynastyId;
	private String name;
	private String content;
	private String miaoHao;
	private String nianHao;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/* 关联关系 */
	private ADemoDynasty dynasty ;
	
	/*--字符串表示--*/
	private String statusStr ; 
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		ADemoKingEnum[] demoKingEnums = ADemoKingEnum.values();
		for (int i = 0; i < demoKingEnums.length; i++)
		{
			ADemoKingEnum demoKingEnum = demoKingEnums[i];
			String key = demoKingEnum.toString() ; 
			if(key.lastIndexOf("_") != -1)
			{
				key = key.substring(0,key.lastIndexOf("_")) ; 
			}
			enumsMap.put(key + "-" + demoKingEnum.getStatus() + "", demoKingEnum.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		ADemoKingEnum[] regionEnums = ADemoKingEnum.values();
		for (int i = 0; i < regionEnums.length; i++)
		{
			ADemoKingEnum regionEnum = regionEnums[i];
			if (regionEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (regionEnum.getStatus() == this.getStatus())
				{
					this.statusStr = regionEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getDynastyId()
	{
		return dynastyId;
	}

	public void setDynastyId(int dynastyId)
	{
		this.dynastyId = dynastyId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getMiaoHao()
	{
		return miaoHao;
	}

	public void setMiaoHao(String miaoHao)
	{
		this.miaoHao = miaoHao;
	}

	public String getNianHao()
	{
		return nianHao;
	}

	public void setNianHao(String nianHao)
	{
		this.nianHao = nianHao;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public ADemoDynasty getDynasty()
	{
		return dynasty;
	}

	public void setDynasty(ADemoDynasty dynasty)
	{
		this.dynasty = dynasty;
	}
}

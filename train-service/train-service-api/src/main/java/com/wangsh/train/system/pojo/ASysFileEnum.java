package com.wangsh.train.system.pojo;

/**
 * 	管理员的枚举
 * @author wangsh
 *
 */
public enum ASysFileEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	/* 文件类型:0:普通文件,1:图片,2:视频 */
	FILETYPE_COMMON(Byte.valueOf("0"), "普通文件"),
	FILETYPE_IMG(Byte.valueOf("1"), "图片"),
	FILETYPE_VIDEO(Byte.valueOf("2"), "视频"),
	
	/* 文件关联id类型;0:usersCate */
	FILERELATYPE_USERSCATE(Byte.valueOf("0"), "用户模板"),
	FILERELATYPE_SYSDATA(Byte.valueOf("2"), "资料"),
	FILERELATYPE_INTERVIEW(Byte.valueOf("3"), "面试笔试照片"),
	FILERELATYPE_OFFER(Byte.valueOf("4"), "offer截图"),
	FILERELATYPE_RESUME(Byte.valueOf("5"), "简历"),
	;
	
	private byte status;
	private String name;

	private ASysFileEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

package com.wangsh.train.cla.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.pojo.BasePojo;

/**
 * 班级的Pojo
 * 
 * @author TeaBig
 */
public class AClass extends BasePojo<AClass>
{
	private int id;
	private int cateId;
	private int regionId;
	private String name;
	private String address;
	private Date stTime;
	private Date edTime;
	private int totalNum ; 
	private byte claStatus;
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;

	/*--字符串表示--*/
	private String statusStr;
	private String claStatusStr ; 
	
	/* 关联的对象 */
	private JSONObject regionJSON ; 
	private JSONObject cateJSON ; 
	
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AClassEnum[] enums = AClassEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AClassEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AClassEnum[] enums = AClassEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AClassEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getClaStatusStr()
	{
		// 根据状态值获取字符串描述
		AClassEnum[] enums = AClassEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AClassEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("ClASTATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getClaStatus())
				{
					this.claStatusStr = enumTemp.getName();
					break;
				}
			}
		}
		return claStatusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getRegionId()
	{
		return regionId;
	}

	public void setRegionId(int regionId)
	{
		this.regionId = regionId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public Date getStTime()
	{
		return stTime;
	}

	public void setStTime(Date stTime)
	{
		this.stTime = stTime;
	}

	public Date getEdTime()
	{
		return edTime;
	}

	public void setEdTime(Date edTime)
	{
		this.edTime = edTime;
	}

	public byte getClaStatus()
	{
		return claStatus;
	}

	public void setClaStatus(byte claStatus)
	{
		this.claStatus = claStatus;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public int getCateId()
	{
		return cateId;
	}

	public void setCateId(int cateId)
	{
		this.cateId = cateId;
	}

	public JSONObject getRegionJSON()
	{
		return regionJSON;
	}

	public void setRegionJSON(JSONObject regionJSON)
	{
		this.regionJSON = regionJSON;
	}

	public int getTotalNum()
	{
		return totalNum;
	}

	public void setTotalNum(int totalNum)
	{
		this.totalNum = totalNum;
	}

	public JSONObject getCateJSON()
	{
		return cateJSON;
	}

	public void setCateJSON(JSONObject cateJSON)
	{
		this.cateJSON = cateJSON;
	}
}

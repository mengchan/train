package com.wangsh.train.job.pojo;

/**
 * 	朝代的枚举
 * @author wangsh
 *
 */
public enum AJobInterviewEnum
{
	/* 状态:0:草稿,1:发布;2:审核通过,3:审核失败;4:录入中;5:录入完成;6:确认 */
	STATUS_DRAFT(Byte.valueOf("0"), "草稿"), 
	STATUS_PUBLISH(Byte.valueOf("1"), "发布"),
	STATUS_AUDITYES(Byte.valueOf("2"), "审核通过"),
	STATUS_AUDITNO(Byte.valueOf("3"), "审核失败"),
	STATUS_INSERTING(Byte.valueOf("4"), "录入中"),
	STATUS_INSERTED(Byte.valueOf("5"), "录入完成"),
	STATUS_CONFIRM(Byte.valueOf("6"), "确认"),
	
	/* 是否模拟:0:正式,1:模拟 */
	SIMULATION_TEST(Byte.valueOf("0"), "模拟"),
	SIMULATION_PROD(Byte.valueOf("1"), "正式"),
	;
	
	private byte status;
	private String name;

	private AJobInterviewEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

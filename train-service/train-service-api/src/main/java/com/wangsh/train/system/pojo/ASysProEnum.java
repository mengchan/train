package com.wangsh.train.system.pojo;

public enum ASysProEnum
{

	/**
	 * 枚举项 STATUS_ENABLE:变量名,数据库中的字段名+含义名 "启用":字符串描述 1:值
	 */
	STATUS_ENABLE("启用", Byte.valueOf("1")), 
	STATUS_DISABLE("禁用", Byte.valueOf("0")),
	
	/*类型:类型:0:默认,1:职位类型;2:学历*/
	PROTYPE_COMMON("普通",Byte.valueOf("0")),
	PROTYPE_POSITION("职位",Byte.valueOf("1")),
	PROTYPE_EDUCE("学历",Byte.valueOf("2")),

	OPERTYPE_EQUAL("=",Byte.valueOf("0")),
	OPERTYPE_GREATERTHAN(">",Byte.valueOf("1")),
	OPERTYPE_LESSTHAN("<",Byte.valueOf("2")),
	OPERTYPE_NOTLESS(">=",Byte.valueOf("3")),
	OPERTYPE_NOTGREATER("<=",Byte.valueOf("4"));
	
	/* 值 */
	private String name;
	private byte status;

	ASysProEnum(String name, byte status)
	{
		this.name = name;
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

}

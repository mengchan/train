package com.wangsh.train.ques.pojo;

/**
 * 	朝代的枚举
 * 
 * @author wangsh
 */
public enum AAskEnum
{
	/* 枚举中不能包含中划线 */
	/*0:单选题,1:问答题;2:编程题;3:判断题;4:多选题 */
	ASKTYPE_SELECT(Byte.valueOf("0"), "单选题"),
	ASKTYPE_QUES(Byte.valueOf("1"), "问答题"),
	ASKTYPE_PROGRAM(Byte.valueOf("2"), "编程题"),
	ASKTYPE_JUDGE(Byte.valueOf("3"), "判断题"),
	ASKTYPE_MULSELECT(Byte.valueOf("4"), "多选题"),
	
	/* souType:来源类型:0:面试题,1:上课提问题;2:录入题 */
	SOUTYPE_INTER(Byte.valueOf("0"), "面试"),
	SOUTYPE_CLASS(Byte.valueOf("1"), "上课提问"),
	SOUTYPE_INSERT(Byte.valueOf("2"), "录入"),
	
	/* 状态:0:草稿,1:发布;2:审核通过,3:审核不通过 */
	STATUS_DRAFT(Byte.valueOf("0"), "草稿"), 
	STATUS_PUBLISH(Byte.valueOf("1"), "发布"),
	STATUS_AUDITYES(Byte.valueOf("2"), "审核通过"),
	STATUS_AUDITNO(Byte.valueOf("3"), "审核不通过"),
	
	;

	private byte status;
	private String name;

	private AAskEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

package com.wangsh.train.ques.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;

/**
 * 示例代码中的考试试卷明细表
 * ~类名为表名,去掉下划线;
 * ~下划线隔开的单词要采用驼峰标识
 * @author Wangsh
 */
public class AExamDetail extends BasePojo<AExamDetail>
{
	private int id;
	private int examId;
	private int askId;
	private String name;
	private byte hardStar;
	private byte askType;
	private int score;
	private String content;;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--字符串表示--*/
	private String statusStr ; 
	private byte souStatus ; 
	private String souStatusStr ; 
	private String askTypeStr ; 
	/*--关联对象--*/
	private AExamDesc examDesc ; 
	private AAsk ask ; 
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_ENABLE-1,值:启用
	 */
	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AExamDetailEnum[] enums = AExamDetailEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDetailEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		AExamDetailEnum[] enums = AExamDetailEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDetailEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AExamDetailEnum[] enums = AExamDetailEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDetailEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getSouStatusStr()
	{
		// 根据状态值获取字符串描述
		AExamDetailEnum[] enums = AExamDetailEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDetailEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getSouStatus())
				{
					this.souStatusStr = enumTemp.getName();
					break;
				}
			}
		}
		return souStatusStr;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getAskTypeStr()
	{
		// 根据状态值获取字符串描述
		AExamDetailEnum[] enums = AExamDetailEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDetailEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("ASKTYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getAskType())
				{
					this.askTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return askTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getExamId()
	{
		return examId;
	}

	public void setExamId(int examId)
	{
		this.examId = examId;
	}

	public int getAskId()
	{
		return askId;
	}

	public void setAskId(int askId)
	{
		this.askId = askId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public byte getHardStar()
	{
		return hardStar;
	}

	public void setHardStar(byte hardStar)
	{
		this.hardStar = hardStar;
	}

	public byte getAskType()
	{
		return askType;
	}

	public void setAskType(byte askType)
	{
		this.askType = askType;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public byte getSouStatus()
	{
		return souStatus;
	}

	public void setSouStatus(byte souStatus)
	{
		this.souStatus = souStatus;
	}

	public AExamDesc getExamDesc()
	{
		return examDesc;
	}

	public void setExamDesc(AExamDesc examDesc)
	{
		this.examDesc = examDesc;
	}

	public AAsk getAsk()
	{
		return ask;
	}

	public void setAsk(AAsk ask)
	{
		this.ask = ask;
	}

	public void setEnumsMap(Map<String, String> enumsMap)
	{
		this.enumsMap = enumsMap;
	}
}

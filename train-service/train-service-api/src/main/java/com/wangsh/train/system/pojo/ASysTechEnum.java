package com.wangsh.train.system.pojo;

/**
 * 	朝代的枚举
 * 
 * @author wangsh
 */
public enum ASysTechEnum
{
	/* 枚举中不能包含中划线 */
	/* 状态:0:禁用,1:启用 */
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"), 
	STATUS_ENABLE(Byte.valueOf("1"), "启用"),
	
	;

	private byte status;
	private String name;

	private ASysTechEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

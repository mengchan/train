package com.wangsh.train.ques.pojo;

/**
 * 	朝代的枚举
 * 
 * @author wangsh
 */
public enum AExamDescEnum
{
	/* 枚举中不能包含中划线 */
	/* 类型:0:测试,1:周考,2:月考;3:机试(问答题);4:面试 */
	EXAMTYPE_TEST(Byte.valueOf("0"), "测试"), 
	EXAMTYPE_WEEK(Byte.valueOf("1"), "周考"), 
	EXAMTYPE_MONTH(Byte.valueOf("2"), "月考"), 
	EXAMTYPE_MACHINETEST(Byte.valueOf("3"), "机试"), 
	EXAMTYPE_INTERVIEW(Byte.valueOf("4"), "面试"), 
	
	
	/* 状态:0:草稿,1:已发布,2;进行中,3:改成绩,4:完成 */
	STATUS_DRAFT(Byte.valueOf("0"), "草稿"), 
	STATUS_PUBLISH(Byte.valueOf("1"), "发布"),
	STATUS_ING(Byte.valueOf("2"), "进行中"),
	STATUS_SCORE(Byte.valueOf("3"), "改成绩"),
	STATUS_FINISH(Byte.valueOf("4"), "完成"),
	
	;

	private byte status;
	private String name;

	private AExamDescEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

package com.wangsh.train.cla.pojo;

/**
 * 	管理员的枚举
 * @author wangsh
 *
 */
public enum ACodeStatEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	CODETYPE_UN(Byte.valueOf("0"), "未统计"), 
	CODETYPE_ING(Byte.valueOf("1"), "统计中"),
	CODETYPE_ED(Byte.valueOf("2"), "已统计")
	;
	
	private byte status;
	private String name;

	private ACodeStatEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

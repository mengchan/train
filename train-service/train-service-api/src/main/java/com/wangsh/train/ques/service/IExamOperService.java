package com.wangsh.train.ques.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.ques.pojo.AExamDesc;

/**
 * Service接口(考试代码)
 * 此接口中只存储数据库表的CRUD
 * @author Wangsh
 */
public interface IExamOperService
{
	/**
	 * 更新考试概要
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> updateOneExamDescService(Map<String, Object> condMap);
	
	/**
	 * 传入一个考试的id,自动生成试卷的答案
	 * @param condMap
	 * @return
	 */
	ApiResponse<AExamDesc> proccedOneExamDescService(Map<String, Object> condMap);
}

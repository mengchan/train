package com.wangsh.train.ques.pojo;

/**
 * 	朝代的枚举
 * 
 * @author wangsh
 */
public enum AAnsSelEnum
{
	/* 枚举中不能包含中划线 */
	
	/* 是否正确:0:正确,1:错误 */
	CORRTYPE_YES(Byte.valueOf("0"), "正确"),
	CORRTYPE_NO(Byte.valueOf("1"), "错误"), 
	
	/* 状态:0:禁用,1:启用 */
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"), 
	STATUS_ENABLE(Byte.valueOf("1"), "启用"),
	
	;

	private byte status;
	private String name;

	private AAnsSelEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

package com.wangsh.train.users.pojo;

/**
 * 	用户的枚举
 * @author wangsh
 *
 */
public enum AUsersEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	/* 阶段:0:意向,1:预科,2:学习,3:就业生,4:工作 */
	STAGETYPE_YIXIANG(Byte.valueOf("0"), "意向"), 
	STAGETYPE_YUKE(Byte.valueOf("1"), "预科"), 
	STAGETYPE_STUDY(Byte.valueOf("2"), "学习"), 
	STAGETYPE_OBTAIN(Byte.valueOf("3"), "就业生"),
	STAGETYPE_JOB(Byte.valueOf("4"), "工作"),
	;
	
	private byte status;
	private String name;

	private AUsersEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

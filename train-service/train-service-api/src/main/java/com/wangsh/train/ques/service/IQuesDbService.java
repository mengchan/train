package com.wangsh.train.ques.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.ques.pojo.AAnsSel;
import com.wangsh.train.ques.pojo.AAsk;

/**
 * Service接口(示例代码)
 * 此接口中只存储数据库表的CRUD
 * @author Wangsh
 */
public interface IQuesDbService
{
	/*----- 问题模块管理开始 -----*/
	/**
	 * 添加一条问题
	 * 
	 * @param Ask	问题的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> saveOneAskService(AAsk ask);

	/**
	 * 更新一条问题
	 * 
	 * @param Ask	问题的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> updateOneAskService(AAsk ask);

	/**
	 * 删除一条问题
	 * 
	 * @param condMap	删除的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> deleteOneAskService(Map<String, Object> condMap);

	/**
	 * 查询多条问题
	 * 
	 * @param pageInfoUtil	封装的分页对象;{{@link PageInfoUtil}
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AAsk
	 */
	ApiResponse<AAsk> findCondListAskService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条问题
	 * 
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AAsk
	 */
	ApiResponse<AAsk> findOneAskService(Map<String, Object> condMap);
	/*----- 问题模块管理结束 -----*/
	
	/*----- 选项模块管理开始 -----*/
	/**
	 * 添加一条选项
	 * 
	 * @param AnsSel	选项的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> saveOneAnsSelService(AAnsSel ansSel);

	/**
	 * 更新一条选项
	 * 
	 * @param AnsSel	选项的对象
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> updateOneAnsSelService(AAnsSel ansSel);

	/**
	 * 删除一条选项
	 * 
	 * @param condMap	删除的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是Object
	 */
	ApiResponse<Object> deleteOneAnsSelService(Map<String, Object> condMap);

	/**
	 * 查询多条选项
	 * 
	 * @param pageInfoUtil	封装的分页对象;{{@link PageInfoUtil}
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AAnsSel
	 */
	ApiResponse<AAnsSel> findCondListAnsSelService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条选项
	 * 
	 * @param condMap	搜索的条件
	 * @return	封装的ApiResponse;详情参见:{@link ApiResponse}},泛型是AAnsSel
	 */
	ApiResponse<AAnsSel> findOneAnsSelService(Map<String, Object> condMap);
	/*----- 选项模块管理结束 -----*/
}

package com.wangsh.train.system.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;

/**
 * 系统__上传文件__表
 * 
 * @author TeaBig
 */
public class ASysFile extends BasePojo<ASysFile>
{
	private int id;
	private int relaId;
	private String name;
	private String fileSize;
	private String filePath;
	private String md5;
	private String content;
	private byte fileType;
	private byte fileRelaType;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--字符串表示--*/
	private String statusStr;
	private String fileTypeStr ; 
	private String fileRelaTypeStr ; 
	
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		ASysFileEnum[] enums = ASysFileEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ASysFileEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		ASysFileEnum[] enums = ASysFileEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ASysFileEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getFileTypeStr()
	{
		// 根据状态值获取字符串描述
		ASysFileEnum[] enums = ASysFileEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ASysFileEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("FILETYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getFileType())
				{
					this.fileTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return fileTypeStr;
	}

	public String getFileRelaTypeStr()
	{
		// 根据状态值获取字符串描述
		ASysFileEnum[] enums = ASysFileEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ASysFileEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("FILERELATYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getFileRelaType())
				{
					this.fileRelaTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return fileRelaTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getRelaId()
	{
		return relaId;
	}

	public void setRelaId(int relaId)
	{
		this.relaId = relaId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getFileSize()
	{
		return fileSize;
	}

	public void setFileSize(String fileSize)
	{
		this.fileSize = fileSize;
	}

	public String getFilePath()
	{
		return filePath;
	}

	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
	}

	public String getMd5()
	{
		return md5;
	}

	public void setMd5(String md5)
	{
		this.md5 = md5;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getFileType()
	{
		return fileType;
	}

	public void setFileType(byte fileType)
	{
		this.fileType = fileType;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public byte getFileRelaType()
	{
		return fileRelaType;
	}

	public void setFileRelaType(byte fileRelaType)
	{
		this.fileRelaType = fileRelaType;
	}
}

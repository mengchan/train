package com.wangsh.train.outer;

import com.alibaba.fastjson.JSONObject;

/**
 * 当前服务请求外部的服务(当前服务是客户端)
 * @author TeaBig
 */
public interface IClientOuterService
{
	/**
	 * 获取地区列表;
	 * parentId:为0;查询第一级
	 * @return
	 */
	JSONObject regionListService(JSONObject dataJSON);
	
	/**
	 * 获取地区详情
	 * 	传入相应的id:
	 * @return
	 */
	JSONObject regionOneService(JSONObject dataJSON);
	
	/**
	 * 获取地区列表,
	 * 由于历史原因,此方法应该叫做地区列表才对;
	 * regionList
	 * @param json
	 * @return
	 */
	JSONObject regionBatchOneService(JSONObject dataJSON);
	
	/**
	 * 查询多条分类信息(查询多条)
	 * @param dataJSON
	 * @return
	 */
	JSONObject cateListService(JSONObject dataJSON);
	
	/**
	 * 获取分类详情,(一条)
	 * 	传入相应的id:
	 * @return
	 */
	JSONObject cateOneService(JSONObject dataJSON);
	
	/**
	 * 批量查询(查询单条)
	 * @param dataJSON
	 * @return
	 */
	JSONObject cateBatchOneService(JSONObject dataJSON);
	
	/**
	 * 验证管理员Token
	 * @param dataJSON
	 * @return
	 */
	JSONObject verifyAdminsTokenService(JSONObject dataJSON);
	
	/**
	 * 验证用户Token
	 * @param dataJSON
	 * @return
	 */
	JSONObject verifyUsersTokenService(JSONObject dataJSON);
	
	/**
	 * 获取用户信息(用户公钥)
	 * @param dataJSON
	 * @return
	 */
	JSONObject usersInfoService(JSONObject dataJSON);
	
	/**
	 * 请求自己服务,获取当前时间戳
	 * @param dataJSON
	 * @return
	 */
	JSONObject selfCurrentDateService(JSONObject dataJSON);
	
	/**
	 * 查询当前的服务
	 * @param dataJSON
	 * @return
	 */
	JSONObject selfFindOneAskService(JSONObject dataJSON);
}

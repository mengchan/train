package com.wangsh.train.users.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;

/**
 * 管理员的Pojo
 * 
 * @author TeaBig
 */
public class AAdmins extends BasePojo<AAdmins>
{
	private int id;
	private int ssoId;
	private String email;
	private String name ; 
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date lastLoginTime;

	/*--字符串表示--*/
	private String statusStr;
	private String tokenStr ; 
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AAdminsEnum[] enums = AAdminsEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAdminsEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AAdminsEnum[] enums = AAdminsEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AAdminsEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getSsoId()
	{
		return ssoId;
	}

	public void setSsoId(int ssoId)
	{
		this.ssoId = ssoId;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getLastLoginTime()
	{
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime)
	{
		this.lastLoginTime = lastLoginTime;
	}

	public String getTokenStr()
	{
		return tokenStr;
	}

	public void setTokenStr(String tokenStr)
	{
		this.tokenStr = tokenStr;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}

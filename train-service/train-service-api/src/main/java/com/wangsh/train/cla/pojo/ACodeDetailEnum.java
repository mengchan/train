package com.wangsh.train.cla.pojo;

/**
 * 	管理员的枚举
 * @author wangsh
 *
 */
public enum ACodeDetailEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	/* 文件的类型 */
	FILETYPE_JAVA(Byte.valueOf("0"), "java"), 
	FILETYPE_SCALA(Byte.valueOf("1"), "scala"),
	FILETYPE_PYTHON(Byte.valueOf("2"), "python"),
	
	EQUALSFLAG_TRUE(Byte.valueOf("0"), "相等"),
	EQUALSFLAG_FALSE(Byte.valueOf("1"), "不相等"),
	;
	
	private byte status;
	private String name;

	private ACodeDetailEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

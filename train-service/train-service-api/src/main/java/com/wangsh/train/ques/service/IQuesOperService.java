package com.wangsh.train.ques.service;

import java.util.Map;

import com.wangsh.train.common.pojo.ApiResponse;

/**
 * Service接口(试题代码)
 * 此接口中只存储数据库表的CRUD
 * @author Wangsh
 */
public interface IQuesOperService
{
	/**
	 * 批量更新问题
	 * 	上传xlsx,批量生成数据;
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> insertBatchAskService(Map<String, Object> condMap);
}

package com.wangsh.train.users.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.common.pojo.BasePojo;

/**
 * 管理员的Pojo
 * 
 * @author TeaBig
 */
public class AUsers extends BasePojo<AUsers>
{
	private int id;
	private int ssoId;
	private int classId;
	private int usersClassId ; 
	private String email;
	private String nickName;
	private String trueName ; 
	private String content;
	private byte stageType ;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date lastLoginTime   ;
	
	/*--关联对象表示--*/
	private AClass cla ;

	/*--字符串表示--*/
	private String statusStr;
	private String stageTypeStr;
	private String tokenStr ; 
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AUsersEnum[] enums = AUsersEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AUsersEnum[] enums = AUsersEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getStageTypeStr()
	{
		// 根据状态值获取字符串描述
		AUsersEnum[] enums = AUsersEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STAGETYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStageType())
				{
					this.stageTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return stageTypeStr;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getSsoId()
	{
		return ssoId;
	}

	public void setSsoId(int ssoId)
	{
		this.ssoId = ssoId;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getLastLoginTime()
	{
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime)
	{
		this.lastLoginTime = lastLoginTime;
	}

	public String getTokenStr()
	{
		return tokenStr;
	}

	public void setTokenStr(String tokenStr)
	{
		this.tokenStr = tokenStr;
	}

	public int getClassId()
	{
		return classId;
	}

	public void setClassId(int classId)
	{
		this.classId = classId;
	}

	public AClass getCla()
	{
		return cla;
	}

	public void setCla(AClass cla)
	{
		this.cla = cla;
	}

	public byte getStageType()
	{
		return stageType;
	}

	public void setStageType(byte stageType)
	{
		this.stageType = stageType;
	}

	public String getNickName()
	{
		return nickName;
	}

	public void setNickName(String nickName)
	{
		this.nickName = nickName;
	}

	public String getTrueName()
	{
		return trueName;
	}

	public void setTrueName(String trueName)
	{
		this.trueName = trueName;
	}

	public int getUsersClassId()
	{
		return usersClassId;
	}

	public void setUsersClassId(int usersClassId)
	{
		this.usersClassId = usersClassId;
	}

}

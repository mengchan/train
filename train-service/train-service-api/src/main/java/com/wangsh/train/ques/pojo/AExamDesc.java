package com.wangsh.train.ques.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.users.pojo.AAdmins;

/**
 * 示例代码中的问题表
 * ~类名为表名,去掉下划线;
 * ~下划线隔开的单词要采用驼峰标识
 * @author Wangsh
 */
public class AExamDesc extends BasePojo<AExamDesc>
{
	private int id;
	private int createId ; 
	private int classId ; 
	private String name;
	private byte hardStar;
	private Date stTime;
	private Date edTime;
	private int score;
	private byte examType ; 
	private String examTech ; 
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--字符串表示--*/
	private String statusStr ; 
	private byte souStatus ; 
	private String examTypeStr ;
	/*--关联对象--*/
	private AAdmins admins ; 
	private AClass cla ; 
	
	/* 单选题结果 */
	private List<AAsk> selResultList = new ArrayList<AAsk>();
	/* 多选题结果 */
	private List<AAsk> multiSelResultList = new ArrayList<AAsk>();
	/* 问答题结果 */
	private List<AAsk> askResultList = new ArrayList<AAsk>();
	/* 判断题结果 */
	private List<AAsk> judeResultList = new ArrayList<AAsk>();
	/* 编程题结果 */
	private List<AAsk> proResultList = new ArrayList<AAsk>();
	/* 面试题结果 */
	private List<AAsk> interResultList = new ArrayList<AAsk>();
	
	/* 单选题结果 */
	private List<AExamDetail> selDetailList = new ArrayList<AExamDetail>();
	/* 多选题结果 */
	private List<AExamDetail> multiSelDetailList = new ArrayList<AExamDetail>();
	/* 问答题结果 */
	private List<AExamDetail> askDetailList = new ArrayList<AExamDetail>();
	/* 判断题结果 */
	private List<AExamDetail> judeDetailList = new ArrayList<AExamDetail>();
	/* 编程题结果 */
	private List<AExamDetail> proDetailList = new ArrayList<AExamDetail>();
	/* 面试题结果 */
	private List<AExamDetail> interDetailList = new ArrayList<AExamDetail>();
	
	/* 考试的题类型
	 * 数据格式:
	 * {
	 * 	"1":
	 * 	{
	 *      技术id
	 * 		"id":1,
	 *      数量
	 * 		"num":"aa"
	 * 	}
	 * }
	 *  */
	private JSONObject examTechJSON = new JSONObject();
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_ENABLE-1,值:启用
	 */
	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AExamDescEnum[] enums = AExamDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDescEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_DRAFT,值:启用
	 */
	public Map<String, String> getEnums()
	{
		// 根据状态值获取字符串描述
		AExamDescEnum[] enums = AExamDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDescEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(enumTemp.toString(), enumTemp.getStatus() + "");
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		AExamDescEnum[] enums = AExamDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDescEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AExamDescEnum[] enums = AExamDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDescEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getExamTypeStr()
	{
		// 根据状态值获取字符串描述
		AExamDescEnum[] enums = AExamDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamDescEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("EXAMTYPE_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getExamType())
				{
					this.examTypeStr = enumTemp.getName();
					break;
				}
			}
		}
		return examTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getCreateId()
	{
		return createId;
	}

	public void setCreateId(int createId)
	{
		this.createId = createId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public byte getHardStar()
	{
		return hardStar;
	}

	public void setHardStar(byte hardStar)
	{
		this.hardStar = hardStar;
	}

	public Date getStTime()
	{
		return stTime;
	}

	public void setStTime(Date stTime)
	{
		this.stTime = stTime;
	}

	public Date getEdTime()
	{
		return edTime;
	}

	public void setEdTime(Date edTime)
	{
		this.edTime = edTime;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public byte getSouStatus()
	{
		return souStatus;
	}

	public void setSouStatus(byte souStatus)
	{
		this.souStatus = souStatus;
	}

	public AAdmins getAdmins()
	{
		return admins;
	}

	public void setAdmins(AAdmins admins)
	{
		this.admins = admins;
	}

	public byte getExamType()
	{
		return examType;
	}

	public void setExamType(byte examType)
	{
		this.examType = examType;
	}

	public String getExamTech()
	{
		return examTech;
	}

	public void setExamTech(String examTech)
	{
		this.examTech = examTech;
	}

	public JSONObject getExamTechJSON()
	{
		try
		{
			this.examTechJSON = JSON.parseObject(this.examTech);
		} catch (Exception e)
		{
			this.examTechJSON = new JSONObject();
		}
		return examTechJSON;
	}

	public void setExamTechJSON(JSONObject examTechJSON)
	{
		this.examTechJSON = examTechJSON;
	}

	public List<AAsk> getSelResultList()
	{
		return selResultList;
	}

	public void setSelResultList(List<AAsk> selResultList)
	{
		this.selResultList = selResultList;
	}

	public List<AAsk> getMultiSelResultList()
	{
		return multiSelResultList;
	}

	public void setMultiSelResultList(List<AAsk> multiSelResultList)
	{
		this.multiSelResultList = multiSelResultList;
	}

	public List<AAsk> getAskResultList()
	{
		return askResultList;
	}

	public void setAskResultList(List<AAsk> askResultList)
	{
		this.askResultList = askResultList;
	}

	public List<AAsk> getJudeResultList()
	{
		return judeResultList;
	}

	public void setJudeResultList(List<AAsk> judeResultList)
	{
		this.judeResultList = judeResultList;
	}

	public List<AAsk> getProResultList()
	{
		return proResultList;
	}

	public void setProResultList(List<AAsk> proResultList)
	{
		this.proResultList = proResultList;
	}

	public List<AAsk> getInterResultList()
	{
		return interResultList;
	}

	public void setInterResultList(List<AAsk> interResultList)
	{
		this.interResultList = interResultList;
	}

	public List<AExamDetail> getSelDetailList()
	{
		return selDetailList;
	}

	public void setSelDetailList(List<AExamDetail> selDetailList)
	{
		this.selDetailList = selDetailList;
	}

	public List<AExamDetail> getMultiSelDetailList()
	{
		return multiSelDetailList;
	}

	public void setMultiSelDetailList(List<AExamDetail> multiSelDetailList)
	{
		this.multiSelDetailList = multiSelDetailList;
	}

	public List<AExamDetail> getAskDetailList()
	{
		return askDetailList;
	}

	public void setAskDetailList(List<AExamDetail> askDetailList)
	{
		this.askDetailList = askDetailList;
	}

	public List<AExamDetail> getJudeDetailList()
	{
		return judeDetailList;
	}

	public void setJudeDetailList(List<AExamDetail> judeDetailList)
	{
		this.judeDetailList = judeDetailList;
	}

	public List<AExamDetail> getProDetailList()
	{
		return proDetailList;
	}

	public void setProDetailList(List<AExamDetail> proDetailList)
	{
		this.proDetailList = proDetailList;
	}

	public List<AExamDetail> getInterDetailList()
	{
		return interDetailList;
	}

	public void setInterDetailList(List<AExamDetail> interDetailList)
	{
		this.interDetailList = interDetailList;
	}

	public void setEnumsMap(Map<String, String> enumsMap)
	{
		this.enumsMap = enumsMap;
	}

	public int getClassId()
	{
		return classId;
	}

	public void setClassId(int classId)
	{
		this.classId = classId;
	}

	public AClass getCla()
	{
		return cla;
	}

	public void setCla(AClass cla)
	{
		this.cla = cla;
	}
}

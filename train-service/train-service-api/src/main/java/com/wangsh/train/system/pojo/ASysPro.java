package com.wangsh.train.system.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.wangsh.train.common.pojo.BasePojo;

/**
 * 系统配置表
 * 
 * @王晨曦
 */
public class ASysPro extends BasePojo<ASysPro>
{

	private int id;
	private String name;
	private String engName;
	private String vals;
	private String content;
	private byte operType;
	private byte proType;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*
	 * 枚举
	 */
	private String operTypeStr;
	private String proTypeStr;
	private String statusStr;

	/* JSON类型 */
	private JSONArray valsJSON = new JSONArray();

	/*
	 * 方便枚举项在网页上显示出来 键为值(数字), 值为字符串描述 只提供get方法 ,enumsMap将前台所有枚举放到这个map中.前台以value值区分
	 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		ASysProEnum[] sysProEnums = ASysProEnum.values();
		for (int i = 0; i < sysProEnums.length; i++)
		{
			ASysProEnum sysProEnum = sysProEnums[i];
			String key = sysProEnum.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + sysProEnum.getStatus() + "", sysProEnum.getName());
		}
		return enumsMap;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getVals()
	{
		return vals;
	}

	public void setVals(String vals)
	{
		this.vals = vals;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public String getStatusStr()
	{
		ASysProEnum[] sysProEnums = ASysProEnum.values();
		for (int i = 0; i < sysProEnums.length; i++)
		{
			ASysProEnum sysProEnum = sysProEnums[i];
			if (sysProEnum.toString().startsWith("STATUS_"))
			{
				if (sysProEnum.getStatus() == this.getStatus())
				{

					this.statusStr = sysProEnum.getName();
				}
			}

		}
		return statusStr;
	}

	public byte getOperType()
	{
		return operType;
	}

	public byte getProType()
	{
		return proType;
	}

	public String getProTypeStr()
	{
		ASysProEnum[] sysProEnums = ASysProEnum.values();
		for (int i = 0; i < sysProEnums.length; i++)
		{
			ASysProEnum sysProEnum = sysProEnums[i];
			if (sysProEnum.toString().startsWith("PROTYPE_"))
			{
				if (sysProEnum.getStatus() == this.getProType())
				{

					this.proTypeStr = sysProEnum.getName();
				}
			}
		}

		return proTypeStr;
	}

	public String getOperTypeStr()
	{
		ASysProEnum[] sysProEnums = ASysProEnum.values();
		for (int i = 0; i < sysProEnums.length; i++)
		{
			ASysProEnum sysProEnum = sysProEnums[i];
			if (sysProEnum.toString().startsWith("OPERTYPE_"))
			{
				if (sysProEnum.getStatus() == this.getOperType())
				{

					this.operTypeStr = sysProEnum.getName();
				}
			}
		}
		return operTypeStr;
	}

	public void setOperType(byte operType)
	{
		this.operType = operType;
	}

	public void setProType(byte proType)
	{
		this.proType = proType;
	}

	public String getEngName()
	{
		return engName;
	}

	public void setEngName(String engName)
	{
		this.engName = engName;
	}

	public JSONArray getValsJSON()
	{

		try
		{
			/* 将字符串变成JSON对象 */
			this.valsJSON = (JSONArray) JSON.parse(vals);
		} catch (Exception e)
		{

		}
		return valsJSON;
	}

	@Override
	public String toString() {
		return "ASysPro [id=" + id + ", name=" + name + ", vals=" + vals + ", content=" + content + ", status=" + status
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + ", pubTime=" + pubTime + ", engName="
				+ engName + ", operType=" + operType + ", proType=" + proType + ", operTypeStr=" + operTypeStr
				+ ", proTypeStr=" + proTypeStr + ", statusStr=" + statusStr + ", valsJSON=" + valsJSON + ", enumsMap="
				+ enumsMap + "]";
	}
	
	
}

package com.wangsh.train.system.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.common.util.DateFormatUtil;
import com.wangsh.train.users.pojo.AAdmins;

/**
 * 示例代码中的技术表
 * ~类名为表名,去掉下划线;
 * ~下划线隔开的单词要采用驼峰标识
 * @author Wangsh
 */
public class ASysTech extends BasePojo<ASysTech>
{
	private int id;
	private int createId;
	private int cateId;
	private String name;
	private String website;
	private long clasTime ; 
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--字符串表示--*/
	private String statusStr ; 
	private String clasTimeStr ; 
	/*--关联对象--*/
	private AAdmins admins ; 
	private JSONObject cateJSON ; 
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_ENABLE-1,值:启用
	 */
	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		ASysTechEnum[] enums = ASysTechEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ASysTechEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		ASysTechEnum[] enums = ASysTechEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ASysTechEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		ASysTechEnum[] enums = ASysTechEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ASysTechEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	/**
	 * 时间倒计时显示
	 * @return
	 */
	public String getClasTimeStr()
	{
		DateFormatUtil dateFormatUtil = new DateFormatUtil();
		/* 日期时间格式转换;clasTime:单位是分钟 */
		this.clasTimeStr = dateFormatUtil.formatTime(this.clasTime * 60 * 1000);
		return clasTimeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getCreateId()
	{
		return createId;
	}

	public void setCreateId(int createId)
	{
		this.createId = createId;
	}

	public int getCateId()
	{
		return cateId;
	}

	public void setCateId(int cateId)
	{
		this.cateId = cateId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getWebsite()
	{
		return website;
	}

	public void setWebsite(String website)
	{
		this.website = website;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public AAdmins getAdmins()
	{
		return admins;
	}

	public void setAdmins(AAdmins admins)
	{
		this.admins = admins;
	}

	public JSONObject getCateJSON()
	{
		return cateJSON;
	}

	public void setCateJSON(JSONObject cateJSON)
	{
		this.cateJSON = cateJSON;
	}

	public long getClasTime()
	{
		return clasTime;
	}

	public void setClasTime(long clasTime)
	{
		this.clasTime = clasTime;
	}
}

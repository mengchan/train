package com.wangsh.train.job.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.users.pojo.AUsers;

/**
 * 示例代码中的公司offer表 ~类名为表名,去掉下划线; ~下划线隔开的单词要采用驼峰标识
 * 
 * @author Wangsh
 */
public class AJobResume extends BasePojo<AJobResume>
{
	private int id;
	private int createId ; 
	private String name;
	private Date makeTime;
	private Date trueTime;
	private byte onTime;
	private int upNum;
	private int downNum;
	private byte finish ; 
	private String auditContent;
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	private Date auditTime ; 

	/*--字符串表示--*/
	private String statusStr;
	private byte souStatus ; 
	private String onTimeStr ; 
	private String finishStr ; 
	/*--对象表示--*/
	private AUsers create ; 
	
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AJobResumeEnum[] enums = AJobResumeEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobResumeEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_DRAFT,值:启用
	 */
	public Map<String, String> getEnums()
	{
		// 根据状态值获取字符串描述
		AJobResumeEnum[] enums = AJobResumeEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobResumeEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(enumTemp.toString(), enumTemp.getStatus() + "");
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		AJobResumeEnum[] enums = AJobResumeEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobResumeEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AJobResumeEnum[] enums = AJobResumeEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobResumeEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getOnTimeStr()
	{
		// 根据状态值获取字符串描述
		AJobResumeEnum[] enums = AJobResumeEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobResumeEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("ONTIME_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getOnTime())
				{
					this.onTimeStr = enumTemp.getName();
					break;
				}
			}
		}
		return onTimeStr;
	}
	
	public String getFinishStr()
	{
		// 根据状态值获取字符串描述
		AJobResumeEnum[] enums = AJobResumeEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobResumeEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("FINISH_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getFinish())
				{
					this.finishStr = enumTemp.getName();
					break;
				}
			}
		}
		return finishStr;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAuditContent()
	{
		return auditContent;
	}

	public void setAuditContent(String auditContent)
	{
		this.auditContent = auditContent;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public int getCreateId()
	{
		return createId;
	}

	public void setCreateId(int createId)
	{
		this.createId = createId;
	}

	public AUsers getCreate()
	{
		return create;
	}

	public void setCreate(AUsers create)
	{
		this.create = create;
	}

	public Date getAuditTime()
	{
		return auditTime;
	}

	public void setAuditTime(Date auditTime)
	{
		this.auditTime = auditTime;
	}

	public byte getSouStatus()
	{
		return souStatus;
	}

	public void setSouStatus(byte souStatus)
	{
		this.souStatus = souStatus;
	}

	public Date getMakeTime()
	{
		return makeTime;
	}

	public void setMakeTime(Date makeTime)
	{
		this.makeTime = makeTime;
	}

	public Date getTrueTime()
	{
		return trueTime;
	}

	public void setTrueTime(Date trueTime)
	{
		this.trueTime = trueTime;
	}

	public byte getOnTime()
	{
		return onTime;
	}

	public void setOnTime(byte onTime)
	{
		this.onTime = onTime;
	}

	public byte getFinish()
	{
		return finish;
	}

	public void setFinish(byte finish)
	{
		this.finish = finish;
	}

	public int getUpNum()
	{
		return upNum;
	}

	public void setUpNum(int upNum)
	{
		this.upNum = upNum;
	}

	public int getDownNum()
	{
		return downNum;
	}

	public void setDownNum(int downNum)
	{
		this.downNum = downNum;
	}
}

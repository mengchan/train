package com.wangsh.train.cla.service;

import java.util.Map;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.cla.pojo.AClassData;
import com.wangsh.train.cla.pojo.ACodeDetail;
import com.wangsh.train.cla.pojo.ACodeStat;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.util.PageInfoUtil;

/**
 * 班级的Service
 * 
 * @author TeaBig
 */
public interface IClassDbService
{
	/*----- 班级模块管理开始 -----*/
	/**
	 * 添加一条班级
	 * 
	 * @param clas
	 * @return
	 */
	ApiResponse<Object> saveOneClassService(AClass clas);

	/**
	 * 更新一条班级
	 * 
	 * @param clas
	 * @return
	 */
	ApiResponse<Object> updateOneClassService(AClass clas);

	/**
	 * 删除一条班级
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> deleteOneClassService(Map<String, Object> condMap);

	/**
	 * 查询多条班级
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClass> findCondListClassService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条班级
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClass> findOneClassService(Map<String, Object> condMap);
	/*----- 班级模块管理结束 -----*/
	
	/*----- 代码统计模块管理开始 -----*/
	/**
	 * 添加一条代码统计
	 * 
	 * @param CodeStat
	 * @return
	 */
	ApiResponse<Object> saveOneCodeStatService(ACodeStat CodeStat);

	/**
	 * 更新一条代码统计
	 * 
	 * @param CodeStat
	 * @return
	 */
	ApiResponse<Object> updateOneCodeStatService(ACodeStat CodeStat);
	
	/**
	 * 更新一条代码统计
	 * 
	 * @param CodeStat
	 * @return
	 */
	ApiResponse<Object> updateOneCodeStatService(ACodeStat CodeStat,Map<String, Object> paramsMap) throws Exception;

	/**
	 * 删除一条代码统计
	 * 
	 * @param CodeStat
	 * @return
	 */
	ApiResponse<Object> deleteOneCodeStatService(Map<String, Object> condMap);

	/**
	 * 查询多条代码统计
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ACodeStat> findCondListCodeStatService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条代码统计
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ACodeStat> findOneCodeStatService(Map<String, Object> condMap);
	/*----- 代码统计模块管理结束 -----*/
	/*----- 代码明细模块管理开始 -----*/
	/**
	 * 添加一条代码明细
	 * 
	 * @param CodeDetail
	 * @return
	 */
	ApiResponse<Object> saveOneCodeDetailService(ACodeDetail CodeDetail);

	/**
	 * 更新一条代码明细
	 * 
	 * @param CodeDetail
	 * @return
	 */
	ApiResponse<Object> updateOneCodeDetailService(ACodeDetail CodeDetail);

	/**
	 * 删除一条代码明细
	 * 
	 * @param CodeDetail
	 * @return
	 */
	ApiResponse<Object> deleteOneCodeDetailService(Map<String, Object> condMap);

	/**
	 * 查询多条代码明细
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ACodeDetail> findCondListCodeDetailService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条代码明细
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<ACodeDetail> findOneCodeDetailService(Map<String, Object> condMap);
	/*----- 代码明细模块管理结束 -----*/
	
	/*----- 课程资料模块管理开始 -----*/
	/**
	 * 添加一条课程资料
	 * 
	 * @param demoData
	 * @return
	 */
	ApiResponse<Object> saveOneDataService(AClassData data);

	/**
	 * 更新一条课程资料
	 * 
	 * @param demoData
	 * @return
	 */
	ApiResponse<Object> updateOneDataService(AClassData data);

	/**
	 * 删除一条课程资料
	 * 
	 * @param demoData
	 * @return
	 */
	ApiResponse<Object> deleteOneDataService(Map<String, Object> condMap);

	/**
	 * 查询多条课程资料
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClassData> findCondListDataService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条课程资料
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClassData> findOneDataService(Map<String, Object> condMap);
	
	/**
	 * 统计课程资料
	 * @return
	 */
	ApiResponse<Map<String, Object>> statDataService(Map<String, Object> condMap);
	/*----- 课程资料模块管理结束 -----*/
}

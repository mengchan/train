package com.wangsh.train.ques.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.users.pojo.AAdmins;
import com.wangsh.train.users.pojo.AUsers;
import com.wangsh.train.users.pojo.AUsersClass;

/**
 * AExamScoreDesc(pojo)
 * @author Wangsh
 */
public class AExamScoreDesc extends BasePojo<AExamScoreDesc>
{
	private int id;
	private int usersId;
	private int usersClaId;
	private int classId ; 
	private int adminsId ; 
	private int examId;
	private String name;
	private int score;
	private Date commitTime;
	private String selfAsse;
	private int violCount ; 
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	
	/*--字符串表示--*/
	private String statusStr ; 
	private byte souStatus ; 
	/*--关联对象--*/
	private AUsers users ; 
	private AUsersClass usersClass ; 
	private AExamDesc examDesc ; 
	private AAdmins admins ;
	private AClass cla ; 
	
	private List<AExamScoreDetail> examScoreDetailList = new ArrayList<AExamScoreDetail>();
	
	/* 单选题结果 */
	private List<AExamScoreDetail> selDetailList = new ArrayList<AExamScoreDetail>();
	/* 多选题结果 */
	private List<AExamScoreDetail> multiSelDetailList = new ArrayList<AExamScoreDetail>();
	/* 问答题结果 */
	private List<AExamScoreDetail> askDetailList = new ArrayList<AExamScoreDetail>();
	/* 判断题结果 */
	private List<AExamScoreDetail> judeDetailList = new ArrayList<AExamScoreDetail>();
	/* 编程题结果 */
	private List<AExamScoreDetail> proDetailList = new ArrayList<AExamScoreDetail>();
	/* 面试题结果 */
	private List<AExamScoreDetail> interDetailList = new ArrayList<AExamScoreDetail>();
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_ENABLE-1,值:启用
	 */
	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AExamScoreDescEnum[] enums = AExamScoreDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamScoreDescEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_DRAFT,值:启用
	 */
	public Map<String, String> getEnums()
	{
		// 根据状态值获取字符串描述
		AExamScoreDescEnum[] enums = AExamScoreDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamScoreDescEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(enumTemp.toString(), enumTemp.getStatus() + "");
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		AExamScoreDescEnum[] enums = AExamScoreDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamScoreDescEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AExamScoreDescEnum[] enums = AExamScoreDescEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AExamScoreDescEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersId()
	{
		return usersId;
	}

	public void setUsersId(int usersId)
	{
		this.usersId = usersId;
	}

	public int getUsersClaId()
	{
		return usersClaId;
	}

	public void setUsersClaId(int usersClaId)
	{
		this.usersClaId = usersClaId;
	}

	public int getExamId()
	{
		return examId;
	}

	public void setExamId(int examId)
	{
		this.examId = examId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}

	public Date getCommitTime()
	{
		return commitTime;
	}

	public void setCommitTime(Date commitTime)
	{
		this.commitTime = commitTime;
	}

	public String getSelfAsse()
	{
		return selfAsse;
	}

	public void setSelfAsse(String selfAsse)
	{
		this.selfAsse = selfAsse;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public byte getSouStatus()
	{
		return souStatus;
	}

	public AUsers getUsers()
	{
		return users;
	}

	public void setUsers(AUsers users)
	{
		this.users = users;
	}

	public AUsersClass getUsersClass()
	{
		return usersClass;
	}

	public void setUsersClass(AUsersClass usersClass)
	{
		this.usersClass = usersClass;
	}

	public AExamDesc getExamDesc()
	{
		return examDesc;
	}

	public void setExamDesc(AExamDesc examDesc)
	{
		this.examDesc = examDesc;
	}

	public List<AExamScoreDetail> getExamScoreDetailList()
	{
		return examScoreDetailList;
	}

	public void setExamScoreDetailList(List<AExamScoreDetail> examScoreDetailList)
	{
		this.examScoreDetailList = examScoreDetailList;
	}

	public List<AExamScoreDetail> getSelDetailList()
	{
		return selDetailList;
	}

	public void setSelDetailList(List<AExamScoreDetail> selDetailList)
	{
		this.selDetailList = selDetailList;
	}

	public List<AExamScoreDetail> getMultiSelDetailList()
	{
		return multiSelDetailList;
	}

	public void setMultiSelDetailList(List<AExamScoreDetail> multiSelDetailList)
	{
		this.multiSelDetailList = multiSelDetailList;
	}

	public List<AExamScoreDetail> getAskDetailList()
	{
		return askDetailList;
	}

	public void setAskDetailList(List<AExamScoreDetail> askDetailList)
	{
		this.askDetailList = askDetailList;
	}

	public List<AExamScoreDetail> getJudeDetailList()
	{
		return judeDetailList;
	}

	public void setJudeDetailList(List<AExamScoreDetail> judeDetailList)
	{
		this.judeDetailList = judeDetailList;
	}

	public List<AExamScoreDetail> getProDetailList()
	{
		return proDetailList;
	}

	public void setProDetailList(List<AExamScoreDetail> proDetailList)
	{
		this.proDetailList = proDetailList;
	}

	public List<AExamScoreDetail> getInterDetailList()
	{
		return interDetailList;
	}

	public void setInterDetailList(List<AExamScoreDetail> interDetailList)
	{
		this.interDetailList = interDetailList;
	}

	public int getAdminsId()
	{
		return adminsId;
	}

	public void setAdminsId(int adminsId)
	{
		this.adminsId = adminsId;
	}

	public AAdmins getAdmins()
	{
		return admins;
	}

	public void setAdmins(AAdmins admins)
	{
		this.admins = admins;
	}

	public int getClassId()
	{
		return classId;
	}

	public void setClassId(int classId)
	{
		this.classId = classId;
	}

	public AClass getCla()
	{
		return cla;
	}

	public void setCla(AClass cla)
	{
		this.cla = cla;
	}

	public int getViolCount()
	{
		return violCount;
	}

	public void setViolCount(int violCount)
	{
		this.violCount = violCount;
	}
}

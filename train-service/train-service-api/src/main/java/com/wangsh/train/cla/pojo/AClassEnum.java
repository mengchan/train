package com.wangsh.train.cla.pojo;

/**
 * 	管理员的枚举
 * @author wangsh
 *
 */
public enum AClassEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	/* 班级的状态 */
	ClASTATUS_UNOPEN(Byte.valueOf("0"), "未开班"), 
	ClASTATUS_ING(Byte.valueOf("1"), "进行中"),
	ClASTATUS_END(Byte.valueOf("2"), "结束");
	
	private byte status;
	private String name;

	private AClassEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

package com.wangsh.train.job.pojo;

/**
 * 	朝代的枚举
 * @author wangsh
 *
 */
public enum AJobResumeEnum
{
	/* 状态:0:草稿,1:发布;2:审核通过,3:审核不通过 */
	STATUS_DRAFT(Byte.valueOf("0"), "草稿"), 
	STATUS_PUBLISH(Byte.valueOf("1"), "发布"),
	STATUS_AUDITYES(Byte.valueOf("2"), "审核通过"),
	STATUS_AUDITNO(Byte.valueOf("3"), "审核不通过"),
	
	/* 是否按时提交:0:否,1:是 */
	ONTIME_NO(Byte.valueOf("0"), "未按时"), 
	ONTIME_YES(Byte.valueOf("1"), "按时"),
	
	/* 是否是最终版本:0:否;1:是 */
	FINISH_NO(Byte.valueOf("0"), "否"), 
	FINISH_YES(Byte.valueOf("1"), "是"),
	
	;
	
	private byte status;
	private String name;

	private AJobResumeEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

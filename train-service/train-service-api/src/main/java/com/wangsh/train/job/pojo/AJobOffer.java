package com.wangsh.train.job.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.pojo.BasePojo;
import com.wangsh.train.users.pojo.AUsers;

/**
 * 示例代码中的公司offer表 ~类名为表名,去掉下划线; ~下划线隔开的单词要采用驼峰标识
 * 
 * @author Wangsh
 */
public class AJobOffer extends BasePojo<AJobOffer>
{
	private int id;
	private int createId ; 
	private int companyId;
	private int comPosiId ; 
	private String name;
	private int wages ;
	private byte fiveRisks ;
	private byte accFund ;
	private int interCount ;
	private String auditContent;
	private String content;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;
	private Date auditTime ; 

	/*--字符串表示--*/
	private String statusStr;
	private byte souStatus ; 
	private String fiveRisksStr ; 
	private String accFundStr ; 
	/*--对象表示--*/
	private AUsers create ; 
	private AJobCompany company ; 
	private AJobComPosition comPosition ; 
	/* 关联的对象 */
	private JSONObject regionJSON ; 
	
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AJobOfferEnum[] enums = AJobOfferEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobOfferEnum enumTemp = enums[i];
			String key = enumTemp.toString();
			if (key.lastIndexOf("_") != -1)
			{
				key = key.substring(0, key.lastIndexOf("_"));
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}
	
	/**
	 * 获取此表相关的所有枚举值
	 * @return	键为枚举的变量名+枚举的值; 值为枚举的名字
	 * 			如:key:STATUS_DRAFT,值:启用
	 */
	public Map<String, String> getEnums()
	{
		// 根据状态值获取字符串描述
		AJobOfferEnum[] enums = AJobOfferEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobOfferEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			enumsMap.put(enumTemp.toString(), enumTemp.getStatus() + "");
		}
		return enumsMap;
	}
	
	/**
	 * 获取状态的字符串描述
	 * @return	此状态对应的字符串描述
	 */
	public String getStatusEnum()
	{
		// 根据状态值获取字符串描述
		AJobOfferEnum[] enums = AJobOfferEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobOfferEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					return enumTemp.toString();
				}
			}
		}
		return null;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AJobOfferEnum[] enums = AJobOfferEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobOfferEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getStatus())
				{
					this.statusStr = enumTemp.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getFiveRisksStr()
	{
		// 根据状态值获取字符串描述
		AJobOfferEnum[] enums = AJobOfferEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobOfferEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("FIVERISKS_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getFiveRisks())
				{
					this.fiveRisksStr = enumTemp.getName();
					break;
				}
			}
		}
		return fiveRisksStr;
	}
	
	public String getAccFundStr()
	{
		// 根据状态值获取字符串描述
		AJobOfferEnum[] enums = AJobOfferEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AJobOfferEnum enumTemp = enums[i];
			if (enumTemp.toString().startsWith("ACCFUND_"))
			{
				// 表示是状态的标识
				if (enumTemp.getStatus() == this.getAccFund())
				{
					this.accFundStr = enumTemp.getName();
					break;
				}
			}
		}
		return accFundStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getCompanyId()
	{
		return companyId;
	}

	public void setCompanyId(int companyId)
	{
		this.companyId = companyId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getWages()
	{
		return wages;
	}

	public void setWages(int wages)
	{
		this.wages = wages;
	}

	public String getAuditContent()
	{
		return auditContent;
	}

	public void setAuditContent(String auditContent)
	{
		this.auditContent = auditContent;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public AJobCompany getCompany()
	{
		return company;
	}

	public void setCompany(AJobCompany company)
	{
		this.company = company;
	}

	public JSONObject getRegionJSON()
	{
		return regionJSON;
	}

	public void setRegionJSON(JSONObject regionJSON)
	{
		this.regionJSON = regionJSON;
	}

	public int getCreateId()
	{
		return createId;
	}

	public void setCreateId(int createId)
	{
		this.createId = createId;
	}

	public AUsers getCreate()
	{
		return create;
	}

	public void setCreate(AUsers create)
	{
		this.create = create;
	}

	public Date getAuditTime()
	{
		return auditTime;
	}

	public void setAuditTime(Date auditTime)
	{
		this.auditTime = auditTime;
	}

	public byte getSouStatus()
	{
		return souStatus;
	}

	public void setSouStatus(byte souStatus)
	{
		this.souStatus = souStatus;
	}

	public byte getFiveRisks()
	{
		return fiveRisks;
	}

	public void setFiveRisks(byte fiveRisks)
	{
		this.fiveRisks = fiveRisks;
	}

	public byte getAccFund()
	{
		return accFund;
	}

	public void setAccFund(byte accFund)
	{
		this.accFund = accFund;
	}

	public int getInterCount()
	{
		return interCount;
	}

	public void setInterCount(int interCount)
	{
		this.interCount = interCount;
	}

	public int getComPosiId()
	{
		return comPosiId;
	}

	public void setComPosiId(int comPosiId)
	{
		this.comPosiId = comPosiId;
	}

	public AJobComPosition getComPosition()
	{
		return comPosition;
	}

	public void setComPosition(AJobComPosition comPosition)
	{
		this.comPosition = comPosition;
	}
}

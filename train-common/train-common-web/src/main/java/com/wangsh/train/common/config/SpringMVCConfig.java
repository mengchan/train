package com.wangsh.train.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 对SpringMVC做的一些配置
 * @author Xh-Win10-Server
 */
@Configuration
public class SpringMVCConfig implements WebMvcConfigurer
{
	/**
	 * 为Controller设置一下后缀名
	 */
//	@Override
//	public void configurePathMatch(PathMatchConfigurer configurer)
//	{
//		configurer.setUseRegisteredSuffixPatternMatch(true);
//	}
	
	/**
	 * 设置首页
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry)
	{
		registry.addViewController("/a.htm").setViewName("redirect:/index.jsp");
	}
}

package com.wangsh.train.common.pojo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * 所有POJO的超类
 * @author wangsh
 */
public class BasePojo<T>
{
	/**
	 * 将对象转换为JSON
	 * @return
	 */
	public JSONObject toJSON()
	{
//		SerializeConfig config = new SerializeConfig() ;
//		config.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
//		return (JSONObject) JSON.toJSON(this,config);
		
		String jsonStr = JSON.toJSONString(this, SerializerFeature.WriteDateUseDateFormat) ; 
		return (JSONObject) JSON.parse(jsonStr);
	}
	/*
	public JSONObject toJSON()
	{
		JSONObject resultJSON = new JSONObject() ; 
		 获取当前对象的Class对象 
		Class cla = this.getClass() ; 
		 获取所有的属性 
		Field[] fields = cla.getDeclaredFields();
		for (int i = 0; i < fields.length; i++)
		{
			Field field = fields[i];
			try
			{
				String name = field.getName() ; 
				if(name.endsWith("Str") || name.endsWith("Object"))
				{
					continue ; 
				}
				
				 通过get方法取值 
				String methodName = "get" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
				Method method = cla.getMethod(methodName);
				Object value = method.invoke(this, null);
				 判断引用类型 
				if(value instanceof BasePojo)
				{
					BasePojo bp = (BasePojo) value ; 
					resultJSON.put(name, bp.toJSON());
					continue ; 
				}
				
				if(value == null)
				{
					value = "" ; 
				}
				resultJSON.put(name, value);
			} catch (Exception e)
			{
			}
		}
		return resultJSON ; 
	}*/
	
	/**
	 * 从json对象中解析对象,
	 *	 交给子类去实现
	 * @param souStr
	 * @return
	 */
	protected T parseJSON(JSONObject souJSON)
	{
		return null ; 
	}
}

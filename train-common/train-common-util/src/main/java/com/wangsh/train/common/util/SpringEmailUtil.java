package com.wangsh.train.common.util;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
/**
 * 使用Spring发邮件的例子
 * @author Wangsh
 */
@Component("springEmailUtil")
public class SpringEmailUtil   
{
	@Resource
	private JavaMailSender javaMailSender ; 
	private SimpleMailMessage simpleMailMessage ;
	@Value("${spring.mail.username}")
	private String  from ; 
	 
	@PostConstruct
	private void init()
	{
		this.simpleMailMessage = new SimpleMailMessage();
		 
	}
 
	 

  
	/**
	 * 发送普通的文本信息 jmail相当于服务器,发送的内容由自己来封装
	 * @param email
	 * @param subject
	 * @param text
	 * @return
	 */
	public boolean sendTextMail(String host,String username,String password,String email, String subject,
			String text) {
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			if(this.javaMailSender instanceof JavaMailSenderImpl)
			{
				JavaMailSenderImpl javaMailSenderImpl = (JavaMailSenderImpl) this.javaMailSender ; 
				javaMailSenderImpl.setHost(host);
				javaMailSenderImpl.setUsername(username);
				javaMailSenderImpl.setPassword(password);
			}
			try 
			{
				//建一个数据库 定义字段，然后查询数据库 获取到字段，将字段传入到方法中，进行调用然后进行发送邮件，发送时随机选择账号进行发送
				this.simpleMailMessage.setFrom(username);
				simpleMailMessage.setTo(email);
				simpleMailMessage.setSubject(subject);
				simpleMailMessage.setText(text);
				javaMailSender.send(simpleMailMessage);
				ConstatFinalUtil.SYS_LOGGER.info(i + ";====普通邮件=====" + email + "邮件发送成功了");
				return true ; 
			} catch (Exception e) {
				ConstatFinalUtil.SYS_LOGGER.error(i + ";=====" + email + "+ 邮件发送失败了,内容为:"+ 
						subject + "-->" + text , e);
			}
		}
		return false ; 
	}
	
	/**
	 * 重载
	 * 发送普通的文本信息 email相当于服务器,发送的内容由自己来封装
	 * @param email
	 * @param subject
	 * @param text
	 * @return
	 */
	public boolean sendTextMail(String email, String subject,
			String text) {
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			try 
			{
				this.simpleMailMessage.setFrom(this.from);
				simpleMailMessage.setTo(email);
				simpleMailMessage.setSubject(subject);
				simpleMailMessage.setText(text);
				javaMailSender.send(simpleMailMessage);
				ConstatFinalUtil.SYS_LOGGER.info(i + ";====普通邮件=====" + email + "邮件发送成功了");
				return true ; 
			} catch (Exception e) {
				ConstatFinalUtil.SYS_LOGGER.error(i + ";=====" + email + "+ 邮件发送失败了,内容为:"+ 
						subject + "-->" + text , e);
			}
		}
		return false ; 
	}
	
	/**
	 * 发送HTML格式的邮件
	 * @param email
	 * @param subject
	 * @param text
	 * @return
	 */
	public boolean sendHTMLMail(String host,String username,String password,String email,String subject ,
				String text)
	{
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			try
			{
				if(this.javaMailSender instanceof JavaMailSenderImpl)
				{
					JavaMailSenderImpl javaMailSenderImpl = (JavaMailSenderImpl) this.javaMailSender ; 
					javaMailSenderImpl.setHost(host);
					javaMailSenderImpl.setUsername(username);
					javaMailSenderImpl.setPassword(password);
				}
				
				MimeMessage message = javaMailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(message ,true, "UTF-8") ;
				helper.setFrom(username);
				helper.setTo(email);
				helper.setSubject(subject);
				helper.setText(text , true);
				javaMailSender.send(message);
				ConstatFinalUtil.SYS_LOGGER.info(i + ";" + email + "====网页邮件=====" + subject + "邮件发送成功了");
				return true ; 
			} catch (Exception e)
			{
				ConstatFinalUtil.SYS_LOGGER.error(i + ";" + email + "====网页邮件=====" + subject + ",邮件发送失败了,内容为:"+ 
						subject + "-->" + text , e);
			}
		}
		return false ; 
	}
	 
	/**
	 * 发送HTML格式的邮件
	 * @param email
	 * @param subject
	 * @param text
	 * @return
	 */
	public boolean sendHTMLMail(String email,String subject ,
				String text)
	{
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			try
			{
				 
				
				MimeMessage message = javaMailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(message ,true, "UTF-8") ;
				helper.setTo(email);
				helper.setFrom(this.from) ; 
				helper.setSubject(subject);
				helper.setText(text , true);
				javaMailSender.send(message);
				ConstatFinalUtil.SYS_LOGGER.info(i + ";" + email + "====网页邮件=====" + subject + "邮件发送成功了");
				return true ; 
			} catch (Exception e)
			{
				ConstatFinalUtil.SYS_LOGGER.error(i + ";" + email + "====网页邮件=====" + subject + ",邮件发送失败了,内容为:"+ 
						subject + "-->" + text , e);
			}
		}
		return false ; 
	}
	 
	
	/**
	 * 运行测试邮件的方法
	 * @param args
	 */
	public static void main(String[] args) {
		
		//ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:spring/java_applicationContext.xml");
		//SpringEmailUtil smail = (SpringEmailUtil) ctx.getBean("springEmailUtil");
		//boolean flag = smail.sendTextMail("wangshanhu186@126.com", "测试邮件", "<a href='http://www.baidu.com'>百度</a><b>你收到了吗?</b>");
		//boolean flag = smail.sendHTMLMail("956680416@qq.com", "测试邮件", "<a href='http://www.baidu.com'>百度</a><b>你收到了吗?</b>");
		//System.out.println(flag); 
		 
 
	}
	 
}

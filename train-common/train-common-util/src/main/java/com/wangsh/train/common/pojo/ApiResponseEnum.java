package com.wangsh.train.common.pojo;

/**
 * 枚举
 * @author wangsh
 */
public enum ApiResponseEnum
{
	STATUS_SUCCESS(Byte.valueOf("0"), "成功"),
	STATUS_SERVER_ERROR(Byte.valueOf("-1"), "系统报错"), 
	STATUS_FAILED(Byte.valueOf("1"), "失败"),
	
	/* 提示信息 */
	INFO_EXCEPTION_CANCEL(Byte.valueOf("-3"), "撤单失败"),
	INFO_EXCEPTION_BALANCE(Byte.valueOf("-2"), "账户余额问题,请查看"),
	INFO_EXCEPTION_SYSTEM(Byte.valueOf("-1"), "系统错误,${errinfo}"),
	INFO_USERNAME_EXISTS(Byte.valueOf("2"), "用户名已存在"),
	INFO_ACCOUNT_DISABLED(Byte.valueOf("3"), "账户禁用"),
	INFO_PASS_INCORRECT(Byte.valueOf("4"), "密码不正确"),
	INFO_EMAIL_NOT_EXISTS(Byte.valueOf("5"), "用户邮箱不存在"),
	INFO_VERIFY_CODE_INCORR(Byte.valueOf("6"), "验证码不正确"),
	INFO_LOGIN_ILLEGAL(Byte.valueOf("7"), "非法访问,请先登陆"),
	INFO_UPLOAD_DATA_EMPTY(Byte.valueOf("9"), "请输入上行参数"),
	INFO_JSON_FORMAT_ERROR(Byte.valueOf("10"),"上传的json数据格式不正确,信息:${info}"),
	INFO_JSON_VERSION_ERROR(Byte.valueOf("11"),"版本号不正确"),
	INFO_JSON_TOKEN_ERROR(Byte.valueOf("12"),"token非法"),
	INFO_JSON_SERVER_ERROR(Byte.valueOf("13"),"系统跑偏了,抛异常了,${info}"),
	INFO_JSON_ATTACK(Byte.valueOf("14"),"恶意攻击"),
	INFO_EMAIL_NO_EMPTY(Byte.valueOf("16"),"邮箱不能为空"),
	INFO_EMAIL_PASSED(Byte.valueOf("18"), "邮箱已经认证"),
	INFO_EMAIL_VERIY_HREF_TIMEOUT(Byte.valueOf("19"), "邮箱链接已经超过一天"),
	INFO_EMAIL_VERIY_HREF_NOTSEC(Byte.valueOf("20"), "邮箱链接已经被篡改"),
	INFO_SOUPASS_INCORRECT(Byte.valueOf("21"), "原密码不正确"),
	INFO_REGISTER_SUCCESS(Byte.valueOf("23"), "注册成功,稍后到邮箱中验证邮箱"),
	INFO_EMAIL_VERIFY_SUCCESS(Byte.valueOf("24"), "邮箱验证成功"),
	INFO_EMAIL_SENDED(Byte.valueOf("25"), "邮箱验证成功"),
	INFO_LOGOUT(Byte.valueOf("26"), "退出成功"),
	INFO_METHOD_ERROR(Byte.valueOf("27"), "方法名不存在"),
	INFO_STATUS_ERROR(Byte.valueOf("28"), "状态不正确"),
	INFO_RECORD_EXISTS(Byte.valueOf("29"), "记录已经存在"),
	INFO_ERROR_FORMAT(Byte.valueOf("30"), "格式不正确"),
	INFO_AUTH_NO(Byte.valueOf("31"), "您木有权限查看"),
	INFO_LOGIN_SUCCES(Byte.valueOf("32"), "登陆成功"),
	INFO_LOGIN_FAILED(Byte.valueOf("33"), "登陆失败"),
	INFO_SOUPASS_SUCCESS(Byte.valueOf("34"), "您输入的是原密码"),
	INFO_COOKIE_SUCCESS(Byte.valueOf("35"), "Cookie正确"),
	INFO_COOKIE_FAILED(Byte.valueOf("36"), "Cookie错误"),
	INFO_THREAD_START(Byte.valueOf("37"), "多线程已经启动,请稍后查看"),
	INFO_UOLOADFILE_NULL(Byte.valueOf("38"), "请您选择要验证的文件之后再点击提交"),
	INFO_COOKIE_NO(Byte.valueOf("39"), "未获取到Cookie"),
	INFO_HISTORY_TYPE(Byte.valueOf("40"), "该操作已处理,请勿重复提交"),
	INFO_OPERATE_ERROR(Byte.valueOf("41"), "操作有误"),
	INFO_COND_SEARCH_BLANK(Byte.valueOf("42"), "搜索条件为空"),
	INFO_HISTORY_TYPE_CANCEL(Byte.valueOf("43"), "该操作已取消,请勿重复操作"),
	INFO_HISTORY_TYPE_SUCCESS(Byte.valueOf("45"), "操作成功，请等待管理员审核！"),
	INFO_USERS_EXTEND(Byte.valueOf("46"), "该用户的信息尚未审核通过！操作失败！"),
	INFO_ASSET_NUM(Byte.valueOf("47"), "余额不足，请先充值！"),
	INFO_NO_ACCEPT_ORDER(Byte.valueOf("48"), "当前订单未接单，不能点击确认！"),
	INFO_STATUS_MY_ERROR(Byte.valueOf("49"), "我方订单状态不正确"),
	INFO_STATUS_OTHER_ERROR(Byte.valueOf("50"), "对方订单状态不正确"),
	INFO_API_SIGN_ERROR(Byte.valueOf("51"), "api签名失败"),
	INFO_RECORD_NOT_EXISTS(Byte.valueOf("52"), "记录不存在"),
	INFO_RECORD_THIRDSN_EXISTS(Byte.valueOf("53"), "第三方流水号已经存在"),
	INFO_TOMANY_REQUEST(Byte.valueOf("54"), "当前操作人数较多，请稍后重试"),
	INFO_NUM_RANGE_ERROR(Byte.valueOf("55"), "订单数量超出限制;数量为{st}-->{ed}"),
	
	/* 所有和demoDynasty相关的变量 */
	NAME_LIST(Byte.valueOf("0"), "list"),
	NAME_ONE(Byte.valueOf("0"), "one") , 
	NAME_PAGEINFOUTIL(Byte.valueOf("0"), "pageInfoUtil");
	
	/* 响应码:在config.json中的info信息的键对应 */
	private byte status;
	/* 响应码:在config.json中的info信息的值对应 */
	private String name;

	private ApiResponseEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}

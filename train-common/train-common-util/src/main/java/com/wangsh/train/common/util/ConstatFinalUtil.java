package com.wangsh.train.common.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 全部是一些静态的常量
 * 
 * @author Wangsh
 */
public class ConstatFinalUtil
{
	/* 定义日志对象 */
	public static final Logger SYS_LOGGER = LogManager.getLogger() ; 
	
	/* 所有的字符串 */
	public static final String ALLSTR = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
	
	/* 日期的默认格式 */
	public static final String DATE_FORMAT = "yyyy-MM-dd" ; 
	/* 日期时间的默认格式 */
	public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss" ; 
	/* 字符串连接 */
	public static final String SPLIT_STR = "|-->" ;
	/* 拆分字符串 */
	public static final String SPLIT_STR_ZHUAN = "\\|-->" ;
	/* 秒 */
	public static final int SECOND = 1000 ;
	/* 编码 */
	public static final String CHARSET = "UTF-8";
	/* 请求次数 */
	public static final int REQ_COUNT = 5 ;
	/*请求服务器超时*/
	public static final int REQ_CONNECT_TIMEOUT = 5 * SECOND;
	/*读取超时*/
	public static final int READ_TIMEOUT = 5 * SECOND ;
	/* 网络失败次数 */
	public static int networkCount = 0 ;
	/* 请求头中忽略的key */
	public static final String REQ_HEADER_IGNORE = "noHeader_" ;
	
	/* 配置文件共享的变量 */
	public static Map<String, Object> SYS_PRO_MAP = new Hashtable<String, Object>();
	/* 精度的长度(小数点以后) */
	public static int PRECISION_LENGTH = 6 ; 
	
	/* 配置文件中的json对象 */
	public static JSONObject SYS_JSON = new JSONObject();
	/* 配置文件中的json对象,Config模块 */
	public static JSONObject CONFIG_JSON = new JSONObject();
	/* 配置文件中的json对象,info模块 */
	public static JSONObject INFO_JSON = new JSONObject();
	/* 配置文件中系统模块的全局配置 */
	public static JSONObject SYS_CONFIG_JSON = new JSONObject();
	/* facebook的公共请求头配置 */
	public static JSONObject FACEBOOK_HEADER_JSON = new JSONObject();
	/* 常见urls */
	public static JSONObject URLS_JSON = new JSONObject();
	/* 存储Token,管理员对应的信息 */
	public static Map<String, JSONObject> ADMINS_MAP = new HashMap<String,JSONObject>();
	/*存放所有线程的容器*/
	public static Map<String, Map<String, Object>> THREAD_MAP = new Hashtable<String, Map<String,Object>>();
	/* 设备判断android的标准 */
	public static List<String> ANDROID_LIST = Arrays.asList("android");
	/* 设备判断android的标准 */
	public static List<String> IPHONE_LIST = Arrays.asList("iphone");
	/* 手机设备标识 */
	public static String ANDROID = "android"; 
	public static String IPHONE = "iphone";
	public static String PC = "pc";
	
	/* 常见的请求头信息 */
	public static JSONArray HTTP_JSONARR = new JSONArray();
	
	/*
	 * 放一些初始化的操作 只执行一次
	 */
	static
	{
		/* FastJSON中默认的格式 */
		JSON.DEFFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss" ; 
		
		FileUtil fileUtil = new FileUtil();
		/* 从Classpath中获取 */
		String result = fileUtil.readFile(ConstatFinalUtil.class.getClassLoader().getResourceAsStream("config.json"));
		/* System.out.println("==ConstatFinalUtil=static==" + result); */
		/* 将字符串变成JSON对象 */
		/* 从Classpath中获取 */
		String http = fileUtil.readFile(ConstatFinalUtil.class.getClassLoader().getResourceAsStream("http.json"));
		/* 将字符串变成JSON对象 */
		try
		{
			SYS_JSON = (JSONObject) JSON.parse(result);
			CONFIG_JSON = SYS_JSON.getJSONObject("config");
			INFO_JSON = SYS_JSON.getJSONObject("info");
			SYS_CONFIG_JSON = CONFIG_JSON.getJSONObject("sysConfig");
			FACEBOOK_HEADER_JSON = SYS_JSON.getJSONObject("facebook_header");
			URLS_JSON = SYS_JSON.getJSONObject("urls");
			
			/* 常见的http请求头 */
			HTTP_JSONARR = (JSONArray) JSON.parse(http);
			if(HTTP_JSONARR == null)
			{
				HTTP_JSONARR = new JSONArray();
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("解析配置文件json,失败了;原文:{}",result);
		}
		
		//设置https协议访问
		//System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2,SSLv3");
	}
	
	/**
	 * 根据user-agent判断设备;
	 * @return 0:pc,1:android,2:ihpone
	 */
	public static String device(String userAgent)
	{
		if(userAgent == null)
		{
			userAgent = "" ; 
		}
		/* 先批量变成小写 */
		userAgent = userAgent.toLowerCase() ;
		/* 先判断是不是android */
		for (Iterator iterator = ANDROID_LIST.iterator(); iterator.hasNext();)
		{
			String temp = (String) iterator.next();
			if(userAgent.indexOf(temp) != -1)
			{
				return ConstatFinalUtil.ANDROID ; 
			}
		}
		
		/* 先判断是不是android */
		for (Iterator iterator = IPHONE_LIST.iterator(); iterator.hasNext();)
		{
			String temp = (String) iterator.next();
			if(userAgent.indexOf(temp) != -1)
			{
				return ConstatFinalUtil.IPHONE ; 
			}
		}
		return ConstatFinalUtil.PC; 
	}
	
	/**
	 * 随即休眠
	 * st-->ed 百毫秒,间隔是300毫秒
	 * type:slow(慢),fast:(快)
	 * 单位是毫秒
	 */
	public static void randSleep(String type)
	{
		int st = 0 ; 
		int ed = 0 ; 
		if("slow".equalsIgnoreCase(type))
		{
			/* 慢 */
			st = 10 ; 
			ed = 30 ; 
		}else if("fast".equalsIgnoreCase(type))
		{
			/* 快 */
			st = 3 ; 
			ed = 10 ; 
		}
		Random rand = new Random();
		while(true)
		{
			int randInt = (rand.nextInt( ed ) ) ; 
			if(randInt >= st)
			{
				try {
					Thread.sleep(randInt * 100);
					ConstatFinalUtil.SYS_LOGGER.info("休眠时长(毫秒):{}",randInt * 100);
					break ; 
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 随机设置Header
	 */
	public static void headerRandom(Map<String, String> headerMap)
	{
		Random random = new Random();
		/* 随机获取 */
		int randInt = random.nextInt(ConstatFinalUtil.HTTP_JSONARR.size());
		/* 获取随机的Cookie */
		JSONObject httpJSON = (JSONObject) ConstatFinalUtil.HTTP_JSONARR.get(randInt);
		/* 获取:userAgent */
		String userAgent = "user-agent" ; 
		if(headerMap.get(userAgent) == null)
		{
			headerMap.put(userAgent, httpJSON.getString(userAgent));
		}
		
		/* 获取:cookie */
		String cookie = "cookie" ; 
		if(headerMap.get(cookie) == null)
		{
			headerMap.put(cookie, httpJSON.getString(cookie));
		}
	}
	
	/**
	 * 把json对象变成map
	 */
	public static Map<String, String> jsonToMap(JSONObject jsonObject)
	{
		Map<String, String> resultMap = new HashMap<String, String>();
		if(jsonObject != null)
		{
			for (Iterator iterator = jsonObject.entrySet().iterator(); iterator.hasNext();) 
			{
				Entry me = (Entry) iterator.next();
				String key = me.getKey() + "" ; 
				String val = me.getValue() + "" ; 
				resultMap.put(key, val);
			}
		}
		return resultMap ; 
	}
}

package com.wangsh.train.common.util;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

/**
 * 浮点数计算,
 * 专门考虑着丢失精度的问题
 * @author Administrator
 */
@Component("doubleOperUtil")
public class DoubleOperUtil
{
	/**
	 * 浮点数，保留小数位;进一法
	 * @param value
	 * @param wei
	 */
	public BigDecimal getFormat(double value , int wei) 
	{
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(wei, BigDecimal.ROUND_HALF_UP);
		return bd;
	}
	
	/**
	 * 对double数据进行取精度(进一法)
	 * 
	 * @param value        double数据.
	 * @param scale        精度位数(保留的小数位数).
	 * @param roundingMode 精度取值方式.
	 * @return 精度计算后的数据.
	 */
	public double roundUp(double value, int scale)
	{
		return this.round(value, scale, BigDecimal.ROUND_UP) ;
	}
	
	/**
	 * 对double数据进行取精度(舍去法)
	 * 
	 * @param value        double数据.
	 * @param scale        精度位数(保留的小数位数).
	 * @param roundingMode 精度取值方式.
	 * @return 精度计算后的数据.
	 */
	public double roundDown(double value, int scale)
	{
		return this.round(value, scale, BigDecimal.ROUND_DOWN) ;
	}
	
	/**
	 * 对double数据进行取精度.
	 * 
	 * @param value        double数据.
	 * @param scale        精度位数(保留的小数位数).
	 * @param roundingMode 精度取值方式.
	 * @return 精度计算后的数据.
	 */
	public double round(double value, int scale, int roundingMode)
	{
		BigDecimal bigDecimal = new BigDecimal(value);
		bigDecimal = bigDecimal.setScale(scale, roundingMode);
		double dou = bigDecimal.doubleValue();
		return dou;
	}

	/**
	 * double 相加
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public BigDecimal sum(double d1, double d2)
	{
		BigDecimal bd1 = new BigDecimal(Double.toString(d1));
		BigDecimal bd2 = new BigDecimal(Double.toString(d2));
		return bd1.add(bd2);
	}

	/**
	 * double 相减
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public BigDecimal sub(double d1, double d2)
	{
		BigDecimal bd1 = new BigDecimal(Double.toString(d1));
		BigDecimal bd2 = new BigDecimal(Double.toString(d2));
		return bd1.subtract(bd2);
	}

	/**
	 * double 乘法
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public BigDecimal mul(double d1, double d2)
	{
		BigDecimal bd1 = new BigDecimal(Double.toString(d1));
		BigDecimal bd2 = new BigDecimal(Double.toString(d2));
		return bd1.multiply(bd2);
	}

	/**
	 * double 除法
	 * 
	 * @param d1
	 * @param d2
	 * @param scale 四舍五入 小数点位数
	 * @return
	 */
	public BigDecimal div(double d1, double d2, int scale)
	{
		// 当然在此之前，你要判断分母是否为0，
		// 为0你可以根据实际需求做相应的处理

		BigDecimal bd1 = new BigDecimal(Double.toString(d1));
		BigDecimal bd2 = new BigDecimal(Double.toString(d2));
		return bd1.divide(bd2, scale, BigDecimal.ROUND_HALF_UP);
	}
}

package com.wangsh.train.common.util;

import org.springframework.stereotype.Component;

/**
 * 执行操作系统中命令的工具类
 * 
 * @author wangshMac
 */
@Component
public class CallCommandUtil
{
	/**
	 * 调用Bat的命令
	 * @param batName
	 */
	public boolean callBat(String batName)
	{
		ConstatFinalUtil.SYS_LOGGER.info("==执行命令==开始:{}",batName);
		Process ps = null;
		try
		{
			/*
			 * cmd /c 是执行完命令后关闭命令窗口。
			 * cmd /k 是执行完命令后不关闭命令窗口。
			 * cmd /c start 会打开一个新窗口后执行指令，原窗口会关闭。
			 * cmd /k start 会打开一个新窗口后执行指令，原窗口不会关闭。
			 */
			ps = Runtime.getRuntime().exec("cmd /c start " + batName);
			ps.waitFor();
			ConstatFinalUtil.SYS_LOGGER.info("==执行命令==结束:{}",batName);
			return true ; 
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("执行命令的时候出错了!",e);
		} finally
		{
			if(ps != null)
			{
				ps.destroy(); // 销毁子进程
				ps = null;
			}
		}
		return false ; 
	}
	
	/**
	 * 切换Ip
	 * @param batName
	 */
	public boolean switchIp(String ip,String country)
	{
		/* 判断一个切换国家的简写是否在黑名单中 */
		if(country != null && !"".equalsIgnoreCase(country))
		{
			/* 说明已经传入了国家 */
			country = country.toUpperCase() ; 
			/* 搜索的字符串 */
			String countryIndex = country + "," ;
			if(ConstatFinalUtil.CONFIG_JSON.getString("system.proxy.country").indexOf(countryIndex) != -1)
			{
				ConstatFinalUtil.SYS_LOGGER.error("切换ip,被忽略;国家:{},ip:{}",country,ip);
				return false ;
			}
		}
		try {
			String command = ConstatFinalUtil.CONFIG_JSON.getString("outer.config.path") ;
			if(ip != null && !"".equalsIgnoreCase(ip))
			{
				command += "/bat/911s5_switch_ip.bat " + ip ; 
			}else
			{
				command += "/bat/911s5_switch_country.bat " + country ; 
			}
			this.callBat(command);
			try
			{
				/* 线程睡眠2秒钟 */
				Thread.sleep(200);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			return true ; 
		} catch (Exception e) {
			ConstatFinalUtil.SYS_LOGGER.error("执行命令的时候出错了!",e);
		}
		return false ; 
	}
	

	public static void main(String[] args)
	{
		String courseFile = System.getProperty("user.dir"); //获取项目跟路径
		/*
		 * 删除指定文件
		 */
//		String batName = "E:\\workspace\\carCon\\carCon-common\\carCon-common-util\\src\\main\\resources\\删除指定文件(需指定文件路径).bat";
//		String path = " e:\\aa\\aa.war";
//		batName += path;
//		callBat(batName);
		
		/*
		 * 删除指定文件夹
		 */
//		String batName = "E:\\workspace\\carCon\\carCon-common\\carCon-common-util\\src\\main\\resources\\删除指定文件夹(需指定文件夹).bat";
//		String path = " e:\\aa";
//		batName += path;
//		callBat(batName);
		
		/*
		 * 拷贝文件
		 */
//		String batName = "E:\\workspace\\carCon\\carCon-common\\carCon-common-util\\src\\main\\resources\\拷贝文件.bat";
//		String path1 = " e:\\aa\\aa.war ";
//		String path2 = " d:\\aa";
//		batName += path1;
//		batName += path2;
//		callBat(batName);
		
		/*
		 * 启动服务
		 */
//		String batName = courseFile + "\\src\\main\\resources\\启动服务.bat";
//		String sev = " tomcat"; //服务名
//		batName += sev;
//		callBat(batName);
		
		/*
		 * 停止服务
		 */
		/*String batName = courseFile + "\\src\\main\\resources\\停止服务.bat";
		String sev = " tomcat"; //服务名
		batName += sev;
		callBat(batName);*/
		
		/*
		 *定时关机 
		 */
		/*String batName = courseFile + "\\src\\main\\resources\\定时关机.bat";
		String time = " 12:00"; //服务名
		batName += time;
		callBat(batName);*/
		
		CallCommandUtil commandUtil = new CallCommandUtil();
	}
}

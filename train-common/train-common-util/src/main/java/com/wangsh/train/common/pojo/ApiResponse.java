package com.wangsh.train.common.pojo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.FileUtil;
import com.wangsh.train.common.util.PageInfoUtil;

/**
 * JSON响应的结构体
  { 
  	"code":"", 
  	"info":"", 
  	"data": 
  	{
   	} 
   }
 * 
 * @author wangsh
 */
public class ApiResponse<T> extends BasePojo<ApiResponse>
{
	/* 响应码 */
	private int code = Integer.valueOf(ApiResponseEnum.STATUS_FAILED.getStatus() + "");
	/* 响应信息 */
	private String info = ConstatFinalUtil.INFO_JSON.getString(code + "");
	/* 数据的存储--单条记录(json) */
	private Map<String, T> dataOne;
	/* 数据的存储--集合记录(json) */
	private Map<String, List<T>> dataList;
	/* 分页的对象 */
	private PageInfoUtil pageInfoUtil ; 
	@Resource
	private FileUtil fileUtil = new FileUtil();
	/* 数据的存储--单条记录(java对象) */
	private T dataOneJava ; 
	/* 数据的存储--集合记录(java对象) */
	private List<T> dataListJava ; 
	/* 存储数据 */
	private Map<String,Object> dataMapJava = new HashMap<String, Object>();
	
	public ApiResponse()
	{
	}

	/**
	 * 构造方法
	 * @param code
	 * @param info
	 * @param dataMap
	 */
	public ApiResponse(int code, String info, Map<String, T> dataOne)
	{
		super();
		this.code = code;
		this.info = info;
		this.dataOne = dataOne;
	}
	
	/**
	 * 成功的状态
	 * @return
	 */
	public static ApiResponse<Object> ofSuccess(Map<String, Object> dataOne)
	{
		return new ApiResponse<Object>(Integer.valueOf(ApiResponseEnum.STATUS_SUCCESS.getStatus() + ""), 
				ConstatFinalUtil.INFO_JSON.getString(ApiResponseEnum.STATUS_SUCCESS.getStatus() + ""), 
				dataOne) ; 
	}
	
	/**
	 * 失败的状态
	 * @return
	 */
	public static ApiResponse<?> ofFailed(int code,String message)
	{
		return new ApiResponse<Object>(code, message, null);
	}
	
	/**
	 * 清空信息
	 */
	public void clearInfo()
	{
		/* 清空信息 */
		this.setInfo("", Collections.EMPTY_MAP);
	}
	
	/**
	 * 把当前的对象转换为一个json字符串
	 */
	public JSONObject toJSON()
	{
		JSONObject resultJSON = new JSONObject() ; 
		resultJSON.put("code", this.code);
		resultJSON.put("info", this.info);
		if(dataList != null && dataList.size() > 0 )
		{	
			Map<String, Object> dataMap = new HashMap<String, Object>();
			if(this.pageInfoUtil != null)
			{
				/* 分页信息 ("pageInfoUtil",jsonObject对象)*/
				dataMap.put(ApiResponseEnum.NAME_PAGEINFOUTIL.getName(), this.pageInfoUtil.toJSON());
			}
			
			for (Iterator iterator = this.dataList.entrySet().iterator(); iterator.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				//此处的val是list 肯定不能成功  应该是想拿list里面的对象和BasePojo进行判断的 代码不是我写的 先不动
				Object val = me.getValue() ; 
				if(val instanceof BasePojo)
				{
					BasePojo b = (BasePojo) val ;
					dataMap.put(me.getKey() + "", b.toJSON());
				}else
				{
					dataMap.put(me.getKey() + "", me.getValue() );
				}
			}
			
			/* 将POJO转换为JavaObject     ApiResponseEnum.NAME_LIST.getName()=list*/
			List<T> list = this.dataList.get(ApiResponseEnum.NAME_LIST.getName());
			JSONArray listArr = new JSONArray() ; 
			for (Iterator iterator = list.iterator(); iterator.hasNext();)
			{
				T t = (T) iterator.next();
				if(t instanceof BasePojo)
				{
					BasePojo b = (BasePojo) t;
					listArr.add(b.toJSON());
				}else {
					listArr.add(t);
				}
			}
			
			dataMap.put(ApiResponseEnum.NAME_LIST.getName(),listArr);
			resultJSON.put("data", dataMap);
		}
		if(dataOne != null && dataOne.size() > 0 )
		{
			JSONObject dataMapJSON = new JSONObject();
			for (Iterator iterator = this.dataOne.entrySet().iterator(); iterator.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				Object val = me.getValue() ; 
				if(val instanceof BasePojo)
				{
					BasePojo b = (BasePojo) val ;
					dataMapJSON.put(me.getKey() + "", b.toJSON());
				}else
				{
					dataMapJSON.put(me.getKey() + "", me.getValue() );
				}
			}
			resultJSON.put("data", dataMapJSON);
		}
		if(dataMapJava != null && dataMapJava.size() > 0 )
		{
			JSONObject dataMapJSON = new JSONObject();
			for (Iterator iterator = this.dataMapJava.entrySet().iterator(); iterator.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				Object val = me.getValue() ; 
				if(val instanceof BasePojo)
				{
					BasePojo b = (BasePojo) val ;
					dataMapJSON.put(me.getKey() + "", b.toJSON());
				}else if(val instanceof List){
					List<T> list = (List<T>) val ; 
					JSONArray listArr = new JSONArray() ; 
					for (Iterator iterator2 = list.iterator(); iterator2.hasNext();)
					{
						T t = (T) iterator2.next();
						if(t instanceof BasePojo)
						{
							BasePojo b = (BasePojo) t;
							listArr.add(b.toJSON());
						}else {
							listArr.add(t);
						}
					}
					
					dataMapJSON.put(ApiResponseEnum.NAME_LIST.getName(),listArr);
				}else
				{
					dataMapJSON.put(me.getKey() + "", me.getValue() );
				}
			}
			
			if(this.pageInfoUtil != null)
			{
				/* 分页信息 ("pageInfoUtil",jsonObject对象)*/
				dataMapJSON.put(ApiResponseEnum.NAME_PAGEINFOUTIL.getName(), this.pageInfoUtil.toJSON());
			}
			
			resultJSON.put("data", dataMapJSON);
		}
		return resultJSON;
	}

	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	public String getInfo()
	{
		return info;
	}

	public void setInfo(String info , Map<String, String> params)
	{
		String errInfo = this.fileUtil.replaceOperator(info , params); 
		this.info = errInfo;
	}

	public Map<String, T> getDataOne()
	{
		return dataOne;
	}

	public void setDataOne(Map<String, T> dataOne)
	{
		this.dataOne = dataOne;
	}

	public Map<String, List<T>> getDataList()
	{
		return dataList;
	}

	public void setDataList(Map<String, List<T>> dataList)
	{
		this.dataList = dataList;
	}

	public PageInfoUtil getPageInfoUtil()
	{
		return pageInfoUtil;
	}

	public void setPageInfoUtil(PageInfoUtil pageInfoUtil)
	{
		this.pageInfoUtil = pageInfoUtil;
	}

	public T getDataOneJava()
	{
		return dataOneJava;
	}

	public void setDataOneJava(T dataOneJava)
	{
		this.dataOneJava = dataOneJava;
	}

	public List<T> getDataListJava()
	{
		return dataListJava;
	}

	public void setDataListJava(List<T> dataListJava)
	{
		this.dataListJava = dataListJava;
	}

	public Map<String, Object> getDataMapJava()
	{
		return dataMapJava;
	}

	public void setDataMapJava(Map<String, Object> dataMapJava)
	{
		this.dataMapJava = dataMapJava;
	}
}

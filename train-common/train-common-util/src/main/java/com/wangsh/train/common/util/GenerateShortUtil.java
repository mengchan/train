package com.wangsh.train.common.util;

import java.security.MessageDigest;

import org.springframework.stereotype.Component;

/**
 * 生成短连接的工具类
 * 
 * @author Administrator
 *
 */
@Component("generateShortUtil")
public class GenerateShortUtil
{
	/**
	 * 将长链接转换为短链接
	 * 
	 * @param url
	 * @return
	 */
	public String gererateShortUrl(String url)
	{
		// 可以自定义生成 MD5 加密字符传前的混合 KEY
		String key = "caron";
		// 要使用生成 URL 的字符
		String[] chars = new String[]
		{ "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
				"w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G",
				"H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"

		};
		// 对传入网址进行 MD5 加密
		String sMD5EncryptResult = MD5(key + url);
		String hex = sMD5EncryptResult;

//        String[] resUrl = new String[4];
//        for ( int i = 0; i < 4; i++) {

		// 把加密字符按照 8 位一组 16 进制与 0x3FFFFFFF 进行位与运算
		String sTempSubString = hex.substring(2 * 8, 2 * 8 + 8); // 固定取第三组

		// 这里需要使用 long 型来转换，因为 Inteper .parseInt() 只能处理 31 位 , 首位为符号位 , 如果不用 long ，则会越界
		long lHexLong = 0x3FFFFFFF & Long.parseLong(sTempSubString, 16);
		String outChars = "";
		for (int j = 0; j < 6; j++)
		{
			// 把得到的值与 0x0000003D 进行位与运算，取得字符数组 chars 索引
			long index = 0x0000003D & lHexLong;
			// 把取得的字符相加
			outChars += chars[(int) index];
			// 每次循环按位右移 5 位
			lHexLong = lHexLong >> 5;
		}
		// 把字符串存入对应索引的输出数组
//            resUrl[i] = outChars;
//        }
		return outChars;
	}

	// MD5加码。32位
	public static String MD5(String inStr)
	{
		MessageDigest md5 = null;
		try
		{
			md5 = MessageDigest.getInstance("MD5");
		} catch (Exception e)
		{
			System.out.println(e.toString());
			e.printStackTrace();
			return "";
		}
		char[] charArray = inStr.toCharArray();
		byte[] byteArray = new byte[charArray.length];

		for (int i = 0; i < charArray.length; i++)
			byteArray[i] = (byte) charArray[i];

		byte[] md5Bytes = md5.digest(byteArray);

		StringBuffer hexValue = new StringBuffer();

		for (int i = 0; i < md5Bytes.length; i++)
		{
			int val = ((int) md5Bytes[i]) & 0xff;
			if (val < 16)
				hexValue.append("0");
			hexValue.append(Integer.toHexString(val));
		}

		return hexValue.toString();
	}

	// 可逆的加密算法
	public static String KL(String inStr)
	{
		// String s = new String(inStr);
		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++)
		{
			a[i] = (char) (a[i] ^ 't');
		}
		String s = new String(a);
		return s;
	}

	// 加密后解密
	public static String JM(String inStr)
	{
		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++)
		{
			a[i] = (char) (a[i] ^ 't');
		}
		String k = new String(a);
		return k;
	}

	public static void main(String[] args)
	{
		// 测试
		GenerateShortUtil g = new GenerateShortUtil();
		String gererateShortUrl = g.gererateShortUrl(
				"https://user.qzone.qq.com/956680416?ADUIN=956680416&ADSESSION=1573892713&ADTAG=CLIENT.QQ.5671_MyTip.0&ADPUBNO=26953&source=namecardhoverstar");
		ConstatFinalUtil.SYS_LOGGER.info("短连接：{}", gererateShortUrl);
	}
}

package com.wangsh.train;

import com.wangsh.train.common.util.ConstatFinalUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;

/**
 * 测试类的父类
 * @author Wangsh
 *
 */
@SpringBootApplication
@SpringBootTest
@Configuration
public class BaseTest
{
	/**
	 * 测试
	 */
	@Test
	public void one()
	{
		ConstatFinalUtil.SYS_LOGGER.info("--one--");
	}
	
	/**
	 * 测试
	 */
	@Test
	public void two()
	{
		ConstatFinalUtil.SYS_LOGGER.info("--two--");
	}
}

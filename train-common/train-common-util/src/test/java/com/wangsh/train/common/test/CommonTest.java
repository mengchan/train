package com.wangsh.train.common.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.BaseTest;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.DateFormatUtil;
import com.wangsh.train.common.util.FileUtil;
import com.wangsh.train.common.util.RedisUtil;
import com.wangsh.train.common.util.SpringEmailUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@EnableAutoConfiguration
public class CommonTest extends BaseTest
{
	@Resource
	private RedisUtil redisUtil;
	@Resource
	private SpringEmailUtil springEmailUtil;
	@Resource
	private FileUtil fileUtil;
	@Resource
	private DateFormatUtil dateFormatUtil;
	
	/**
	 * 往Redis存储一个值
	 */
	@Test
	public void redis_put()
	{
		boolean flag = this.redisUtil.put("abc", "abc-----");
		ConstatFinalUtil.SYS_LOGGER.info("==flag:{}===",flag);
	}
	
	/**
	 * 往Redis取一个值
	 */
	@Test
	public void redis_get()
	{
		Object redisVal = this.redisUtil.get("abcd");
		ConstatFinalUtil.SYS_LOGGER.info("==flag:{}===",redisVal);
	}
	
	/**
	 * 发送邮件
	 */
	@Test
	public void sendHTMLMail()
	{
		//boolean flag = smail.sendTextMail("wangshanhu186@126.com", "测试邮件", "<a href='http://www.baidu.com'>百度</a><b>你收到了吗?</b>");
		boolean flag = this.springEmailUtil.sendHTMLMail("724259669@qq.com", "测试邮件", "<a href='http://www.baidu.com'>百度</a><b>你收到了吗?</b>");
		ConstatFinalUtil.SYS_LOGGER.info(flag);
	}
	
	/**
	 * 往Redis存储一个值带过期时间
	 */
	@Test
	public void redis_put_timeout()
	{
		boolean flag = this.redisUtil.put("abcd", "abcd-----", 30);
		ConstatFinalUtil.SYS_LOGGER.info("==flag:{}===",flag);
	}
	
	@Test
	public void redis_get_timeout()
	{
		Object redisVal = this.redisUtil.get("abcd");
		ConstatFinalUtil.SYS_LOGGER.info("==flag:{}===",redisVal);
	}
	
	/**
	 * JSON的一些测试
	 */
	@Test
	public void json()
	{
		JSONObject json = new JSONObject();
		json.put("code", "1");
		json.put("info", "2");
		JSONArray arr = new JSONArray();
		arr.add(json);
		ConstatFinalUtil.SYS_LOGGER.info("---{}",json.toJSONString());
		String arrStr = arr.toJSONString() ; 
		ConstatFinalUtil.SYS_LOGGER.info("---{}",arrStr);
		
		JSONArray arrRes = (JSONArray) JSON.parse(arrStr);
		for (Iterator iterator = arrRes.iterator(); iterator.hasNext();)
		{
			JSONObject temp = (JSONObject) iterator.next();
			ConstatFinalUtil.SYS_LOGGER.info("---{}",temp.toJSONString());
		}
	}
	
	/**
	 * 解压缩
	 */
	@Test
	public void unzip()
	{
		File file = new File("e:/test/common.zip");
		File tarFile = new File("e:/test/common-tar");
		boolean flag = this.fileUtil.unzip(file, tarFile);
		ConstatFinalUtil.SYS_LOGGER.info("unzip=={}",flag);
	}
	
	/**
	 * 代码统计
	 */
	@Test
	public void codeStatJavaZipFile()
	{
		File tarZipFile = new File("e:/test/common.zip");
		File tarTrueFile = new File("e:/test/jh01.zip-tar");
		List<Map<String, Object>> javaFileList = this.fileUtil.codeStatJavaZipFile(tarZipFile, tarTrueFile);
		for (Iterator iterator = javaFileList.iterator(); iterator.hasNext();)
		{
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			ConstatFinalUtil.SYS_LOGGER.info("明细:{}",map);
		}
	}
	
	/**
	 * 倒计时格式
	 */
	@Test
	public void formatTime()
	{
		long millSecond = 100000000 ; 
		String result = this.dateFormatUtil.formatTime(millSecond);
		ConstatFinalUtil.SYS_LOGGER.info("倒计时:{}",result);
	}
}

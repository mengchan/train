package com.wangsh.train.common.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.util.ConstatFinalUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@EnableAutoConfiguration
public class SpringbootJarApplicationTests extends BaseTest
{

	@Test
	public void contextLoads()
	{
		ConstatFinalUtil.SYS_LOGGER.info("=====");
	}

}

package com.wangsh.train.common.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.POIUtil;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 测试
 * 
 * @author Administrator
 */
@EnableAutoConfiguration
public class PoiUtilTest extends BaseTest
{
	@Resource
	private POIUtil poiUtil;

	/**
	 * 写出XLS
	 */
	@Test
	public void writeExcel()
	{
		try
		{
			InputStream is = this.getClass().getResourceAsStream("/template/excel/temp.xls");
			FileOutputStream fos = new FileOutputStream("d:/1.xls");
			
			
			List<Map> dataList = new ArrayList<Map>();
			Map<String, String> firstRowMap = new HashMap<String, String>();
			firstRowMap.put("日期", "2018-01-01");
			firstRowMap.put("总数量","20");
			firstRowMap.put("最大价格","10.2");
			dataList.add(firstRowMap);
			
			Map<String, String> secondRowMap = new HashMap<String, String>();
			secondRowMap.put("日期", "2018-02-01");
			secondRowMap.put("总数量","30");
			secondRowMap.put("最大价格","11.2");
			dataList.add(secondRowMap);
			
			this.poiUtil.writeExcelModel(is, dataList, fos);
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public void wordReadTest()
	{
		try
		{
			/* doc格式 */
			InputStream is = this.getClass().getResourceAsStream("/template/word/test.doc");
			HWPFDocument document = new HWPFDocument(is);
			WordExtractor extractor = new WordExtractor(document);
			/* 内容*/ 
			ConstatFinalUtil.SYS_LOGGER.info("===============" + extractor.getText());
			ConstatFinalUtil.SYS_LOGGER.info("===============" + extractor.getTextFromPieces());
			ConstatFinalUtil.SYS_LOGGER.info("===============" + extractor.getParagraphText());
			extractor.close();
			
			/* docx格式 */
			FileInputStream fis = new FileInputStream(new File("d:/test.docx"));
			XWPFDocument xwpfDocu = new XWPFDocument(fis) ;
			XWPFWordExtractor wordExtractor = new XWPFWordExtractor(xwpfDocu);
			ConstatFinalUtil.SYS_LOGGER.info("===============" + wordExtractor.getText());
			wordExtractor.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Word文档保存
	 */
	@Test
	public void wordWriteTest()
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(new File("d:/1.doc"));
			XWPFDocument document = new XWPFDocument();
			XWPFParagraph paragraph =  document.createParagraph() ; 
			XWPFRun createRun = paragraph.createRun();
			createRun.setText("这个是一段文字");
			document.write(fos);
			
			document.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Word文档保存
	 */
	@Test
	public void wordWriteFromModel()
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(new File("d:/1.docx"));
			/* doc格式 */
			InputStream is = this.getClass().getResourceAsStream("/template/word/test.docx");
			XWPFDocument document = new XWPFDocument(is);
			List<XWPFParagraph> paragraphList = document.getParagraphs() ; 
			/**
			 * 在循环的时候对数据进行处理
			 */
			
			/* 将内容写到文件中 */
			document.write(fos);
			document.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Word文档保存
	 */
	@Test
	public void wordWriteUtil()
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(new File("d:/1.docx"));
			/* doc格式 */
			InputStream is = this.getClass().getResourceAsStream("/template/word/test.docx");
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("a", "测试代码---");
			map.put("abc", "comeon---");
			this.poiUtil.writeWordModel(is, map, fos);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public void wordReporttil()
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(new File("d:/1.docx"));
			/* doc格式 */
			InputStream is = ConstatFinalUtil.class.getResourceAsStream("/template/word/exportWordTemplate.docx");
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("testDate", "2018-01-01");
			map.put("fs", "2");
			map.put("wd", "3");
			map.put("dqy", "3111");
			map.put("sd", "12");
			map.put("testName", "王丽");
			map.put("cdpd", "23");
			map.put("sxdl", "32");
			map.put("ddjd", "111");
			map.put("ddwd", "323");
			map.put("hzxtxh", "11");
			map.put("jzjcbh", "2323");
			map.put("hphm", "1212");
			map.put("pcolor", "1212");
			
			//map.put("image_01", "d:/1.jpg");
			//map.put("image_02", "d:/1.jpg");
			map.put("pfxz","0");
			map.put("dxpdjg","0");
			map.put("zpdjg","0");
			map.put("ydz","0");
			this.poiUtil.writeWordModel(is, map, fos);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}

package com.wangsh.train.common.test;

import com.wangsh.train.BaseTest;
import com.wangsh.train.common.util.ConstatFinalUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.util.Random;

/**
 * 这个是所有类的公共测试
 * @author TeaBig
 */
@EnableAutoConfiguration
public class AllTest extends BaseTest
{
	/**
	 * 随机点名(提问)
	 * ~需求:10-->60;每一位同学都有编号;
	 * ~系统随机生成一个10-->60的编号;让编号对应的同学回答问题
	 */
	@Test
	public void randQues()
	{
		Random random = new Random();
		
		/* 生成一个1->50的随机数 */
		int randInt = random.nextInt(50) + 1 + 10 ;
		ConstatFinalUtil.SYS_LOGGER.info("==恭喜您!{},回答问题==",randInt);
	}
}

package com.wangsh.train.common.test;

import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.FileUtil;
import com.wangsh.train.common.util.HttpUtil;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 爬虫抓取一些页面
 */
public class SpiderTest {
    private File parentFile = new File("d:/test/stream");
    private HttpUtil httpUtil = new HttpUtil();
    /**
     * 根网址
     */
    private String homeUrl = "http://www.ysxs8.com/" ;
    
    /**
     * 记录列表数据
     * @return
     */
    @Test
    public void recordList(){
        String url = homeUrl + "/downlist/9703.html" ;
        /* 抓取网页数据 */
        try {
            Connection connect = Jsoup.connect(url);
            Document document = connect.get();
            /* 获取列表信息1 */
            Elements playEles = document.getElementsByClass("playlist");
            ConstatFinalUtil.SYS_LOGGER.info("==列表大小:{}==", playEles.size());
            Element playEle = playEles.get(0);
            /* 获取列表信息2 */
            Elements playInfoEles = playEle.getElementsByTag("li");
            for (Element playInfoEle : playInfoEles) {
                Element aEle = playInfoEle.getElementsByTag("a").get(0);
                /* 名称 */
                String name = aEle.text().trim();
                String href = aEle.attr("href");
                ConstatFinalUtil.SYS_LOGGER.info("名称:{},链接:{}",name,href);
                this.recordInfo(homeUrl + href,name);
            }
        } catch (IOException e) {
            ConstatFinalUtil.SYS_LOGGER.error("报错了:", e);
        }
    }

    /**
     * 抓取单条记录
     */
    private void recordInfo(String url, String fileName) {
        /* 抓取网页数据 */
        try {
            Connection connect = Jsoup.connect(url);
            Document document = connect.get();
            ConstatFinalUtil.SYS_LOGGER.info("====={}",document);
            /* 获取播放器 */
            Element ysPlayer = document.getElementById("jp_audio_0");
            /* 获取地址 */
            String src = ysPlayer.attr("src");
            ConstatFinalUtil.SYS_LOGGER.info("名称:{},链接:{}",fileName,src);
        } catch (IOException e) {
            ConstatFinalUtil.SYS_LOGGER.error("报错了:", e);
        }
    }

    @Test
    public void renameFile(){
        String path = "d:/test/";
        File folder = new File("D:\\BaiduNetdiskDownload\\话说汉朝\\新建文件夹");
        File[] files = folder.listFiles();
        DecimalFormat df = new DecimalFormat("000");
        System.out.println("===" + df.format(1));
        for (File file : files) {
            String fileName = file.getName() ;
            fileName = fileName.replaceAll("[()]", "").trim();
            fileName = df.format(Integer.parseInt(fileName.substring(0, fileName.lastIndexOf(".")))) + fileName.substring(fileName.lastIndexOf(".",fileName.length()));
            File tempFile = new File(path + fileName);
            file.renameTo(tempFile);
            ConstatFinalUtil.SYS_LOGGER.info("文件名:{},{}",fileName,file.getName());
        }
    }

    /**
     * 从文件中读取数据
     */
    @Test
    public void recordListFile(){
        FileUtil fileUtil = new FileUtil();
        /* 创建一个文件
        * */
        try {
            File file = new File("D:\\workspace\\code\\demo_code\\train\\train-common\\train-common-util\\src\\main\\resources\\data\\data_01.js");
            FileInputStream fis = new FileInputStream(file);
            String fileStr = fileUtil.readFile(fis);
            /* 拆分列表 */
            String[] recordList = fileStr.split(",");
            for (String recordInfo : recordList) {
                String recordTemp = recordInfo.replaceAll("'", "");
                String[] recordTemps = recordTemp.split("\\$");
                if(recordTemps.length != 3){
                    continue;
                }
                String fileName = recordTemps[0];
                fileName = StringEscapeUtils.unescapeJava(fileName);
                String url = recordTemps[1];
                ConstatFinalUtil.SYS_LOGGER.info("名称:{},url:{};",fileName,url);
                this.recordStream(url, fileName);
                ConstatFinalUtil.randSleep("slow");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载文件
     */
    public void recordStream(String url, String fileName){
//        String url = "http://audio.xmcdn.com/group9/M07/92/91/wKgDYldgtOzSEQZhAIql0mkCmf8527.m4a" ;
//        if("1".equalsIgnoreCase("1")){
//            ConstatFinalUtil.SYS_LOGGER.info("名称:{},链接:{}",fileName,url);
//            return ;
//        }

        Map<String,String> headerMap = new HashMap<>();
        Map<String,String> requestMap = new HashMap<>();
        Map<String,String> responseMap = new HashMap<>();
        InputStream is = httpUtil.methodStreamGet(url, headerMap, requestMap, responseMap);
        /* 扩展名 */
        String endName = url.substring(url.lastIndexOf("."), url.length());
        fileName = fileName + endName ;
        File tempFile = new File(parentFile,fileName);
        if(!tempFile.getParentFile().exists()){
            tempFile.getParentFile().mkdirs();
        }
        try {
            FileOutputStream fos = new FileOutputStream(tempFile);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            BufferedInputStream bis = new BufferedInputStream(is);
            /* 拷贝流 */
            FileUtil fileUtil = new FileUtil();
            fileUtil.copyFile(bis, bos);
            ConstatFinalUtil.SYS_LOGGER.info("==拷贝成功;文件名:{}==",tempFile.getName());
        } catch (FileNotFoundException e) {
            ConstatFinalUtil.SYS_LOGGER.error("报错了:{}",tempFile.getName(), e);
        }
    }
}

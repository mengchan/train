package com.wangsh.train.common.dao;

import java.util.List;
import java.util.Map;

/**
 * DAO的父接口
 * @author Wangsh
 */
public interface IBaseDao<T>
{

	/**
	 * 保存一条记录
	 * @param admins	对象
	 * @return	表示的是此sql语句执行后对数据库的影响条数
	 */
	int save(T t);
	
	/**
	 * 更新一条记录
	 * 更新的记录往往是对象的属性,
	 * 
	 * @param admins
	 * @return
	 */
	int update(T t);
	
	/**
	 * 更新一条记录
	 * 按照时间范围来更新:建议再写一个方法
	 * 
	 * @param admins
	 * @return
	 */
	int updateBatch(Map<String, Object> condMap);
	/**
	 * 批量更新
	 * @param dataList
	 * @return
	 */
	int updateListBatch(List<Map> dataList);
	/**
	 * 批量保存
	 * @param dataList
	 * @return
	 */
	int saveBatch(List<Map> dataList);
	/**
	 * 删除记录
	 * 
	 * 按照id删除,
	 * 按照时间范围删除
	 * 
	 * @param condMap	条件
	 * @return
	 */
	int delete(Map<String, Object> condMap);
	
	/**
	 * 查询一条记录
	 * @param condMap 查询的条件;键=#{},值是条件
	 * @return
	 */
	T findOne(Map<String, Object> condMap);
	
	/**
	 * 查询多条记录
	 */
	List<T> findList(Map<String,Object> condMap);
	
	/**
	 * 统计记录
	 * @param condMap	参数
	 * @return	统计的结果
	 */
	List<Map<String,Object>> statMap(Map<String, Object> condMap);
}

package com.wangsh.train.common.exception;

/**
 * 创建一个异常,就是让事务回滚的
 * @author TeaBig
 */
public class RollbackException extends RuntimeException
{
	/**
	 * 构造方法
	 */
	public RollbackException(String message)
	{
		super(message);
	}
}

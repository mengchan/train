package com.wangsh.train.common.service;

import javax.annotation.Resource;

import com.wangsh.train.common.util.CallCommandUtil;
import com.wangsh.train.common.util.DateFormatUtil;
import com.wangsh.train.common.util.DoubleOperUtil;
import com.wangsh.train.common.util.EncryptUtil;
import com.wangsh.train.common.util.FileUtil;
import com.wangsh.train.common.util.HttpUtil;
import com.wangsh.train.common.util.IpUtil;
import com.wangsh.train.common.util.POIUtil;
import com.wangsh.train.common.util.RedisUtil;
import com.wangsh.train.common.util.RegexUtil;
import com.wangsh.train.common.util.SpringEmailUtil;

/**
 * 所有Service实现类的父类
 * @author Wangsh
 */
public class BaseServiceImpl
{
	@Resource
	protected DateFormatUtil dateFormatUtil ;
	@Resource
	protected FileUtil fileUtil ; 
	@Resource
	protected RedisUtil redisUtil ;
	@Resource
	protected RegexUtil regexUtil ;
	@Resource
	protected EncryptUtil encryptUtil ;
	@Resource
	protected SpringEmailUtil springEmailUtil;
	@Resource
	protected HttpUtil httpUtil ; 
	@Resource
	protected CallCommandUtil commandUtil ;
	@Resource
	protected POIUtil poiUtil;
	@Resource
	protected DoubleOperUtil doubleOperUtil;
	@Resource
	protected IpUtil ipUtil ;
}

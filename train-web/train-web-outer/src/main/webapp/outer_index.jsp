<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>首页 - 客户端单机系统</title>
		<%@ include file="/common/include/title.jsp" %>
		<script type="text/javascript">
			$(function()
			{
				/* 所有的textarea都隐藏起来 */
				$("textarea").hide();
				/* 为tr增加点击事件 */
				$("tr").click(function()
				{
					$("textarea").hide();
					$(this).find("textarea").show();
				});
				
				$("tbody tr").get(0).click();
			});
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp" %>
		<!-- 网页的主体部分 -->
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-sm-offset-1">
					<div class="table-responsive mt-2">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>名称</th>
									<th>请求</th>
									<th>响应体</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>接口格式</td>
									<td>
										<form action="${rootPath }/outer/client.htm" method="post"
											target="blank">
											上行参数: <br/>
											<textarea rows="10" cols="50" name="json">
{
    /* 版本 */
	"version":"1",
	/* 方法名 */
	"method":"method",
	/* 时间戳 */
	"time":"",
	/* 公钥,邮箱 */
	"pubKey":"",
	/* 加密字符串:version + pubKey + time, HmacUtils.HMAC_SHA_256("私钥", "加密字符串"); */
	"encrypt":"",
    /* 数据 */
	"data":
	{
	}
}
						</textarea>
											<br/>
											<input type="submit" value="提交">
										</form>
									</td>
									<td>响应体:
										<textarea rows="10" cols="50" name="json">
{
	/* 响应码 */
	"code":"1",
	/* 信息 */
	"info":"",
	/* 数据 */
	"data":
	{
		/* 正确的响应结果 */
		"response":"",
		/* 正确的相应体,jsonObject*/
		"responseHeader":{},
	}
}
</textarea>
									</td>
								</tr>
								<tr>
									<td>当前时间戳</td>
									<td>
										<form action="${rootPath }/outer/server.htm" method="post"
											target="blank">
											上行参数: <br/>
											<textarea rows="10" cols="50" name="json">
{
    /* 版本 */
    "version": "1",
    /* 方法名 */
    "method": "currentDate",
    /* 时间戳 */
	"time":"",
	/* 公钥,邮箱 */    
    "pubKey": "",
    /* 加密字符串:version + pubKey + time, HmacUtils.HMAC_SHA_256("私钥", "加密字符串"); */
	"encrypt":"",
    /* 数据 */
    "data": {
    }
}
						</textarea>
											<br/>
											<input type="submit" value="提交">
										</form>
									</td>
									<td>响应体:
										<textarea rows="10" cols="50" name="json">
{
    "code": 0, 
    "data": {
        "list": [
            {
                "request": "111", 
                "statusStr": "启用", 
                "pubTime": "2019-10-30 14:36:06", 
                "enumsMap": {
                    "STATUS-1": "启用", 
                    "STATUS-0": "禁用"
                }, 
                "updateTime": "2019-10-30 12:25:08", 
                "content": "1", 
                "url": "11", 
                "websiteId": 1, 
                "webid": {}, 
                "createTime": "2019-10-30 12:25:08", 
                "header": "111", 
                "id": 7, 
                "status": 1
            }
        ]
    }, 
    "info": "成功"
}
</textarea>
									</td>
								</tr>
								<tr>
									<td>查看问题</td>
									<td>
										<form action="${rootPath }/outer/server.htm" method="post"
											target="blank">
											上行参数: <br/>
											<textarea rows="10" cols="50" name="json">
{
    /* 版本 */
    "version": "1",
    /* 方法名 */
    "method": "findOneAsk",
    /* 时间戳 */
	"time":"",
	/* 公钥,邮箱 */
    "pubKey": "026b55e4413e",
    /* 加密字符串:version + pubKey + time, HmacUtils.HMAC_SHA_256("私钥", "加密字符串"); */
	"encrypt":"",
    /* 数据 */
    "data": {
    	/* id:问题Id */
    	"id":"1",
    }
}
						</textarea>
											<br/>
											<input type="submit" value="提交">
										</form>
									</td>
									<td>响应体:
										<textarea rows="10" cols="50" name="json">
{
    "code": 0, 
    "data": {
        "list": [
            {
                "request": "111", 
                "statusStr": "启用", 
                "pubTime": "2019-10-30 14:36:06", 
                "enumsMap": {
                    "STATUS-1": "启用", 
                    "STATUS-0": "禁用"
                }, 
                "updateTime": "2019-10-30 12:25:08", 
                "content": "1", 
                "url": "11", 
                "websiteId": 1, 
                "webid": {}, 
                "createTime": "2019-10-30 12:25:08", 
                "header": "111", 
                "id": 7, 
                "status": 1
            }
        ]
    }, 
    "info": "成功"
}
</textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 网页头部开始 -->
<nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
	<a class="navbar-brand" href="#">Logo</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active"><a class="nav-link"
				href="${rootPath }">首页 <span class="sr-only">(current)</span>
			</a></li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> 设置 </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="${rootPath }/main.htm?method=sysPropertyList"
						target="_blank">配置列表</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item"
						href="${rootPath }/head/system.htm?method=threadList"
						target="_blank">多线程列表</a>
					<!-- <a class="dropdown-item" href="#">子菜单-3</a> -->
				</div></li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> 接口信息 </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="${rootPath }/outer_index.jsp"
						target="_blank">接口列表</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item"
						href="${rootPath }/head/system.htm?method=serverInsert" target="_blank">发起攻击</a> <a
						class="dropdown-item"
						href="${rootPath }/head/system.htm?method=serverList" target="_blank">攻击网址列表</a>
				</div></li>

			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> 扫号器 </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<!-- <div class="dropdown-divider"></div> -->
					<a class="dropdown-item"
						href="${rootPath }/head/system.htm?method=uploadOrdersUser" target="_blank">用户过滤(FaceBook)</a>
						<a class="dropdown-item"
						href="${rootPath }/head/system.htm?method=historyFileList" target="_blank">历史文件列表</a>
				</div></li>
		</ul>
		<!-- 小分辨率要藏起来 -->
		<form class="form-inline my-2 my-lg-0 hidden-sm">
			<input class="form-control mr-sm-2" type="search" placeholder="关键字"
				aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
		</form>

		<ul class="navbar-nav">
			<!-- 小分辨率要藏起来 -->
			<li class="nav-item navbar-text">
				<a href="${rootPath }/head/users.htm?method=main">
					<img src="${rootPath }/img/user-pic.png" width="28" height="28">
				</a>
			</li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdownRight"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> 个人中心 <span class="sr-only">(current)</span>
			</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownRight">
					<a class="dropdown-item" href="${rootPath }/head/users.htm?method=main" target="_blank">个人主页</a>
					<a class="dropdown-item" href="${usersCenterLogin }" target="_blank">用户中心</a>
				</div>
			</li>
		</ul>
	</div>
</nav>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- 设置一些常量 --%>
<c:set value="${pageContext.request.contextPath }" var="rootPath"/>

<%-- 设置当前时间 --%>
<jsp:useBean class="java.util.Date" id="sys_now"/>
<%-- 所有网页截取的字符串长度 --%>
<c:set value="10" var="subStrLen"/>
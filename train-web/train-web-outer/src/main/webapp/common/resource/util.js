/*
 * js的全局变量
 * */
var rootPath = "/web-forward-web/";

/**
 * 返回值:如果为false,事件阻止; ~获取当前页的dom元素,为当前页的dom元素赋值; ~获取每页多少条的dom元素,为每页多少条的dom元素赋值;
 * ~表单提交;
 */
function pageFormSubmit(formId, currentPageId, currentPageVal, pageSizeId,
		pageSizeVal) {
	// console.log("=====>" + $("#" + currentPageId).length);
	/* 获取当前页的dom元素,为当前页的dom元素赋值; */
	$("#" + currentPageId).prop("value", currentPageVal);
	$("#" + pageSizeId).prop("value", pageSizeVal);
	/*
	 * 通过js让表单提交 通过Jquery里面的id选择器来做
	 */
	$("#" + formId).submit();
	return false;
}

/**
 * 刷新验证码
 * 
 * @return
 */
function refreshCode(imgId) {
	/* 验证码的图片 */
	var imgPath = rootPath + "/common/randImg.htm?now=" + Math.random(); 
	$("#" + imgId).attr("src", imgPath)
	return false;
}

/**
 * 全选
 * 
 * @return
 */
function checkAll(currid, tarname) {
	$("input[name=" + tarname + "]").each(function() {
		$(this).prop("checked", $("#" + currid).prop("checked"));
	});
	return false;
}

/**
 * 检查必须选中一个
 * 
 * @return
 */
function checkOneSubmit(tarids) {
	if (!window.confirm('确认执行此操作吗?')) {
		return false;
	}

	// 设置标志位
	var flag = "0";
	$("input[name=" + tarids + "]").each(function() {
		if ($(this).prop("checked") && flag == "0") {
			flag = '1';
		}
	});

	if (flag == '1') {
		return true;
	}
	alert("至少选择一个");
	return false;
}

/**
 * 查询地区
 * @param currId 当前元素的Iid
 * @param tarId 目标元素的Id
 * @param finiId 最终存储值的id
 * @returns
 */
function selectRegion(currId,tarId,finiId)
{
	var curr = $("#" + currId);
	var parentId = "0" ;  
	if(curr != 'undefined')
	{
		parentId = curr.val()
	}
	
	/* 如果取不到值 */
	if(parentId == '')
	{
		parentId = "0" ;
	}
	if(tarId == '')
	{
		return false ; 
	}
	
	if(finiId != '')
	{
		$("#" + finiId).val(parentId);
	}
	
	var tar = $("#" + tarId);
	/* ajax请求服务器,获取json数据 */
	$.post(
		rootPath + "/outer/client/regionList.htm",
		"json={'id':'"+ parentId +"'}",
		function (data)
		{
			if(data.code == '0')
			{
				var region = data.data.one ;
				
				/*
				拼装的目标HMTL
				<option value='-1'>请选择</option>
				 */
				var childrenList = region.childrenList ; 
				var resSb = "<option value='-1'>请选择</option>" ; 
				for(var i = 0 ; i < region.childrenList.length ; i ++ )
				{
					var regionTemp = childrenList[i] ; 
					resSb += "<option value='"+ regionTemp.id +"'>"+ regionTemp.name +"</option>" ; 
				}
				
				//alert(resSb);
				tar.html(resSb);
			}else
			{
				console.error(data.info);
			}
		},
		'json'
	);
	return false ; 
}
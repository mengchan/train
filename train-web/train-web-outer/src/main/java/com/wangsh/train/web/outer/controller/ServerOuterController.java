package com.wangsh.train.web.outer.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.outer.IServerOuterService;

/**
 * 外部 接口的 服务器端
 * 别人请求本服务
 * @author Wangsh
 */
@Controller
@RequestMapping(value = "/outer", produces = "text/html;charset=UTF-8")
@ResponseBody
public class ServerOuterController extends BaseController {
	@Resource
	private IServerOuterService serverOuterService;
	
	@RequestMapping("/server")
	public String server(String json, HttpServletRequest request) {
		// 用来存储获取到的客户端信息
		ConstatFinalUtil.SYS_LOGGER.info("--server--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 处理参数 */
		if (json == null) {
			json = "";
		}

		if (!"".equalsIgnoreCase(json)) {
			JSONObject reqJSON = new JSONObject();
			/* 传了上行参数,解析 */
			try {
				// String decode = URLDecoder.decode(json);
				reqJSON = (JSONObject) JSON.parse(json);
			} catch (Exception e) {
				ConstatFinalUtil.SYS_LOGGER.error("解析上行参数失败了,上行参数:{}", reqJSON, e);
				/* 上传的json数据格式不正确 */
				Map<String, String> infoMap = new HashMap<String, String>();
				infoMap.put("info", e.toString());
				// code=10
				apiResponse.setCode(ApiResponseEnum.INFO_JSON_FORMAT_ERROR.getStatus());
				// 上传的json数据格式不正确,信息:${info},("info",e.toString)
				apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), infoMap);
				return apiResponse.toJSON().toJSONString();
			}

			try {
				/* 第一个协议版本 */
				apiResponse = this.serverOuterService.serverVersionService(reqJSON);
				/* 版本号不正确 */
				// apiResponse.setCode(ApiResponseEnum.INFO_JSON_VERSION_ERROR.getStatus());
			} catch (Exception e) {
				ConstatFinalUtil.SYS_LOGGER.error("处理接口报错了,json:{}", json, e);
				// resultJSON.put("code", "13");
				Map<String, String> infoMap = new HashMap<String, String>();
				infoMap.put("info", e.toString());
				apiResponse.setCode(ApiResponseEnum.INFO_JSON_SERVER_ERROR.getStatus());
				apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), infoMap);
				return apiResponse.toJSON().toJSONString();
			}
		} else {
			/* 请上传上行参数 */
			// resultJSON.put("code", "9");
			apiResponse.setCode(ApiResponseEnum.INFO_UPLOAD_DATA_EMPTY.getStatus());
		}
		return apiResponse.toJSON().toJSONString();
	}
}

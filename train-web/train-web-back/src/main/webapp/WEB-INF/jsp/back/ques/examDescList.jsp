<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.pageInfoUtil }" var="pageInfoUtil"/>
<c:set value="${requestScope.response.data.list }" var="list"/>
<c:set value="${requestScope.sysTechResponse.data.list}" var="sysProList"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AExamDesc" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>试卷概要列表 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 试卷概要管理 
			<span class="c-gray en">&gt;</span> 试卷概要列表 
			<a class="btn btn-success radius r btn-refresh"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<div class="page-container">
			<form action="${rootPath}/back/ques/examDescList.htm" method="post">
				<input type="hidden" name="classId" value="${requestScope.classId }">
				<div class="text-c">
					<!-- <button onclick="removeIframe()" class="btn btn-primary radius">关闭</button> -->
					<input type="text" name="keyword" id="" placeholder=" 关键字" value="${requestScope.keyword }"
						style="width: 250px" class="input-text">
					<span class="select-box inline">
						状态:
						<select name="status" class="select">
							<option value="">请选择</option>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'STATUS')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
					</span>
					<span class="select-box inline">
						类型:
						<select name="examType" class="select">
							<option value="">请选择</option>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'EXAMTYPE_')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.examType == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
					</span>
					日期范围：
					<input type="text" name="st" value="${requestScope.st }" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						id="logmin" readonly="readonly" class="input-text Wdate" style="width: 160px;">
					- 
					<input type="text" name="ed" value="${requestScope.ed }" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						id="logmax" readonly="readonly" class="input-text Wdate" style="width: 160px;">
					<button name="" id="" class="btn btn-success" type="submit">
						<i class="Hui-iconfont">&#xe665;</i> 搜索
					</button>
				</div>
			</form>
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				<span class="l">
					<a href="javascript:;" onclick="datadel()"
						class="btn btn-danger radius">
						<i class="Hui-iconfont">&#xe6e2;</i>
						批量删除
					</a>
					<!-- 新选项卡打开 -->
					<%-- <a class="btn btn-primary radius" data-title="添加试卷概要"
						data-href="${rootPath }/back/ques/examDescInsert.htm" onclick="Hui_admin_tab(this)"
						href="javascript:;">
						<i class="Hui-iconfont">&#xe600;</i> 
						添加试卷概要
					</a> --%>
					<a class="btn btn-primary radius" data-title="添加试卷概要"
						onclick="article_add('添加试卷概要','${rootPath }/back/ques/examDescInsert.htm?classId=${requestScope.classId}','','510')"
						href="javascript:;">
						<i class="Hui-iconfont">&#xe600;</i> 
						添加试卷概要
					</a>
				</span> 
				<span class="r">
					共有数据：<strong>${pageInfoUtil.totalRecord }</strong> 条
				</span>
			</div>
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				批量操作类型:
				<span class="select-box inline">
					<select name="operType" class="select" id="operType" onchange="batchSwitch('operType')">
						<option value="">请选择</option>
						<option value="updateStatus">批量更新状态</option>
					</select>
				</span>
				<span class="select-box inline selectInfo" id="updateStatus" style="display: none;">
					状态:
					<select name="status" class="select" id="status">
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'STATUS')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
							</c:if>
						</c:forEach>
					</select>
				</span>
				<a href="javascript:;" onclick="return batchOper('ids','${rootPath}/back/ques/examDescBatch.htm','operType='+ $('#operType').val() +'&status=' + $('#status').val())"
					class="btn btn-danger radius">
					<i class="Hui-iconfont">&#xe6e2;</i>
					提交
				</a>
			</div>
			<div class="mt-20">
				<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
					<table
						class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
						<thead>
							<tr class="text-c">
								<th width="25"><input type="checkbox" name="" value=""></th>
								<th width="50">序号</th>
								<th width="50">Id</th>
								<th width="80">创建人</th>
								<th width="80">班级</th>
								<th width="80">名称</th>
								<th width="50">难易</th>
								<th width="120">开始时间</th>
								<th width="120">结束时间</th>
								<th width="50">总分</th>
								<th width="50">类型</th>
								<th width="50">状态</th>
								<th width="120">发布时间</th>
								<th width="100">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list }" var="listTemp" varStatus="stat">
								<tr class="text-c">
									<td><input type="checkbox" value="${listTemp.id}" name="ids"></td>
									<td>${stat.count }</td>
									<td>${listTemp.id }</td>
									<td class="text-l">
										<u style="cursor: pointer" class="text-primary href_info"
											onClick="article_add('查看','${rootPath}/back/admins/adminsUpdate.htm?id=${listTemp.createId }','','510')" title="查看">
											${fn:substring(listTemp.admins.name,0,subStrLen) }
										</u>
									</td>
									<td class="text-l" title="${listTemp.cla.name }">
										<a class="ml-5 href_info"
											data-href="${rootPath}/back/class/classUpdate.htm?id=${listTemp.classId}"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="${listTemp.cla.name }" title="${listTemp.cla.name }">
											${fn:substring(listTemp.cla.name,0,subStrLen) }
										</a>
									</td>
									<td class="text-l">
										<a class="ml-5 href_info"
											data-href="${rootPath}/back/ques/examDescUpdate.htm?id=${listTemp.id}"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
											${fn:substring(listTemp.name,0,subStrLen) }
										</a>
									</td>
									<td>
										${listTemp.hardStar }
									</td>
									<td>
										${listTemp.stTime }
									</td>
									<td>
										${listTemp.edTime }
									</td>
									<td>${listTemp.score }</td>
									<td class="td-status">
										<span class="label label-success radius">${listTemp.examTypeStr }</span>
									</td>
									<td class="td-status">
										<span class="label label-success radius">${listTemp.statusStr }</span>
									</td>
									<td>${listTemp.pubTime }</td>
									<td class="f-14 td-manage">
										<c:if test="${listTemp.statusEnum == 'STATUS_DRAFT'}">
											<a style="text-decoration: none" class="ml-5"
												onClick="article_add('试卷概要编辑','${rootPath}/back/ques/examDescUpdate.htm?operType=update&id=${listTemp.id}','','510')"
												href="javascript:;" title="试卷概要编辑">
												<i class="Hui-iconfont">&#xe6df;</i>
											</a>
											<a style="text-decoration: none" class="ml-5"
												data-href="${rootPath}/back/system/sysTechList.htm?operType=examDesc&id=${listTemp.id}"
												onclick="Hui_admin_tab(this)"
												href="javascript:;" data-title="${listTemp.name }的考试范围" title="${listTemp.name }的考试范围">
												<i class="Hui-iconfont">&#xe667;</i>
											</a>
										</c:if>
										<a style="text-decoration: none" class="ml-5"
											data-href="${rootPath}/back/ques/examDescUpdate.htm?operType=askList&id=${listTemp.id}"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="${listTemp.name }的生成试卷" title="${listTemp.name }的生成试卷">
											<i class="Hui-iconfont">&#xe667;</i>
										</a>
										<c:if test="${listTemp.statusEnum != 'STATUS_DRAFT'}">
											<a style="text-decoration: none" class="ml-5"
												onClick="article_add('试卷概要审核','${rootPath}/back/ques/examDescUpdate.htm?operType=audit&id=${listTemp.id}','','510')"
												href="javascript:;" title="试卷概要审核">
												<i class="Hui-iconfont">&#xe6e1;</i>
											</a>
										</c:if>
										<a style="text-decoration: none" class="ml-5"
											data-href="${rootPath}/back/ques/examScoreDescList.htm?examId=${listTemp.id}"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="${listTemp.name }的成绩列表" title="${listTemp.name }的成绩列表">
											<i class="Hui-iconfont">&#xe667;</i>
										</a>
										<!-- <a style="text-decoration: none" class="ml-5"
											onClick="article_del(this,'10001')" href="javascript:;"
											title="删除">
											<i class="Hui-iconfont">&#xe6e2;</i>
										</a> -->
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					
					<form action="${rootPath}/back/ques/examDescList.htm" method="post" id="pageForm">
						<input type="hidden" name="keyword" value="${requestScope.keyword }">
						<input type="hidden" name="status" value="${requestScope.status }">
						<input type="hidden" name="st" value="${requestScope.st }">
						<input type="hidden" name="ed" value="${requestScope.ed }">
						
						<input type="hidden" name="examType" value="${requestScope.examType }">
						<input type="hidden" name="classId" value="${requestScope.classId }">
						
						<div class="dataTables_info" id="DataTables_Table_0_info"
							role="status" aria-live="polite">
							共${pageInfoUtil.totalRecord }条,共${pageInfoUtil.totalPage}页
						</div>
						<div class="dataTables_paginate paging_simple_numbers"
							id="DataTables_Table_0_paginate">
							<a class="paginate_button previous disabled"
								aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '1', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_previous">
								首页
							</a>
							<a class="paginate_button previous disabled"
								aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.prePage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_previous">
								上一页
							</a>
							<a class="paginate_button next disabled" 
								aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.nextPage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_next">
								下一页
							</a>
							<a class="paginate_button next disabled" 
								aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.totalPage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_next">
								尾页
							</a>
							第<input type="text" class="paginate_button" id="currentPage" name="currentPage" value="${pageInfoUtil.currentPage }" size="5" maxlength="5">页
							每页<input type="text" class="paginate_button" id="pageSize" name="pageSize" value="${pageInfoUtil.pageSize }" size="5" maxlength="5">条
							<input type="submit" value="GO" class="paginate_button">
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
		
		<script type="text/javascript">
			$('.table-sort').dataTable({
				//"aaSorting" : [ [ 1, "desc" ] ],//默认第几个排序
				"bStateSave" : false,//状态保存
				"paging" : false,
				"info":false,
			});
	
			/*试卷概要-添加*/
			function article_add(title, url, w, h) {
				var index = layer.open({
					type : 2,
					title : title,
					content : url
				});
				layer.full(index);
			}
			/*试卷概要-删除*/
			function article_del(obj, id) {
				layer.confirm('确认要删除吗？', function(index) {
					$.ajax({
						type : 'POST',
						url : '',
						dataType : 'json',
						success : function(data) {
							$(obj).parents("tr").remove();
							layer.msg('已删除!', {
								icon : 1,
								time : 1000
							});
						},
						error : function(data) {
							console.log(data.msg);
						},
					});
				});
			}
			
			/*
				批量操作切换
			*/
			function batchSwitch(selId)
			{
				var selVal = $("#" + selId).val();
				/* 所有的选择信息都隐藏掉 */
				$(".selectInfo").hide();
				
				if('updateStatus' == selVal)
				{
					$("#" + selVal).show();
				}
			}
		</script>
	</body>
</html>
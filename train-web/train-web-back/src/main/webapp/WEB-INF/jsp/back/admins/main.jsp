<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>登陆后首页 - 培训机构</title>
		<meta name="keywords" content="培训机构 keywords">
		<meta name="description" content="培训机构 description">
	</head>
	<body>
		<header class="navbar-wrapper">
			<div class="navbar navbar-fixed-top">
				<div class="container-fluid cl">
					<a class="logo navbar-logo f-l mr-10 hidden-xs"
						href="${rootPath }/back/admins/main.htm?menu=system">培训中心</a>
					<a class="logo navbar-logo-m f-l mr-10 visible-xs"
						href="${rootPath }/back/admins/main.htm">培训</a>
					<span class="logo navbar-slogan f-l mr-10 hidden-xs">v3.1</span>
					<a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs"
						href="javascript:;">&#xe667;</a>
					<nav class="nav navbar-nav">
						<ul class="cl">
							<li class="dropDown dropDown_hover">
								<a href="javascript:;" class="dropDown_A">
									<i class="Hui-iconfont">&#xe600;</i> 新增 
									<i class="Hui-iconfont">&#xe6d5;</i>
								</a>
								<ul class="dropDown-menu menu radius box-shadow">
									<li>
										<a href="javascript:;" onclick="article_add('添加资讯','article-add.html')">
											<i class="Hui-iconfont">&#xe616;</i> 资讯
										</a>
									</li>
									<li>
										<a href="javascript:;" onclick="picture_add('添加资讯','picture-add.html')">
											<i class="Hui-iconfont">&#xe613;</i> 图片
										</a>
									</li>
									<li>
										<a href="javascript:;" onclick="product_add('添加资讯','product-add.html')">
											<i class="Hui-iconfont">&#xe620;</i> 产品
										</a>
									</li>
									<li>
										<a href="javascript:;" onclick="member_add('添加用户','member-add.html','','510')">
											<i class="Hui-iconfont">&#xe60d;</i> 用户
										</a>
									</li>
								</ul>
								<li class="navbar-levelone current"><a href="javascript:;">平台</a></li>
								<c:if test="${param.menu == 'system' }">
									<li class="navbar-levelone current"><a href="javascript:;">模块管理</a></li>
									<li class="navbar-levelone"><a href="javascript:;">例子</a></li>
								</c:if>
							</li>
						</ul>
					</nav>
					<nav id="Hui-userbar"
						class="nav navbar-nav navbar-userbar hidden-xs">
						<ul class="cl">
							<!-- <li>超级管理员</li> -->
							<li class="dropDown dropDown_hover">
								<a href="#" class="dropDown_A">
									${sessionScope.admins.email }<i class="Hui-iconfont">&#xe6d5;</i>
								</a>
								<ul class="dropDown-menu menu radius box-shadow">
									<li><a href="javascript:;" onClick="myselfinfo()">个人信息</a></li>
									<li><a href="${requestScope.usersCenterBackUrl}" target="_blank">用户中心</a></li>
									<li>
										<a href="javascript:;" onclick="return batchOper('','${rootPath}/back/admins/adminsUpdateSubmit.htm?operType=updateSyna&self=true','')">
											同步数据
										</a>
									</li>
									<li><a href="${rootPath }/back/admins/logout.htm" onclick="return confirm('确认退出吗?')">退出</a></li>
								</ul>
							</li>
							<li id="Hui-msg">
								<a href="#" title="消息">
									<span class="badge badge-danger">1</span>
									<i class="Hui-iconfont" style="font-size: 18px">&#xe68a;</i>
								</a>
							</li>
							<li id="Hui-skin" class="dropDown right dropDown_hover"><a
								href="javascript:;" class="dropDown_A" title="换肤"><i
									class="Hui-iconfont" style="font-size: 18px">&#xe62a;</i></a>
								<ul class="dropDown-menu menu radius box-shadow">
									<li><a href="javascript:;" data-val="default"
										title="默认（黑色）">默认（黑色）</a></li>
									<li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
									<li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
									<li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
									<li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
									<li><a href="javascript:;" data-val="orange" title="橙色">橙色</a></li>
								</ul></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<aside class="Hui-aside">
			<div class="menu_dropdown bk_2" style="display: none">
				<dl id="menu-aaaaa">
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> 主模板<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li>
								<a data-href="${rootPath }/back/demo/dynastyList.htm" data-title="朝代列表"
									href="javascript:void(0)"> 朝代列表</a>
							</li>
						 	<li>
								<a data-href="${rootPath }/back/demo/kingList.htm" data-title="皇上列表"
									href="javascript:void(0)"> 皇上列表</a>
							</li> 
						</ul>
					</dd>
				</dl>
			</div>
			<div class="menu_dropdown bk_2" style="display: none">
				<dl id="menu-aaaaa">
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> 用户管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li>
								<a data-href="${rootPath }/back/admins/adminsList.htm" data-title="管理员列表"
									href="javascript:void(0)">管理员列表</a>
							</li>
						 	<li>
								<a data-href="${rootPath }/back/users/usersList.htm" data-title="用户列表"
									href="javascript:void(0)">用户列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/users/usersClassList.htm" data-title="用户班级列表"
									href="javascript:void(0)">用户班级列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/users/usersCateList.htm?cateTempId=${requestScope.sys_map.usersCate_template_id_stuDay.vals}" data-title="用户日报"
									href="javascript:void(0)">用户日报</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/users/usersCateList.htm?cateTempId=${requestScope.sys_map.usersCate_template_id_week.vals}" data-title="用户周报"
									href="javascript:void(0)">用户周报</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/users/usersCateList.htm?cateTempId=${requestScope.sys_map.usersCate_template_id_month.vals}" data-title="用户月报"
									href="javascript:void(0)">用户月报</a>
							</li>
						</ul>
					</dd>
				</dl>
				<dl id="menu-aaaaa">
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> 班级管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li>
								<a data-href="${rootPath }/back/class/classList.htm" data-title="班级列表"
									href="javascript:void(0)">班级列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/class/codeStatList.htm" data-title="代码统计列表"
									href="javascript:void(0)">代码统计列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/class/codeDetailList.htm" data-title="代码统计明细列表"
									href="javascript:void(0)">代码统计明细列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/class/dataList.htm" data-title="课程资料列表"
									href="javascript:void(0)">课程资料列表</a>
							</li>
						</ul>
					</dd>
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> 问题管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li>
								<a data-href="${rootPath }/back/ques/askList.htm" data-title="问题列表"
									href="javascript:void(0)">问题列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/ques/examDescList.htm" data-title="试卷概要列表"
									href="javascript:void(0)">试卷概要列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/ques/examScoreDescList.htm" data-title="成绩列表"
									href="javascript:void(0)">成绩列表</a>
							</li>
						</ul>
					</dd>
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> 工作管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li>
								<a data-href="${rootPath }/back/job/jobCompanyList.htm" data-title="公司列表"
									href="javascript:void(0)">公司列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/job/comPositionList.htm" data-title="职位列表"
									href="javascript:void(0)">职位列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/job/interviewList.htm" data-title="面试列表"
									href="javascript:void(0)">面试列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/job/offerList.htm" data-title="offer列表"
									href="javascript:void(0)">offer列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/job/resumeList.htm" data-title="简历列表"
									href="javascript:void(0)">简历列表</a>
							</li>
						</ul>
					</dd>
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> 系统配置<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li>
								<a data-href="${rootPath }/back/system/sysFileList.htm?operType=update" data-title="系统文件列表"
									href="javascript:void(0)">系统文件列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/system/sysTechList.htm" data-title="系统技术列表"
									href="javascript:void(0)">系统技术列表</a>
							</li>
							<li>
								<a data-href="${rootPath }/back/system/sysProList.htm" data-title="系统配置列表"
									href="javascript:void(0)">系统配置列表</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>					
			
			<div class="menu_dropdown bk_2" style="display: none">
				<dl id="menu-aaaaa">
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> CRUD管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li>
								<a data-href="${rootPath }/back/demo/dynastyList.htm" data-title="朝代列表"
									href="javascript:void(0)"> 朝代列表</a>
							</li>
						 	<li>
								<a data-href="${rootPath }/back/demo/kingList.htm" data-title="皇上列表"
									href="javascript:void(0)"> 皇上列表</a>
							</li> 
						</ul>
					</dd>
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> 统计管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li>
								<a data-href="${rootPath }/back/demo/dynastyList.htm" data-title="朝代列表"
									href="javascript:void(0)"> 皇上统计</a>
							</li>
						</ul>
					</dd>
				</dl>
			</div>	
			<div class="menu_dropdown bk_2" style="display: none">
				<dl id="menu-bbbbb">
					<dt>
						<i class="Hui-iconfont">&#xe616;</i> 二级导航2<i
							class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
					</dt>
					<dd>
						<ul>
							<li><a data-href="article-list.html" data-title="资讯管理"
								href="javascript:void(0)">三级导航</a></li>
						</ul>
					</dd>
				</dl>
			</div>
		</aside>
		<div class="dislpayArrow hidden-xs">
			<a class="pngfix" href="javascript:void(0);"
				onClick="displaynavbar(this)"></a>
		</div>
		<section class="Hui-article-box">
			<div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
				<div class="Hui-tabNav-wp">
					<ul id="min_title_list" class="acrossTab cl">
						<li class="active">
							<span title="我的桌面" data-href="${rootPath }/back/admins/welcome.htm">我的桌面</span>
							<em></em>
						</li>
					</ul>
				</div>
				<div class="Hui-tabNav-more btn-group">
					<a id="js-tabNav-prev" class="btn radius btn-default size-S"
						href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a><a
						id="js-tabNav-next" class="btn radius btn-default size-S"
						href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a>
				</div>
			</div>
			<div id="iframe_box" class="Hui-article">
				<div class="show_iframe">
					<div style="display: none" class="loading"></div>
					<iframe scrolling="yes" frameborder="0" src="${rootPath }/back/admins/welcome.htm"></iframe>
				</div>
			</div>
		</section>
	
		<div class="contextMenu" id="Huiadminmenu">
			<ul>
				<li id="closethis">关闭当前</li>
				<li id="closeother">关闭其它</li>
				<li id="closeall">关闭全部</li>
			</ul>
		</div>
		
		<%@ include file="/common/include/footer.jsp" %>
			
		<script type="text/javascript">
			$(function() {
				/*$("#min_title_list li").contextMenu('Huiadminmenu', {
					bindings: {
						'closethis': function(t) {
							console.log(t);
							if(t.find("i")){
								t.find("i").trigger("click");
							}		
						},
						'closeall': function(t) {
							alert('Trigger was '+t.id+'\nAction was Email');
						},
					}
				});*/
	
				$("body").Huitab({
					tabBar : ".navbar-wrapper .navbar-levelone",
					tabCon : ".Hui-aside .menu_dropdown",
					className : "current",
					index : 0,
				});
			});
			/*个人信息*/
			function myselfinfo() {
				layer.open({
					type : 1,
					area : [ '300px', '200px' ],
					fix : false, //不固定
					maxmin : true,
					shade : 0.4,
					title : '查看信息',
					content : '<div>管理员信息</div>'
				});
			}
	
			/*资讯-添加*/
			function article_add(title, url) {
				var index = layer.open({
					type : 2,
					title : title,
					content : url
				});
				layer.full(index);
			}
			/*图片-添加*/
			function picture_add(title, url) {
				var index = layer.open({
					type : 2,
					title : title,
					content : url
				});
				layer.full(index);
			}
			/*产品-添加*/
			function product_add(title, url) {
				var index = layer.open({
					type : 2,
					title : title,
					content : url
				});
				layer.full(index);
			}
			/*用户-添加*/
			function member_add(title, url, w, h) {
				layer_show(title, url, w, h);
			}
		</script>
	</body>
</html>
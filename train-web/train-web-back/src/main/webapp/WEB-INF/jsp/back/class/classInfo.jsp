<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="clas" />
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看班级 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 班级管理 
			<span class="c-gray en">&gt;</span> 查看班级
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/demo/clasUpdateSubmit.htm">
				<input type="hidden" name="id" value="${clas.id }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>名称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${clas.name }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>分类：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${clas.cateJSON.treeName}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>地区：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:choose>
							<c:when test="${fn:contains(clas.regionJSON.treeName,'|-->')}">
								${fn:replace(clas.regionJSON.treeName,'|-->','  ')}
							</c:when>
							<c:otherwise>
								${clas.regionJSON.treeName} 
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">地址：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${clas.address }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">开班时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${clas.stTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">结束时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${clas.edTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${clas.pubTime}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">班级状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${clas.claStatusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${clas.statusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${clas.createTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${clas.updateTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${clas.pubTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${clas.content}
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="dateOne"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看班级 - 培训机构</title>
	</head>
	<body>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/demo/clasUpdateSubmit.htm">
				<input type="hidden" name="id" value="${clas.id }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>相关Id：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${dateOne.relaId }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>名字：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${dateOne.name }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>大小：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<fmt:formatNumber value="${dateOne.fileSize }" pattern="#,###,###"/>Byte
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>下载文件：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<a class="href_info" href="${websiteFileUrl}${dateOne.filePath }" target="_blank">下载源文件</a>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${dateOne.pubTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>文件关联类型：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${dateOne.fileRelaTypeStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">文件类型：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${dateOne.fileTypeStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${dateOne.statusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${dateOne.createTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${dateOne.updateTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${dateOne.pubTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>源文件：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:choose>
							<c:when test="${dateOne.filePath != null && dateOne.filePath != '' }">
								<img alt="" src="${websiteFileUrl}${dateOne.filePath }" width="100" height="80">
							</c:when>
							<c:otherwise>
								<img alt="" src="${rootPath }/img/no_pic.png" width="100" height="80">
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${dateOne.content}
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one" />
<jsp:useBean class="com.wangsh.train.job.pojo.AJobComPosition" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新职位 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 工作管理 
			<span class="c-gray en">&gt;</span> 更新职位
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/job/comPositionUpdateSubmit.htm">
				<input type="hidden" name="id" value="${one.id }">
				<input type="hidden" name="operType" value="${param.operType }">
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>名称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<a style="text-decoration: none" class="ml-5 href_info"
							data-href="${rootPath }/back/job/comPositionUpdate.htm?id=${one.id }"
							onclick="Hui_admin_tab(this)"
							href="javascript:;" data-title="职位查看_${one.name}" title="职位查看_${one.name}">
							${one.name }
						</a>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>状态：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.statusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<div class="radio-box">
							<!-- 拆分,按照-拆分,取最后一个 -->
							<label for="status2" class="form-check-label">
								<input type="radio" id="status2" name="status" value="2" ${one.status == '2' || one.status == '1' ? 'checked' : '' }>
								审核通过
							</label>
						</div>
						<div class="radio-box">
							<label for="status3" class="form-check-label">
								<input type="radio" id="status3" name="status" value="3" ${one.status == '3' ? 'checked' : '' }>
								审核不通过
							</label>
						</div>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>审核原因：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea rows="10" cols="80" name="auditContent">${one.auditContent }</textarea>
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<button 
							class="btn btn-primary radius" type="submit">
							<i class="Hui-iconfont">&#xe632;</i> 提交
						</button>
						<!-- <button onClick="article_save();" class="btn btn-secondary radius"
							type="button">
							<i class="Hui-iconfont">&#xe632;</i> 保存草稿
						</button>
						<button onClick="removeIframe();" class="btn btn-default radius"
							type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button> -->
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
		<script type="text/javascript">
			$(function(){
				$('.skin-minimal input').iCheck({
					checkboxClass: 'icheckbox-blue',
					radioClass: 'iradio-blue',
					increaseArea: '20%'
				});
				
				//表单验证
				$("#formSubmit").validate({
					rules:{
						name:{
							required:true,
						},
						capital:{
							required:true,
						},
						stYear:{
							required:true,
							isDigits:true,
						},
						edYear:{
							required:true,
							isDigits:true,
						}
					},
					onkeyup:false,
					focusCleanup:true,
					success:"valid",
					submitHandler:function(form){
						$(form).ajaxSubmit({
							type: 'post',
							success: function(data){
								/* 弹出框 */
								layer.msg(data.info,{icon:1,time:3000},function()
								{
									
								});
							},
							dataType:"json"
						});
					}
				});
				/* 初始化百度编辑器 */
				var ue = UE.getEditor('editor');
			});
		</script>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.cla.pojo.ACodeStat" id="pojo"/>
<c:set value="${requestScope.response.data.one }" var="pojoTemp"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看公司 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 工作管理 
			<span class="c-gray en">&gt;</span> 查看公司
			<a class="btn btn-success radius r btn-refresh"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/job/jobCompanyUpdateSubmit.htm">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建人：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.create.nickName}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">名字：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.name }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">网站：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<a href="${pojoTemp.website }" target="_blank" class="href_info">${pojoTemp.website }</a>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>地区：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:choose>
							<c:when test="${fn:contains(pojoTemp.regionJSON.treeName,'|-->')}">
								${fn:replace(pojoTemp.regionJSON.treeName,'|-->','  ')}
							</c:when>
							<c:otherwise>
								${pojoTemp.regionJSON.treeName} 
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">地址：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.address }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">联系人：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.contact }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">手机：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.phone }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">顶：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.upNum }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">踩：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.downNum }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">Logo：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:choose>
							<c:when test="${pojoTemp.logoPath != null && pojoTemp.logoPath != '' }">
								<img alt="" src="${websiteFileUrl}${pojoTemp.logoPath }" width="100" height="80">
							</c:when>
							<c:otherwise>
								<img alt="" src="${rootPath }/img/no_pic.png" width="100" height="80">
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojoTemp.statusStr}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.pubTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.createTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.updateTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.content }
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
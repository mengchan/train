<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one" />
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看朝代 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 例子管理 
			<span class="c-gray en">&gt;</span> 查看朝代
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/demo/dynastyUpdateSubmit.htm">
				<input type="hidden" name="id" value="${one.id }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>名称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.name }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">首都：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.capital }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">开国年份：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.stYear }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">亡国年份：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.edYear }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.pubTime}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.statusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.createTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.updateTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.pubTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.content}
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
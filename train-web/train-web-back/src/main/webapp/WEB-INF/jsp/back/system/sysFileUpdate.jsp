<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.system.pojo.ASysFile" id="pojo"/>
<c:set value="${requestScope.response.data.one}" var="dateOne"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新班级 - 培训机构</title>
	</head>
	<body>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/system/sysFileUpdateSubmit.htm">
				<input type="hidden" name="id" value="${dateOne.id }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>相关Id：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="${dateOne.relaId }" placeholder="请输入相关Id"
							id="relaId" name="relaId">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" readonly="readonly" value="${dateOne.pubTime }"
							onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							id="pubTimeStr" name="pubTimeStr" class="input-text Wdate" style="width: 160px;">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>文件关联类型：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'FILERELATYPE')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="fileRelaType" value="${keys[1] }" ${dateOne.fileRelaType == keys[1] ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">文件类型：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'FILETYPE')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="fileType" value="${keys[1] }" ${dateOne.fileType == keys[1] ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'STATUS')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="status" value="${keys[1] }" ${dateOne.status == keys[1] ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<script name="content" id="editor" type="text/plain"
							style="width: 100%; height: 200px;">${dateOne.content}</script>
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<button 
							class="btn btn-primary radius" type="submit">
							<i class="Hui-iconfont">&#xe632;</i> 保存
						</button>
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
		<script type="text/javascript">
			$(function(){
				$('.skin-minimal input').iCheck({
					checkboxClass: 'icheckbox-blue',
					radioClass: 'iradio-blue',
					increaseArea: '20%'
				});
				
				//表单验证
				$("#formSubmit").validate({
					rules:{
						name:{
							required:true,
						},
						capital:{
							required:true,
						},
						stYear:{
							required:true,
						},
						edYear:{
							required:true,
						}
			
					},
					onkeyup:false,
					focusCleanup:true,
					success:"valid",
					submitHandler:function(form){
						$(form).ajaxSubmit({
							type: 'post',
							success: function(data){
								/* 弹出框 */
								layer.msg(data.info,{icon:1,time:3000},function()
								{
									if(data.code == '0')
									{
										var index = parent.layer.getFrameIndex(window.name);
										parent.$('.btn-refresh').click();
										parent.layer.close(index);
									}
								});
							},
							dataType:"json"
						});
					}
				});
				/* 初始化百度编辑器 */
				var ue = UE.getEditor('editor');
				
				/* 查询所有的一级地区 */
			  	selectRegion('region1','region1','')
			  	
			  	selectCate('cate1','cate1','')
			});
		</script>
	</body>
</html>
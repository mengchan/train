<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="sysPro" />
<jsp:useBean class="com.wangsh.train.system.pojo.ASysPro" id="sysProPojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新系统配置 - 培训机构</title>
	</head>
	<body>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/system/sysProUpdateSubmit.htm">
				<input type="hidden" name="id" value="${sysPro.id }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>名字：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="${sysPro.name}" placeholder=""
							id="name" name="name">
					</div>
				</div>
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>英文名字：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="${sysPro.engName}" placeholder=""
							id="engName" name="engName">
					</div>
				</div>
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>值：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="${sysPro.vals}" placeholder=""
							id="vals" name="vals">
					</div>
				</div>

				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">数据操作类型：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:forEach items="${sysProPojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'OPERTYPE')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="operType" value="${keys[1] }" ${keys[1] == sysPro.operType ? 'checked' : ''}>
										&nbsp;${me.value }
									</label>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">类型：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:forEach items="${sysProPojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'PROTYPE')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="proType" value="${keys[1] }" ${keys[1] == sysPro.proType ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">标识字段：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:forEach items="${sysProPojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'STATUS')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="status" value="${keys[1] }" ${keys[1] == sysPro.status ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" readonly="readonly" value="${sysPro.pubTime}"
							onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							id="pubTimeStr" name="pubTimeStr" class="input-text Wdate">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>描述信息：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<script name="content" id="editor" type="text/plain"
							style="width: 100%; height: 200px;">${sysPro.content}</script>
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<button 
							class="btn btn-primary radius" type="submit">
							<i class="Hui-iconfont">&#xe632;</i> 提交
						</button>
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
		<script type="text/javascript">
			$(function(){
				$('.skin-minimal input').iCheck({
					checkboxClass: 'icheckbox-blue',
					radioClass: 'iradio-blue',
					increaseArea: '20%'
				});
				
				//表单验证
				$("#formSubmit").validate({
					rules:{
						name : {
							required : true,
							minlength : 2,
							maxlength : 200
						},
						content : {
							required : true,
						},
						engName : {
							required : true,
						},
						vals : {
							required : true,
						},
						pubTimeStr : {
							required : true,
						}
					},
					onkeyup:false,
					focusCleanup:true,
					success:"valid",
					submitHandler:function(form){
						$(form).ajaxSubmit({
							type: 'post',
							success: function(data){
								/* 弹出框 */
								layer.msg(data.info,{icon:1,time:3000},function()
								{
									if(data.code == '0')
									{
										var index = parent.layer.getFrameIndex(window.name);
										parent.$('.btn-success').click();
										parent.layer.close(index);
									}
								});
							},
							dataType:"json"
						});
					}
				});
				/* 初始化百度编辑器 */
				var ue = UE.getEditor('editor');
			});
		</script>
	</body>
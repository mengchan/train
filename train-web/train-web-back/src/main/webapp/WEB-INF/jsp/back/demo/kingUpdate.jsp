<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one" />
<jsp:useBean class="com.wangsh.train.demo.pojo.ADemoKing" id="kingPojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新皇上 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 例子管理 
			<span class="c-gray en">&gt;</span> 更新皇上 
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/demo/kingUpdateSubmit.htm">
				<input type="hidden" name="id" value="${one.id }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>朝代：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<span class="select-box">
							<select name="dynastyId" class="select">
								<c:forEach items="${dynastyResponse.data.list }" var="dynasty" varStatus="stat">
									<option value="${dynasty.id }" ${one.dynastyId == dynasty.id ? 'selected' : '' }>${dynasty.name }</option>
								</c:forEach>
							</select>
						</span>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>名称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="${one.name }" placeholder=""
							id="name" name="name">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">庙号：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="${one.miaoHao }" placeholder=""
							id="miaoHao" name="miaoHao">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">年号：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="${one.nianHao }" placeholder=""
							id="nianHao" name="nianHao">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" readonly="readonly" value="${one.pubTime}"
							onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							id="pubTimeStr" name="pubTimeStr" class="input-text Wdate">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:forEach items="${kingPojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'STATUS')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="status" value="${keys[1] }" ${keys[1] == one.status ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<script name="content" id="editor" type="text/plain"
							style="width: 100%; height: 200px;">${one.content}</script>
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<button 
							class="btn btn-primary radius" type="submit">
							<i class="Hui-iconfont">&#xe632;</i> 提交
						</button>
						<!-- <button onClick="article_save();" class="btn btn-secondary radius"
							type="button">
							<i class="Hui-iconfont">&#xe632;</i> 保存草稿
						</button>
						<button onClick="removeIframe();" class="btn btn-default radius"
							type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button> -->
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
		<script type="text/javascript">
			$(function(){
				$('.skin-minimal input').iCheck({
					checkboxClass: 'icheckbox-blue',
					radioClass: 'iradio-blue',
					increaseArea: '20%'
				});
				
				//表单验证
				$("#formSubmit").validate({
					rules:{
						name:{
							required:true,
						},
						miaoHao:{
							required:true,
						},
						niaoHao:{
							required:true,
						}
			
					},
					onkeyup:false,
					focusCleanup:true,
					success:"valid",
					submitHandler:function(form){
						$(form).ajaxSubmit({
							type: 'post',
							success: function(data){
								/* 弹出框 */
								layer.msg(data.info,{icon:1,time:3000},function()
								{
									if(data.code == '0')
									{
										//parent.$('.btn-refresh').click();
										/* 刷新父窗口 */
										window.parent.location.reload();
										
										var index = parent.layer.getFrameIndex(window.name);
										parent.$('.btn-refresh').click();
										parent.layer.close(index);
									}
								});
							},
							dataType:"json"
						});
					}
				});
				/* 初始化百度编辑器 */
				var ue = UE.getEditor('editor');
			});
		</script>
	</body>
</html>
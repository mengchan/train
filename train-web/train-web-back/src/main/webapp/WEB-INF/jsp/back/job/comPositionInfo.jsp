<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one" />
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看职位 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 工作管理 
			<span class="c-gray en">&gt;</span> 查看职位
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/ques/askUpdateSubmit.htm">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>创建人：
					</label>
					<div class="formControls col-xs-8 col-sm-9">
						<a style="text-decoration: none" class="ml-5 href_info"
							data-href="${rootPath}/back/users/usersUpdate.htm?id=${one.createId }"
							onclick="Hui_admin_tab(this)"
							href="javascript:;" data-title="查看个人信息_${one.create.nickName }" title="查看个人信息_${one.create.nickName }">
							${one.create.nickName }
						</a>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>公司：
					</label>
					<div class="formControls col-xs-8 col-sm-9">
						<a style="text-decoration: none" class="ml-5 href_info"
							data-href="${rootPath}/back/job/jobCompanyUpdate.htm?id=${one.companyId }"
							onclick="Hui_admin_tab(this)"
							href="javascript:;" data-title="查看个人信息_${one.company.name }" title="查看个人信息_${one.company.name  }">
							${one.company.name }
						</a>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>名称：
					</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:out value="${one.name }" escapeXml="true"/>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">工资：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.wages}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">工作经验(起始)：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.experSt }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">工作经验(结束)：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.experEd }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">地区：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:choose>
							<c:when test="${fn:contains(one.regionJSON.treeName,'|-->')}">
								${fn:replace(one.regionJSON.treeName,'|-->','  ')}
							</c:when>
							<c:otherwise>
								${one.regionJSON.treeName} 
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">地址：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.address }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">顶：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.upNum }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">踩：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.downNum }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.statusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.createTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.updateTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.pubTime}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">审核时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.auditTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">审核内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.auditContent}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.content}
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<script type="text/javascript">
	$(function(){
		$('.skin-minimal input').iCheck({
			checkboxClass: 'icheckbox-blue',
			radioClass: 'iradio-blue',
			increaseArea: '20%'
		});
		$("#tab-system").Huitab({
			index:0
		});
	});
</script>
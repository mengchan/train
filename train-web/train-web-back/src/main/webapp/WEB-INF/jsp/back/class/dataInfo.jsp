<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one" />
<c:set value="${requestScope.sysTechResponse.data.list }" var="sysTechlist"/>
<jsp:useBean class="com.wangsh.train.cla.pojo.AClassData" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看课程资料 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 班级管理 
			<span class="c-gray en">&gt;</span> 查看课程资料
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/class/dataUpdateSubmit.htm">
				<input type="hidden" name="id" value="${one.id }">
				<input type="hidden" name="operType" value="${param.operType }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>技术：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<a style="text-decoration: none" class="ml-5 href_info"
							data-href="${rootPath}/back/system/sysTechUpdate.htm?id=${one.techId}"
							onclick="Hui_admin_tab(this)"
							href="javascript:;" data-title="${one.tech.name }" title="${one.tech.name }">
							${one.tech.name }
						</a>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>名称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.name }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">视频路径：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.videoPath}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">视频解码：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.videoCode }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">时长(分)：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.timeLong }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.pubTime}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.statusStr}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.content}
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
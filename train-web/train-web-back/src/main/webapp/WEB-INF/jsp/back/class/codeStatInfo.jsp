<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.cla.pojo.ACodeStat" id="pojo"/>
<c:set value="${requestScope.response.data.one}" var="pojoTemp"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看代码统计 - 培训机构</title>
	</head>
	<body>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/demo/clasUpdateSubmit.htm">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>用户：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.users.email }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>昵称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.users.nickName }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>姓名：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.users.trueName }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>班级：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.usersClass.cla.name }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>名称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.name }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>原始：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.oriName }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>下载文件：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<a class="href_info" href="${websiteFileUrl}${pojoTemp.filePath }">下载源文件</a>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>总行数：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.totalCount }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>单行数：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.sinComCount }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>多行数：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.mulComCount }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>空行数：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.blankCount }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>有效代码行数：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.codeCount }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>代码状态：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.codeTypeStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>压缩包大小：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<fmt:formatNumber value="${pojoTemp.fileSize }" pattern="#,###,###"/>Byte
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>文件总大小：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<fmt:formatNumber value="${pojoTemp.totalFileSize }" pattern="#,###,###"/>Byte
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>压缩比：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<fmt:formatNumber value="${pojoTemp.fileSize / pojoTemp.totalFileSize }" pattern="#.###%"/>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.pubTime}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojoTemp.statusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojoTemp.createTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojoTemp.updateTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojoTemp.pubTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojoTemp.content}
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
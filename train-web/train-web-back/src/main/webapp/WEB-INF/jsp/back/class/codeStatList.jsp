<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.pageInfoUtil }" var="pageInfoUtil"/>
<c:set value="${requestScope.response.data.list }" var="list"/>
<jsp:useBean class="com.wangsh.train.cla.pojo.ACodeStat" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>代码统计列表 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 代码统计管理 
			<span class="c-gray en">&gt;</span> 代码统计列表 
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<div class="page-container">
			<form action="${rootPath}/back/class/codeStatList.htm" method="post">
				<div class="text-c">
					<!-- <button onclick="removeIframe()" class="btn btn-primary radius">关闭</button> -->
					<input type="text" name="keyword" id="" placeholder=" 关键字" value="${requestScope.keyword }"
						style="width: 250px" class="input-text">
					代码状态:
					<span class="select-box inline">
						<select name="codeType" class="select">
							<option value="">请选择</option>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'CODETYPE')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.codeType == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
					</span>
					状态:
					<span class="select-box inline">
						<select name="status" class="select">
							<option value="">请选择</option>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'STATUS')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
					</span>
					排序:
					<span class="select-box inline">
						<select name="orderType" class="select">
							<option value="">请选择</option>
							<option value="totalCountDesc" ${requestScope.orderType == 'totalCountDesc' ? 'selected' : '' }>总行数降序</option>
							<option value="totalCountAsc" ${requestScope.orderType == 'totalCountAsc' ? 'selected' : '' }>总行数升序</option>
						</select>
					</span>
					日期范围：
					<input type="text" name="st" value="${requestScope.st }" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						id="logmin" readonly="readonly" class="input-text Wdate" style="width: 160px;">
					- 
					<input type="text" name="ed" value="${requestScope.ed }" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						id="logmax" readonly="readonly" class="input-text Wdate" style="width: 160px;">
					<button name="" id="" class="btn btn-success" type="submit">
						<i class="Hui-iconfont">&#xe665;</i> 搜索
					</button>
				</div>
			</form>
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				<span class="l">
					<a href="javascript:;" onclick="datadel()"
						class="btn btn-danger radius">
						<i class="Hui-iconfont">&#xe6e2;</i>
						批量删除
					</a>
					<!-- 新选项卡打开 -->
					<%-- <a class="btn btn-primary radius" data-title="添加代码统计"
						data-href="${rootPath }/back/class/classInsert.htm" onclick="Hui_admin_tab(this)"
						href="javascript:;">
						<i class="Hui-iconfont">&#xe600;</i> 
						添加代码统计
					</a> --%>
					<a class="btn btn-primary radius" data-title="添加代码统计"
						onclick="article_add('添加代码统计','${rootPath }/back/class/codeStatInsert.htm','','510')"
						href="javascript:;">
						<i class="Hui-iconfont">&#xe600;</i> 
						添加代码统计
					</a>
				</span> 
				<span class="r">
					共有数据：<strong>${pageInfoUtil.totalRecord }</strong> 条
				</span>
			</div>
			<div class="mt-20">
				<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
					<table
						class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
						<thead>
							<tr class="text-c">
								<th width="25"><input type="checkbox" name="" value=""></th>
								<th width="60">序号</th>
								<th width="60">Id</th>
								<th width="80">用户</th>
								<th width="80">姓名</th>
								<th width="80">班级</th>
								<th width="80">名称</th>
								<th width="50">总行</th>
								<!-- <th width="50" title="单行注释">单注</th> -->
								<th width="50" title="多行注释">多注</th>
								<th width="50">空行</th>
								<th width="50">代码数</th>
								<th width="50" title="代码状态">代状</th>
								<th width="50">状态</th>
								<th width="120">更新时间</th>
								<th width="120">发布时间</th>
								<th width="80">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list }" var="pojoTemp" varStatus="stat">
								<tr class="text-c">
									<td><input type="checkbox" value="" name=""></td>
									<td>${stat.count}</td>
									<td>${pojoTemp.id }</td>
									<td title="${pojoTemp.users.nickName }">
										${fn:substring(pojoTemp.users.nickName,0,subStrLen) }
									</td>
									<td title="${pojoTemp.users.trueName }">
										${fn:substring(pojoTemp.users.trueName,0,subStrLen) }
									</td>
									<td title="${pojoTemp.usersClass.cla.name }">
										${fn:substring(pojoTemp.usersClass.cla.name,0,subStrLen) }
									</td>
									<td class="text-l">
										<u style="cursor: pointer" class="text-primary href_info"
											onClick="article_add('查看','${rootPath}/back/class/codeStatUpdate.htm?id=${pojoTemp.id}','','510')" title="查看">
											${fn:substring(pojoTemp.name,0,subStrLen) }
										</u>
									</td>
									<td>${pojoTemp.totalCount }</td>
									<%-- <td>${pojoTemp.sinComCount }</td> --%>
									<td>${pojoTemp.mulComCount }</td>
									<td>${pojoTemp.blankCount }</td>
									<td>${pojoTemp.codeCount }</td>
									<td class="td-status">
										<span class="label label-success radius">${pojoTemp.codeTypeStr }</span>
									</td>
									<td class="td-status">
										<span class="label label-success radius">${pojoTemp.statusStr }</span>
									</td>
									<td>${pojoTemp.updateTime }</td>
									<td>${pojoTemp.pubTime }</td>
									<td class="f-14 td-manage">
										<!-- <a style="text-decoration: none" 
											onClick="article_stop(this,'10001')" href="javascript:;" title="下架">
											<i class="Hui-iconfont">&#xe6de;</i>
										</a> -->
										<a style="text-decoration: none" class="ml-5"
											onClick="article_add('代码统计编辑','${rootPath}/back/class/codeStatUpdate.htm?operType=update&id=${pojoTemp.id}','','510')"
											href="javascript:;" title="编辑">
											<i class="Hui-iconfont">&#xe6df;</i>
										</a>
										<a style="text-decoration: none" class="ml-5"
											onClick="article_add('代码统计上传文件','${rootPath}/back/class/codeStatUpdate.htm?operType=file&id=${pojoTemp.id}','','510')"
											href="javascript:;" title="代码统计上传文件">
											<i class="Hui-iconfont">&#xe63e;</i>
										</a>
										<a style="text-decoration: none" class="ml-5"
											onClick="article_add('代码统计明细','${rootPath}/back/class/codeDetailList.htm?codeStatId=${pojoTemp.id}','','510')"
											href="javascript:;" title="代码统计明细">
											<i class="Hui-iconfont">&#xe667;</i>
										</a>
										<!-- <a style="text-decoration: none" class="ml-5"
											onClick="article_del(this,'10001')" href="javascript:;"
											title="删除">
											<i class="Hui-iconfont">&#xe6e2;</i>
										</a> -->
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					
					<form action="${rootPath}/back/class/codeStatList.htm" method="post" id="pageForm">
						<input type="hidden" name="keyword" value="${requestScope.keyword }">
						<input type="hidden" name="status" value="${requestScope.status }">
						<input type="hidden" name="codeType" value="${requestScope.codeType }">
						<input type="hidden" name="orderType" value="${requestScope.orderType }">
						<input type="hidden" name="st" value="${requestScope.st }">
						<input type="hidden" name="ed" value="${requestScope.ed }">
						
						<div class="dataTables_info" id="DataTables_Table_0_info"
							role="status" aria-live="polite">
							共${pageInfoUtil.totalRecord }条,共${pageInfoUtil.totalPage}页
						</div>
						<div class="dataTables_paginate paging_simple_numbers"
							id="DataTables_Table_0_paginate">
							<a class="paginate_button previous disabled"
								aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '1', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_previous">
								首页
							</a>
							<a class="paginate_button previous disabled"
								aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.prePage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_previous">
								上一页
							</a>
							<a class="paginate_button next disabled" 
								aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.nextPage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_next">
								下一页
							</a>
							<a class="paginate_button next disabled" 
								aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.totalPage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_next">
								尾页
							</a>
							第<input type="text" class="paginate_button" id="currentPage" name="currentPage" value="${pageInfoUtil.currentPage }" size="5" maxlength="5">页
							每页<input type="text" class="paginate_button" id="pageSize" name="pageSize" value="${pageInfoUtil.pageSize }" size="5" maxlength="5">条
							<input type="submit" value="GO" class="paginate_button">
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
		
		<script type="text/javascript">
			$('.table-sort').dataTable({
				//"aaSorting" : [ [ 1, "desc" ] ],//默认第几个排序
				"bStateSave" : false,//状态保存
				"paging" : false,
				"info":false,
			});
	
			/*代码统计-添加*/
			function article_add(title, url, w, h) {
				var index = layer.open({
					type : 2,
					title : title,
					content : url
				});
				layer.full(index);
			}
			/*代码统计-删除*/
			function article_del(obj, id) {
				layer.confirm('确认要删除吗？', function(index) {
					$.ajax({
						type : 'POST',
						url : '',
						dataType : 'json',
						success : function(data) {
							$(obj).parents("tr").remove();
							layer.msg('已删除!', {
								icon : 1,
								time : 1000
							});
						},
						error : function(data) {
							console.log(data.msg);
						},
					});
				});
			}
		</script>
	</body>
</html>
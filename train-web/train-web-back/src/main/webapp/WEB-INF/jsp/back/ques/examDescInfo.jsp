<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one" />
<jsp:useBean class="com.wangsh.train.ques.pojo.AAsk" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看试卷概要 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 问题管理 
			<span class="c-gray en">&gt;</span> 查看问题
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/ques/examDescUpdateSubmit.htm">
				<input type="hidden" name="id" value="${one.id }">
				<input type="hidden" name="operType" value="${param.operType}">
				<div id="tab-system" class="HuiTab">
					<div class="tabBar cl">
						<span>基本信息</span>
						<span>单选列表</span>
						<span>多选列表</span>
						<span>判断列表</span>
						<span>问答列表</span>
						<span>编程列表</span>
						<span>面试题列表</span>
					</div>
					<div class="tabCon">
						<input type="hidden" name="id" value="${one.id }">
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>名称：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.name}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>创建人：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.admins.name}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>班级：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.cla.name}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>总分：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.score}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>难度：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.hardStar}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">开始时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.stTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">结束时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.edTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.pubTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>题型：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.examTech}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>类型：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.examTypeStr}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">状态：</label>
							<div class="formControls col-xs-8 col-sm-9 skin-minimal">
								${one.statusStr }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">内容：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.content}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.createTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.updateTime}
							</div>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.selDetailList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.multiSelDetailList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.judeDetailList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.askDetailList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.proDetailList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.interDetailList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<script type="text/javascript">
	$(function(){
		$('.skin-minimal input').iCheck({
			checkboxClass: 'icheckbox-blue',
			radioClass: 'iradio-blue',
			increaseArea: '20%'
		});
		$("#tab-system").Huitab({
			index:0
		});
	});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="sysPro" />
<jsp:useBean class="com.wangsh.train.system.pojo.ASysPro" id="sysProPojo"/>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/common/include/title.jsp"%>
<title>系统查看 - 培训机构</title>
</head>
<body>
	<article class="page-container">
		<form class="form form-horizontal" id="formSubmit"
			action="${rootPath }/back/system/sysProUpdateSubmit.htm">
			<input type="hidden" name="id" value="${sysPro.id }">
	
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2"> <span
					class="c-red"></span>名字：
				</label>
				<div class="formControls col-xs-8 col-sm-9">${sysPro.name}
				</div>
			</div>
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2"> <span
					class="c-red"></span>英文名字：
				</label>
				<div class="formControls col-xs-8 col-sm-9">${sysPro.engName}
				</div>
			</div>
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2"> <span
					class="c-red"></span>值：
				</label>
				<div class="formControls col-xs-8 col-sm-9">${sysPro.vals}
				</div>
			</div>
		
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">数据操作类型：</label>
				<div class="formControls col-xs-8 col-sm-9 skin-minimal">
					${sysPro.operTypeStr}</div>
			</div>
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">类型：</label>
				<div class="formControls col-xs-8 col-sm-9 skin-minimal">
					${sysPro.proTypeStr}</div>
			</div>
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2"> <span
					class="c-red"></span>描述信息：
				</label>
				<div class="formControls col-xs-8 col-sm-9">${sysPro.content}
				</div>
			</div>
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">标识字段：</label>
				<div class="formControls col-xs-8 col-sm-9 skin-minimal">
					${sysPro.statusStr}</div>
			</div>
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
				<div class="formControls col-xs-8 col-sm-9">
					${sysPro.createTime}</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
				<div class="formControls col-xs-8 col-sm-9">
					${sysPro.updateTime}</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
				<div class="formControls col-xs-8 col-sm-9">
					${sysPro.pubTime}</div>
			</div>
		</form>
	</article>
	<%@ include file="/common/include/footer.jsp"%>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one" />
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看问题 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 问题管理 
			<span class="c-gray en">&gt;</span> 查看问题
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/ques/askUpdateSubmit.htm">
				<div id="tab-system" class="HuiTab">
					<div class="tabBar cl">
						<span>基本信息</span>
						<span>选项列表</span>
					</div>
					<div class="tabCon">
						<input type="hidden" name="id" value="${one.id }">
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">
								<span class="c-red"></span>用户：
							</label>
							<div class="formControls col-xs-8 col-sm-9">
								<a class="href_info" href="${rootPath }/back/users/usersUpdate.htm?id=${one.usersId }"
									target="_blank">
									${one.users.nickName }
								</a>
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">
								<span class="c-red"></span>技术：
							</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.tech.name }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">
								<span class="c-red"></span>名称：
							</label>
							<div class="formControls col-xs-8 col-sm-9">
								<c:out value="${one.name }" escapeXml="true"/>
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">难易程度：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.hardStar}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">分数：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.score }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">回答正确：</label>
							<div class="formControls col-xs-8 col-sm-9">
								<a href="${rootPath }/head/users/usersUpdate.htm?id=${one.ansUsersId }" target="_blank">${one.ansUsers.nickName }</a>
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">类型：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.askTypeStr }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">来源：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.souTypeStr }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.pubTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">状态：</label>
							<div class="formControls col-xs-8 col-sm-9 skin-minimal">
								${one.statusStr }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
							<div class="formControls col-xs-8 col-sm-9 skin-minimal">
								${one.createTime }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
							<div class="formControls col-xs-8 col-sm-9 skin-minimal">
								${one.updateTime }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">审核时间：</label>
							<div class="formControls col-xs-8 col-sm-9 skin-minimal">
								${one.auditTime }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">审核内容：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.auditContent}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">知识点：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.examContent}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">答案：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.answer}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">内容：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.content}
							</div>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="80">编号</th>
									<th width="100">选项</th>
									<th width="60">状态</th>
									<th width="60" title="是否正确">正确</th>
									<th width="120">创建时间</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
									<%--<th width="120">操作</th>--%>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.ansSelList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td>${listTemp.sn }</td>
											<td><c:out value="${listTemp.name }" escapeXml="true"/></td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.corrTypeStr }</span>
											</td>
											<td>${listTemp.createTime }</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<script type="text/javascript">
	$(function(){
		$('.skin-minimal input').iCheck({
			checkboxClass: 'icheckbox-blue',
			radioClass: 'iradio-blue',
			increaseArea: '20%'
		});
		$("#tab-system").Huitab({
			index:0
		});
	});
</script>
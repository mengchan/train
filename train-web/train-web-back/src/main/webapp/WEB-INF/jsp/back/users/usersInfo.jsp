<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="pojo" />
<c:set value="${requestScope.classResponse.data.list }" var="clasList" />
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看用户 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 用户管理 
			<span class="c-gray en">&gt;</span> 查看用户 
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/demo/clasUpdateSubmit.htm">
				<input type="hidden" name="id" value="${clas.id }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">
						<span class="c-red"></span>ssoId：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojo.ssoId }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>班级：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojo.cla.name}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>邮箱：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojo.email}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">昵称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojo.nickName }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">真实姓名：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojo.trueName }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">阶段：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${pojo.stageTypeStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojo.statusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojo.createTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojo.updateTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">上次登陆时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojo.lastLoginTime }
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
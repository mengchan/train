<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>后台登录 - 培训机构</title>
		<link href="${rootPath}/common/resource/H-ui.admin/static/h-ui.admin/css/H-ui.login.css" rel="stylesheet" type="text/css" />
		<meta name="keywords" content="培训机构 keywords">
		<meta name="description" content="培训机构 description">
	</head>
	<body>
		<input type="hidden" id="TenantId" name="TenantId" value="" />
		<div class="header"></div>
		<div class="loginWraper">
			<div id="loginform" class="loginBox">
				<form class="form form-horizontal" action="${rootPath }/loginSubmit.htm" method="post">
					<input type="hidden" name="returnUrl" value="${param.returnUrl }">
					<div class="row cl">
						<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
						<div class="formControls col-xs-8">
							<input id="email" name="email" type="text" placeholder="邮箱" value="${requestScope.admins.email }"
								class="input-text size-L">
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i></label>
						<div class="formControls col-xs-8">
							<input id="password" name="password" type="password" placeholder="密码"
								class="input-text size-L">
						</div>
					</div>
					<c:if test="${sessionScope.failedCount > 3 }">
						<div class="row cl">
							<div class="formControls col-xs-8 col-xs-offset-3">
								<input class="input-text size-L" type="text" placeholder="验证码" name="code" id="code"
									style="width: 150px;">
								<a href="#" tabindex="-1" onclick="return refreshCode('imgCode');">
									<img id="imgCode" src="${rootPath }/common/randImg.htm" title="点击刷新验证码" width="100" height="35" />
								</a>
							</div>
						</div>
					</c:if>
					<div class="row cl">
						<div class="formControls col-xs-8 col-xs-offset-3">
							<label for="online"> <input type="checkbox" name="online"
								id="online" value=""> 使我保持登录状态
							</label>
						</div>
					</div>
					<div class="row cl">
						<div class="formControls col-xs-8 col-xs-offset-3">
							<input name="" type="submit" class="btn btn-success radius size-L"
								value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;"> <input
								name="" type="reset" class="btn btn-default radius size-L"
								value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
						</div>
					</div>
					<div class="row cl">
						<div class="formControls col-xs-8 col-xs-offset-3">
							<p class="error_info">${response.info }</p>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="footer">Copyright 你的公司名称 by 培训机构</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="pojo" />
<c:set value="${requestScope.usesResponse.data.one }" var="users" />
<c:set value="${requestScope.classResponse.data.list }" var="classList" />
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看用户 - 培训机构</title>
	</head>
	<body>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/users/usersClassUpdateSubmit.htm">
				<input type="hidden" name="id" value="${pojo.id }">
				<input type="hidden" name="operType" value="${param.operType }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>邮箱：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${users.email}
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">用户昵称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${users.nickName }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">用户真实姓名：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${users.trueName }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">班级：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<span class="select-box inline">
							<select name="classId" class="select">
								<c:forEach items="${classList }" var="listTemp" varStatus="stat">
									<option value="${listTemp.id }" ${pojo.classId == listTemp.id ? 'selected' : '' }>${listTemp.name }</option>
								</c:forEach>
							</select>
						</span>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" readonly="readonly" value="${pojo.pubTime }"
							onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							id="pubTimeStr" name="pubTimeStr" class="input-text Wdate" style="width: 160px;">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">类型：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:set value="1" var="currCount"/>
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'CLASSTYPE')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="classType" value="${keys[1] }" ${pojo.classType == keys[1] ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
								<c:set value="${currCount + 1 }" var="currCount"/>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">默认类型：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:set value="1" var="currCount"/>
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'DEFTYPE')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="defType" value="${keys[1] }" ${pojo.defType == keys[1] ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
								<c:set value="${currCount + 1 }" var="currCount"/>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:set value="1" var="currCount"/>
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'STATUS')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="status" value="${keys[1] }" ${pojo.status == keys[1] ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
								<c:set value="${currCount + 1 }" var="currCount"/>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojo.createTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${pojo.updateTime }
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<button 
							class="btn btn-primary radius" type="submit">
							<i class="Hui-iconfont">&#xe632;</i> 保存
						</button>
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
		<script type="text/javascript">
			$(function(){
				$('.skin-minimal input').iCheck({
					checkboxClass: 'icheckbox-blue',
					radioClass: 'iradio-blue',
					increaseArea: '20%'
				});
				
				//表单验证
				$("#formSubmit").validate({
					rules:{
						name:{
							required:true,
						},
						capital:{
							required:true,
						},
						stYear:{
							required:true,
						},
						edYear:{
							required:true,
						}
			
					},
					onkeyup:false,
					focusCleanup:true,
					success:"valid",
					submitHandler:function(form){
						$(form).ajaxSubmit({
							type: 'post',
							success: function(data){
								/* 弹出框 */
								layer.msg(data.info,{icon:1,time:3000},function()
								{
									if(data.code == '0')
									{
										var index = parent.layer.getFrameIndex(window.name);
										parent.$('.btn-refresh').click();
										parent.layer.close(index);
									}
								});
							},
							dataType:"json"
						});
					}
				});
				/* 初始化百度编辑器 */
				var ue = UE.getEditor('editor');
			});
		</script>
	</body>
</html>
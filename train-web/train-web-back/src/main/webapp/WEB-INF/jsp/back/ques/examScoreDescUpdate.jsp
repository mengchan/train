<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one" />
<jsp:useBean class="com.wangsh.train.ques.pojo.AExamDesc" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>修改成绩概要 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 问题管理 
			<span class="c-gray en">&gt;</span> 修改成绩
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/ques/examScoreDescUpdateSubmit.htm">
				<input type="hidden" name="id" value="${one.id }">
				<input type="hidden" name="operType" value="${requestScope.operType}">
				
				<div class="row cl">
					<div class="formControls col-xs-11 col-sm-11">
						客观题分数:<span id="keFen" class="error_info">${one.score}</span>
						主观题分数:<span id="zhuFen" class="error_info"></span>
						总分:<span id="zongFen" class="error_info"></span>
						<button 
							class="btn btn-primary radius" type="submit">
							<i class="Hui-iconfont">&#xe632;</i> 保存
						</button>
					</div>
				</div>
				
				<div id="tab-system" class="HuiTab">
					<div class="tabBar cl">
						<span>基本信息</span>
						<c:if test="${fn:length(one.selDetailList) > 0 }">
							<span>单选列表(每题${sys_map.ques_sel.vals }分)</span>
						</c:if>
						<c:if test="${fn:length(one.multiSelDetailList) > 0 }">
							<span>多选列表(每题${sys_map.ques_multiSel_score.vals }分)</span>
						</c:if>
						<c:if test="${fn:length(one.judeDetailList) > 0 }">
							<span>判断列表(每题${sys_map.ques_judge_score.vals }分)</span>
						</c:if>
						<c:if test="${fn:length(one.askDetailList) > 0  && one.examDesc.examType != pojo.enums['EXAMTYPE_INTERVIEW']}">
							<span>问答列表(每题${sys_map.ques_ask.vals }分)</span>
						</c:if>
						<c:if test="${fn:length(one.proDetailList) > 0 }">
							<span>编程列表(每题${sys_map.ques_pro.vals }分)</span>
						</c:if>
						<c:if test="${fn:length(one.interDetailList) > 0 && one.examDesc.examType == pojo.enums['EXAMTYPE_INTERVIEW']}">
							<span>面试题列表(每题${sys_map.ques_ask_inter_score.vals }分)</span>
						</c:if>
					</div>
					<div class="tabCon">
						<input type="hidden" name="id" value="${one.id }">
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>姓名：</label>
							<div class="formControls col-xs-8 col-sm-9">
								<a class="ml-5 href_info"
									data-href="${rootPath}/back/users/usersUpdate.htm?id=${one.usersId }"
									onclick="Hui_admin_tab(this)"
									href="javascript:;" data-title="${one.users.trueName }" title="${one.users.trueName }">
									${one.users.trueName }
								</a>
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>班级：</label>
							<div class="formControls col-xs-8 col-sm-9">
								<a class="ml-5 href_info"
									data-href="${rootPath}/back/class/classUpdate.htm?id=${one.usersClass.classId}"
									onclick="Hui_admin_tab(this)"
									href="javascript:;" data-title="${one.users.trueName }" title="${one.users.trueName }">
									${one.usersClass.cla.name }
								</a>
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>试卷：</label>
							<div class="formControls col-xs-8 col-sm-9">
								<a class="ml-5 href_info"
									data-href="${rootPath}/back/ques/examDescUpdate.htm?id=${one.examId}"
									onclick="Hui_admin_tab(this)"
									href="javascript:;" data-title="${one.examDesc.name}" title="${one.examDesc.name}">
									${one.examDesc.name}
								</a>
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>改卷人：</label>
							<div class="formControls col-xs-8 col-sm-9">
								<a class="ml-5 href_info"
									data-href="${rootPath}/back/admins/adminsUpdate.htm?id=${one.adminsId }"
									onclick="Hui_admin_tab(this)"
									href="javascript:;" data-title="${one.examDesc.name}" title="${one.examDesc.name}">
									${one.admins.name}
								</a>
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">名称：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.name}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">分数：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.score}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">状态：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.statusStr}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">内容：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.content}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.pubTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">交卷时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.commitTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.createTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.updateTime}
							</div>
						</div>
					</div>
					
					<c:if test="${fn:length(one.selDetailList) > 0 }">
						<div class="tabCon">
							<div class="row cl">
								<table
										class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
									<thead>
										<tr class="text-c">
											<th width="50">序号</th>
											<th width="50">Id</th>
											<th width="100">名称</th>
											<th width="50">分数</th>
											<th width="50">类型</th>
											<th width="50">状态</th>
											<th width="120">创建时间</th>
											<th width="120">更新时间</th>
											<th width="120">发布时间</th>
											<th width="120">操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${one.selDetailList }" var="listTemp" varStatus="stat">
											<tr class="text-c">
												<td>${stat.count }</td>
												<td>${listTemp.id }</td>
												<td class="text-l" title="${listTemp.name }">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														${fn:substring(listTemp.name,0,subStrLen) }
													</a>
												</td>
												<td>
													${listTemp.score }
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.askTypeStr }</span>
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.statusStr }</span>
												</td>
												<td>${listTemp.createTime }</td>
												<td>${listTemp.updateTime }</td>
												<td>${listTemp.pubTime }</td>
												<td>
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														查看答案
													</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</c:if>
					<c:if test="${fn:length(one.multiSelDetailList) > 0 }">
						<div class="tabCon">
							<div class="row cl">
								<table
										class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
									<thead>
										<tr class="text-c">
											<th width="50">序号</th>
											<th width="50">Id</th>
											<th width="100">名称</th>
											<th width="50">分数</th>
											<th width="50">类型</th>
											<th width="50">状态</th>
											<th width="120">创建时间</th>
											<th width="120">更新时间</th>
											<th width="120">发布时间</th>
											<th width="120">操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${one.multiSelDetailList }" var="listTemp" varStatus="stat">
											<tr class="text-c">
												<td>${stat.count }</td>
												<td>${listTemp.id }</td>
												<td class="text-l" title="${listTemp.name }">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														${fn:substring(listTemp.name,0,subStrLen) }
													</a>
												</td>
												<td>
													${listTemp.score }
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.askTypeStr }</span>
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.statusStr }</span>
												</td>
												<td>${listTemp.createTime }</td>
												<td>${listTemp.updateTime }</td>
												<td>${listTemp.pubTime }</td>
												<td>
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														查看答案
													</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</c:if>
					<c:if test="${fn:length(one.judeDetailList) > 0 }">
						<div class="tabCon">
							<div class="row cl">
								<table
										class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
									<thead>
										<tr class="text-c">
											<th width="50">序号</th>
											<th width="50">Id</th>
											<th width="100">名称</th>
											<th width="50">分数</th>
											<th width="50">类型</th>
											<th width="50">状态</th>
											<th width="120">创建时间</th>
											<th width="120">更新时间</th>
											<th width="120">发布时间</th>
											<th width="120">操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${one.judeDetailList }" var="listTemp" varStatus="stat">
											<tr class="text-c">
												<td>${stat.count }</td>
												<td>${listTemp.id }</td>
												<td class="text-l" title="${listTemp.name }">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														${fn:substring(listTemp.name,0,subStrLen) }
													</a>
												</td>
												<td>
													${listTemp.score }
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.askTypeStr }</span>
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.statusStr }</span>
												</td>
												<td>${listTemp.createTime }</td>
												<td>${listTemp.updateTime }</td>
												<td>${listTemp.pubTime }</td>
												<td>
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														查看答案
													</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</c:if>
					
					<c:if test="${fn:length(one.askDetailList) > 0 && one.examDesc.examType != pojo.enums['EXAMTYPE_INTERVIEW']}">
						<div class="tabCon">
							<div class="row cl">
								<table
										class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
									<thead>
										<tr class="text-c">
											<th width="50">序号</th>
											<th width="50">Id</th>
											<th width="100">名称</th>
											<th width="50">分数</th>
											<th width="50">类型</th>
											<th width="50">状态</th>
											<th width="120">创建时间</th>
											<th width="120">更新时间</th>
											<th width="120">发布时间</th>
											<th width="120">操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${one.askDetailList }" var="listTemp" varStatus="stat">
											<tr class="text-c">
												<td>${stat.count }</td>
												<td>${listTemp.id }</td>
												<td class="text-l" title="${listTemp.name }">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														${fn:substring(listTemp.name,0,subStrLen) }
													</a>
												</td>
												<td>
													${listTemp.score }
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.askTypeStr }</span>
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.statusStr }</span>
												</td>
												<td>${listTemp.createTime }</td>
												<td>${listTemp.updateTime }</td>
												<td>${listTemp.pubTime }</td>
												<td>
													<input type="text" name="detailId_${listTemp.id }" value="${listTemp.score }" size="5" maxlength="5" 
													onchange="return calTotalScore('${sys_map.ques_ask.vals }')">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														查看答案
													</a>
												</td>
											</tr>
											<tr class="text-c">
												<td colspan="2">您的答案:</td>
												<td colspan="8">
													${listTemp.answer }
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</c:if>
					<c:if test="${fn:length(one.proDetailList) > 0 }">
						<div class="tabCon">
							<div class="row cl">
								<table
										class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
									<thead>
										<tr class="text-c">
											<th width="50">序号</th>
											<th width="50">Id</th>
											<th width="100">名称</th>
											<th width="50">分数</th>
											<th width="50">类型</th>
											<th width="50">状态</th>
											<th width="120">创建时间</th>
											<th width="120">更新时间</th>
											<th width="120">发布时间</th>
											<th width="120">操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${one.proDetailList }" var="listTemp" varStatus="stat">
											<tr class="text-c">
												<td>${stat.count }</td>
												<td>${listTemp.id }</td>
												<td class="text-l" title="${listTemp.name }">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														${fn:substring(listTemp.name,0,subStrLen) }
													</a>
												</td>
												<td>
													${listTemp.score }
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.askTypeStr }</span>
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.statusStr }</span>
												</td>
												<td>${listTemp.createTime }</td>
												<td>${listTemp.updateTime }</td>
												<td>${listTemp.pubTime }</td>
												<td>
													<input type="text" name="detailId_${listTemp.id }" value="${listTemp.score }" size="5" maxlength="5" 
													onchange="return calTotalScore('${sys_map.ques_pro.vals }')">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														查看答案
													</a>
												</td>
											</tr>
											<tr class="text-c">
												<td colspan="2">您的答案:</td>
												<td colspan="8">
													${listTemp.answer }
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</c:if>
					
					<c:if test="${fn:length(one.interDetailList) > 0 && one.examDesc.examType == pojo.enums['EXAMTYPE_INTERVIEW']}">
						<div class="tabCon">
							<div class="row cl">
								<table
										class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
									<thead>
										<tr class="text-c">
											<th width="50">序号</th>
											<th width="50">Id</th>
											<th width="100">名称</th>
											<th width="50">分数</th>
											<th width="50">类型</th>
											<th width="50">状态</th>
											<th width="120">创建时间</th>
											<th width="120">更新时间</th>
											<th width="120">发布时间</th>
											<th width="120">操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${one.interDetailList }" var="listTemp" varStatus="stat">
											<tr class="text-c">
												<td>${stat.count }</td>
												<td>${listTemp.id }</td>
												<td class="text-l" title="${listTemp.name }">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														${fn:substring(listTemp.name,0,subStrLen) }
													</a>
												</td>
												<td>
													${listTemp.score }
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.askTypeStr }</span>
												</td>
												<td class="td-status">
													<span class="label label-success radius">${listTemp.statusStr }</span>
												</td>
												<td>${listTemp.createTime }</td>
												<td>${listTemp.updateTime }</td>
												<td>${listTemp.pubTime }</td>
												<td>
													<input type="text" name="detailId_${listTemp.id }" value="${listTemp.score }" size="5" maxlength="5" 
													onchange="return calTotalScore('${sys_map.ques_ask_inter_score.vals }')">
													<a class="ml-5 href_info"
														data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.askId}"
														onclick="Hui_admin_tab(this)"
														href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
														查看答案
													</a>
												</td>
											</tr>
											<tr class="text-c">
												<td colspan="2">您的答案:</td>
												<td colspan="8">
													${listTemp.answer }
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:if>
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<script type="text/javascript">
	$(function(){
		$('.skin-minimal input').iCheck({
			checkboxClass: 'icheckbox-blue',
			radioClass: 'iradio-blue',
			increaseArea: '20%'
		});
		$("#tab-system").Huitab({
			index:0
		});
		
		//表单验证
		$("#formSubmit").validate({
			rules:{
				name:{
					required:true,
				},
				score:{
					required:true,
					isDigits:true,
				}
			},
			onkeyup:false,
			focusCleanup:true,
			success:"valid",
			submitHandler:function(form){
				$(form).ajaxSubmit({
					type: 'post',
					success: function(data){
						/* 弹出框 */
						layer.msg(data.info,{icon:1,time:3000},function()
						{
							
						});
					},
					dataType:"json"
				});
			}
		});
	});
	
	/**
		计算总分
	*/
	function calTotalScore(maxScore)
	{
		var inputs$ = $("#formSubmit input[name^=detailId_]") ;
		/* 客观分 */
		var keFen = $("#keFen").html();
		
		var totalScore = 0 ; 
		/* 循环数据 */
		inputs$.each(function(index){
			/* 最大分;用户输入的最大分 */
			var tempScore = parseInt($(this).val());
			maxScore = parseInt(maxScore);
			if(tempScore > maxScore)
			{
				tempScore = maxScore; 
			}
			$(this).val(tempScore);
			totalScore += tempScore ; 
		});
		/* 主观分 */
		$("#zhuFen").html(totalScore);
		/* 总分 */
		$("#zongFen").html(totalScore + parseInt(keFen));
		return false ; 
	}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看技术 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 系统模块管理 
			<span class="c-gray en">&gt;</span> 查看技术
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/system/sysTechUpdateSubmit.htm">
				<input type="hidden" name="id" value="${one.id }">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>分类Id：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:choose>
							<c:when test="${fn:contains(one.cateJSON.treeName,'|-->')}">
								${fn:replace(one.cateJSON.treeName,'|-->','  ')}
							</c:when>
							<c:otherwise>
								${one.cateJSON.treeName}
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>名称：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.name }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>官网：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<a href="${one.website }" target="_blank">${one.website }</a>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>课时：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.clasTime }
						(${one.clasTimeStr })
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.pubTime }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						${one.statusStr }
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						${one.content}
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
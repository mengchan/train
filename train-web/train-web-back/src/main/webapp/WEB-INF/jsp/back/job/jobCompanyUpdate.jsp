<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.job.pojo.AJobCompany" id="pojo"/>
<c:set value="${requestScope.response.data.one }" var="pojoTemp"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新公司 - 培训机构</title>
	</head>
	<body>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/job/jobCompanyUpdateSubmit.htm">
				<input type="hidden" name="id" value="${pojoTemp.id }">
				<input type="hidden" name="operType" value="${param.operType}">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">名字：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  value="${pojoTemp.name }" placeholder="请输入名字"
							id="name" name="name" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">网站：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  value="${pojoTemp.website }" placeholder="请输入网站"
							id="website" name="website" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2"><span
						class="c-red">*</span>地区：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<c:choose>
							<c:when test="${fn:contains(pojoTemp.regionJSON.treeName,'|-->')}">
								${fn:replace(pojoTemp.regionJSON.treeName,'|-->','  ')}
							</c:when>
							<c:otherwise>
								${pojoTemp.regionJSON.treeName}
							</c:otherwise>
						</c:choose>
						<input type="hidden" name="regionId" id="regionId" value="${pojoTemp.regionId}">
						<br/>
						<span>
							<select id="region1" class="custom-select" onchange="return selectRegion('region1','region2','regionId','false');">
								<option value="0">请选择</option>
							</select>
							<select id="region2" class="custom-select" onchange="return selectRegion('region2','region3','regionId','false');">
							</select>
							<select id="region3" class="custom-select" onchange="return selectRegion('region3','region4','regionId','false');">
							</select>
							<select id="region4" class="custom-select" onchange="return selectRegion('region4','region5','regionId','false');">
							</select>
							<select id="region5" class="custom-select" onchange="return selectRegion('region5','region6','regionId','false');">
							</select>
							<select id="region6" class="custom-select" onchange="return selectRegion('region6','region7','regionId','false');">
							</select>
						</span>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">地址：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  value="${pojoTemp.address }" placeholder="请输入地址"
							id="address" name="address" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">联系人：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  value="${pojoTemp.contact }" placeholder="请输入联系人"
							id="website" name="contact" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">手机：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  value="${pojoTemp.phone }" placeholder="请输入手机"
							id="website" name="phone" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" readonly="readonly" value="${pojoTemp.pubTime }"
							onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							id="pubTimeStr" name="pubTimeStr" class="input-text Wdate" style="width: 160px;">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">状态：</label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<c:set value="1" var="currCount"/>
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'STATUS')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<div class="radio-box">
									<label for="${me.key }">
										<input type="radio" id="${me.key }" name="status" value="${keys[1] }" ${pojoTemp.status == keys[1] ? 'checked' : '' }>
										&nbsp;${me.value }
									</label>
								</div>
								<c:set value="${currCount + 1 }" var="currCount"/>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">内容：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<script name="content" id="editor" type="text/plain"
							style="width: 100%; height: 200px;">${pojoTemp.content }</script>
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<button 
							class="btn btn-primary radius" type="submit">
							<i class="Hui-iconfont">&#xe632;</i> 保存
						</button>
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
		<script type="text/javascript">
			$(function(){
				$('.skin-minimal input').iCheck({
					checkboxClass: 'icheckbox-blue',
					radioClass: 'iradio-blue',
					increaseArea: '20%'
				});
				
				//表单验证
				$("#formSubmit").validate({
					rules:{
						name:{
							required:true,
						},
						website:{
							required:true,
						},
						address:{
							required:true,
						},
						contact:{
							required:true,
						},
						phone:{
							required:true,
							isPhone:true,
						}
					},
					onkeyup:false,
					focusCleanup:true,
					success:"valid",
					submitHandler:function(form){
						$(form).ajaxSubmit({
							type: 'post',
							success: function(data){
								/* 弹出框 */
								layer.msg(data.info,{icon:1,time:3000},function()
								{
									if(data.code == '0')
									{
										var index = parent.layer.getFrameIndex(window.name);
										parent.$('.btn-refresh').click();
										parent.layer.close(index);
									}
								});
							},
							dataType:"json"
						});
					}
				});
				/* 初始化百度编辑器 */
				var ue = UE.getEditor('editor');
				
				/* 查询一级节点 */
				selectRegion('region1','region1','','true');
			});
		</script>
	</body>
</html>
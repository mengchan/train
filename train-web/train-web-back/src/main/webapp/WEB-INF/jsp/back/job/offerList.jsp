<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.pageInfoUtil }" var="pageInfoUtil"/>
<c:set value="${requestScope.response.data.list }" var="list"/>
<jsp:useBean class="com.wangsh.train.job.pojo.AJobOffer" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>offer列表 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 工作管理 
			<span class="c-gray en">&gt;</span> offer列表 
			<a class="btn btn-success radius r btn-refresh"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<div class="page-container">
			<form action="${rootPath}/back/job/offerList.htm" method="post">
				<input type="hidden" name="companyId" value="${requestScope.companyId }">
				
				<div class="text-c">
					<!-- <button onclick="removeIframe()" class="btn btn-primary radius">关闭</button> -->
					<input type="text" name="keyword" id="" placeholder=" 关键字" value="${requestScope.keyword }"
						style="width: 250px" class="input-text">
					<span class="select-box inline">
						五险:
						<select name="fiveRisks" class="select">
							<option value="">请选择</option>
							<c:set value="1" var="currCount"/>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'FIVERISKS')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${keys[1] == requestScope.fiveRisks ? 'selected' : '' }>${me.value }</option>
									<c:set value="${currCount + 1 }" var="currCount"/>
								</c:if>
							</c:forEach>
						</select>
					</span>
					<span class="select-box inline">
						类别:
						<select name="accFund" class="select">
							<option value="">请选择</option>
							<c:set value="1" var="currCount"/>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'ACCFUND')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${keys[1] == requestScope.accFund ? 'selected' : '' }>${me.value }</option>
									<c:set value="${currCount + 1 }" var="currCount"/>
									&nbsp;&nbsp;
								</c:if>
							</c:forEach>
						</select>
					</span>
					<span class="select-box inline">
						状态:
						<select name="status" class="select">
							<option value="">请选择</option>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'STATUS')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
					</span>
					日期范围：
					<input type="text" name="st" value="${requestScope.st }" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						id="logmin" readonly="readonly" class="input-text Wdate" style="width: 160px;">
					- 
					<input type="text" name="ed" value="${requestScope.ed }" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						id="logmax" readonly="readonly" class="input-text Wdate" style="width: 160px;">
					<button name="" id="" class="btn btn-success" type="submit">
						<i class="Hui-iconfont">&#xe665;</i> 搜索
					</button>
				</div>
			</form>
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				<span class="l">
					<a href="javascript:;" onclick="datadel()"
						class="btn btn-danger radius">
						<i class="Hui-iconfont">&#xe6e2;</i>
						批量删除
					</a>
					<!-- 新选项卡打开 -->
					<%-- <a class="btn btn-primary radius" data-title="添加offer"
						data-href="${rootPath }/back/job/offerInsert.htm" onclick="Hui_admin_tab(this)"
						href="javascript:;">
						<i class="Hui-iconfont">&#xe600;</i> 
						添加offer
					</a> --%>
					<%-- <a class="btn btn-primary radius" data-title="添加offer"
						data-href="${rootPath }/back/job/offerInsert.htm" onclick="article_add('添加offer','${rootPath }/back/job/offerInsert.htm','','510')"
						href="javascript:;">
						<i class="Hui-iconfont">&#xe600;</i> 
						添加offer
					</a> --%>
				</span> 
				<span class="r">
					共有数据：<strong>${pageInfoUtil.totalRecord }</strong> 条
				</span>
			</div>
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				批量操作类型:
				<span class="select-box inline">
					<select name="operType" class="select" id="operType" onchange="batchSwitch('operType')">
						<option value="">请选择</option>
						<option value="status">批量更新状态</option>
					</select>
				</span>
				<span class="select-box inline selectInfo" id="updatestatus" style="display: none;">
					状态:
					<select name="status" class="select" id="statusBatch">
						<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
							<c:if test="${fn:startsWith(me.key,'STATUS')}">
								<!-- 拆分,按照-拆分,取最后一个 -->
								<c:set value="${fn:split(me.key, '-')}" var="keys"/>
								<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
							</c:if>
						</c:forEach>
					</select>
				</span>
				<a href="javascript:;" onclick="return batchOper('ids','${rootPath}/back/job/offerBatch.htm','operType='+ $('#operType').val() +'&status=' + $('#statusBatch').val() + '&teaId=' + $('#teaIdBatch').val() + '&askType=' + $('#askTypeBatch').val())"
					class="btn btn-danger radius">
					<i class="Hui-iconfont">&#xe6e2;</i>
					提交
				</a>
			</div>
			<div class="mt-20">
				<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
					<table
						class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
						<thead>
							<tr class="text-c">
								<th width="25"><input type="checkbox" name="" value=""></th>
								<th width="50">序号</th>
								<th width="50">Id</th>
								<th width="80">创建人</th>
								<th width="80">公司</th>
								<th width="80">职位</th>
								<th width="100">名称</th>
								<th width="50">工资</th>
								<th width="50">五险</th>
								<th width="50">公积金</th>
								<th width="50" title="面试次数">面次</th>
								<th width="50">状态</th>
								<th width="120">更新时间</th>
								<th width="120">发布时间</th>
								<th width="100">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list }" var="listTemp" varStatus="stat">
								<tr class="text-c">
									<td><input type="checkbox" value="${listTemp.id}" name="ids"></td>
									<td>${stat.count }</td>
									<td>${listTemp.id }</td>
									<td class="text-l" title="${listTemp.create.nickName }">
										<a style="text-decoration: none" class="ml-5 href_info"
											data-href="${rootPath}/back/users/usersUpdate.htm?id=${listTemp.createId }"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="查看个人信息_${listTemp.create.nickName }" title="查看个人信息_${listTemp.create.nickName }">
											${fn:substring(listTemp.create.nickName ,0,subStrLen) }
										</a>
									</td>
									<td title="${listTemp.company.name }">
										<a style="text-decoration: none" class="ml-5 href_info"
											data-href="${rootPath}/back/job/jobCompanyUpdate.htm?id=${listTemp.companyId }"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="查看公司信息_${listTemp.company.name }" title="查看公司信息_${listTemp.company.name }">
											${fn:substring(listTemp.company.name,0,subStrLen) }
										</a>
									</td>
									<td title="${listTemp.comPosition.name }">
										<a style="text-decoration: none" class="ml-5 href_info"
											data-href="${rootPath}/back/job/comPositionUpdate.htm?id=${listTemp.comPosiId }"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="查看职位信息_${listTemp.company.name }" title="查看职位信息_${listTemp.company.name }">
											${fn:substring(listTemp.comPosition.name,0,subStrLen) }
										</a>
									</td>
									<td title="${listTemp.name }">
										<a style="text-decoration: none" class="ml-5 href_info"
											data-href="${rootPath}/back/job/offerUpdate.htm?id=${listTemp.id }"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="查看offer_${listTemp.company.name }" title="查看offer_${listTemp.company.name }">
											${fn:substring(listTemp.name,0,subStrLen) }
										</a>
									</td>
									<td title="${listTemp.wages }">
										${listTemp.wages}
									</td>
									<td>
										${listTemp.fiveRisksStr }
									</td>
									<td>
										${listTemp.accFundStr }
									</td>
									<td>${listTemp.interCount }</td>
									<td class="td-status">
										<span class="label label-success radius">${listTemp.statusStr }</span>
									</td>
									<td>${listTemp.updateTime }</td>
									<td>${listTemp.pubTime }</td>
									<td class="f-14 td-manage">
										<%-- <a style="text-decoration: none" class="ml-5"
											data-href="${rootPath}/back/ques/kingList.htm?askId=${listTemp.id}"
											onclick="Hui_admin_tab(this)"
											href="javascript:;" data-title="${listTemp.name }的皇上列表" title="${listTemp.name }的皇上列表">
											<i class="Hui-iconfont">&#xe667;</i>
										</a> --%>
										<c:if test="${listTemp.statusEnum == 'STATUS_PUBLISH'}">
											<a style="text-decoration: none" class="ml-5 href_info"
												data-href="${rootPath}/back/job/offerUpdate.htm?operType=audit&id=${listTemp.id}"
												onclick="Hui_admin_tab(this)"
												href="javascript:;" data-title="offer审核_${listTemp.name}" title="offer审核_${listTemp.name}">
												<i class="Hui-iconfont">&#xe6e1;</i>
											</a>
										</c:if>
										<!-- <a style="text-decoration: none" class="ml-5"
											onClick="article_del(this,'10001')" href="javascript:;"
											title="删除">
											<i class="Hui-iconfont">&#xe6e2;</i>
										</a> -->
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					
					<form action="${rootPath}/back/job/offerList.htm" method="post" id="pageForm">
						<input type="hidden" name="keyword" value="${requestScope.keyword }">
						<input type="hidden" name="status" value="${requestScope.status }">
						<input type="hidden" name="st" value="${requestScope.st }">
						<input type="hidden" name="ed" value="${requestScope.ed }">
						
						<input type="hidden" name="companyId" value="${requestScope.companyId }">
						<input type="hidden" name="fiveRisks" value="${requestScope.fiveRisks }">
						<input type="hidden" name="accFund" value="${requestScope.accFund }">
						
						<div class="dataTables_info" id="DataTables_Table_0_info"
							role="status" aria-live="polite">
							共${pageInfoUtil.totalRecord }条,共${pageInfoUtil.totalPage}页
						</div>
						<div class="dataTables_paginate paging_simple_numbers"
							id="DataTables_Table_0_paginate">
							<a class="paginate_button previous disabled"
								aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '1', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_previous">
								首页
							</a>
							<a class="paginate_button previous disabled"
								aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.prePage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_previous">
								上一页
							</a>
							<a class="paginate_button next disabled" 
								aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.nextPage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_next">
								下一页
							</a>
							<a class="paginate_button next disabled" 
								aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0"
								onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.totalPage}', 'pageSize','${pageInfoUtil.pageSize}')"
								id="DataTables_Table_0_next">
								尾页
							</a>
							第<input type="text" class="paginate_button" id="currentPage" name="currentPage" value="${pageInfoUtil.currentPage }" size="5" maxlength="5">页
							每页<input type="text" class="paginate_button" id="pageSize" name="pageSize" value="${pageInfoUtil.pageSize }" size="5" maxlength="5">条
							<input type="submit" value="GO" class="paginate_button">
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
		
		<script type="text/javascript">
			$('.table-sort').dataTable({
				//"aaSorting" : [ [ 1, "desc" ] ],//默认第几个排序
				"bStateSave" : false,//状态保存
				"paging" : false,
				"info":false,
			});
	
			/*offer-添加*/
			function article_add(title, url, w, h) {
				var index = layer.open({
					type : 2,
					title : title,
					content : url
				});
				layer.full(index);
			}
			/*offer-删除*/
			function article_del(obj, id) {
				layer.confirm('确认要删除吗？', function(index) {
					$.ajax({
						type : 'POST',
						url : '',
						dataType : 'json',
						success : function(data) {
							$(obj).parents("tr").remove();
							layer.msg('已删除!', {
								icon : 1,
								time : 1000
							});
						},
						error : function(data) {
							console.log(data.msg);
						},
					});
				});
			}
			
			/*
				批量操作切换
			*/
			function batchSwitch(selId)
			{
				var selVal = $("#" + selId).val();
				/* 所有的选择信息都隐藏掉 */
				$(".selectInfo").hide();
				
				if('status' == selVal)
				{
					$("#update" + selVal).show();
				}else if('teaId' == selVal)
				{
					$("#update" + selVal).show();
				}else if('askType' == selVal)
				{
					$("#update" + selVal).show();
				}
			}
		</script>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="one"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>试卷概要列表 - 培训机构</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 问题管理 
			<span class="c-gray en">&gt;</span> 查看问题
			<a class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新">
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<article class="page-container">
			<form class="form form-horizontal" id="formSubmit" action="${rootPath }/back/ques/examDescUpdateSubmit.htm">
				<input type="hidden" name="id" value="${one.id }">
				<input type="hidden" name="operType" value="${param.operType}">
				<button 
					class="btn btn-primary radius" type="submit">
					<i class="Hui-iconfont">&#xe632;</i> 保存
				</button>
				<div id="tab-system" class="HuiTab">
					<div class="tabBar cl">
						<span>基本信息</span>
						<span>单选列表</span>
						<span>多选列表</span>
						<span>判断列表</span>
						<span>问答列表</span>
						<span>编程列表</span>
						<span>面试题列表</span>
					</div>
					<div class="tabCon">
						<input type="hidden" name="id" value="${one.id }">
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>名称：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.name}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>总分：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.score}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>难度：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.hardStar}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">开始时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.stTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">结束时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.edTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">发布时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.pubTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>题型：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.examTech}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2"><span
								class="c-red">*</span>类型：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.examTypeStr}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">状态：</label>
							<div class="formControls col-xs-8 col-sm-9 skin-minimal">
								${one.statusStr }
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">内容：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.content}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">创建时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.createTime}
							</div>
						</div>
						<div class="row cl">
							<label class="form-label col-xs-4 col-sm-2">更新时间：</label>
							<div class="formControls col-xs-8 col-sm-9">
								${one.updateTime}
							</div>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="25"><input type="checkbox" name="" value=""></th>
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="80">创建人</th>
									<th width="80">技术</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">来源</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.selResultList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td><input type="checkbox" value="${listTemp.id}" name="ids" checked="checked"></td>
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l">
												<u style="cursor: pointer" class="text-primary href_info"
													onClick="article_add('查看','${rootPath}/back/users/usersUpdate.htm?id=${listTemp.usersId }','','510')" title="查看">
													${fn:substring(listTemp.users.nickName,0,subStrLen) }
												</u>
											</td>
											<td title="${listTemp.tech.name }">
												${fn:substring(listTemp.tech.name,0,subStrLen) }
											</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.id}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.souTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="25"><input type="checkbox" name="" value=""></th>
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="80">创建人</th>
									<th width="80">技术</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">来源</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.multiSelResultList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td><input type="checkbox" value="${listTemp.id}" name="ids" checked="checked"></td>
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l">
												<u style="cursor: pointer" class="text-primary href_info"
													onClick="article_add('查看','${rootPath}/back/users/usersUpdate.htm?id=${listTemp.usersId }','','510')" title="查看">
													${fn:substring(listTemp.users.nickName,0,subStrLen) }
												</u>
											</td>
											<td title="${listTemp.tech.name }">
												${fn:substring(listTemp.tech.name,0,subStrLen) }
											</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.id}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.souTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="25"><input type="checkbox" name="" value=""></th>
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="80">创建人</th>
									<th width="80">技术</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">来源</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.judeResultList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td><input type="checkbox" value="${listTemp.id}" name="ids" checked="checked"></td>
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l">
												<u style="cursor: pointer" class="text-primary href_info"
													onClick="article_add('查看','${rootPath}/back/users/usersUpdate.htm?id=${listTemp.usersId }','','510')" title="查看">
													${fn:substring(listTemp.users.nickName,0,subStrLen) }
												</u>
											</td>
											<td title="${listTemp.tech.name }">
												${fn:substring(listTemp.tech.name,0,subStrLen) }
											</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.id}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.souTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="25"><input type="checkbox" name="" value=""></th>
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="80">创建人</th>
									<th width="80">技术</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">来源</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.askResultList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td><input type="checkbox" value="${listTemp.id}" name="ids" checked="checked"></td>
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l">
												<u style="cursor: pointer" class="text-primary href_info"
													onClick="article_add('查看','${rootPath}/back/users/usersUpdate.htm?id=${listTemp.usersId }','','510')" title="查看">
													${fn:substring(listTemp.users.nickName,0,subStrLen) }
												</u>
											</td>
											<td title="${listTemp.tech.name }">
												${fn:substring(listTemp.tech.name,0,subStrLen) }
											</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.id}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.souTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="25"><input type="checkbox" name="" value=""></th>
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="80">创建人</th>
									<th width="80">技术</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">来源</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.proResultList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td><input type="checkbox" value="${listTemp.id}" name="ids" checked="checked"></td>
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l">
												<u style="cursor: pointer" class="text-primary href_info"
													onClick="article_add('查看','${rootPath}/back/users/usersUpdate.htm?id=${listTemp.usersId }','','510')" title="查看">
													${fn:substring(listTemp.users.nickName,0,subStrLen) }
												</u>
											</td>
											<td title="${listTemp.tech.name }">
												${fn:substring(listTemp.tech.name,0,subStrLen) }
											</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.id}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.souTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="tabCon">
						<div class="row cl">
							<table
									class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
								<thead>
								<tr class="text-c">
									<th width="25"><input type="checkbox" name="" value=""></th>
									<th width="50">序号</th>
									<th width="50">Id</th>
									<th width="80">创建人</th>
									<th width="80">技术</th>
									<th width="100">名称</th>
									<th width="50">难易</th>
									<th width="50">分数</th>
									<th width="50">类型</th>
									<th width="50">来源</th>
									<th width="50">状态</th>
									<th width="120">更新时间</th>
									<th width="120">发布时间</th>
								</tr>
								</thead>
								<tbody>
									<c:forEach items="${one.interResultList }" var="listTemp" varStatus="stat">
										<tr class="text-c">
											<td><input type="checkbox" value="${listTemp.id}" name="ids" checked="checked"></td>
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td class="text-l">
												<u style="cursor: pointer" class="text-primary href_info"
													onClick="article_add('查看','${rootPath}/back/users/usersUpdate.htm?id=${listTemp.usersId }','','510')" title="查看">
													${fn:substring(listTemp.users.nickName,0,subStrLen) }
												</u>
											</td>
											<td title="${listTemp.tech.name }">
												${fn:substring(listTemp.tech.name,0,subStrLen) }
											</td>
											<td class="text-l" title="${listTemp.name }">
												<a class="ml-5 href_info"
													data-href="${rootPath}/back/ques/askUpdate.htm?id=${listTemp.id}"
													onclick="Hui_admin_tab(this)"
													href="javascript:;" data-title="${listTemp.name }" title="${listTemp.name }">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.askTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.souTypeStr }</span>
											</td>
											<td class="td-status">
												<span class="label label-success radius">${listTemp.statusStr }</span>
											</td>
											<td>${listTemp.updateTime }</td>
											<td>${listTemp.pubTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</form>
		</article>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<script type="text/javascript">
	$(function(){
		$('.skin-minimal input').iCheck({
			checkboxClass: 'icheckbox-blue',
			radioClass: 'iradio-blue',
			increaseArea: '20%'
		});
		$("#tab-system").Huitab({
			index:0
		});
		
		//表单验证
		$("#formSubmit").validate({
			rules:{
				name:{
					required:true,
				},
				score:{
					required:true,
					isDigits:true,
				}
			},
			onkeyup:false,
			focusCleanup:true,
			success:"valid",
			submitHandler:function(form){
				$(form).ajaxSubmit({
					type: 'post',
					success: function(data){
						/* 弹出框 */
						layer.msg(data.info,{icon:1,time:3000},function()
						{
							
						});
					},
					dataType:"json"
				});
			}
		});
	});
</script>
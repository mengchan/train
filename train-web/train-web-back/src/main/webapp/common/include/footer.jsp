<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/layer/2.4/layer.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/static/h-ui/js/H-ui.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/jquery.contextmenu/jquery.contextmenu.r2.js"></script>

<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/laypage/1.2/laypage.js"></script>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/webuploader/0.1.5/webuploader.min.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/ueditor/1.4.3/ueditor.config.js"></script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/ueditor/1.4.3/ueditor.all.min.js"> </script>
<script type="text/javascript"
	src="${rootPath}/common/resource/H-ui.admin/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>

<!-- 开发人员的js -->
<script type="text/javascript" src="${rootPath}/common/resource/util.js"></script>

<!--此乃百度统计代码，请自行删除-->
<script>
	var _hmt = _hmt || [];
	(function() {
		var hm = document.createElement("script");
		hm.src = "https://hm.baidu.com/hm.js?080836300300be57b7f34f4b3e97d911";
		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(hm, s);
	})();
</script>
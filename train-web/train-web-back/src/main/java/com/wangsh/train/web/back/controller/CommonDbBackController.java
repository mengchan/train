package com.wangsh.train.web.back.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.job.pojo.AJobCompany;
import com.wangsh.train.job.service.IJobDbService;
import com.wangsh.train.system.service.ISystemDbService;
import com.wangsh.train.users.pojo.AAdmins;

/**
 * 	公共操作;
 *	不需要登陆,需要连接数据库
 * 	@author wangsh
 */
@Controller
@RequestMapping("/commonDb/")
public class CommonDbBackController extends BaseController
{
	@Resource
	private ISystemDbService systemDbService;
	@Resource
	private IJobDbService jobDbService;
	
	/**
	 * 打开上传文件(前台)
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/uploadFile")
	public String uploadFile(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		ConstatFinalUtil.SYS_LOGGER.info("==uploadFile==");
		String operType = request.getParameter("operType");
		/* 接收数据id */
		String dataId = request.getParameter("dataId");
		/* 公司logo上传 */
		if("companyLogo".equalsIgnoreCase(operType))
		{
			/* 上传文件 */
			request.setAttribute("recName", "上传公司logo");
			request.setAttribute("name", "公司Logo");
			request.setAttribute("url", "/commonDb/uploadFileSubmit.htm?operType=" + operType + "&dataId=" + dataId);
		}
		return "uploadFile";
	}
	
	/**
	 * 打开上传文件(提交)
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "uploadFileSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String uploadFileSubmit(HttpServletRequest request,HttpServletResponse response,
			MultipartFile file) throws IOException
	{
		ConstatFinalUtil.SYS_LOGGER.info("==uploadFileSubmit==");
		ApiResponse<Object> apiResultResponse = new ApiResponse<Object>();
		
		AAdmins admins = (AAdmins) this.findObjfromSession(request, "admins") ; 
		
		/* 处理上传文件 */
		Map<String, Object> paramsMap = new HashMap<String,Object>();
		
		String operType = request.getParameter("operType");
		/* 接收数据id */
		String dataId = request.getParameter("dataId");
		try
		{
			/* 公司logo上传 */
			if("companyLogo".equalsIgnoreCase(operType))
			{
				/* 公司上传文件 */
				/* 准备请求的信息 */
				Map<String, Object> condMap = new HashMap<String, Object>();
				condMap.put("id", dataId);
				ApiResponse<AJobCompany> apiResponse = this.jobDbService.findOneJobCompanyService(condMap);
				AJobCompany jobCompany = apiResponse.getDataOneJava() ; 
				
				if(admins.getId() == jobCompany.getCreateId())
				{
					boolean flag = false ; 
					try
					{
						if(file != null)
						{
							ConstatFinalUtil.SYS_LOGGER.info("name:{},oriName:{}" , file.getName() , file.getOriginalFilename());
							paramsMap.put("fileName", file.getOriginalFilename());
							paramsMap.put("inputStream", file.getInputStream());
							paramsMap.put("fileSize", file.getSize());
							
							flag = true ; 
						}
					} catch (IOException e)
					{
						ConstatFinalUtil.SYS_LOGGER.error("上传文件失败了;id:{}",jobCompany.getId());
					}
					
					if(flag)
					{
						apiResultResponse = this.jobDbService.updateOneJobCompanyService(jobCompany,paramsMap);
						ConstatFinalUtil.SYS_LOGGER.info("--代码统计添加返回结果:{}--",apiResultResponse.toJSON());
					}
				}else
				{
					apiResultResponse.setCode(ApiResponseEnum.INFO_AUTH_NO.getStatus());
					apiResultResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(ApiResponseEnum.INFO_AUTH_NO.getStatus() + ""), Collections.EMPTY_MAP);
				}
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			apiResultResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			apiResultResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResultResponse.getCode() + "") , infoMap);
		}
		return apiResultResponse.toJSON().toJSONString();
	}
}

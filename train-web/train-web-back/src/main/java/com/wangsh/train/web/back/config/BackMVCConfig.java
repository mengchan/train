package com.wangsh.train.web.back.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import com.wangsh.train.common.config.SpringMVCConfig;

/**
 * 自己项目的配置
 * @author Xh-Win10-Server
 *
 */
@Configuration
public class BackMVCConfig extends SpringMVCConfig
{
	@Resource
	private HandlerInterceptor authInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry)
	{
		registry.addInterceptor(authInterceptor).addPathPatterns("/back/**");
	}
}

package com.wangsh.train.web.back.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.job.pojo.AJobComPosition;
import com.wangsh.train.job.pojo.AJobCompany;
import com.wangsh.train.job.pojo.AJobInterview;
import com.wangsh.train.job.pojo.AJobOffer;
import com.wangsh.train.job.pojo.AJobResume;
import com.wangsh.train.job.service.IJobDbService;
import com.wangsh.train.system.pojo.ASysPro;
import com.wangsh.train.system.pojo.ASysProEnum;
import com.wangsh.train.system.service.ISystemDbService;
import com.wangsh.train.users.pojo.AAdmins;

/**
 * 	工作后台操作
 * 	@author wangsh
 */
@Controller
@RequestMapping("/back/job/")
public class JobBackController extends BaseController
{
	@Resource
	private IJobDbService jobDbService;
	@Resource
	private ISystemDbService systemDbService;
	
	/**
	 * 用户班级列表页面
	 * @return
	 */
	@RequestMapping("/jobCompanyList")
	public String jobCompanyList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		String regionId = request.getParameter("regionId");
		request.setAttribute("regionId", regionId);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		condMap.put("regionId",regionId);
		condMap.put("extend", "true");
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobCompany> response = this.jobDbService.findCondListJobCompanyService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/back/job/jobCompanyList";
	}
	
	/**
	 * 打开用户班级添加页面
	 * @return
	 */
	@RequestMapping("/jobCompanyInsert")
	public String jobCompanyInsert(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyInsert--");
		return "/back/job/jobCompanyInsert";
	}
	
	/**
	 * 打开用户班级添加页面
	 * @return
	 */
	@RequestMapping(value = "/jobCompanyInsertSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String jobCompanyInsertSubmit(HttpServletRequest request,Model model, AJobCompany jobCompany)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyInsertSubmit--");
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		AAdmins admins = (AAdmins) this.findObjfromSession(request, "admins");
		
		try
		{
			String pubTimeStr = request.getParameter("pubTimeStr");
			/* 为对象赋值 */
			jobCompany.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			jobCompany.setCreateId(admins.getId());
			dbResponse = this.jobDbService.saveOneJobCompanyService(jobCompany);
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("添加一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return dbResponse.toJSON().toJSONString();
	}
	
	/**
	 * 打开用户班级更新页面
	 * @return
	 */
	@RequestMapping("/jobCompanyUpdate")
	public String jobCompanyUpdate(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyUpdate--");
		/* 从session中获取信息 */
		String id = request.getParameter("id");
		/* 准备请求的信息 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		ApiResponse<AJobCompany> apiResponse = this.jobDbService.findOneJobCompanyService(condMap);
		model.addAttribute("response", apiResponse.toJSON());
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 根据不同的请求跳转不同的页面 */
			return "/back/job/jobCompanyUpdate";
		}else if("audit".equalsIgnoreCase(operType))
		{
			return "/back/job/jobCompanyAudit";
		}
//		else if("file".equalsIgnoreCase(operType))
//		{
//			/* 上传文件 */
//			model.addAttribute("name", "公司的Logo");
//			model.addAttribute("url", "/back/job/jobCompanyUpdateSubmit.htm?operType=" + operType + "&id=" + id);
//			return this.uploadFile;
//		}
		return "/back/job/jobCompanyInfo";
	}
	
	/**
	 * 打开用户班级更新页面
	 * @return
	 */
	@RequestMapping(value = "/jobCompanyUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String jobCompanyUpdateSubmit(HttpServletRequest request,MultipartFile file,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyUpdateSubmit--");
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		try
		{
			/* 从session中获取信息 */
			String id = request.getParameter("id");
			/* 准备请求的信息 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", id);
			ApiResponse<AJobCompany> apiResponse = this.jobDbService.findOneJobCompanyService(condMap);
			AJobCompany jobCompany = apiResponse.getDataOneJava() ; 
			model.addAttribute("response", apiResponse.toJSON());
			
			Map<String, Object> paramsMap = new HashMap<String,Object>();
			boolean flag = false ; 
			String operType = request.getParameter("operType");
			if("audit".equalsIgnoreCase(operType))
			{
				/* 接收参数 */
				String auditContent = request.getParameter("auditContent");
				String status = request.getParameter("status");
				
				/* 设置属性 */
				jobCompany.setStatus(Byte.valueOf(status));
				jobCompany.setAuditContent(auditContent);
				
				flag = true; 
			}
//			else else if("file".equalsIgnoreCase(operType))
//			{
//				try
//				{
//					if(file != null)
//					{
//						ConstatFinalUtil.SYS_LOGGER.info("name:{},oriName:{}" , file.getName() , file.getOriginalFilename());
//						paramsMap.put("fileName", file.getOriginalFilename());
//						paramsMap.put("inputStream", file.getInputStream());
//						paramsMap.put("fileSize", file.getSize());
//						
//						flag = true ; 
//					}
//				} catch (IOException e)
//				{
//					ConstatFinalUtil.SYS_LOGGER.error("上传文件失败了;id:{}",jobCompany.getId());
//				}
//			}
			
			if(flag)
			{
				dbResponse = this.jobDbService.updateOneJobCompanyService(jobCompany,paramsMap);
			}
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return dbResponse.toJSON().toJSONString();
	}
	
	/**
	 * 公司批量操作
	 * @return
	 */
	@RequestMapping(value = "/jobCompanyBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String jobCompanyBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				condMap.clear();
				condMap.put("id", id);
				ApiResponse<AJobCompany> apiResponseDb = this.jobDbService.findOneJobCompanyService(condMap);
				AJobCompany jobCompany = apiResponseDb.getDataOneJava();
				
				boolean updateFlag = false ; 
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					/* 设置状态 */
					jobCompany.setStatus(Byte.valueOf(status));
					
					updateFlag = true ; 
				}
				
				if(updateFlag)
				{
					ApiResponse<Object> jobCompanyClassResponse = this.jobDbService.updateOneJobCompanyService(jobCompany);
					if(jobCompanyClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*==== 公司模块结束 ====*/
	
	/*==== 职位模块开始 ====*/
	/**
	 * 职位列表页面
	 * @return
	 */
	@RequestMapping("/comPositionList")
	public String comPositionList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 增加一个companyId */
		String companyId = request.getParameter("companyId");
		if(companyId == null)
		{
			companyId = "" ; 
		}
		condMap.put("companyId", companyId);
		request.setAttribute("companyId", companyId);
		
		/* 增加一个regionId */
		String regionId = request.getParameter("regionId");
		if(regionId == null)
		{
			regionId = "" ; 
		}
		condMap.put("regionId", regionId);
		request.setAttribute("regionId", regionId);
		
		/* 增加一个cateId */
		String cateId = request.getParameter("cateId");
		if(cateId == null)
		{
			cateId = "" ; 
		}
		condMap.put("cateId", cateId);
		request.setAttribute("cateId", cateId);
		
		/* 增加一个educId */
		String educId = request.getParameter("educId");
		if(educId == null)
		{
			educId = "" ; 
		}
		condMap.put("educId", educId);
		request.setAttribute("educId", educId);
		
		condMap.put("extend", "true");
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobComPosition> response = this.jobDbService.findCondListComPositionService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		
		/* 职位类型 */
		condMap.clear();
		condMap.put("operType", ASysProEnum.OPERTYPE_EQUAL.getStatus());
		condMap.put("proType", ASysProEnum.PROTYPE_POSITION.getStatus());
		ApiResponse<ASysPro> cateApiResponse = this.systemDbService.findCondListSysProService(null, condMap);
		request.setAttribute("cateApiResponse", cateApiResponse.toJSON());
		
		/* 学历 */
		condMap.clear();
		condMap.put("operType", ASysProEnum.OPERTYPE_EQUAL.getStatus());
		condMap.put("proType", ASysProEnum.PROTYPE_EDUCE.getStatus());
		ApiResponse<ASysPro> educeApiResponse = this.systemDbService.findCondListSysProService(null, condMap);
		request.setAttribute("educeApiResponse", educeApiResponse.toJSON());
		return "/back/job/comPositionList";
	}

	/**
	 * 打开更新职位页面
	 * @return
	 */
	@RequestMapping("/comPositionUpdate")
	public String comPositionUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AJobComPosition> response = this.jobDbService.findOneComPositionService(condMap);
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("audit".equalsIgnoreCase(operType))
		{
			return "/back/job/comPositionAudit";
		}
		return "/back/job/comPositionInfo";
	}
	
	/**
	 * 更新职位提交操作
	 * @return
	 */
	@RequestMapping(value = "/comPositionUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String comPositionUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionUpdateSubmit--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobComPosition> apiResponseRes= this.jobDbService.findOneComPositionService(condMap);
		/* 获取java对象 */
		AJobComPosition comPosition = apiResponseRes.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("audit".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String auditContent = request.getParameter("auditContent");
			String status = request.getParameter("status");
			
			/* 设置属性 */
			comPosition.setStatus(Byte.valueOf(status));
			comPosition.setAuditContent(auditContent);
			
			flag = true; 
		}
		
		/* 数据库操作 */
		if(flag)
		{
			/* 数据库操作 */
			apiResponse = jobDbService.updateOneComPositionService(comPosition);
			ConstatFinalUtil.SYS_LOGGER.info("--职位更新返回结果:{}--",apiResponse.toJSON());
		}
		return apiResponse.toJSON().toJSONString();
	}
	
	/**
	 * 职位批量操作
	 * @return
	 */
	@RequestMapping(value = "/comPositionBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String comPositionBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				condMap.clear();
				condMap.put("id", id);
				ApiResponse<AJobComPosition> apiResponseDb = this.jobDbService.findOneComPositionService(condMap);
				AJobComPosition comPosition = apiResponseDb.getDataOneJava();
				
				boolean updateFlag = false ; 
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					/* 设置状态 */
					comPosition.setStatus(Byte.valueOf(status));
					
					updateFlag = true ; 
				}
				
				if(updateFlag)
				{
					ApiResponse<Object> comPositionClassResponse = this.jobDbService.updateOneComPositionService(comPosition);
					if(comPositionClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*==== 职位模块结束 ====*/
	
	/*==== 面试模块开始 ====*/
	/**
	 * 面试列表页面
	 * @return
	 */
	@RequestMapping("/interviewList")
	public String interviewList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 增加一个companyId */
		String companyId = request.getParameter("companyId");
		if(companyId == null)
		{
			companyId = "" ; 
		}
		condMap.put("companyId", companyId);
		request.setAttribute("companyId", companyId);
		
		/* 增加一个cateId */
		String simulation = request.getParameter("simulation");
		if(simulation == null)
		{
			simulation = "" ; 
		}
		condMap.put("simulation", simulation);
		request.setAttribute("simulation",simulation);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobInterview> response = this.jobDbService.findCondListInterviewService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		
		return "/back/job/interviewList";
	}

	/**
	 * 打开更新面试页面
	 * @return
	 */
	@RequestMapping("/interviewUpdate")
	public String interviewUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AJobInterview> response = this.jobDbService.findOneInterviewService(condMap);
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("audit".equalsIgnoreCase(operType))
		{
			return "/back/job/interviewAudit";
		}
		return "/back/job/interviewInfo";
	}
	
	/**
	 * 更新面试提交操作
	 * @return
	 */
	@RequestMapping(value = "/interviewUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String interviewUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewUpdateSubmit--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobInterview> apiResponseRes= this.jobDbService.findOneInterviewService(condMap);
		/* 获取java对象 */
		AJobInterview interview = apiResponseRes.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("audit".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String auditContent = request.getParameter("auditContent");
			String status = request.getParameter("status");
			
			/* 设置属性 */
			interview.setStatus(Byte.valueOf(status));
			interview.setAuditContent(auditContent);
			
			flag = true; 
		}
		
		/* 数据库操作 */
		if(flag)
		{
			/* 数据库操作 */
			apiResponse = jobDbService.updateOneInterviewService(interview);
			ConstatFinalUtil.SYS_LOGGER.info("--面试更新返回结果:{}--",apiResponse.toJSON());
		}
		return apiResponse.toJSON().toJSONString();
	}
	
	/**
	 * 面试批量操作
	 * @return
	 */
	@RequestMapping(value = "/interviewBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String interviewBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				condMap.clear();
				condMap.put("id", id);
				ApiResponse<AJobInterview> apiResponseDb = this.jobDbService.findOneInterviewService(condMap);
				AJobInterview interview = apiResponseDb.getDataOneJava();
				
				boolean updateFlag = false ; 
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					/* 设置状态 */
					interview.setStatus(Byte.valueOf(status));
					
					updateFlag = true ; 
				}
				
				if(updateFlag)
				{
					ApiResponse<Object> interviewClassResponse = this.jobDbService.updateOneInterviewService(interview);
					if(interviewClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*==== 面试模块结束 ====*/
	
	/*==== offer模块开始 ====*/
	/**
	 * offer列表页面
	 * @return
	 */
	@RequestMapping("/offerList")
	public String offerList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 增加一个companyId */
		String companyId = request.getParameter("companyId");
		if(companyId == null)
		{
			companyId = "" ; 
		}
		condMap.put("companyId", companyId);
		request.setAttribute("companyId", companyId);
		
		/* 增加一个fiveRisks */
		String fiveRisks = request.getParameter("fiveRisks");
		if(fiveRisks == null)
		{
			fiveRisks = "" ; 
		}
		condMap.put("fiveRisks", fiveRisks);
		request.setAttribute("fiveRisks", fiveRisks);
		
		/* 增加一个accFund */
		String accFund = request.getParameter("accFund");
		if(accFund == null)
		{
			accFund = "" ; 
		}
		condMap.put("accFund", accFund);
		request.setAttribute("accFund", accFund);
		
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobOffer> response = this.jobDbService.findCondListOfferService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		
		return "/back/job/offerList";
	}

	/**
	 * 打开更新offer页面
	 * @return
	 */
	@RequestMapping("/offerUpdate")
	public String offerUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AJobOffer> response = this.jobDbService.findOneOfferService(condMap);
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("audit".equalsIgnoreCase(operType))
		{
			return "/back/job/offerAudit";
		}
		return "/back/job/offerInfo";
	}
	
	/**
	 * 更新offer提交操作
	 * @return
	 */
	@RequestMapping(value = "/offerUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String offerUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerUpdateSubmit--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobOffer> apiResponseRes= this.jobDbService.findOneOfferService(condMap);
		/* 获取java对象 */
		AJobOffer offer = apiResponseRes.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("audit".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String auditContent = request.getParameter("auditContent");
			String status = request.getParameter("status");
			
			/* 设置属性 */
			offer.setStatus(Byte.valueOf(status));
			offer.setAuditContent(auditContent);
			
			flag = true; 
		}
		
		/* 数据库操作 */
		if(flag)
		{
			/* 数据库操作 */
			apiResponse = jobDbService.updateOneOfferService(offer);
			ConstatFinalUtil.SYS_LOGGER.info("--offer更新返回结果:{}--",apiResponse.toJSON());
		}
		return apiResponse.toJSON().toJSONString();
	}
	
	/**
	 * offer批量操作
	 * @return
	 */
	@RequestMapping(value = "/offerBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String offerBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				condMap.clear();
				condMap.put("id", id);
				ApiResponse<AJobOffer> apiResponseDb = this.jobDbService.findOneOfferService(condMap);
				AJobOffer offer = apiResponseDb.getDataOneJava();
				
				boolean updateFlag = false ; 
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					/* 设置状态 */
					offer.setStatus(Byte.valueOf(status));
					
					updateFlag = true ; 
				}
				
				if(updateFlag)
				{
					ApiResponse<Object> offerClassResponse = this.jobDbService.updateOneOfferService(offer);
					if(offerClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*==== offer模块结束 ====*/
	
	/*==== 简历模块开始 ====*/
	/**
	 * 简历列表页面
	 * @return
	 */
	@RequestMapping("/resumeList")
	public String resumeList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 增加一个finish */
		String finish = request.getParameter("finish");
		if(finish == null)
		{
			finish = "" ; 
		}
		condMap.put("finish", finish);
		request.setAttribute("finish", finish);
		
		/* 增加一个finish */
		String onTime = request.getParameter("onTime");
		if(onTime == null)
		{
			onTime = "" ; 
		}
		condMap.put("onTime", onTime);
		request.setAttribute("onTime", onTime);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobResume> response = this.jobDbService.findCondListResumeService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		
		return "/back/job/resumeList";
	}

	/**
	 * 打开更新简历页面
	 * @return
	 */
	@RequestMapping("/resumeUpdate")
	public String resumeUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AJobResume> response = this.jobDbService.findOneResumeService(condMap);
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("audit".equalsIgnoreCase(operType))
		{
			return "/back/job/resumeAudit";
		}
		return "/back/job/resumeInfo";
	}
	
	/**
	 * 更新简历提交操作
	 * @return
	 */
	@RequestMapping(value = "/resumeUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String resumeUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeUpdateSubmit--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobResume> apiResponseRes= this.jobDbService.findOneResumeService(condMap);
		/* 获取java对象 */
		AJobResume resume = apiResponseRes.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("audit".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String auditContent = request.getParameter("auditContent");
			String status = request.getParameter("status");
			String finish = request.getParameter("finish");
			
			/* 设置属性 */
			resume.setStatus(Byte.valueOf(status));
			resume.setAuditContent(auditContent);
			resume.setFinish(Byte.valueOf(finish));
			
			flag = true; 
		}
		
		/* 数据库操作 */
		if(flag)
		{
			/* 数据库操作 */
			apiResponse = jobDbService.updateOneResumeService(resume);
			ConstatFinalUtil.SYS_LOGGER.info("--简历更新返回结果:{}--",apiResponse.toJSON());
		}
		return apiResponse.toJSON().toJSONString();
	}
	
	/**
	 * 简历批量操作
	 * @return
	 */
	@RequestMapping(value = "/resumeBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String resumeBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				condMap.clear();
				condMap.put("id", id);
				ApiResponse<AJobResume> apiResponseDb = this.jobDbService.findOneResumeService(condMap);
				AJobResume resume = apiResponseDb.getDataOneJava();
				
				boolean updateFlag = false ; 
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					/* 设置状态 */
					resume.setStatus(Byte.valueOf(status));
					
					updateFlag = true ; 
				}
				
				if(updateFlag)
				{
					ApiResponse<Object> resumeClassResponse = this.jobDbService.updateOneResumeService(resume);
					if(resumeClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*==== 简历模块结束 ====*/
}

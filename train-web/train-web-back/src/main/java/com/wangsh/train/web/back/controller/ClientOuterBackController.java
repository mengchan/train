package com.wangsh.train.web.back.controller;

import java.util.Collections;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.outer.IClientOuterService;

/**
 * 	外部的后台Controller
 * 	@author wangsh
 */
@Controller
@RequestMapping(value = "/outer/",produces = "text/html;charset=UTF-8")
@ResponseBody
public class ClientOuterBackController extends BaseController
{
	@Resource
	private IClientOuterService clientOuterService;
	
	/**
	 * 请求用户中心外部的接口
	 * @return
	 */
	@RequestMapping("/requestOuter")
	public String requestOuter(HttpServletRequest request)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--requestOuter--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 请求获取地区列表 */
		JSONObject resultJSON = new JSONObject();
		String method = request.getParameter("method");
		/* 接收数据 */
		String json = request.getParameter("json");
		try
		{
			/* 转换成JSON对象 */
			JSONObject dataJSON = (JSONObject) JSON.parse(json);
			if("regionList".equalsIgnoreCase(method))
			{
				resultJSON = this.clientOuterService.regionListService(dataJSON);
			}else if("regionOne".equalsIgnoreCase(method))
			{
				resultJSON = this.clientOuterService.regionOneService(dataJSON);
			}else if("regionBatchOne".equalsIgnoreCase(method))
			{
				resultJSON = this.clientOuterService.regionBatchOneService(dataJSON);
			}else if("cateList".equalsIgnoreCase(method))
			{
				resultJSON = this.clientOuterService.cateListService(dataJSON);
			}else if("cateOne".equalsIgnoreCase(method))
			{
				resultJSON = this.clientOuterService.cateOneService(dataJSON);
			}else if("cateBatchOne".equalsIgnoreCase(method))
			{
				resultJSON = this.clientOuterService.cateBatchOneService(dataJSON);
			}else 
			{
				apiResponse.setCode(ApiResponseEnum.INFO_METHOD_ERROR.getStatus());
				apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
				resultJSON = apiResponse.toJSON() ; 
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("请求用户中心接口报错了,上行数据:{}",json,e);
			apiResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			resultJSON = apiResponse.toJSON() ;
		}
		return resultJSON.toJSONString() ; 
	}
}

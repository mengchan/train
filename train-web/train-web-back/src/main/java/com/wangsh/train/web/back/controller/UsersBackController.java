package com.wangsh.train.web.back.controller;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.cla.pojo.AClassEnum;
import com.wangsh.train.cla.service.IClassDbService;
import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.users.pojo.AUsers;
import com.wangsh.train.users.pojo.AUsersCate;
import com.wangsh.train.users.pojo.AUsersClass;
import com.wangsh.train.users.pojo.AUsersClassEnum;
import com.wangsh.train.users.service.IUsersDbService;

/**
 * 	用户后台操作
 * 	@author wangsh
 */
@Controller
@RequestMapping("/back/users/")
public class UsersBackController extends BaseController
{
	@Resource
	private IUsersDbService usersDbService;
	@Resource
	private IClassDbService classDbService;
	
	/**
	 * 用户列表页面
	 * @return
	 */
	@RequestMapping("/usersList")
	public String usersList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		String stageType = request.getParameter("stageType");
		request.setAttribute("stageType", stageType);
		String classId = request.getParameter("classId");
		request.setAttribute("classId", classId);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		condMap.put("stageType",stageType);
		condMap.put("classId", classId);
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AUsers> response = this.usersDbService.findCondListUsersService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		
		/* 查询所有启用的班级 */
		/* 搜索条件,生成公共的搜索条件 */
		condMap.clear();
		condMap.put("status", AClassEnum.STATUS_ENABLE.getStatus());
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AClass> classResponse = this.classDbService.findCondListClassService(pageInfoUtil, condMap);
		model.addAttribute("classResponse", classResponse.toJSON());
		return "/back/users/usersList";
	}
	
	/**
	 * 打开用户更新页面
	 * @return
	 */
	@RequestMapping("/usersUpdate")
	public String usersUpdate(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersUpdate--");
		/* 从session中获取信息 */
		String id = request.getParameter("id");
		/* 准备请求的信息 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		ApiResponse<AUsers> apiResponse = this.usersDbService.findOneUsersService(condMap);
		model.addAttribute("response", apiResponse.toJSON());
		
		String operType = request.getParameter("operType");
		if("updateInfo".equalsIgnoreCase(operType))
		{
			/* 查询所有启用的班级 */
			condMap.clear();
			condMap.put("status", AClassEnum.STATUS_ENABLE.getStatus());
			ApiResponse<AClass> classResponse = this.classDbService.findCondListClassService(null, condMap);
			model.addAttribute("classResponse", classResponse.toJSON());
			/* 根据不同的请求跳转不同的页面 */
			return "/back/users/usersUpdateInfo";
		}
		return "/back/users/usersInfo";
	}
	
	/**
	 * 打开用户更新页面
	 * @return
	 */
	@RequestMapping(value = "/usersUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String usersUpdateSubmit(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersUpdateSubmit--");
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();;
		try
		{
			/* 从session中获取信息 */
			String id = request.getParameter("id");
			/* 准备请求的信息 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", id);
			ApiResponse<AUsers> apiResponse = this.usersDbService.findOneUsersService(condMap);
			AUsers users = apiResponse.getDataOneJava() ; 
			model.addAttribute("response", apiResponse.toJSON());
			
			boolean flag = false ; 
			String operType = request.getParameter("operType");
			if("updateInfo".equalsIgnoreCase(operType))
			{
				/* 根据不同的请求跳转不同的页面 */
				String classId = request.getParameter("classId");
				String stageType = request.getParameter("stageType");
				String status = request.getParameter("status");
				
				users.setClassId(Integer.valueOf(classId));
				users.setStageType(Byte.valueOf(stageType));
				users.setStatus(Byte.valueOf(status));
				flag = true;
			}
			
			if(flag)
			{
				dbResponse = this.usersDbService.updateOneUsersService(users);
			}
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (NumberFormatException e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return dbResponse.toJSON().toJSONString();
	}
	
	
	/**
	 * 打开用户更新页面
	 * @return
	 */
	@RequestMapping(value = "/usersBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String usersBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersUpdate--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] usersIds = request.getParameterValues("usersIds");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		String classId = request.getParameter("classId");
		if(usersIds != null)
		{
			int totalNum = usersIds.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < usersIds.length; i++)
			{
				String usersId = usersIds[i];
				if("usersClass".equalsIgnoreCase(operType))
				{
					AUsersClass usersClass = new AUsersClass();
					usersClass.setUsersId(Integer.valueOf(usersId));
					usersClass.setClassId(Integer.valueOf(classId));
					
					usersClass.setStatus(Byte.valueOf(AUsersClassEnum.STATUS_ENABLE.getStatus()));
					usersClass.setClassType(AUsersClassEnum.CLASSTYPE_NORMAL.getStatus());
					usersClass.setDefType(AUsersClassEnum.DEFTYPE_YES.getStatus());
					usersClass.setPubTime(new Date());
					
					ApiResponse<Object> usersClassResponse = this.usersDbService.saveOneUsersClassService(usersClass);
					if(usersClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/**
	 * 用户班级列表页面
	 * @return
	 */
	@RequestMapping("/usersClassList")
	public String usersClassList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersClassList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		String classId = request.getParameter("classId");
		String classType = request.getParameter("classType");
		String usersId = request.getParameter("usersId");
		String defType = request.getParameter("defType");
		request.setAttribute("classId", classId);
		request.setAttribute("classType", classType);
		request.setAttribute("usersId", usersId);
		request.setAttribute("defType", defType);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		condMap.put("classType",classType);
		condMap.put("classId", classId);
		condMap.put("usersId", usersId);
		condMap.put("defType", defType);
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AUsersClass> response = this.usersDbService.findCondListUsersClassService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/back/users/usersClassList";
	}
	
	/**
	 * 打开用户班级添加页面
	 * @return
	 */
	@RequestMapping("/usersClassInsert")
	public String usersClassInsert(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersClassInsert--");
		/* 准备请求的信息 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		/* 查询所有启用的班级 */
		condMap.clear();
		condMap.put("status", AClassEnum.STATUS_ENABLE.getStatus());
		ApiResponse<AClass> classResponse = this.classDbService.findCondListClassService(null, condMap);
		model.addAttribute("classResponse", classResponse.toJSON());
		
		/* 从session中获取信息 */
		String usersId = request.getParameter("usersId");
		/* 准备请求的信息 */
		condMap.clear();
		condMap.put("id", usersId);
		ApiResponse<AUsers> apiResponse = this.usersDbService.findOneUsersService(condMap);
		model.addAttribute("usesResponse", apiResponse.toJSON());
		return "/back/users/usersClassInsert";
	}
	
	/**
	 * 打开用户班级添加页面
	 * @return
	 */
	@RequestMapping(value = "/usersClassInsertSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String usersClassInsertSubmit(HttpServletRequest request,Model model, AUsersClass usersClass)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersClassInsertSubmit--");
		
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		try
		{
			String pubTimeStr = request.getParameter("pubTimeStr");
			/* 为对象赋值 */
			usersClass.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			
			dbResponse = this.usersDbService.saveOneUsersClassService(usersClass);
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("添加一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return dbResponse.toJSON().toJSONString();
	}
	
	/**
	 * 打开用户班级更新页面
	 * @return
	 */
	@RequestMapping("/usersClassUpdate")
	public String usersClassUpdate(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersClassUpdate--");
		/* 从session中获取信息 */
		String id = request.getParameter("id");
		/* 准备请求的信息 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		ApiResponse<AUsersClass> apiResponse = this.usersDbService.findOneUsersClassService(condMap);
		model.addAttribute("response", apiResponse.toJSON());
		
		String operType = request.getParameter("operType");
		if("updateInfo".equalsIgnoreCase(operType))
		{
			/* 从session中获取信息 */
			String usersId = request.getParameter("usersId");
			/* 准备请求的信息 */
			condMap.clear();
			condMap.put("id", usersId);
			ApiResponse<AUsers> apiUsersResponse = this.usersDbService.findOneUsersService(condMap);
			model.addAttribute("usesResponse", apiUsersResponse.toJSON());
			
			/* 查询所有启用的班级 */
			condMap.clear();
			condMap.put("status", AClassEnum.STATUS_ENABLE.getStatus());
			ApiResponse<AClass> classResponse = this.classDbService.findCondListClassService(null, condMap);
			model.addAttribute("classResponse", classResponse.toJSON());
			/* 根据不同的请求跳转不同的页面 */
			return "/back/users/usersClassUpdate";
		}
		return "/back/users/usersClassInfo";
	}
	
	/**
	 * 打开用户班级更新页面
	 * @return
	 */
	@RequestMapping(value = "/usersClassUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String usersClassUpdateSubmit(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersClassUpdateSubmit--");
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		try
		{
			/* 从session中获取信息 */
			String id = request.getParameter("id");
			/* 准备请求的信息 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", id);
			ApiResponse<AUsersClass> apiResponse = this.usersDbService.findOneUsersClassService(condMap);
			AUsersClass usersClass = apiResponse.getDataOneJava() ; 
			model.addAttribute("response", apiResponse.toJSON());
			
			/* 根据不同的请求跳转不同的页面 */
			String classId = request.getParameter("classId");
			String classType = request.getParameter("classType");
			String status = request.getParameter("status");
			String defType = request.getParameter("defType");
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			
			usersClass.setClassId(Integer.valueOf(classId));
			usersClass.setClassType(Byte.valueOf(classType));
			usersClass.setStatus(Byte.valueOf(status));
			usersClass.setDefType(Byte.valueOf(defType));
			usersClass.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			
			dbResponse = this.usersDbService.updateOneUsersClassService(usersClass);
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return dbResponse.toJSON().toJSONString();
	}
	
	/**
	 * 用户分类列表页面
	 * @return
	 */
	@RequestMapping("/usersCateList")
	public String usersCateList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersCateList--");
		/* 接收参数 */
		String cateTempId = request.getParameter("cateTempId");
		if(cateTempId == null)
		{
			cateTempId = ""; 
		}
		request.setAttribute("cateTempId", cateTempId);
		
		/* 只能查询自己的用户分类 */
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 查询用户指定的班级 */
		condMap.put("cateTempId", cateTempId);
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AUsersCate> response = this.usersDbService.findCondListUsersCateService(pageInfoUtil, condMap);
		response.clearInfo();
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/back/users/usersCateList";
	}
	
	/**
	 * 打开更新用户分类页面
	 * @return
	 */
	@RequestMapping("/usersCateUpdate")
	public String usersCateUpdate(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--usersCateUpdate--");
		String id = request.getParameter("id");
		String operType = request.getParameter("operType");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AUsersCate> response = this.usersDbService.findOneUsersCateService(condMap);
		response.clearInfo();
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		return "/back/users/usersCateInfo";
	}
}

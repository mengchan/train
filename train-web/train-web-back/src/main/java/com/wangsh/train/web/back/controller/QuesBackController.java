package com.wangsh.train.web.back.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.cla.service.IClassDbService;
import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.ques.pojo.AAsk;
import com.wangsh.train.ques.pojo.AExamDesc;
import com.wangsh.train.ques.pojo.AExamDescEnum;
import com.wangsh.train.ques.pojo.AExamDetail;
import com.wangsh.train.ques.pojo.AExamDetailEnum;
import com.wangsh.train.ques.pojo.AExamScoreDesc;
import com.wangsh.train.ques.pojo.AExamScoreDescEnum;
import com.wangsh.train.ques.pojo.AExamScoreDetail;
import com.wangsh.train.ques.service.IExamDbService;
import com.wangsh.train.ques.service.IExamOperService;
import com.wangsh.train.ques.service.IQuesDbService;
import com.wangsh.train.system.pojo.ASysProEnum;
import com.wangsh.train.system.pojo.ASysTech;
import com.wangsh.train.system.service.ISystemDbService;
import com.wangsh.train.users.pojo.AAdmins;

/**
 * 	问题示例代码操作
 * 	@author wangsh
 */
@Controller
@RequestMapping("/back/ques/")
public class QuesBackController extends BaseController
{
	@Resource
	private IQuesDbService quesDbService;
	@Resource
	private ISystemDbService systemDbService;
	@Resource
	private IExamDbService examDbService;
	@Resource
	private IExamOperService examOperService;
	@Resource
	private IClassDbService classDbService;
	
	/*==== 问题模块开始 ====*/
	/**
	 * 问题列表页面
	 * @return
	 */
	@RequestMapping("/askList")
	public String askList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/*==添加额外的搜索条件==*/
		/*====来源开始====*/
		String souType = request.getParameter("souType");
		if(souType == null)
		{
			souType = "" ;
		}
		condMap.put("souType", souType);
		request.setAttribute("souType", souType);
		/*====来源结束====*/
		/*====类型开始====*/
		String askType = request.getParameter("askType");
		if(askType == null)
		{
			askType = "" ;
		}
		condMap.put("askType", askType);
		request.setAttribute("askType", askType);
		/*====类型结束====*/
		/*====技术开始====*/
		String techId = request.getParameter("techId");
		if(techId == null)
		{
			techId = "" ;
		}
		condMap.put("techId", techId);
		request.setAttribute("techId", techId);
		/*====类型结束====*/
		
		/*====interviewId开始====*/
		String interviewId = request.getParameter("interviewId");
		if(interviewId == null)
		{
			interviewId = "" ;
		}
		condMap.put("interviewId", interviewId);
		request.setAttribute("interviewId", interviewId);
		/*====interviewId结束====*/
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AAsk> response = this.quesDbService.findCondListAskService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		
		/* 查询所有的技术 */
		condMap.clear();
		condMap.put("status", ASysProEnum.STATUS_ENABLE.getStatus());
		ApiResponse<ASysTech> sysTechApiResponse = this.systemDbService.findCondListSysTechService(null, condMap);
		request.setAttribute("sysTechResponse", sysTechApiResponse.toJSON());
		return "/back/ques/askList";
	}

	/**
	 * 打开更新问题页面
	 * @return
	 */
	@RequestMapping("/askUpdate")
	public String askUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AAsk> response = this.quesDbService.findOneAskService(condMap);
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("audit".equalsIgnoreCase(operType))
		{
			return "/back/ques/askAudit";
		}
		return "/back/ques/askInfo";
	}
	
	/**
	 * 更新问题提交操作
	 * @return
	 */
	@RequestMapping(value = "/askUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String askUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askUpdateSubmit--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AAsk> apiResponseRes= this.quesDbService.findOneAskService(condMap);
		/* 获取java对象 */
		AAsk ask = apiResponseRes.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("audit".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String auditContent = request.getParameter("auditContent");
			String status = request.getParameter("status");
			
			/* 设置属性 */
			ask.setStatus(Byte.valueOf(status));
			ask.setAuditContent(auditContent);
			
			flag = true; 
		}
		
		/* 数据库操作 */
		if(flag)
		{
			/* 数据库操作 */
			apiResponse = quesDbService.updateOneAskService(ask);
			ConstatFinalUtil.SYS_LOGGER.info("--问题更新返回结果:{}--",apiResponse.toJSON());
		}
		return apiResponse.toJSON().toJSONString();
	}
	
	/**
	 * 问题批量操作
	 * @return
	 */
	@RequestMapping(value = "/askBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String askBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				condMap.clear();
				condMap.put("id", id);
				ApiResponse<AAsk> apiResponseDb = this.quesDbService.findOneAskService(condMap);
				AAsk ask = apiResponseDb.getDataOneJava();
				
				boolean updateFlag = false ; 
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					/* 设置状态 */
					ask.setStatus(Byte.valueOf(status));
					
					updateFlag = true ; 
				}else if("askType".equalsIgnoreCase(operType))
				{
					/* 设置问题类型 */
					String askType = request.getParameter("askType");
					ask.setAskType(Byte.valueOf(askType));
					updateFlag = true ; 
				}else if("teaId".equalsIgnoreCase(operType))
				{
					/* 设置问题类型 */
					String teaId = request.getParameter("teaId");
					ask.setTechId(Integer.valueOf(teaId));
					updateFlag = true ; 
				}
				
				if(updateFlag)
				{
					ApiResponse<Object> askClassResponse = this.quesDbService.updateOneAskService(ask);
					if(askClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*==== 问题模块结束 ====*/
	
	/*==== 考试试卷概要模块开始 ====*/
	/**
	 * 考试试卷概要列表页面
	 * @return
	 */
	@RequestMapping("/examDescList")
	public String examDescList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examDescList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/*==添加额外的搜索条件==*/
		/*====考试类型开始====*/
		String examType = request.getParameter("examType");
		if(examType == null)
		{
			examType = "" ;
		}
		condMap.put("examType", examType);
		request.setAttribute("examType", examType);
		/*====考试类型结束====*/
		
		/*====classId开始====*/
		String classId = request.getParameter("classId");
		if(classId == null)
		{
			classId = "" ;
		}
		condMap.put("classId", classId);
		request.setAttribute("classId", classId);
		/*====classId结束====*/
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AExamDesc> response = this.examDbService.findCondListExamDescService(pageInfoUtil, condMap);
		/* 将结果存储到Reexamt中 */
		model.addAttribute("response", response.toJSON());
		return "/back/ques/examDescList";
	}
	
	/**
	 * 打开添加考试试卷概要页面
	 * @return
	 */
	@RequestMapping("/examDescInsert")
	public String examDescInsert(HttpServletRequest request)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examDescInsert--");
		/* 接收班级Id */
		String classId = request.getParameter("classId");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		/* 根据id查询班级 */
		condMap.clear();
		condMap.put("id", classId);
		ApiResponse<AClass> classApiResponse = this.classDbService.findOneClassService(condMap);
		request.setAttribute("classApiResponse", classApiResponse.toJSON());
		
		return "/back/ques/examDescInsert";
	}
	
	/**
	 * 添加考试试卷概要提交操作
	 * @return
	 */
	@RequestMapping(value = "/examDescInsertSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String examDescInsertSubmit(String pubTimeStr , AExamDesc examDesc,HttpServletRequest request)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examDescInsertSubmit--");
		AAdmins admins = (AAdmins) this.findObjfromSession(request, "admins");
		
		examDesc.setCreateId(admins.getId());
		String stTimeStr = request.getParameter("stTimeStr");
		String edTimeStr = request.getParameter("edTimeStr");
		
		/* 为对象赋值 */
		examDesc.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		examDesc.setCreateTime(new Date());
		examDesc.setUpdateTime(new Date());
		
		examDesc.setStTime(this.dateFormatUtil.strDateTime(stTimeStr));
		examDesc.setEdTime(this.dateFormatUtil.strDateTime(edTimeStr));
		
		/* 保存结果 */
		ApiResponse response = this.examDbService.saveOneExamDescService(examDesc);
		ConstatFinalUtil.SYS_LOGGER.info("--考试试卷概要添加返回结果:{}--",response.toJSON());
		return response.toJSON().toJSONString();
	}

	/**
	 * 打开更新考试试卷概要页面
	 * @return
	 */
	@RequestMapping("/examDescUpdate")
	public String examDescUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examDescUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AExamDesc> apiResponse = this.examDbService.findOneExamDescService(condMap);
		/* 存储reexamt */
		model.addAttribute("response", apiResponse.toJSON());
		
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/ques/examDescUpdate";
		}else if("audit".equalsIgnoreCase(operType))
		{
			return "/back/ques/examDescAudit";
		}else if("askList".equalsIgnoreCase(operType))
		{
			apiResponse = this.examOperService.proccedOneExamDescService(condMap);
			model.addAttribute("response", apiResponse.toJSON());
			/* 一键生成试卷 */
			return "/back/ques/examDescAskUpdate";
		}
		return "/back/ques/examDescInfo";
	}
	
	/**
	 * 更新考试试卷概要提交操作
	 * @return
	 */
	@RequestMapping(value = "/examDescUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String examDescUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examDescUpdateSubmit--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AExamDesc> apiResponseRes= this.examDbService.findOneExamDescService(condMap);
		/* 获取java对象 */
		AExamDesc examDesc = apiResponseRes.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 修改 */
			String name = request.getParameter("name");
			String hardStar = request.getParameter("hardStar");
			String stTimeStr = request.getParameter("stTimeStr");
			String edTimeStr = request.getParameter("edTimeStr");
			String score = request.getParameter("score");
			String content = request.getParameter("content");
			String examType = request.getParameter("examType");
			String status = request.getParameter("status");
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			
			examDesc.setName(name);
			examDesc.setHardStar(Byte.valueOf(hardStar));
			examDesc.setStTime(this.dateFormatUtil.strDateTime(stTimeStr));
			examDesc.setEdTime(this.dateFormatUtil.strDateTime(edTimeStr));
			examDesc.setScore(Integer.valueOf(score));
			examDesc.setContent(content);
			examDesc.setExamType(Byte.valueOf(examType));
			examDesc.setStatus(Byte.valueOf(status));
			examDesc.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			
			flag = true; 
		}else if("audit".equalsIgnoreCase(operType))
		{
			String status = request.getParameter("status");
			examDesc.setStatus(Byte.valueOf(status));
			flag = true ; 
		}else if("askList".equalsIgnoreCase(operType))
		{
			if(examDesc.getStatus() == AExamDescEnum.STATUS_DRAFT.getStatus())
			{
				/* 一键生成试卷 */
				String[] ids = request.getParameterValues("ids");
				for (int i = 0; i < ids.length; i++)
				{
					String idTemp = ids[i];
					
					/* 查询问题 */
					condMap.clear();
					condMap.put("id", idTemp);
					
					ApiResponse<AAsk> askApiResponse = this.quesDbService.findOneAskService(condMap);
					AAsk askRes = askApiResponse.getDataOneJava();
					
					AExamDetail examDetail = new AExamDetail();
					examDetail.setExamId(examDesc.getId());
					examDetail.setAskId(askRes.getId());
					
					/* 设置其他信息 */
					examDetail.setName(askRes.getName());
					examDetail.setAskType(askRes.getAskType());
					
					examDetail.setContent(askRes.getContent());
					
					examDetail.setHardStar(askRes.getHardStar());
					examDetail.setScore(askRes.getScore());
					examDetail.setStatus(AExamDetailEnum.STATUS_ENABLE.getStatus());
					examDetail.setCreateTime(askRes.getCreateTime());
					examDetail.setUpdateTime(askRes.getUpdateTime());
					examDetail.setPubTime(askRes.getPubTime());
					
					/* 保存试卷问题 */
					apiResponse = this.examDbService.saveOneExamDetailService(examDetail);
				}
				
				examDesc.setStatus(AExamDescEnum.STATUS_PUBLISH.getStatus());
				
				flag = true ; 
			}else
			{
				/* 状态不正确 */
				apiResponse.setCode(ApiResponseEnum.INFO_STATUS_ERROR.getStatus());
				apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);;
			}
		}
		
		/* 数据库操作 */
		if(flag)
		{
			/* 数据库操作 */
			apiResponse = examDbService.updateOneExamDescService(examDesc);
			ConstatFinalUtil.SYS_LOGGER.info("--考试试卷概要更新返回结果:{}--",apiResponse.toJSON());
		}
		return apiResponse.toJSON().toJSONString();
	}
	
	/**
	 * 考试试卷概要批量操作
	 * @return
	 */
	@RequestMapping(value = "/examDescBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String examDescBatch(HttpServletRequest reexamt,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examDescBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = reexamt.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = reexamt.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				if("updateStatus".equalsIgnoreCase(operType))
				{
					String status = reexamt.getParameter("status");
					
					condMap.clear();
					condMap.put("id", id);
					ApiResponse<AExamDesc> apiResponseDb = this.examDbService.findOneExamDescService(condMap);
					AExamDesc examDesc = apiResponseDb.getDataOneJava();
					/* 设置状态 */
					examDesc.setStatus(Byte.valueOf(status));
					examDesc.setPubTime(new Date());
					
					ApiResponse<Object> examDescClassResponse = this.examDbService.updateOneExamDescService(examDesc);
					if(examDescClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*==== 考试试卷概要模块结束 ====*/
	
	/*==== 试卷模块开始 ====*/
	/**
	 * 试卷列表页面
	 * @return
	 */
	@RequestMapping("/examScoreDescList")
	public String examScoreDescList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/*==添加额外的搜索条件==*/
		/*====usersId开始====*/
		String usersId = request.getParameter("usersId");
		if(usersId == null)
		{
			usersId = "" ;
		}
		condMap.put("usersId", usersId);
		request.setAttribute("usersId", usersId);
		/*====usersId结束====*/
		/*====usersClaId开始====*/
		String usersClaId = request.getParameter("usersClaId");
		if(usersClaId == null)
		{
			usersClaId = "" ;
		}
		condMap.put("usersClaId", usersClaId);
		request.setAttribute("usersClaId", usersClaId);
		/*====usersClaId结束====*/
		/*====adminsId开始====*/
		String adminsId = request.getParameter("adminsId");
		if(adminsId == null)
		{
			adminsId = "" ;
		}
		condMap.put("adminsId", adminsId);
		request.setAttribute("adminsId", adminsId);
		/*====adminsId结束====*/
		/*====examId开始====*/
		String examId = request.getParameter("examId");
		if(examId == null)
		{
			examId = "" ;
		}
		condMap.put("examId", examId);
		request.setAttribute("examId", examId);
		/*====examId结束====*/
		/*====classId开始====*/
		String classId = request.getParameter("classId");
		if(classId == null)
		{
			classId = "" ;
		}
		condMap.put("classId", classId);
		request.setAttribute("classId", classId);
		/*====classId结束====*/
		/*====orderBy开始====*/
		String orderBy = request.getParameter("orderBy");
		if(orderBy == null)
		{
			orderBy = "" ;
		}
		condMap.put("orderBy", orderBy);
		request.setAttribute("orderBy", orderBy);
		/*====orderBy结束====*/
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AExamScoreDesc> response = this.examDbService.findCondListExamScoreDescService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		
		return "/back/ques/examScoreDescList";
	}

	/**
	 * 打开更新试卷页面
	 * @return
	 */
	@RequestMapping("/examScoreDescUpdate")
	public String examScoreDescUpdate(String id , Model model,HttpServletRequest request)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AExamScoreDesc> apiResponse = this.examDbService.findOneExamScoreDescService(condMap);
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		request.setAttribute("operType", operType);
		
		if("socreUpdate".equalsIgnoreCase(operType))
		{
			return "/back/ques/examScoreDescUpdate";
		}
		return "/back/ques/examScoreDescInfo";
	}
	
	/**
	 * 更新试卷提交操作
	 * @return
	 */
	@RequestMapping(value = "/examScoreDescUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String examScoreDescUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescUpdateSubmit--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		AAdmins admins = (AAdmins) this.findObjfromSession(request, "admins");
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AExamScoreDesc> apiResponseRes = this.examDbService.findOneExamScoreDescService(condMap);
		AExamScoreDesc examScoreDesc = apiResponseRes.getDataOneJava() ; 
		
		boolean flag = false ;
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("socreUpdate".equalsIgnoreCase(operType))
		{
			List<AExamScoreDetail> examScoreDetailList = new ArrayList<AExamScoreDetail>() ; 
			/* 获取所有的参数 */
			Map<String, String[]> parameterMap = request.getParameterMap();
			for (Iterator iterator = parameterMap.entrySet().iterator(); iterator.hasNext();)
			{
				Entry<String, String[]> me = (Entry<String, String[]>) iterator.next();
				String key = me.getKey() ;
				String[] values = me.getValue() ; 
				
				if(key.startsWith("detailId_"))
				{
					AExamScoreDetail examScoreDetail = new AExamScoreDetail();
					
					/* 是多选择的问题;单选,多选 */
					String[] keys = key.split("_");
					/* 问题id;askId */
					String detailId = keys[1] ; 
					/* 选项的值;会有多选 */
					String val = values[0] ; 
					
					examScoreDetail.setId(Integer.valueOf(detailId));
					examScoreDetail.setScore(Integer.valueOf(val));
					
					/* 添加到集合中 */
					examScoreDetailList.add(examScoreDetail);
				}
			}
			
			examScoreDesc.setAdminsId(admins.getId());
			
			/* 存储集合 */
			examScoreDesc.setExamScoreDetailList(examScoreDetailList);
			
			examScoreDesc.setStatus(AExamScoreDescEnum.STATUS_FINISH.getStatus());
			/* 为对象赋值 */
			examScoreDesc.setPubTime(new Date());
			
			flag = true ; 
		}
		if(flag)
		{
			/* 更新数据 */
			apiResponse = this.examDbService.updateOneExamScoreDescService(examScoreDesc);
			ConstatFinalUtil.SYS_LOGGER.info("--分数概要添加返回结果:{}--",apiResponse.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", apiResponse.toJSON());
		}
		return apiResponse.toJSON().toJSONString();
	}
	
	/**
	 * 试卷批量操作
	 * @return
	 */
	@RequestMapping(value = "/examScoreDescBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String examScoreDescBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				condMap.clear();
				condMap.put("id", id);
				ApiResponse<AExamScoreDesc> apiResponseDb = this.examDbService.findOneExamScoreDescService(condMap);
				AExamScoreDesc examScoreDesc = apiResponseDb.getDataOneJava();
				
				boolean updateFlag = false ; 
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					/* 设置状态 */
					examScoreDesc.setStatus(Byte.valueOf(status));
					
					updateFlag = true ; 
				}
				
				if(updateFlag)
				{
					ApiResponse<Object> examScoreDescClassResponse = this.examDbService.updateOneExamScoreDescService(examScoreDesc);
					if(examScoreDescClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*==== 试卷模块结束 ====*/
}

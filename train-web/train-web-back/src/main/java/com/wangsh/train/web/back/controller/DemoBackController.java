package com.wangsh.train.web.back.controller;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.demo.pojo.ADemoDynasty;
import com.wangsh.train.demo.pojo.ADemoDynastyEnum;
import com.wangsh.train.demo.pojo.ADemoKing;
import com.wangsh.train.demo.service.IDemoDbService;

/**
 * 	模板示例代码操作
 * 	@author wangsh
 */
@Controller
@RequestMapping("/back/demo/")
public class DemoBackController extends BaseController
{
	@Resource
	private IDemoDbService demoDbService;
	
	/**
	 * 朝代列表页面
	 * @return
	 */
	@RequestMapping("/dynastyList")
	public String dynastyList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dynastyList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<ADemoDynasty> response = this.demoDbService.findCondListDynastyService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/back/demo/dynastyList";
	}

	/**
	 * 打开添加朝代页面
	 * @return
	 */
	@RequestMapping("/dynastyInsert")
	public String dynastyInsert()
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dynastyInsert--");
		return "/back/demo/dynastyInsert";
	}
	
	/**
	 * 添加朝代提交操作
	 * @return
	 */
	@RequestMapping(value = "/dynastyInsertSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String dynastyInsertSubmit(String pubTimeStr , ADemoDynasty dynasty)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dynastyInsertSubmit--");
		/* 为对象赋值 */
		dynasty.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		dynasty.setCreateTime(new Date());
		dynasty.setUpdateTime(new Date());
		/* 保存结果 */
		ApiResponse response = this.demoDbService.saveOneDynastyService(dynasty);
		ConstatFinalUtil.SYS_LOGGER.info("--朝代添加返回结果:{}--",response.toJSON());
		return response.toJSON().toJSONString();
	}
	
	/**
	 * 打开更新朝代页面
	 * @return
	 */
	@RequestMapping("/dynastyUpdate")
	public String dynastyUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dynastyUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<ADemoDynasty> response = this.demoDbService.findOneDynastyService(condMap);
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/demo/dynastyUpdate";
		}
		return "/back/demo/dynastyInfo";
	}
	
	/**
	 * 添加朝代提交操作
	 * @return
	 */
	@RequestMapping(value = "/dynastyUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String dynastyUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dynastyUpdateSubmit--");
		ApiResponse<Object> dbRes = new ApiResponse<Object>();
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<ADemoDynasty> response = this.demoDbService.findOneDynastyService(condMap);
		/* 获取java对象 */
		ADemoDynasty dynasty = response.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String name = request.getParameter("name");
			String capital = request.getParameter("capital");
			String stYear = request.getParameter("stYear");
			String edYear = request.getParameter("edYear");
			String status = request.getParameter("status");
			String pubTimeStr = request.getParameter("pubTimeStr");
			String content = request.getParameter("content");
			/* 设置属性 */
			dynasty.setName(name);
			dynasty.setCapital(capital);
			dynasty.setStYear(Integer.valueOf(stYear));
			dynasty.setEdYear(Integer.valueOf(edYear));
			dynasty.setContent(content);
			dynasty.setStatus(Byte.valueOf(status));
			
			dynasty.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			dynasty.setUpdateTime(new Date());
			
			flag = true ; 
		}
		
		if(flag)
		{
			/* 数据库操作 */
			dbRes = demoDbService.updateOneDynastyService(dynasty);
			ConstatFinalUtil.SYS_LOGGER.info("--朝代添加返回结果:{}--",dbRes.toJSON());
		}
		return dbRes.toJSON().toJSONString();
	}
	
	/**
	 * 朝代批量操作
	 * @return
	 */
	@RequestMapping(value = "/dynastyBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String dynastyBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dynastyBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				if("updateStatus".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					condMap.clear();
					condMap.put("id", id);
					ApiResponse<ADemoDynasty> apiResponseDb = this.demoDbService.findOneDynastyService(condMap);
					ADemoDynasty dynasty = apiResponseDb.getDataOneJava();
					/* 设置状态 */
					dynasty.setStatus(Byte.valueOf(status));
					dynasty.setPubTime(new Date());
					
					ApiResponse<Object> dynastyClassResponse = this.demoDbService.updateOneDynastyService(dynasty);
					if(dynastyClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	
	/**
	 * 皇上列表页面
	 * @return
	 */
	@RequestMapping("/kingList")
	public String kingList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--kingList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 增加一个朝代Id */
		String dynastyId = request.getParameter("dynastyId");
		if(dynastyId == null)
		{
			dynastyId = "" ; 
		}
		condMap.put("dynastyId", dynastyId);
		request.setAttribute("dynastyId", dynastyId);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<ADemoKing> response = this.demoDbService.findCondListKingService(pageInfoUtil, condMap);
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		
		/* 查询所有启用的朝代 */
		condMap.clear();
		condMap.put("status", ADemoDynastyEnum.STATUS_ENABLE.getStatus());
		ApiResponse<ADemoDynasty> dynastyResponse = this.demoDbService.findCondListDynastyService(null, condMap);
		model.addAttribute("dynastyResponse", dynastyResponse.toJSON());
		
		return "/back/demo/kingList";
	}

	/**
	 * 打开添加皇上页面
	 * @return
	 */
	@RequestMapping("/kingInsert")
	public String kingInsert(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--kingInsert--");
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 查询所有启用的朝代 */
		condMap.clear();
		condMap.put("status", ADemoDynastyEnum.STATUS_ENABLE.getStatus());
		ApiResponse<ADemoDynasty> dynastyResponse = this.demoDbService.findCondListDynastyService(null, condMap);
		model.addAttribute("dynastyResponse", dynastyResponse.toJSON());
		return "/back/demo/kingInsert";
	}
	
	/**
	 * 添加皇上提交操作
	 * @return
	 */
	@RequestMapping(value = "/kingInsertSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String kingInsertSubmit(String pubTimeStr , ADemoKing king)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--kingInsertSubmit--");
		/* 为对象赋值 */
		king.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		king.setCreateTime(new Date());
		king.setUpdateTime(new Date());
		/* 保存结果 */
		ApiResponse response = this.demoDbService.saveOneKingService(king);
		ConstatFinalUtil.SYS_LOGGER.info("--皇上添加返回结果:{}--",response.toJSON());
		return response.toJSON().toJSONString();
	}
	
	/**
	 * 打开更新皇上页面
	 * @return
	 */
	@RequestMapping("/kingUpdate")
	public String kingUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--kingUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<ADemoKing> response = this.demoDbService.findOneKingService(condMap);
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("update".equalsIgnoreCase(operType))
		{
			/* 查询所有启用的朝代 */
			condMap.clear();
			condMap.put("status", ADemoDynastyEnum.STATUS_ENABLE.getStatus());
			ApiResponse<ADemoDynasty> dynastyResponse = this.demoDbService.findCondListDynastyService(null, condMap);
			model.addAttribute("dynastyResponse", dynastyResponse.toJSON());
			
			return "/back/demo/kingUpdate";
		}
		return "/back/demo/kingInfo";
	}
	
	/**
	 * 添加皇上提交操作
	 * @return
	 */
	@RequestMapping(value = "/kingUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String kingUpdateSubmit(String id,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--kingUpdateSubmit--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<ADemoKing> response = this.demoDbService.findOneKingService(condMap);
		/* 获取java对象 */
		ADemoKing king = response.getDataOneJava() ;
		
		String dynastyId = request.getParameter("dynastyId");
		String name = request.getParameter("name");
		String content = request.getParameter("content");
		String miaoHao = request.getParameter("miaoHao");
		String nianHao = request.getParameter("nianHao");
		String status = request.getParameter("status");
		String pubTimeStr = request.getParameter("pubTimeStr");
		
		/* 设置属性 */
		king.setDynastyId(Integer.valueOf(dynastyId));
		king.setName(name);
		king.setMiaoHao(miaoHao);
		king.setNianHao(nianHao);
		king.setContent(content);
		king.setStatus(Byte.valueOf(status));
		
		king.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		king.setUpdateTime(new Date());
		/* 数据库操作 */
		ApiResponse dbRes = demoDbService.updateOneKingService(king);
		ConstatFinalUtil.SYS_LOGGER.info("--皇上添加返回结果:{}--",dbRes.toJSON());
		return dbRes.toJSON().toJSONString();
	}
}

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<c:set value="${requestScope.cateApiResponse.data.list }" var="cateList"/>
<c:set value="${requestScope.educeApiResponse.data.list }" var="educeList"/>
<jsp:useBean class="com.wangsh.train.job.pojo.AJobOffer" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看offer - 培训机构</title>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		
		<link rel="stylesheet" href="${rootPath }/common/resource/bootstrapvalidator/css/form-validation.css" rel="stylesheet">
	  	<script type="text/javascript" src="${rootPath }/common/resource/bootstrapvalidator/js/form-validation.js"></script>
	  	
	  	<script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.config.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.all.min.js"> </script>
	    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
	    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/lang/zh-cn/zh-cn.js"></script>
	  	
	  	<script type="text/javascript">
	  		$(function()
			{
			  	$("#form").FormValidation({
		  			fields:
	  				{
		  				/* 名称 */
	  					name:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '名称不能为空'
		  	                    },
		  	                    stringLength: {
		  	                        min: 2,
		  	                        max:200,
		  	                        message: '名称的长度为在2--200位'
		  	                    }
		  	                }
	  					},
	  					wages:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '工资不能为空'
		  	                    },
		  	                  	regexp: {
		  	                  		regexp: /^[\d]+$/,
		  	                        message: '工资格式不正确,必须为数字'
		  	                    }
		  	                }
	  					},
	  					interCount:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '面试次数不能为空'
		  	                    },
		  	                  	regexp: {
		  	                  		regexp: /^[\d]+$/,
		  	                        message: '面试次数格式不正确,必须为数字'
		  	                    }
		  	                }
	  					}
	  				}
				});
			  	/* 百度编辑器 */
			  	var ue = UE.getEditor('editor');
			});
	  	</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看offer</h2>
					<form id="form" class="form-horizontal" action="${rootPath }/head/job/offerUpdateSubmit.htm" method="post">
						
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称:</label>
							${one.name }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">工资:</label>
							${one.wages }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">五险:</label>
							${one.fiveRisksStr }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">公积金:</label>
							${one.accFundStr }
						</div>
						<div class="form-group row">
							<label for="interCount" class="col-sm-2 control-label">面试次数:</label>
							${one.interCount }
						</div>
						<div class="form-group row">
							<label for="interCount" class="col-sm-2 control-label">offer截图:</label>
							<a href="${rootPath}/head/system/sysFileList.htm?relaId=${one.id}&fileRelaType=4" target="_blank">
								查看附件
							</a>
						</div>
						<div class="form-group row">
							<label for="status" class="col-sm-2 control-label">状态:</label>
							${one.statusStr }
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">创建时间:</label>
							${one.createTime }
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">更新时间:</label>
							${one.updateTime }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							<c:if test="${one.pubTime != null && one.pubTime != '' }">
								<c:set value="${one.pubTime }" var="sys_now_format"/>
							</c:if>
							${sys_now_format}
						</div>
						<div class="form-group row">
							<label for="content" class="col-sm-2 control-label">内容:</label>
							${one.content}
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
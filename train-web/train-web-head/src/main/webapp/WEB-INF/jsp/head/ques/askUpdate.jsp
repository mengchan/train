<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<c:set value="${requestScope.sysTechResponse.data.list}" var="sysProList"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AAsk" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新问题 - 培训机构</title>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		
		<link rel="stylesheet" href="${rootPath }/common/resource/bootstrapvalidator/css/form-validation.css" rel="stylesheet">
	  	<script type="text/javascript" src="${rootPath }/common/resource/bootstrapvalidator/js/form-validation.js"></script>
	  	
	  	<script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.config.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.all.min.js"> </script>
	    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
	    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/lang/zh-cn/zh-cn.js"></script>
	  	
	  	<script type="text/javascript">
	  		$(function()
			{
			  	$("#form").FormValidation({
		  			fields:
	  				{
		  				/* 名称 */
	  					name:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '名称不能为空'
		  	                    },
		  	                    stringLength: {
		  	                        min: 2,
		  	                        max:200,
		  	                        message: '名称的长度为在2--200位'
		  	                    }
		  	                }
	  					},
	  					score:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '分数不能为空'
		  	                    },
		  	                  	regexp: {
		  	                  		regexp: /^[\d]+$/,
		  	                        message: '分数格式不正确,必须为数字'
		  	                    }
		  	                }
	  					}
	  				}
				});
			  	/* 百度编辑器 */
			  	var ue = UE.getEditor('editor');
			  	var ue2 = UE.getEditor('editor2');
			  	
			  	var ue1 = UE.getEditor('editor1',{
		            //这里可以选择自己需要的工具按钮名称,此处仅选择如下五个
		            toolbars:[['FullScreen', 'Source', 'Undo', 'Redo','Bold','test']],
		            //关闭字数统计
		            wordCount:false,
		            //关闭elementPath
		            elementPathEnabled:false,
		        });
			});
	  	</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">更新问题</h2>
					<form id="form" class="form-horizontal" action="${rootPath }/head/ques/askUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">技术:</label>
							<div class="col-sm-8">
								<select name="techId" id="techId" class="form-control mr-sm-2">
									<c:forEach items="${sysProList }" var="listTemp" varStatus="stat">
										<option value="${listTemp.id}" ${one.techId == listTemp.id ? 'selected' : '' }>${listTemp.name}</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称:</label>
							<div class="col-sm-8">
								<input type="text" value="${one.name }"
									id="name" name="name" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">难易程度:</label>
						</div>
						<div class="form-group">
							<div class="col-sm-8 form-check form-check-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<c:forEach begin="1" end="5" step="1" var="temp">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<label for="hardStar${temp }" class="form-check-label">
										<input type="radio" id="hardStar${temp }" name="hardStar" class="form-check-input" value="${temp }" ${one.hardStar == temp ? 'checked' : '' }>
										${temp}
									</label>
									&nbsp;&nbsp;
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							<div class="col-sm-8">
								<c:if test="${one.pubTime != null && one.pubTime != '' }">
									<c:set value="${one.pubTime }" var="sys_now_format"/>
								</c:if>
								<input type="text" readonly="readonly" value="${sys_now_format}"
									onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
									id="pubTimeStr" name="pubTimeStr" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">类型:</label>
						</div>
						<div class="form-group">
							<div class="col-sm-8 form-check form-check-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<c:set value="1" var="currCount"/>
								<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'ASKTYPE_')}">
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<label for="${me.key }" class="form-check-label">
											<input type="radio" id="${me.key }" name="askType" class="form-check-input" value="${keys[1] }" ${one.askType == keys[1] ? 'checked' : '' }>
											${me.value }
										</label>
										<c:set value="${currCount + 1 }" var="currCount"/>
										&nbsp;&nbsp;
									</c:if>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">来源:</label>
						</div>
						<div class="form-group">
							<div class="col-sm-8 form-check form-check-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<c:set value="1" var="currCount"/>
								<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'SOUTYPE_')}">
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<label for="${me.key }" class="form-check-label">
											<input type="radio" id="${me.key }" name="souType" class="form-check-input" value="${keys[1] }" ${one.souType == keys[1] ? 'checked' : '' }>
											${me.value }
										</label>
										<c:set value="${currCount + 1 }" var="currCount"/>
										&nbsp;&nbsp;
									</c:if>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">状态:</label>
						</div>
						<div class="form-group">
							<div class="col-sm-8 form-check form-check-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<!-- 拆分,按照-拆分,取最后一个 -->
								<label for="status0" class="form-check-label">
									<input type="radio" id="status0" name="status" class="form-check-input" value="0" ${one.status == '0' || one.status == '3' ? 'checked' : '' }>
									草稿
								</label>
								&nbsp;&nbsp;
								<label for="status1" class="form-check-label">
									<input type="radio" id="status1" name="status" class="form-check-input" value="1" ${one.status == '1' ? 'checked' : '' }>
									发布
								</label>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">知识点:</label>
							<div class="col-sm-8">
								<script id="editor1" name="examContent" type="text/plain" style="width:800px;height:200px;">${one.examContent}</script>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">答案:</label>
							<div class="col-sm-8">
								<script id="editor2" name="answer" type="text/plain" style="width:800px;height:300px;">${one.answer}</script>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">内容:</label>
							<div class="col-sm-8">
								<script id="editor" name="content" type="text/plain" style="width:800px;height:300px;">${one.content}</script>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
						<div class="form-group">
							<label for="currDateStr" class="col-sm-2 control-label">注意事项:</label>
							<ol class="text-info">
								<li>如果是判断题,答案是:(0:正确,1:错误)</li>
							</ol>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<c:set value="${requestScope.cateApiResponse.data.list }" var="cateList"/>
<c:set value="${requestScope.educeApiResponse.data.list }" var="educeList"/>
<jsp:useBean class="com.wangsh.train.job.pojo.AJobInterview" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看面试 - 培训机构</title>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		
		<link rel="stylesheet" href="${rootPath }/common/resource/bootstrapvalidator/css/form-validation.css" rel="stylesheet">
	  	<script type="text/javascript" src="${rootPath }/common/resource/bootstrapvalidator/js/form-validation.js"></script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看面试</h2>
					<%-- 自己不能顶和踩自己 --%>
					<c:if test="${sessionScope.users.id != one.createId }">
						<a href="${rootPath }/head/job/interviewUpdateSubmit.htm?id=${one.id}&operType=upView" class="href_info" onclick="return confirm('确认要操作吗?')">
							顶
						</a>
						<a href="${rootPath }/head/job/interviewUpdateSubmit.htm?id=${one.id}&operType=downView" class="href_info" onclick="return confirm('确认要操作吗?')">
							踩
						</a>
					</c:if>
					<form id="form" class="form-horizontal" action="${rootPath }/head/job/interviewUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">创建人:</label>
							${one.create.nickName }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">公司:</label>
							<a href="${rootPath}/head/job/jobCompanyUpdate.htm?id=${one.companyId}" target="_blank">
								${one.company.name }
							</a>
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称:</label>
							${one.name }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label" title="面试时间">面时:</label>
							${one.interTime }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">单价:</label>
							${one.price }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">面试类型:</label>
							${one.simulationStr }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">笔试照片:</label>
							<a href="${rootPath}/head/system/sysFileList.htm?relaId=${one.id }&fileRelaType=3" target="_blank">
								查看
							</a>
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">录音路径:</label>
							<a href="${one.voicePath }" target="_blank">${one.voicePath }</a>
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">录音解码:</label>
							${one.voiceCode }
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">顶:</label>
							${one.upNum }
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">踩:</label>
							${one.downNum }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label" title="面试官评价">评价:</label>
							${one.eval }
						</div>
						<div class="form-group row">
							<label for="content" class="col-sm-2 control-label">状态:</label>
							${one.statusStr}
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">创建时间:</label>
							${one.createTime }
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">更新时间:</label>
							${one.updateTime }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							${one.pubTime}
						</div>
						<div class="form-group row">
							<label for="content" class="col-sm-2 control-label">审核时间:</label>
							${one.auditTime}
						</div>
						<div class="form-group row">
							<label for="content" class="col-sm-2 control-label">审核意见:</label>
							${one.auditContent}
						</div>
						<div class="form-group row">
							<label for="content" class="col-sm-2 control-label">内容:</label>
							${one.content}
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
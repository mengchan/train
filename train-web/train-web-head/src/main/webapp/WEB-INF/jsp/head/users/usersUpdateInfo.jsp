<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean id="usersPojo" class="com.wangsh.train.users.pojo.AUsers"/>
<c:set value="${requestScope.usersResponse.data.one }" var="users"/>
<c:set value="${requestScope.classResponse.data.list }" var="list"/>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>修改个人信息 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
	    <link rel="stylesheet" href="${rootPath }/common/resource/bootstrapvalidator/css/form-validation.css" rel="stylesheet">
	  	<script type="text/javascript" src="${rootPath }/common/resource/bootstrapvalidator/js/form-validation.js"></script>
	  	
	  	<script language="javascript" type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
	  	<script type="text/javascript">
	  		$(function()
			{
			  	$("#form").FormValidation({
		  			fields:
	  				{
	  				}
				});
			  	
			});
	  	</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页的主体 -->
		<div class="container">
			<div class="col-md-8 col-sm-10">
				<form id="form" method="post" class="form-horizontal" action="${rootPath }/head/users/usersUpdateSubmit.htm">
					<input type="hidden" name="operType" value="${param.operType }">
	
					<h2
						class="col-sm-10 col-sm-offset-1 form-signin-heading text-center">修改个人信息</h2>
					<div class="form-group">
						<label for="email" class="col-sm-3 control-label">邮箱</label>
						<div class="col-sm-8">
							${users.email }
							<a href="${rootPath }/head/users/usersUpdateSubmit.htm?operType=updateSyna">同步个人信息</a>
						</div>
					</div>
					<%-- <div class="form-row">
						<div class="col-auto">
							<label for="inputPassword" class="col-sm-10 control-label">班级</label>
							<div class="col-auto">
								<span>
									<select name="classId" class="custom-select">
										<c:forEach items="${list }" var="listTemp" varStatus="stat">
											<option value="${listTemp.id }" ${listTemp.id == users.classId ? 'selected' : ''}>${listTemp.name }</option>
										</c:forEach>
									</select>
								</span>
							</div>
						</div>
					</div> --%>
					<div class="form-group">
						<div class="col-sm-8 col-sm-offset-2">
							<button class="btn btn-md btn-primary" type="submit">提交</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
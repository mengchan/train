<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean id="usersPojo" class="com.wangsh.train.users.pojo.AUsers"/>
<c:set value="${requestScope.usersResponse.data.one }" var="users"/>
<c:set value="${requestScope.classResponse.data.list }" var="list"/>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>修改个人信息 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页的主体 -->
		<div class="container">
			<div class="col-md-8 col-sm-3">
				<form id="form" method="post" class="form-horizontal" action="${rootPath }/head/users/usersUpdateSubmit.htm">
					<input type="hidden" name="operType" value="${param.operType }">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center"">查看个人信息</h2>
					<div class="form-group row">
						<label for="email" class="col-sm-3 control-label">邮箱</label>
						<div class="col-sm-8">
							${users.email }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-3 control-label">昵称</label>
						<div class="col-sm-8">
							${users.nickName }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-3 control-label">真实姓名</label>
						<div class="col-sm-8">
							${users.trueName }
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword" class="col-sm-3 control-label">班级</label>
						<div class="col-sm-8">
							${users.cla.name }
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword" class="col-sm-3 control-label">单点登陆id</label>
						<div class="col-sm-8">
							${users.ssoId }
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword" class="col-sm-3 control-label">阶段</label>
						<div class="col-sm-8">
							${users.stageTypeStr }
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword" class="col-sm-3 control-label">创建时间</label>
						<div class="col-sm-8">
							${users.createTime}
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword" class="col-sm-3 control-label">更新时间</label>
						<div class="col-sm-8">
							${users.updateTime}
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword" class="col-sm-3 control-label">上次登陆时间</label>
						<div class="col-sm-8">
							${users.lastLoginTime}
						</div>
					</div>
				</form>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
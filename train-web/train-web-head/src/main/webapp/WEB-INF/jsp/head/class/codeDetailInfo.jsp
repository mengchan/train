<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.cla.pojo.ACodeDetail" id="pojo"/>
<c:set value="${requestScope.response.data.one}" var="pojoTemp"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看代码统计 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center">查看代码统计明细</h2>
					<input type="hidden" name="operType" value="update">
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">代码统计Id:</label>
						<div class="col-sm-8">
							${pojoTemp.codeStat.name}
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">名称:</label>
						<div class="col-sm-8">
							${pojoTemp.name }
						</div> 
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">原始:</label>
						<div class="col-sm-8">
							${pojoTemp.oriName }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">文件路径:</label>
						<div class="col-sm-8">
							${pojoTemp.filePath }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">总行数:</label>
						<div class="col-sm-8">
							${pojoTemp.totalCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">单行数:</label>
						<div class="col-sm-8">
							${pojoTemp.sinComCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">多行数:</label>
						<div class="col-sm-8">
							${pojoTemp.mulComCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">空行数:</label>
						<div class="col-sm-8">
							${pojoTemp.blankCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">有效代码:</label>
						<div class="col-sm-8">
							${pojoTemp.codeCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">文件类型:</label>
						<div class="col-sm-8">
							${pojoTemp.fileTypeStr }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">文件Md5:</label>
						<div class="col-sm-8">
							${pojoTemp.fileMd5 }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">相等:</label>
						<div class="col-sm-8">
							${pojoTemp.equalsFlagStr }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">发布时间:</label>
						<div class="col-sm-8">
							${pojoTemp.pubTime}
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-2">状态：</label>
						<div class="formControls col-xs-8 col-sm-9 skin-minimal">
							${pojoTemp.statusStr }
						</div>
					</div>
					<div class="form-group row">
						<label for="address" class="col-sm-2 col-form-label">创建时间:</label>
						<div class="col-sm-8">
							${pojoTemp.createTime }
						</div>
					</div>
					<div class="form-group row">
						<label for="address" class="col-sm-2 col-form-label">更新时间:</label>
						<div class="col-sm-8">
							${pojoTemp.updateTime }
						</div>
					</div>
					<div class="form-group row">
						<label for="address" class="col-sm-2 col-form-label">内容:</label>
						<div class="col-sm-8">
							${pojoTemp.content }
						</div>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
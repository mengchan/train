<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<c:set value="${requestScope.cateApiResponse.data.list }" var="cateList"/>
<c:set value="${requestScope.educeApiResponse.data.list }" var="educeList"/>
<jsp:useBean class="com.wangsh.train.job.pojo.AJobComPosition" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新职位 - 培训机构</title>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		
		<link rel="stylesheet" href="${rootPath }/common/resource/bootstrapvalidator/css/form-validation.css" rel="stylesheet">
	  	<script type="text/javascript" src="${rootPath }/common/resource/bootstrapvalidator/js/form-validation.js"></script>
	  	
	  	<script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.config.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.all.min.js"> </script>
	    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
	    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/lang/zh-cn/zh-cn.js"></script>
	  	
	  	<script type="text/javascript">
	  		$(function()
			{
			  	$("#form").FormValidation({
		  			fields:
	  				{
		  				/* 名称 */
	  					name:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '名称不能为空'
		  	                    },
		  	                    stringLength: {
		  	                        min: 2,
		  	                        max:200,
		  	                        message: '名称的长度为在2--200位'
		  	                    }
		  	                }
	  					},
	  					wages:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '工资不能为空'
		  	                    },
		  	                  	regexp: {
		  	                  		regexp: /^[\d]+$/,
		  	                        message: '工资格式不正确,必须为数字'
		  	                    }
		  	                }
	  					},
	  					experSt:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '工作经验(起)不能为空'
		  	                    },
		  	                  	regexp: {
		  	                  	regexp: /^[\d]+$/,
		  	                        message: '工作经验(起)格式不正确,必须为数字'
		  	                    }
		  	                }
	  					},
	  					experEd:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '工作经验(止)不能为空'
		  	                    },
		  	                  	regexp: {
		  	                  		regexp: /^[\d]+$/,
		  	                        message: '工作经验(止)格式不正确,必须为数字'
		  	                    }
		  	                }
	  					}
	  				}
				});
			  	/* 百度编辑器 */
			  	var ue = UE.getEditor('editor');
			  	
			  	/* 查询一级节点 */
				selectRegion('region1','region1','','true');
			});
	  	</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">更新职位</h2>
					<form id="form" class="form-horizontal" action="${rootPath }/head/job/comPositionUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称:</label>
							<div class="col-sm-8">
								<input type="text" value="${one.name }"
									id="name" name="name" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">工资:</label>
							<div class="col-sm-8">
								<input type="text" value="${one.wages }"
									id="wages" name="wages" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-3 control-label">工作经验(起):</label>
							<div class="col-sm-8">
								<input type="text" value="${one.experSt }"
									id="experSt" name="experSt" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-3 control-label">工作经验(止):</label>
							<div class="col-sm-8">
								<input type="text" value="${one.experEd }"
									id="experEd" name="experEd" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-3 control-label">学历:</label>
							<div class="col-sm-8">
								<select name="educId" class="form-control mr-sm-2">
									<c:forEach items="${educeList}" var="listTemp" varStatus="stat">
										<option value="${listTemp.id }" ${one.educId == listTemp.id ? 'selected' : '' }>${listTemp.name }</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-3 control-label">类别:</label>
							<div class="col-sm-8">
								<select name="cateId" class="form-control mr-sm-2">
									<c:forEach items="${cateList}" var="listTemp" varStatus="stat">
										<option value="${listTemp.id }" ${one.cateId == listTemp.id ? 'selected' : '' }>${listTemp.name }</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-3 control-label">地区:</label>
							<div class="col-auto">
								<c:choose>
									<c:when test="${fn:contains(one.regionJSON.treeName,'|-->')}">
										${fn:replace(one.regionJSON.treeName,'|-->','  ')}
									</c:when>
									<c:otherwise>
										${one.regionJSON.treeName}
									</c:otherwise>
								</c:choose>
								<input type="hidden" name="regionId" id="regionId" value="${one.regionId}">
								<span>
									<select id="region1" class="custom-select" onchange="return selectRegion('region1','region2','regionId','false');">
										<option value="0">请选择</option>
									</select>
									<select id="region2" class="custom-select" onchange="return selectRegion('region2','region3','regionId','false');">
									</select>
									<select id="region3" class="custom-select" onchange="return selectRegion('region3','region4','regionId','false');">
									</select>
									<select id="region4" class="custom-select" onchange="return selectRegion('region4','region5','regionId','false');">
									</select>
									<select id="region5" class="custom-select" onchange="return selectRegion('region5','region6','regionId','false');">
									</select>
									<select id="region6" class="custom-select" onchange="return selectRegion('region6','region7','regionId','false');">
									</select>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-3 control-label">地址:</label>
							<div class="col-sm-8">
								<input type="text" value="${one.address }"
									id="address" name="address" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							<div class="col-sm-8">
								<c:if test="${one.pubTime != null && one.pubTime != '' }">
									<c:set value="${one.pubTime }" var="sys_now_format"/>
								</c:if>
								<input type="text" readonly="readonly" value="${sys_now_format}"
									onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
									id="pubTimeStr" name="pubTimeStr" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">状态:</label>
						</div>
						<div class="form-group">
							<div class="col-sm-8 form-check form-check-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<!-- 拆分,按照-拆分,取最后一个 -->
								<label for="status0" class="form-check-label">
									<input type="radio" id="status0" name="status" class="form-check-input" value="0" ${one.status == '0' || one.status == '3' ? 'checked' : '' }>
									草稿
								</label>
								&nbsp;&nbsp;
								<label for="status1" class="form-check-label">
									<input type="radio" id="status1" name="status" class="form-check-input" value="1" ${one.status == '1' ? 'checked' : '' }>
									发布
								</label>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">内容:</label>
							<div class="col-sm-8">
								<script id="editor" name="content" type="text/plain" style="width:800px;height:300px;">${one.content}</script>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
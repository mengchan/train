<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AAsk" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看问题 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看问题</h2>
					<form id="form" class="form-horizontal" action="${rootPath }/head/ques/askUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">用户：</label>
							<a href="${rootPath }/head/users/usersUpdate.htm?id=${one.usersId }" target="_blank">${one.users.nickName }</a>
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">技术：</label>
							${one.tech.name }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称：</label>
							<c:out value="${one.name }" escapeXml="true"/>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">难易程度：</label>
							${one.hardStar}
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">分数：</label>
							${one.score }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间：</label>
							<c:if test="${one.pubTime != null && one.pubTime != '' }">
								<c:set value="${one.pubTime }" var="sys_now_format"/>
							</c:if>
							${sys_now_format}
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">回答正确：</label>
							<a href="${rootPath }/head/users/usersUpdate.htm?id=${one.ansUsersId }" target="_blank">${one.ansUsers.nickName }</a>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">类型：</label>
							${one.askTypeStr }
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">来源：</label>
							${one.souTypeStr }
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">状态：</label>
							${one.statusStr }
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">创建时间：</label>
							${one.createTime }
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">更新时间：</label>
							${one.updateTime }
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">知识点：</label>
							<div class="col-sm-8">
								${one.examContent}
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">答案：</label>
							<div class="col-sm-8">
								${one.answer}
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">内容：</label>
							<div class="col-sm-8">
								${one.content}
							</div>
						</div>
						<c:if test="${one.askType == pojo.enums['ASKTYPE_SELECT'] || one.askType == pojo.enums['ASKTYPE_MULSELECT']}">
							<div class="form-group">
								<label for="pubTimeStr" class="col-sm-2 control-label"></label>
									<a class="btn btn-primary" data-toggle="collapse" href="#ansSelList" role="button" aria-expanded="true" aria-controls="ansSelList">
									   选项列表
									</a>
								<c:if test="${one.statusEnum == 'STATUS_DRAFT' || one.statusEnum == 'STATUS_AUDITNO'}">
									<a class="btn btn-primary" href="${rootPath }/head/ques/ansSelInsert.htm?askId=${one.id}" target="_blank">
									   选项添加
									</a>
								</c:if>
							</div>
						</c:if>
					</form>
				</div>
			</div>
			<c:if test="${one.askType == pojo.enums['ASKTYPE_SELECT'] || one.askType == pojo.enums['ASKTYPE_MULSELECT']}">
				<!-- 答案选项列表 -->
				<div class="row show" id="ansSelList">
				<div class="col-md-11 col-sm-offset-1">
					<h4 class="sub-header">答案选项列表</h4>
					<div class="table-responsive mt-2">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>序号</th>
									<th>编号</th>
									<th>Id</th>
									<th width="30%">选项</th>
									<th>状态</th>
									<th title="是否正确">正确</th>
									<th>创建时间</th>
									<th>发布时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${one.ansSelList }" var="listTemp" varStatus="stat">
									<tr>
										<td>${stat.count }</td>
										<td>${listTemp.sn }</td>
										<td>${listTemp.id }</td>
										<td><c:out value="${listTemp.name }" escapeXml="true"/></td>
										<td>${listTemp.statusStr }</td>
										<td>${listTemp.corrTypeStr }</td>
										<td>${listTemp.createTime }</td>
										<td>${listTemp.pubTime }</td>
										<td>
											<c:if test="${one.statusEnum == 'STATUS_DRAFT' || one.statusEnum == 'STATUS_AUDITNO'}">
												<a href="${rootPath }/head/ques/ansSelUpdate.htm?operType=update&id=${listTemp.id}" target="_blank">
													修改
												</a>
											</c:if>
											<c:set value="${listTemp.id}," var="tempStr"/>
											<c:if test="${fn:indexOf(param.answer,tempStr) != -1}">
												<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check2" fill="currentColor" xmlns="http://www.w3.org/2000/svg" title="您选择的">
												  <path fill-rule="evenodd" d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
												</svg>
											</c:if>
										</td>
									</tr>
								</c:forEach>							
							</tbody>
						</table>
					</div>
				</div>
			</div>
			</c:if>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
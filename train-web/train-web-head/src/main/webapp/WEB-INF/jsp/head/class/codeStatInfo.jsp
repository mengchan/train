<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.cla.pojo.ACodeStat" id="pojo"/>
<c:set value="${requestScope.response.data.one}" var="pojoTemp"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看代码统计 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center">查看代码统计</h2>
					<input type="hidden" name="operType" value="update">
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">用户:</label>
						<div class="col-sm-8">
							${pojoTemp.users.nickName }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">班级:</label>
						<div class="col-sm-8">
							${pojoTemp.usersClass.cla.name }
						</div> 
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">名称:</label>
						<div class="col-sm-8">
							${pojoTemp.name }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">原始:</label>
						<div class="col-sm-8">
							${pojoTemp.oriName }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">下载文件:</label>
						<div class="col-sm-8">
							<a class="href_info" href="${websiteFileUrl}${pojoTemp.filePath }">下载源文件</a>
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">总行数:</label>
						<div class="col-sm-8">
							${pojoTemp.totalCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">单行数:</label>
						<div class="col-sm-8">
							${pojoTemp.sinComCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">多行数:</label>
						<div class="col-sm-8">
							${pojoTemp.mulComCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">空行数:</label>
						<div class="col-sm-8">
							${pojoTemp.blankCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">有效代码行数:</label>
						<div class="col-sm-8">
							${pojoTemp.codeCount }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">代码状态:</label>
						<div class="col-sm-8">
							${pojoTemp.codeTypeStr }
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">压缩包大小:</label>
						<div class="col-sm-8">
							<fmt:formatNumber value="${pojoTemp.fileSize }" pattern="#,###,###"/>Byte
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">文件总大小:</label>
						<div class="col-sm-8">
							<fmt:formatNumber value="${pojoTemp.totalFileSize }" pattern="#,###,###"/>Byte
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">压缩比:</label>
						<div class="col-sm-8">
							<fmt:formatNumber value="${pojoTemp.fileSize / pojoTemp.totalFileSize }" pattern="#.###%"/>
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">发布时间:</label>
						<div class="col-sm-8">
							${pojoTemp.pubTime}
						</div>
					</div>
					<div class="form-group row">
						<label for="address" class="col-sm-2 col-form-label">创建时间:</label>
						<div class="col-sm-8">
							${pojoTemp.createTime }
						</div>
					</div>
					<div class="form-group row">
						<label for="address" class="col-sm-2 col-form-label">更新时间:</label>
						<div class="col-sm-8">
							${pojoTemp.updateTime }
						</div>
					</div>
					<div class="form-group row">
						<label for="address" class="col-sm-2 col-form-label">内容:</label>
						<div class="col-sm-8">
							${pojoTemp.content }
						</div>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
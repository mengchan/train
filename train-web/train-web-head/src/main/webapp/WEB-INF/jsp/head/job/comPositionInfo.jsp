<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<c:set value="${requestScope.cateApiResponse.data.list }" var="cateList"/>
<c:set value="${requestScope.educeApiResponse.data.list }" var="educeList"/>
<jsp:useBean class="com.wangsh.train.job.pojo.AJobComPosition" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看职位 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看职位</h2>
					<%-- 自己不能顶和踩自己 --%>
					<c:if test="${sessionScope.users.id != one.createId }">
						<a href="${rootPath }/head/job/comPositionUpdateSubmit.htm?id=${one.id}&operType=upView" class="href_info" onclick="return confirm('确认要操作吗?')">
							顶
						</a>
						<a href="${rootPath }/head/job/comPositionUpdateSubmit.htm?id=${one.id}&operType=downView" class="href_info" onclick="return confirm('确认要操作吗?')">
							踩
						</a>
					</c:if>
					<form id="form" class="form-horizontal" action="${rootPath }/head/job/comPositionUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">创建人:</label>
							${one.create.nickName }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">公司:</label>
							<a href="${rootPath}/head/job/jobCompanyUpdate.htm?id=${one.companyId}" target="_blank">
								${one.company.name }
							</a>
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称:</label>
							${one.name }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">工资:</label>
							${one.wages }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">工作经验(起):</label>
							${one.experSt }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">工作经验(止):</label>
							${one.experEd }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">学历:</label>
							${one.educPro.name }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">类别:</label>
							${one.catePro.name }
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">顶:</label>
							${one.upNum }
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">踩:</label>
							${one.downNum }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">地区:</label>
							<c:choose>
								<c:when test="${fn:contains(one.regionJSON.treeName,'|-->')}">
									${fn:replace(one.regionJSON.treeName,'|-->','  ')}
								</c:when>
								<c:otherwise>
									${one.regionJSON.treeName}
								</c:otherwise>
							</c:choose>
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">地址:</label>
							${one.address }
						</div>
						<div class="form-group row">
							<label for="content" class="col-sm-2 control-label">状态:</label>
							${one.statusStr}
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">创建时间:</label>
							${one.createTime }
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">更新时间:</label>
							${one.updateTime }
						</div>
						<div class="form-group row">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							<c:if test="${one.pubTime != null && one.pubTime != '' }">
								<c:set value="${one.pubTime }" var="sys_now_format"/>
							</c:if>
							${sys_now_format}
						</div>
						<div class="form-group row">
							<label for="content" class="col-sm-2 control-label">审核意见:</label>
							${one.auditContent}
						</div>
						<div class="form-group row">
							<label for="content" class="col-sm-2 control-label">内容:</label>
							${one.content}
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.pageInfoUtil }" var="pageInfoUtil"/>
<c:set value="${requestScope.response.data.list }" var="list"/>
<c:set value="${requestScope.sysTechResponse.data.list}" var="sysProList"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AAsk" id="pojo"/>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>问题列表 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			/*
				批量操作切换
				selId:要选择的select
				tarVal:要具体判断的值
				tarDiv:最终要显示的dom
			*/
			function batchSwitch(selId)
			{
				var selVal = $("#" + selId).val();
				/* 所有的选择信息都隐藏掉 */
				$(".selectInfo").hide();
				
				if('status' == selVal)
				{
					$("#update" + selVal).show();
				}else if('teaId' == selVal)
				{
					$("#update" + selVal).show();
				}else if('askType' == selVal)
				{
					$("#update" + selVal).show();
				}
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页的主体部分 -->
		<div class="container">
			<div class="row">
				<div class="col-">
					<h2 class="sub-header">问题列表${param.searchType == 'self'?'(自己)':'(全部)' }</h2>
					<form action="${rootPath }/head/ques/askList.htm" method="post" class="form-inline">
						<input type="hidden" name="searchType" value="${requestScope.searchType }">
						<input type="hidden" name="interviewId" value="${param.interviewId }">
						<label for="keyword">关键字:</label>
						<input type="text" name="keyword" value="${requestScope.keyword }" class="form-control mr-sm-2">
						<label for="askType">类型:</label>
						<select name="askType" id="askType" class="form-control mr-sm-2">
							<option value="">请选择</option>
							<c:set value="1" var="currCount"/>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'ASKTYPE_')}">
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.askType == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
						<label for="souType">来源:</label>
						<select name="souType" id="souType" class="form-control mr-sm-2">
							<option value="">请选择</option>
							<c:set value="1" var="currCount"/>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'SOUTYPE_')}">
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.souType == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
						<c:if test="${param.searchType == 'self' }">
							<label for="status">状态:</label>
							<select name="status" id="status" class="form-control mr-sm-2">
								<option value="">请选择</option>
								<c:set value="1" var="currCount"/>
								<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'STATUS_')}">
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
									</c:if>
								</c:forEach>
							</select>
						</c:if>
						<label for="souType">技术:</label>
						<select name="techId" id="techId" class="form-control mr-sm-2">
							<option value="">请选择</option>
							<c:forEach items="${sysProList }" var="listTemp" varStatus="stat">
								<option value="${listTemp.id}" ${requestScope.techId == listTemp.id ? 'selected' : '' }>${listTemp.name}</option>
							</c:forEach>
						</select>
						<label for="pubTime">发布时间:</label>
						<input type="text" name="st" value="${requestScope.st }" readonly="true" class="form-control mr-sm-2" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate"/>
						-->
						<input type="text" name="ed" value="${requestScope.ed }" readonly="true" class="form-control mr-sm-2" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate"/>
						<input type="submit" class="btn btn-primary" value="搜索">
						<a href="${rootPath }/head/ques/askInsert.htm?interviewId=${param.interviewId}" class="btn btn-primary ml-2" target="_blank">添加</a>
					</form>
				</div>
				<div class="col-">
					<div class="table-responsive mt-2">
						<!-- 要做批量操作 -->
						<form action="${rootPath }/head/ques/askBatch.htm" method="post" class="form-inline" id="batchForm">
							<label for="status">操作类型:</label>
							<select name="operType" id="operType" class="form-control mr-sm-2" onchange="batchSwitch('operType')">
								<option value="">请选择</option>
								<option value="status">批量更新状态</option>
								<option value="teaId">批量更新技术</option>
								<option value="askType">批量问题类型</option>
							</select>
							<div class="form-row selectInfo" id="updatestatus" style="display: none;">
								<label for="status">状态:</label>
								<select name="status" class="form-control mr-sm-2" id="statusBatch">
									<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
										<c:if test="${fn:startsWith(me.key,'STATUS')}">
											<!-- 拆分,按照-拆分,取最后一个 -->
											<c:set value="${fn:split(me.key, '-')}" var="keys"/>
											<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
							<span class="form-row selectInfo" id="updateteaId" style="display: none;">
								技术:
								<select name="teaId" id="teaIdBatch" class="form-control mr-sm-2">
									<option value="">请选择</option>
									<c:forEach items="${sysProList }" var="listTemp" varStatus="stat">
										<option value="${listTemp.id}" ${requestScope.techId == listTemp.id ? 'selected' : '' }>${listTemp.name}</option>
									</c:forEach>
								</select>
							</span>
							<span class="form-row selectInfo" id="updateaskType" style="display: none;">
								类型:
								<select name="askType" id="askTypeBatch" class="form-control mr-sm-2">
									<option value="">请选择</option>
									<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
										<c:if test="${fn:startsWith(me.key,'ASKTYPE_')}">
											<!-- 拆分,按照-拆分,取最后一个 -->
											<c:set value="${fn:split(me.key, '-')}" var="keys"/>
											<option value="${keys[1] }" ${requestScope.askType == keys[1] ? 'selected' : '' }>${me.value }</option>
										</c:if>
									</c:forEach>
								</select>
							</span>
							<a href="javascript:;" onclick="return batchOper('ids','${rootPath}/head/ques/askBatch.htm','status=' + $('#statusBatch').val() + '&teaId=' + $('#teaIdBatch').val() + '&askType=' + $('#askTypeBatch').val())"
								class="btn btn-primary">
								提交
							</a>
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>
											<div class="form-group form-check">
												<input type="checkbox" name="selectAll" id="selectAll" class="form-check-input" onclick="checkAll('selectAll','ids')">
											</div>
										</th>
										<th>序号</th>
										<th>创建人</th>
										<th>技术</th>
										<th>名称</th>
										<th>难易</th>
										<th>分数</th>
										<th>类型</th>
										<th>来源</th>
										<th>状态</th>
										<th>发布时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${list }" var="listTemp" varStatus="stat">
										<tr>
											<td>
												<div class="form-group form-check">
													<input type="checkbox" name="ids" value="${listTemp.id}" class="form-check-input">
												</div>
											</td>
											<td>${stat.count }</td>
											<td title="${listTemp.users.nickName }">
												<a href="${rootPath }/head/users/usersUpdate.htm?id=${listTemp.usersId }&classId=${listTemp.users.classId}" target="_blank">
													${fn:substring(listTemp.users.nickName,0,subStrLen) }
												</a>
											</td>
											<td title="${listTemp.techPro.name }">
												${fn:substring(listTemp.tech.name,0,subStrLen) }
											</td>
											<td title="<c:out value="${listTemp.name }" escapeXml="true"/>">
												<a href="${rootPath}/head/ques/askUpdate.htm?id=${listTemp.id}" target="_blank">
													<c:out value="${fn:substring(listTemp.name,0,subStrLen) }" escapeXml="true"/>
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.score }
											</td>
											<td>
												${listTemp.askTypeStr }
											</td>
											<td>
												${listTemp.souTypeStr }
											</td>
											<td>
												${listTemp.statusStr }
											</td>
											<td>${listTemp.pubTime }</td>
											<td>
												<c:if test="${listTemp.statusEnum == 'STATUS_DRAFT' || listTemp.statusEnum == 'STATUS_AUDITNO'}">
													<a href="${rootPath}/head/ques/askUpdate.htm?operType=update&id=${listTemp.id}" target="_blank">
														修改
													</a>
												</c:if>
											</td>
										</tr>
									</c:forEach>							
								</tbody>
							</table>
						</form>
					</div>
				</div>
				<div class="col-">
					<form id="pageForm" action="${rootPath }/head/ques/askList.htm"
						method="post" class="form-inline">
						<!-- 设置一些参数 -->
						<input type="hidden" name="keyword" value="${requestScope.keyword }">
						<input type="hidden" name="status" value="${requestScope.status }">
						<input type="hidden" name="st" value="${requestScope.st }">
						<input type="hidden" name="ed" value="${requestScope.ed }">
						
						<input type="hidden" name="askType" value="${requestScope.askType }">
						<input type="hidden" name="souType" value="${requestScope.souType }">
						<input type="hidden" name="techId" value="${requestScope.techId }">
						<input type="hidden" name="searchType" value="${requestScope.searchType }">
						<input type="hidden" name="interviewId" value="${param.interviewId }">

						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-end">
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '1', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>首页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.prePage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>上一页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.nextPage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>下一页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.totalPage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>尾页</span>
									</a>
								</li>
								<li class="page-item">共${pageInfoUtil.totalRecord }条,共${pageInfoUtil.totalPage}页</li>
								<li class="page-item">
									第<input type="text" id="currentPage" name="currentPage" class="form-control" value="${pageInfoUtil.currentPage }"  size="5" maxlength="5"/>页
								</li>
								<li class="page-item">
									<input type="text" id="pageSize" name="pageSize" class="form-control" value="${pageInfoUtil.pageSize }" size="5" maxlength="5"/>
								</li>
								<li class="page-item"><input type="submit" value="GO" class="form-control btn btn-primary"/></li>
							</ul>
						</nav>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
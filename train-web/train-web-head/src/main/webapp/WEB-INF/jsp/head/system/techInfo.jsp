<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<jsp:useBean class="com.wangsh.train.system.pojo.ASysTech" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看技术 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看技术</h2>
					<form id="form" class="form-horizontal" action="${rootPath }/head/demo/dynastyUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">分类:</label>
							${one.cateJSON.treeName }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称:</label>
							${one.name }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">官网:</label>
							<a class="href_info" href="${one.website }" target="_blank">
								${one.website }
							</a>
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label" title="总课时(分钟)">总课时:</label>
							${one.clasTime}
							(${one.clasTimeStr })
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">创建时间:</label>
							${one.createTime}
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">更新时间:</label>
							${one.updateTime}
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							${one.pubTime}
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">内容:</label>
							<div class="col-sm-8">
								${one.content}
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.pageInfoUtil }" var="pageInfoUtil"/>
<c:set value="${requestScope.response.data.list }" var="list"/>
<jsp:useBean class="com.wangsh.train.cla.pojo.AClass" id="pojo"/>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>班级列表 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页的主体部分 -->
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-sm-offset-1">
					<h2 class="sub-header">班级列表</h2>
	
					<form action="${rootPath }/head/class/classList.htm" method="post" class="form-inline">
						<label for="keyword">关键字:</label>
						<input type="text" name="keyword" value="${requestScope.keyword }" class="form-control mr-sm-2">
						<label for="status">班级状态:</label>
						<select name="claStatus" class="form-control mr-sm-2">
							<option value="">请选择</option>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'ClASTATUS')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.claStatus == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
						<label for="pubTime">发布时间:</label>
						<input type="text" name="st" value="${requestScope.st }" readonly="true" class="form-control mr-sm-2" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate"/>
						-->
						<input type="text" name="ed" value="${requestScope.ed }" readonly="true" class="form-control mr-sm-2" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate"/>
						<input type="submit" class="btn btn-primary" value="搜索">
					</form>
				</div>
				<div class="col-md-11 col-sm-offset-1">
					<div class="table-responsive mt-2">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>序号</th>
									<th>分类</th>
									<th>地区</th>
									<th>名称</th>
									<th>开班时间</th>
									<th>班级状态</th>
									<th>更新时间</th>
									<th>发布时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list }" var="cla" varStatus="stat">
									<tr>
										<td>${stat.count}</td>
										<td>${cla.cateId }</td>
										<td title="${cla.regionJSON.treeName }">${fn:substring(cla.regionJSON.name,0,subStrLen) }</td>
										<td title="${cla.name}">
											<a href="${rootPath}/head/class/classUpdate.htm?id=${cla.id}" target="_blank">
												${fn:substring(cla.name,0,subStrLen) }
											</a>
										</td>
										<td>${cla.stTime }</td>
										<td>
											${cla.claStatusStr }
										</td>
										<td>${cla.updateTime }</td>
										<td>${cla.pubTime }</td>
										<td>
											<a href="${rootPath}/head/users/usersList.htm?classId=${cla.id}" target="_blank">
												查看学生
											</a>
											<br/>
											<a href="${rootPath}/head/ques/examDescList.htm?classId=${cla.id}" target="_blank">
												试卷列表
											</a>
											<%-- <br/>
											<a href="${rootPath}/head/ques/examScoreDescList.htm?classId=${cla.id}" target="_blank">
												成绩列表
											</a> --%>
										</td>
									</tr>
								</c:forEach>							
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-11 col-sm-offset-1">
					<form id="pageForm" action="${rootPath }/head/class/classList.htm"
						method="post" class="form-inline col-md-11 col-sm-offset-1">
						<!-- 设置一些参数 -->
						<input type="hidden" name="keyword" value="${requestScope.keyword }">
						<input type="hidden" name="status" value="${requestScope.status }">
						<input type="hidden" name="claStatus" value="${requestScope.claStatus }">
						<input type="hidden" name="st" value="${requestScope.st }">
						<input type="hidden" name="ed" value="${requestScope.ed }">
	
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-end">
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '1', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>首页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.prePage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>上一页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.nextPage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>下一页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.totalPage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>尾页</span>
									</a>
								</li>
								<li class="page-item">共${pageInfoUtil.totalRecord }条,共${pageInfoUtil.totalPage}页</li>
								<li class="page-item">
									第<input type="text" id="currentPage" name="currentPage" class="form-control" value="${pageInfoUtil.currentPage }"  size="5" maxlength="5"/>页
								</li>
								<li class="page-item">
									<input type="text" id="pageSize" name="pageSize" class="form-control" value="${pageInfoUtil.pageSize }" size="5" maxlength="5"/>
								</li>
								<li class="page-item"><input type="submit" value="GO" class="form-control btn btn-primary"/></li>
							</ul>
						</nav>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
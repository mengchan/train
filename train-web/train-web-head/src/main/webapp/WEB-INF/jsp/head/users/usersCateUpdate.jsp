<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.users.pojo.AUsersCate" id="pojo"/>
<c:set value="${requestScope.response.data.one}" var="pojoTemp"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新公司 - 培训机构</title>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		
		<link rel="stylesheet" href="${rootPath }/common/resource/bootstrapvalidator/css/form-validation.css" rel="stylesheet">
	  	<script type="text/javascript" src="${rootPath }/common/resource/bootstrapvalidator/js/form-validation.js"></script>
	  	
	  	<script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.config.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.all.min.js"> </script>
	    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
	    <!--这里加载的语言文件会覆盖你在配置项目里更新的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/lang/zh-cn/zh-cn.js"></script>
	  	
	  	<script type="text/javascript">
	  		$(function()
			{
			  	$("#form").FormValidation({
		  			fields:
	  				{
		  				/* 压缩包名字 */
	  					name:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '名字不能为空'
		  	                    },
		  	                    stringLength: {
		  	                        min: 2,
		  	                        max:20,
		  	                        message: '名字的长度为在2--20位'
		  	                    }
		  	                }
	  					}
	  				}
				});
			  	
			  	var ue = UE.getEditor('editor');
			  	
				selectBatchCate('cateTempId','header_title','');
			});
	  	</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${pojoTemp.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">更新</h2>
					<form id="form" class="form-horizontal" action="${rootPath }/head/users/usersCateUpdateSubmit.htm" method="post">
						<input type="hidden" name="operType" value="update">
						<input type="hidden" name="id" value="${pojoTemp.id }">
						<div class="form-group">
							<label for="currDateStr" class="col-sm-2 control-label">日期:</label>
							
							<div class="col-sm-8">
								<c:choose>
									<c:when test="${pojoTemp.cateTempId == requestScope.sys_map.usersCate_template_id_stuDay.vals}">
										<c:set value="${fn:indexOf(pojoTemp.currDate,' 00:00:00')}" var="dateIndex"/>
										<input type="text" readonly="readonly" value="${fn:substring(pojoTemp.currDate,0,dateIndex) }"
											onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
											id="currDateStr" name="currDateStr" class="form-control">
									</c:when>
									<c:when test="${pojoTemp.cateTempId == requestScope.sys_map.usersCate_template_id_week.vals}">
										<c:set value="${fn:indexOf(pojoTemp.currDate,' 00:00:00')}" var="dateIndex"/>
										<input type="text" readonly="readonly" value="${fn:substring(pojoTemp.currDate,0,dateIndex) }"
											onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
											id="currDateStr" name="currDateStr" class="form-control">
									</c:when>
									<c:when test="${pojoTemp.cateTempId == requestScope.sys_map.usersCate_template_id_month.vals}">
										<c:set value="${fn:indexOf(pojoTemp.currDate,'-01 00:00:00')}" var="dateIndex"/>
										<input type="text" readonly="readonly" value="${fn:substring(pojoTemp.currDate,0,dateIndex) }"
											onfocus="WdatePicker({dateFmt:'yyyy-MM'})"
											id="currDateStr" name="currDateStr" class="form-control">
									</c:when>
									<c:otherwise>
										<c:set value="${fn:indexOf(pojoTemp.currDate,' 00:00:00')}" var="dateIndex"/>
										<input type="text" readonly="readonly" value="${fn:substring(pojoTemp.currDate,0,dateIndex) }"
											onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
											id="currDateStr" name="currDateStr" class="form-control">
									</c:otherwise>
								</c:choose>
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							<div class="col-sm-8">
								<input type="text" readonly="readonly" value="${pojoTemp.pubTime }"
									onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
									id="pubTimeStr" name="pubTimeStr" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">状态:</label>
						</div>
						<div class="form-group">
							<div class="col-sm-8 form-check form-check-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<c:set value="1" var="currCount"/>
								<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'STATUS')}">
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<input type="radio" id="${me.key }" name="status" class="form-check-input" value="${keys[1] }" ${keys[1] == pojoTemp.status ? 'checked' : '' }>
										<label for="${me.key }" class="form-check-label">${me.value }</label>
										<c:set value="${currCount + 1 }" var="currCount"/>
										&nbsp;&nbsp;
									</c:if>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">内容:</label>
							<div class="col-sm-8">
								<script id="editor" name="content" type="text/plain" style="width:800px;height:300px;">${pojoTemp.content}</script>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
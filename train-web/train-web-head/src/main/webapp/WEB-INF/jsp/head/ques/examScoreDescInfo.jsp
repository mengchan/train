<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AExamDesc" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>成绩详情 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<form id="form" class="form-horizontal" action="${rootPath }/head/ques/examScoreDescInsertSubmit.htm" method="post">
				<input type="hidden" name="examId" value="${one.id }">
				<div class="row">
					<div class="col-sm-11 col-sm-offset-1">
						<h2 class="form-signin-heading text-center" id="header_title">${one.name }</h2>
						<div class="table-responsive mt-2">
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td>
											姓名:${one.users.trueName }
										</td>
										<td>
											班级:${one.usersClass.cla.name }
										</td>
										<td>
											试卷:${one.examDesc.name}
										</td>
										<td>
											分数:<span id="spanNow" class="error_info">${one.score}</span>
										</td>
										<td>
											状态:${one.statusStr}
										</td>
									</tr>
									<tr>
										<td>
											违规次数:${one.violCount }
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<c:if test="${fn:length(one.selDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">选择题(单选)(每题${sys_map.ques_sel.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td width="50"></td>
										<td>名称</td>
										<td>分数</td>
										<td>结果</td>
										<td>操作</td>
									</tr>
									<c:forEach items="${one.selDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>${listTemp.name }</td>
											<td>${listTemp.score }</td>
											<td>${listTemp.corrTypeStr }</td>
											<td><a href="${rootPath }/head/ques/askUpdate.htm?id=${listTemp.askId}&answer=${listTemp.answer}" target="_blank">查看答案</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.multiSelDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">选择题(多选)(每题${sys_map.ques_multiSel_score.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td width="50"></td>
										<td>名称</td>
										<td>分数</td>
										<td>结果</td>
										<td>操作</td>
									</tr>
									<c:forEach items="${one.multiSelDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>${listTemp.name }</td>
											<td>${listTemp.score }</td>
											<td>${listTemp.corrTypeStr }</td>
											<td><a href="${rootPath }/head/ques/askUpdate.htm?id=${listTemp.askId}&answer=${listTemp.answer}" target="_blank">查看答案</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.judeDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">判断题(每题${sys_map.ques_judge_score.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td width="50"></td>
										<td>名称</td>
										<td width="80">分数</td>
										<td width="100" title="评分结果">结果</td>
										<td width="100" title="查看答案">
											查看答案
										</td>
									</tr>
									<c:forEach items="${one.judeDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>${listTemp.name }</td>
											<td>${listTemp.score }</td>
											<td>${listTemp.corrTypeStr }</td>
											<td title="查看答案">
												<a href="${rootPath }/head/ques/askUpdate.htm?id=${listTemp.askId}&answer=${listTemp.answer}" target="_blank">查看答案</a>
											</td>
										</tr>
										<tr>
											<td colspan="5">
												您的答案
												${fn:indexOf(listTemp.answer,'0') != '-1' ? '正确':'错误'}
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.askDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">问答题(每题${sys_map.ques_ask.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td width="50"></td>
										<td>名称</td>
										<td>分数</td>
										<td>操作</td>
									</tr>
									<c:forEach items="${one.askDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>${listTemp.name }</td>
											<td>${listTemp.score }</td>
											<td><a href="${rootPath }/head/ques/askUpdate.htm?id=${listTemp.askId}" target="_blank">查看答案</a></td>
										</tr>
										<tr>
											<td colspan="4">
												<c:set value="${fn:length(listTemp.answer) }" var="strLen"/>
												${fn:substring(listTemp.answer,0,strLen - 1) }
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.proDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">编程题(每题${sys_map.ques_pro.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td width="50"></td>
										<td>名称</td>
										<td>分数</td>
										<td>操作</td>
									</tr>
									<c:forEach items="${one.proDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>${listTemp.name }</td>
											<td>${listTemp.score }</td>
											<td><a href="${rootPath }/head/ques/askUpdate.htm?id=${listTemp.askId}" target="_blank">查看答案</a></td>
										</tr>
										<tr>
											<td colspan="4">
												<c:set value="${fn:length(listTemp.answer) }" var="strLen"/>
												${fn:substring(listTemp.answer,0,strLen - 1) }
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.interDetailList) > 0 && one.examDesc.examType == pojo.enums['EXAMTYPE_INTERVIEW']}">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">面试题(每题${sys_map.ques_ask_inter_score.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td width="50"></td>
										<td>名称</td>
										<td>分数</td>
										<td>操作</td>
									</tr>
									<tr>
											<td width="50">${stat.count }</td>
											<td>${listTemp.name }</td>
											<td>${listTemp.score }</td>
											<td><a href="${rootPath }/head/ques/askUpdate.htm?id=${listTemp.askId}" target="_blank">查看答案</a></td>
										</tr>
										<tr>
											<td colspan="4">
												<c:set value="${fn:length(listTemp.answer) }" var="strLen"/>
												${fn:substring(listTemp.answer,0,strLen - 1) }
											</td>
										</tr>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
			</form>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
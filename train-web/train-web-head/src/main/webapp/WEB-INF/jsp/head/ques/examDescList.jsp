<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.pageInfoUtil }" var="pageInfoUtil"/>
<c:set value="${requestScope.response.data.list }" var="list"/>
<c:set value="${requestScope.sysTechResponse.data.list}" var="sysProList"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AExamDesc" id="pojo"/>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>试卷列表 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			/*
				批量操作切换
				selId:要选择的select
				tarVal:要具体判断的值
				tarDiv:最终要显示的dom
			*/
			function batchSwitch(selId)
			{
				var selVal = $("#" + selId).val();
				/* 所有的选择信息都隐藏掉 */
				$(".selectInfo").hide();
				
				if('status' == selVal)
				{
					$("#update" + selVal).show();
				}else if('teaId' == selVal)
				{
					$("#update" + selVal).show();
				}else if('examDescType' == selVal)
				{
					$("#update" + selVal).show();
				}
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页的主体部分 -->
		<div class="container">
			<div class="row">
				<div class="col-">
					<h2 class="sub-header">试卷列表</h2>
					<form action="${rootPath }/head/ques/examDescList.htm" method="post" class="form-inline">
						<input type="hidden" name="searchType" value="${requestScope.searchType }">
						<input type="hidden" name="classId" value="${requestScope.classId }">
						<label for="keyword">关键字:</label>
						<input type="text" name="keyword" value="${requestScope.keyword }" class="form-control mr-sm-2">
						<label for="examType">类型:</label>
						<select name="examType" id="examType" class="form-control mr-sm-2">
							<option value="">请选择</option>
							<c:set value="1" var="currCount"/>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'EXAMTYPE_')}">
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.examType == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
						<label for="status">状态:</label>
						<select name="status" id="status" class="form-control mr-sm-2">
							<option value="">请选择</option>
							<c:set value="1" var="currCount"/>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'STATUS_')}">
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
						<label for="pubTime">发布时间:</label>
						<input type="text" name="st" value="${requestScope.st }" readonly="true" class="form-control mr-sm-2" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate"/>
						-->
						<input type="text" name="ed" value="${requestScope.ed }" readonly="true" class="form-control mr-sm-2" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate"/>
						<input type="submit" class="btn btn-primary" value="搜索">
						<%-- <a href="${rootPath }/head/ques/examDescInsert.htm" class="btn btn-primary ml-2" target="_blank">添加</a> --%>
					</form>
				</div>
				<div class="col-">
					<div class="table-responsive mt-2">
						<!-- 要做批量操作 -->
						<form action="${rootPath }/head/ques/examDescBatch.htm" method="post" class="form-inline" id="batchForm">
							<label for="status">操作类型:</label>
							<select name="operType" id="operType" class="form-control mr-sm-2" onchange="batchSwitch('operType')">
								<option value="">请选择</option>
								<option value="status">批量更新状态</option>
								<option value="teaId">批量更新技术</option>
								<option value="examDescType">批量试卷类型</option>
							</select>
							<div class="form-row selectInfo" id="updatestatus" style="display: none;">
								<label for="status">状态:</label>
								<select name="status" class="form-control mr-sm-2" id="statusBatch">
									<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
										<c:if test="${fn:startsWith(me.key,'STATUS')}">
											<!-- 拆分,按照-拆分,取最后一个 -->
											<c:set value="${fn:split(me.key, '-')}" var="keys"/>
											<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
							<span class="form-row selectInfo" id="updateteaId" style="display: none;">
								技术:
								<select name="teaId" id="teaIdBatch" class="form-control mr-sm-2">
									<option value="">请选择</option>
									<c:forEach items="${sysProList }" var="listTemp" varStatus="stat">
										<option value="${listTemp.id}" ${requestScope.techId == listTemp.id ? 'selected' : '' }>${listTemp.name}</option>
									</c:forEach>
								</select>
							</span>
							<span class="form-row selectInfo" id="updateexamDescType" style="display: none;">
								类型:
								<select name="examDescType" id="examDescTypeBatch" class="form-control mr-sm-2">
									<option value="">请选择</option>
									<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
										<c:if test="${fn:startsWith(me.key,'ASKTYPE_')}">
											<!-- 拆分,按照-拆分,取最后一个 -->
											<c:set value="${fn:split(me.key, '-')}" var="keys"/>
											<option value="${keys[1] }" ${requestScope.examDescType == keys[1] ? 'selected' : '' }>${me.value }</option>
										</c:if>
									</c:forEach>
								</select>
							</span>
							<a href="javascript:;" onclick="return batchOper('ids','${rootPath}/head/ques/examDescBatch.htm','status=' + $('#statusBatch').val() + '&teaId=' + $('#teaIdBatch').val() + '&examDescType=' + $('#examDescTypeBatch').val())"
								class="btn btn-primary">
								提交
							</a>
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>
											<div class="form-group form-check">
												<input type="checkbox" name="selectAll" id="selectAll" class="form-check-input" onclick="checkAll('selectAll','ids')">
											</div>
										</th>
										<th>序号</th>
										<th>Id</th>
										<th>创建人</th>
										<th>班级</th>
										<th>名称</th>
										<th>难易</th>
										<th>开始时间</th>
										<th>总分</th>
										<th>类型</th>
										<th>状态</th>
										<th>发布时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${list }" var="listTemp" varStatus="stat">
										<tr>
											<td>
												<div class="form-group form-check">
													<input type="checkbox" name="ids" value="${listTemp.id}" class="form-check-input">
												</div>
											</td>
											<td>${stat.count }</td>
											<td>${listTemp.id }</td>
											<td title="${listTemp.admins.name }">
												${fn:substring(listTemp.admins.name,0,subStrLen) }
											</td>
											<td title="${listTemp.cla.name }">
												<a href="${rootPath }/head/class/classUpdate.htm?id=${listTemp.classId }" target="_blank">
													${fn:substring(listTemp.cla.name,0,subStrLen) }
												</a>
											</td>
											<td title="${listTemp.name }">
												<a href="${rootPath }/head/ques/examDescUpdate.htm?id=${listTemp.id }" target="_blank">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td>
												${listTemp.hardStar }
											</td>
											<td>
												${listTemp.stTime }
											</td>
											<td>${listTemp.score }</td>
											<td>
												${listTemp.examTypeStr }
											</td>
											<td>
												${listTemp.statusStr }
											</td>
											<td>${listTemp.pubTime }</td>
											<td>
												<c:if test="${pojo.enums['STATUS_ING'] == listTemp.status && listTemp.classId == sessionScope.users.cla.id}">
													<a href="${rootPath }/head/ques/examScoreDescInsert.htm?examId=${listTemp.id}" target="_blank">开始答卷</a>
													<br/>
												</c:if>
												<c:if test="${pojo.enums['STATUS_FINISH'] == listTemp.status}">
													<a href="${rootPath}/head/ques/examScoreDescList.htm?examId=${listTemp.id}" target="_blank">
														查看成绩
													</a>
												</c:if>
											</td>
										</tr>
									</c:forEach>						
								</tbody>
							</table>
						</form>
					</div>
				</div>
				<div class="col-">
					<form id="pageForm" action="${rootPath }/head/ques/examDescList.htm"
						method="post" class="form-inline">
						<!-- 设置一些参数 -->
						<input type="hidden" name="keyword" value="${requestScope.keyword }">
						<input type="hidden" name="status" value="${requestScope.status }">
						<input type="hidden" name="st" value="${requestScope.st }">
						<input type="hidden" name="ed" value="${requestScope.ed }">
						
						<input type="hidden" name="examType" value="${requestScope.examType }">
						<input type="hidden" name="searchType" value="${requestScope.searchType }">
						<input type="hidden" name="classId" value="${requestScope.classId }">
						
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-end">
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '1', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>首页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.prePage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>上一页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.nextPage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>下一页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.totalPage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>尾页</span>
									</a>
								</li>
								<li class="page-item">共${pageInfoUtil.totalRecord }条,共${pageInfoUtil.totalPage}页</li>
								<li class="page-item">
									第<input type="text" id="currentPage" name="currentPage" class="form-control" value="${pageInfoUtil.currentPage }"  size="5" maxlength="5"/>页
								</li>
								<li class="page-item">
									<input type="text" id="pageSize" name="pageSize" class="form-control" value="${pageInfoUtil.pageSize }" size="5" maxlength="5"/>
								</li>
								<li class="page-item"><input type="submit" value="GO" class="form-control btn btn-primary"/></li>
							</ul>
						</nav>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
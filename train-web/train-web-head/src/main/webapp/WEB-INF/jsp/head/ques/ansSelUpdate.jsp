<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AAnsSel" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新选项 - 培训机构</title>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		
		<link rel="stylesheet" href="${rootPath }/common/resource/bootstrapvalidator/css/form-validation.css" rel="stylesheet">
	  	<script type="text/javascript" src="${rootPath }/common/resource/bootstrapvalidator/js/form-validation.js"></script>
	  	
	  	<script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.config.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.all.min.js"> </script>
	    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
	    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/lang/zh-cn/zh-cn.js"></script>
	  	
	  	<script type="text/javascript">
	  		$(function()
			{
			  	$("#form").FormValidation({
		  			fields:
	  				{
		  				/* 名称 */
	  					name:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '名称不能为空'
		  	                    },
		  	                    stringLength: {
		  	                        min: 2,
		  	                        max:200,
		  	                        message: '名称的长度为在2--200位'
		  	                    }
		  	                }
	  					},
	  					score:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '分数不能为空'
		  	                    },
		  	                  	regexp: {
		  	                  		regexp: /^[\d]+$/,
		  	                        message: '分数格式不正确,必须为数字'
		  	                    }
		  	                }
	  					}
	  				}
				});
			  	/* 百度编辑器 */
			  	var ue = UE.getEditor('editor');
			});
	  	</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">更新选项</h2>
					<form id="form" class="form-horizontal" action="${rootPath }/head/ques/ansSelUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">答案:</label>
							<div class="col-sm-8">
								<textarea rows="5" cols="80" name="name">${one.name }</textarea>
							</div> 
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							<div class="col-sm-8">
								<c:if test="${one.pubTime != null && one.pubTime != '' }">
									<c:set value="${one.pubTime }" var="sys_now_format"/>
								</c:if>
								<input type="text" readonly="readonly" value="${sys_now_format}"
									onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
									id="pubTimeStr" name="pubTimeStr" class="form-control">
							</div> 
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">是否正确:</label>
						</div>
						<div class="form-group">
							<div class="col-sm-8 form-check form-check-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<c:set value="1" var="currCount"/>
								<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'CORRTYPE_')}">
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<label for="${me.key }" class="form-check-label">
											<input type="radio" id="${me.key }" name="corrType" class="form-check-input" value="${keys[1] }" ${one.corrType == keys[1] ? 'checked' : '' }>
											${me.value }
										</label>
										<c:set value="${currCount + 1 }" var="currCount"/>
										&nbsp;&nbsp;
									</c:if>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
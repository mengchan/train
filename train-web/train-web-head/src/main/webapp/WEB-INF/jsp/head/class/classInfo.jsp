<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one }" var="clas" />
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>查看班级 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center">查看班级</h2>
					
						<input type="hidden" name="operType" value="update">
						<div class="form-group row">
							<label for="email" class="col-sm-2 col-form-label">名称:</label>
							<div class="col-sm-8">
								${clas.name }
							</div>
						</div>
						<div class="form-group row">
							<label for="email" class="col-sm-2 col-form-label">分类:</label>
							<div class="col-sm-8">
								${clas.cateJSON.treeName }
							</div> 
						</div>
						<div class="form-group row">
							<label for="email" class="col-sm-2 col-form-label">地区:</label>
							<div class="col-sm-8">
								<c:choose>
									<c:when test="${fn:contains(clas.regionJSON.treeName,'|-->')}">
										${fn:replace(clas.regionJSON.treeName,'|-->','  ')}
									</c:when>
									<c:otherwise>
										${clas.regionJSON.treeName} 
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="form-group row">
							<label for="multiSel" class="col-sm-2 col-form-label">地址:</label>
							<div class="col-sm-8">
								${clas.address }
							</div>
						</div>
						<div class="form-group row">
							<label for="regionId" class="col-sm-2 col-form-label">开班时间:</label>
							<div class="col-sm-8">
								${clas.stTime }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">结束时间:</label>
							<div class="col-sm-8">
								${clas.edTime }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">发布时间:</label>
							<div class="col-sm-8">
								${clas.pubTime }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">班级状态:</label>
							<div class="col-sm-8">
								${clas.claStatusStr }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">状态:</label>
							<div class="col-sm-8">
								${clas.statusStr }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">创建时间:</label>
							<div class="col-sm-8">
								${clas.createTime }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">更新时间:</label>
							<div class="col-sm-8">
								${clas.updateTime }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">内容:</label>
							<div class="col-sm-8">
								${clas.content }
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.job.pojo.AJobCompany" id="pojo"/>
<c:set value="${requestScope.response.data.one}" var="pojoTemp"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看系统文件 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看</h2>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">关联id:</label>
						<div class="col-sm-8">
							${pojoTemp.name}
						</div> 
					</div>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">大小:</label>
						<div class="col-sm-8">
							<fmt:formatNumber value="${dateOne.fileSize }" pattern="#,###,###"/>Byte
						</div> 
					</div>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">关联id:</label>
						<div class="col-sm-8">
							${pojoTemp.relaId}
						</div> 
					</div>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">发布时间:</label>
						<div class="col-sm-8">
							${pojoTemp.pubTime}
						</div> 
					</div>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">文件关联类型:</label>
						<div class="col-sm-8">
							${pojoTemp.fileRelaTypeStr }
						</div> 
					</div>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">文件类型:</label>
						<div class="col-sm-8">
							${pojoTemp.fileTypeStr }
						</div> 
					</div>
					<div class="form-group row">
						<label for="status" class="col-sm-2 control-label">状态:</label>
						<div class="col-sm-8">
							${pojoTemp.statusStr }
						</div>
					</div>
					<div class="form-group row">
						<label for="pubTimeStr" class="col-sm-2 control-label">创建时间:</label>
						<div class="col-sm-8">
							${pojoTemp.createTime }
						</div> 
					</div>
					<div class="form-group row">
						<label for="pubTimeStr" class="col-sm-2 control-label">更新时间:</label>
						<div class="col-sm-8">
							${pojoTemp.updateTime }
						</div> 
					</div>
					<div class="form-group row">
						<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
						<div class="col-sm-8">
							${pojoTemp.pubTime }
						</div> 
					</div>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">下载文件:</label>
						<div class="col-sm-8">
							<a class="href_info" href="${websiteFileUrl}${pojoTemp.filePath }" target="_blank">下载源文件</a>
						</div> 
					</div>
					<div class="form-group row">
						<label for="pubTimeStr" class="col-sm-2 control-label">文件:</label>
						<div class="col-sm-8">
							<c:choose>
								<c:when test="${pojoTemp.filePath != null && pojoTemp.filePath != '' }">
									<img alt="" src="${websiteFileUrl}${pojoTemp.filePath }" width="100" height="80">
								</c:when>
								<c:otherwise>
									<img alt="" src="${rootPath }/img/no_pic.png" width="100" height="80">
								</c:otherwise>
							</c:choose>
						</div> 
					</div>
					<div class="form-group row">
						<label for="content" class="col-sm-2 control-label">内容:</label>
						<div class="col-sm-8">
							${pojoTemp.content}
						</div>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
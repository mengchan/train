<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.users.pojo.AUsersCate" id="pojo"/>
<c:set value="${requestScope.response.data.one}" var="one"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>更新公司 - 培训机构</title>
		<script type="text/javascript">
			$(function()
			{
				selectBatchCate('cateTempId','header_title','');
			});
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${one.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看</h2>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">用户:</label>
						${one.usersClass.users.nickName}
					</div>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">班级:</label>
						${one.usersClass.cla.name}
					</div>
					<div class="form-group row">
						<label for="currDateStr" class="col-sm-2 control-label">日期:</label>
						<c:set value="${fn:indexOf(one.currDate,' 00:00:00')}" var="dateIndex"/>
						${fn:substring(one.currDate,0,dateIndex) }
					</div>
					<div class="form-group row">
						<label for="pubTimeStr" class="col-sm-2 control-label">附件:</label>
						<a href="${rootPath}/head/system/sysFileList.htm?relaId=${one.id }&fileRelaType=0" target="_blank">
							查看
						</a>
					</div>
					<div class="form-group row">
						<label for="status" class="col-sm-2 control-label">状态:</label>
						${one.statusStr }
					</div>
					<div class="form-group row">
						<label for="pubTimeStr" class="col-sm-2 control-label">创建时间:</label>
						${one.createTime }
					</div>
					<div class="form-group row">
						<label for="pubTimeStr" class="col-sm-2 control-label">更新时间:</label>
						${one.updateTime }
					</div>
					<div class="form-group row">
						<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
						${one.pubTime }
					</div>
					<div class="form-group row">
						<label for="content" class="col-sm-2 control-label">内容:</label>
						${one.content}
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
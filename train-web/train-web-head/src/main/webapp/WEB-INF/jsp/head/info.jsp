<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>信息页面 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页的主体 -->
		<div class="container">
			<div class="col-md-8 col-sm-10">
				<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center">信息提示</h2>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean class="com.wangsh.train.cla.pojo.ACodeStat" id="pojo"/>
<c:set value="${requestScope.response.data.one }" var="one"/>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>查看公司 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center">查看公司</h2>
					<%-- 自己不能顶和踩自己 --%>
					<c:if test="${sessionScope.users.id != one.createId }">
						<a href="${rootPath }/head/job/jobCompanyUpdateSubmit.htm?id=${one.id}&operType=upView" class="href_info" onclick="return confirm('确认要操作吗?')">
							顶
						</a>
						<a href="${rootPath }/head/job/jobCompanyUpdateSubmit.htm?id=${one.id}&operType=downView" class="href_info" onclick="return confirm('确认要操作吗?')">
							踩
						</a>
					</c:if>
					<form id="form" class="form-horizontal" action="${rootPath }/head/job/jobCompanyUpdateSubmit.htm" method="post">
						<div class="form-group row">
							<label for="email" class="col-sm-2 col-form-label">创建用户:</label>
							<div class="col-sm-8">
								${one.create.nickName }
							</div>
						</div>
						<div class="form-group row">
							<label for="email" class="col-sm-2 col-form-label">名字:</label>
							<div class="col-sm-8">
								${one.name }
							</div>
						</div>
						<div class="form-group row">
							<label for="email" class="col-sm-2 col-form-label">网站:</label>
							<div class="col-sm-8">
								<a href="${one.website }" target="_blank">${one.website }</a>
							</div> 
						</div>
						<div class="form-group row">
							<label for="email" class="col-sm-2 col-form-label">地区:</label>
							<div class="col-sm-8">
								<c:choose>
									<c:when test="${fn:contains(one.regionJSON.treeName,'|-->')}">
										${fn:replace(one.regionJSON.treeName,'|-->','  ')}
									</c:when>
									<c:otherwise>
										${one.regionJSON.treeName} 
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="form-group row">
							<label for="multiSel" class="col-sm-2 col-form-label">地址:</label>
							<div class="col-sm-8">
								${one.address }
							</div>
						</div>
						<div class="form-group row">
							<label for="regionId" class="col-sm-2 col-form-label">联系人:</label>
							<div class="col-sm-8">
								${one.contact }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">手机:</label>
							<div class="col-sm-8">
								${one.phone }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">顶:</label>
							<div class="col-sm-8">
								${one.upNum }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">踩:</label>
							<div class="col-sm-8">
								${one.downNum }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">Logo:</label>
							<div class="col-sm-8">
								<c:choose>
									<c:when test="${one.logoPath != null && one.logoPath != '' }">
										<img alt="" src="${websiteFileUrl}${one.logoPath }" width="100" height="80">
									</c:when>
									<c:otherwise>
										<img alt="" src="${rootPath }/img/no_pic.png" width="100" height="80">
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">发布时间:</label>
							<div class="col-sm-8">
								${one.pubTime }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">状态:</label>
							<div class="col-sm-8">
								${one.statusStr}
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">创建时间:</label>
							<div class="col-sm-8">
								${one.createTime }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">更新时间:</label>
							<div class="col-sm-8">
								${one.updateTime }
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">内容:</label>
							<div class="col-sm-8">
								${one.content }
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp" %>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AAsk" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看试卷 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看试卷</h2>
					<form id="form" class="form-horizontal" action="${rootPath }/head/ques/askUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称：</label>
							${one.name }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">创建人：</label>
							${one.admins.name }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">班级：</label>
							<a href="${rootPath }/head/class/classUpdate.htm?id=${one.classId }" target="_blank">
								${one.cla.name }
							</a>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">总分：</label>
							${one.score}
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">难度：</label>
							${one.hardStar }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">开始时间：</label>
							${one.stTime }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">结束时间：</label>
							${one.edTime }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间：</label>
							<c:if test="${one.pubTime != null && one.pubTime != '' }">
								<c:set value="${one.pubTime }" var="sys_now_format"/>
							</c:if>
							${sys_now_format}
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">题型：</label>
							${one.examTech }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">类型：</label>
							${one.examTypeStr }
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">状态：</label>
							${one.statusStr }
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">创建时间：</label>
							${one.createTime }
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">更新时间：</label>
							${one.updateTime }
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">内容：</label>
							<div class="col-sm-8">
								${one.content}
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
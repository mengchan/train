<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<c:set value="${requestScope.sysTechResponse.data.list}" var="sysProList"/>
<jsp:useBean class="com.wangsh.train.ques.pojo.AExamDesc" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>${one.name }开始答卷 - 培训机构</title>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		
		<link rel="stylesheet" href="${rootPath }/common/resource/bootstrapvalidator/css/form-validation.css" rel="stylesheet">
	  	<script type="text/javascript" src="${rootPath }/common/resource/bootstrapvalidator/js/form-validation.js"></script>
	  	
	  	<script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.config.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/ueditor.all.min.js"> </script>
	    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
	    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
	    <script type="text/javascript" charset="utf-8" src="${rootPath }/common/resource/ueditor/lang/zh-cn/zh-cn.js"></script>
	  	
	  	<script type="text/javascript">
	  		/* 离开桌面3次,自动提交 */
  			var totalCount = ${requestScope.sys_map['exam_viol_totalCount'].vals} ; 
	  		$(function()
			{
			  	$("#form").FormValidation({
		  			fields:
	  				{
		  				/* 名称 */
	  					name:
	  					{
		  					validators: {
		  	                    notEmpty: {
		  	                        message: '名称不能为空'
		  	                    },
		  	                    stringLength: {
		  	                        min: 2,
		  	                        max:200,
		  	                        message: '名称的长度为在2--200位'
		  	                    }
		  	                }
	  					}
	  				}
				});
			  	
			  	/* 为了确保session不失效;每隔10秒钟请求一次服务器 */
			  	window.setInterval(function(){
			  		/* 请求服务器;获取时间 */
			  		$.post(
		  				"${rootPath}/outer/requestOuter.htm",
		  				"method=currentDate&json={}",
		  				function(data)
		  				{
		  					$("#spanNow").html(data.data.time);
		  				},
		  				"json"
		  			);
			  	},20000);
			  	
			  	$("#totalCount").html(totalCount);
			});
	  		
	  		
	  		/**
	  			显示问题选项
	  		*/
	  		function showSelect(tarShowDom,askId,askType)
	  		{
	  			var tarShowDom = $("#" + tarShowDom) ; 
	  			if(tarShowDom.children("li").length > 0)
  				{
	  				return false ; 
  				}
	  			
	  			var askTypeTemp = 'radio';
				if(askType == 'multi')
				{
					askTypeTemp = 'checkbox';
				}
	  			
	  			$.post(
	  				"${rootPath}/outer/requestOuter.htm",
	  				"method=findOneAsk&json={'id':'"+ askId +"'}",
	  				function(data)
	  				{
	  					/* <li><label><input type="radio" name="aa" value="aa"/>aaa</label></li> */
	  					var resSb = "" ; 
	  					var ansSelList = data.data.one.ansSelList ; 
	  					/* 字符串 */
	  					var ch = 'A' ; 
	  					for(var i = 0 ; i < ansSelList.length ; i ++)
  						{
  							var tempObj = ansSelList[i]; 
  							resSb += "<li><label><input type='"+ askTypeTemp +"' name='askId_"+ askId +"' value='"+ tempObj.id +"'/>&nbsp;&nbsp;"+ ch + ".&nbsp;&nbsp;" + tempObj.name +"</label></li>" ; 
  							/* 把字符变成ascii */
  							var chInt = ch.charCodeAt() ; 
  							chInt ++ ; 
  							/* 把ascii变成字符 */
  							ch = String.fromCharCode(chInt);
  						}
	  					tarShowDom.html(resSb);
	  					tarShowDom.show();
	  					//console.info(tarShowDom + "==showSelect==>" + askId + "===>" + data);
	  				},
	  				"json"
	  			);
	  			return false ; 
	  		}
	  		
	  		var violCount = 0 ; 
	  		/* 违规次数相加 */
	  		function autoCommitAdd()
	  		{
	  			violCount ++ ; 
	  			//alert("==您又违规了;当前次数:"+ violCount +"次,总次数:"+ totalCount +"==");
	  			console.info(totalCount + "===自动提交检测===" + violCount);
	  			$("#violCount").html(violCount);
	  			$("#violCountHid").val(violCount);
	  			if(violCount >= totalCount)
  				{
	  				alert("==您已经违规"+ totalCount +"次,系统自动提交==");
  					$("#form").submit();
  					violCount = 0 ; 
  				}
	  			return false ; 
	  		}
	  		
	  		/* 违规次数相减 */
	  		function autoCommitSub()
	  		{
	  			console.info("==移除焦点,违规次数减少1次==")
	  			if(violCount > 0 )
				{
					violCount --  ;
				}
				$("#violCount").html(violCount);
				$("#violCountHid").val(violCount);
	  		}
	  		
	  		window.onblur = function()
	  		{
	  			autoCommitAdd();
	  		}
	  	</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<form id="form" class="form-horizontal" action="${rootPath }/head/ques/examScoreDescInsertSubmit.htm" method="post">
				<input type="hidden" name="examId" value="${one.id }">
				<input type="hidden" id="violCountHid" name="violCount" value="0">
				<div class="row">
					<div class="col-sm-11 col-sm-offset-1">
						<h2 class="form-signin-heading text-center" id="header_title">${one.name }</h2>
						<div class="table-responsive mt-2">
							<table class="table table-striped table-hover">
								<tbody>
									<tr>
										<td>开始时间:${one.stTime}</td>
										<td>结束时间:${one.edTime}</td>
										<td>难度:${one.hardStar}</td>
										<td>类型:${one.examTypeStr}</td>
										<td>班级:${one.cla.name}</td>
										<td>姓名:${sessionScope.users.trueName}</td>
									</tr>
									<tr>
										<td>当前:<span id="spanNow" class="error_info"></span></td>
										<td>违规总次数:<span id="totalCount" class="error_info"></span></td>
										<td>违规次数:<span id="violCount" class="error_info"></span></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<c:if test="${fn:length(one.selDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">选择题(单选)(每题${sys_map.ques_sel.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<c:forEach items="${one.selDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>
												<c:out value="${listTemp.name }" escapeXml="true"/>
											</td>
											<td><a href="" onclick="return showSelect('sel${listTemp.askId }','${listTemp.askId }','single')">显示选项</a></td>
										</tr>
										<tr>
											<td colspan="3">
												<ul class="list-unstyled" id="sel${listTemp.askId }" style="display: none">
													<!-- <li><label><input type="radio" name="aa" value="aa"/>aaa</label></li> -->
												</ul>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.multiSelDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">选择题(多选)(每题${sys_map.ques_multiSel_score.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<c:forEach items="${one.multiSelDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>
												<c:out value="${listTemp.name }" escapeXml="true"/>
											</td>
											<td><a href="" onclick="return showSelect('sel${listTemp.askId }','${listTemp.askId }','multi')">显示选项</a></td>
										</tr>
										<tr>
											<td colspan="3">
												<ul class="list-unstyled" id="sel${listTemp.askId }" style="display: none">
												</ul>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.judeDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">判断题(每题${sys_map.ques_judge_score.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<c:forEach items="${one.judeDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>
												<c:out value="${listTemp.name }" escapeXml="true"/>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<ul class="list-unstyled" id="jude${listTemp.id }">
													<li><label><input type='radio' name='askId_${listTemp.askId }' value='0'/>&nbsp;&nbsp;正确</label></li>
													<li><label><input type='radio' name='askId_${listTemp.askId }' value='1'/>&nbsp;&nbsp;错误</label></li>
												</ul>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.askDetailList) > 0 && one.examType != one.enums['EXAMTYPE_INTERVIEW']}">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">问答题(每题${sys_map.ques_ask.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<c:forEach items="${one.askDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>${listTemp.name }</td>
										</tr>
										<c:if test="${listTemp.content != null && listTemp.content != ''}">
											<tr>
												<td colspan="2">
													${listTemp.content }
												</td>
											</tr>
										</c:if>
										<tr>
											<td colspan="2">
												<script id="askId_${listTemp.askId }" name="askId_${listTemp.askId }" type="text/plain" style="width:100%;height:200px;"></script>
												<script type="text/javascript">
													var ue_${listTemp.askId } = UE.getEditor('askId_${listTemp.askId }');
													ue_${listTemp.askId }.addListener("focus",function(editor)
													{
														autoCommitSub();
													});
												</script>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.proDetailList) > 0 }">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">编程题(每题${sys_map.ques_pro.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<c:forEach items="${one.proDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>
												<c:out value="${listTemp.name }" escapeXml="true"/>
											</td>
										</tr>
										<c:if test="${listTemp.content != null && listTemp.content != ''}">
											<tr>
												<td colspan="2">
													${listTemp.content }
												</td>
											</tr>
										</c:if>
										<tr>
											<td colspan="2">
												<script id="askId_${listTemp.askId }" name="askId_${listTemp.askId }" type="text/plain" style="width:100%;height:200px;"></script>
												<script type="text/javascript">
													var ue_${listTemp.askId } = UE.getEditor('askId_${listTemp.askId }');
													ue_${listTemp.askId }.addListener("focus",function(editor)
													{
														autoCommitSub();
													});
												</script>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${fn:length(one.interDetailList) > 0 && one.examType == pojo.enums['EXAMTYPE_INTERVIEW']}">
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1 main">
							<h5 class="col-sm-11 col-sm-offset-1 form-signin-heading" id="header_title">面试题(每题${sys_map.ques_ask_inter_score.vals }分)</h5>
							<table class="table table-striped table-hover">
								<tbody>
									<c:forEach items="${one.interDetailList }" var="listTemp" varStatus="stat">
										<tr>
											<td width="50">${stat.count }</td>
											<td>
												<c:out value="${listTemp.name }" escapeXml="true"/>
											</td>
										</tr>
										<c:if test="${listTemp.content != null && listTemp.content != ''}">
											<tr>
												<td colspan="2">
													${listTemp.content }
												</td>
											</tr>
										</c:if>
										<tr>
											<td colspan="2">
												<script id="askId_${listTemp.askId }" name="askId_${listTemp.askId }" type="text/plain" style="width:100%;height:200px;"></script>
												<script type="text/javascript">
													var ue_${listTemp.askId } = UE.getEditor('askId_${listTemp.askId }');
													ue_${listTemp.askId }.addListener("focus",function(editor)
													{
														autoCommitSub();
													});
												</script>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<div class="row">
					<div class="col-sm-11 col-sm-offset-1 main">
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">交卷</button>
							</div>
						</div>
						<div class="form-group">
							<label for="currDateStr" class="col-sm-2 control-label">注意事项:</label>
							<ol class="text-info">
								<li>进出编辑器计算违规;编辑器之间相互切换不违规</li>
								<li>编程题需要提供截图,代码压缩上传</li>
								<li>上传附件,大于20m无法上传</li>
							</ol>
						</div>
					</div>
				</div>
			</form>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
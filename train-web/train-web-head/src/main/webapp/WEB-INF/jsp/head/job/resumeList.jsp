<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.pageInfoUtil }" var="pageInfoUtil"/>
<c:set value="${requestScope.response.data.list }" var="list"/>
<jsp:useBean class="com.wangsh.train.job.pojo.AJobResume" id="pojo"/>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>简历列表 - 培训机构</title>
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript" src="${rootPath }/common/resource/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			/*
				批量操作切换
				selId:要选择的select
				tarVal:要具体判断的值
				tarDiv:最终要显示的dom
			*/
			function batchSwitch(selId)
			{
				var selVal = $("#" + selId).val();
				/* 所有的选择信息都隐藏掉 */
				$(".selectInfo").hide();
				/* 相关的显示 */
				if('status' == selVal)
				{
					$("#update" + selVal).show();
				}
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页的主体部分 -->
		<div class="container">
			<div class="row">
				<div class="col-">
					<h2 class="sub-header">简历列表</h2>
					<form action="${rootPath }/head/job/resumeList.htm" method="post" class="form-inline">
						<input type="hidden" name="searchType" value="${requestScope.searchType }">
						
						<label for="keyword">关键字:</label>
						<input type="text" name="keyword" value="${requestScope.keyword }" class="form-control mr-sm-2">
						<c:if test="${param.searchType == 'self' }">
							<label for="techId">状态:</label>
							<select name="status" class="form-control mr-sm-2">
								<option value="">请选择</option>
								<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'STATUS')}">
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
									</c:if>
								</c:forEach>
							</select>
						</c:if>
						<label for="finish">最终:</label>
						<select name="finish" class="form-control mr-sm-2">
							<option value="">请选择</option>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'FINISH')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.finish == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
						<label for="onTime">按时:</label>
						<select name="onTime" class="form-control mr-sm-2">
							<option value="">请选择</option>
							<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'ONTIME')}">
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.onTime == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
						<label for="pubTime">发布时间:</label>
						<input type="text" name="st" value="${requestScope.st }" readonly="true" class="form-control mr-sm-2" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate"/>
						-->
						<input type="text" name="ed" value="${requestScope.ed }" readonly="true" class="form-control mr-sm-2" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate"/>
						<input type="submit" class="btn btn-primary" value="搜索">
						<a href="${rootPath }/head/job/resumeInsert.htm?companyId=${param.companyId}" class="btn btn-primary ml-2" target="_blank">添加</a>
					</form>
				</div>
				<div class="col-">
					<div class="table-responsive mt-2">
						<!-- 要做批量操作 -->
						<form action="${rootPath }/head/demo/dynastyBatch.htm" method="post" class="form-inline" id="batchForm">
							<label for="status">操作类型:</label>
							<select name="operType" id="operType" class="form-control mr-sm-2" onchange="batchSwitch('operType')">
								<option value="">请选择</option>
								<option value="status">修改状态</option>
							</select>
							<div class="form-row selectInfo" id="updatestatus" style="display: none;">
								<label for="status">状态:</label>
								<select name="status" class="form-control mr-sm-2">
									<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
										<c:if test="${fn:startsWith(me.key,'STATUS')}">
											<!-- 拆分,按照-拆分,取最后一个 -->
											<c:set value="${fn:split(me.key, '-')}" var="keys"/>
											<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
							<input type="button" class="btn btn-primary" value="提交" onclick="return requestConfirm('batchForm','ids')">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>
											<div class="form-group form-check">
												<input type="checkbox" name="selectAll" id="selectAll" class="form-check-input" onclick="checkAll('selectAll','ids')">
											</div>
										</th>
										<th>序号</th>
										<th title="创建人">创人</th>
										<th>名称</th>
										<th title="预约时间-->提交时间">
											预时-->按时
										</th>
										<th title="是否按时提交">按时</th>
										<th title="顶数量">顶</th>
										<th title="踩数量">踩</th>
										<th title="是否最终">最终</th>
										<th>状态</th>
										<th>发布时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${list }" var="listTemp" varStatus="stat">
										<tr>
											<td>
												<div class="form-group form-check">
													<input type="checkbox" name="ids" value="${listTemp.id}" class="form-check-input">
												</div>
											</td>
											<td>${stat.count }</td>
											<td title="${listTemp.create.nickName }">
												${fn:substring(listTemp.create.nickName,0,subStrLen) }
											</td>
											<td title="${listTemp.name }">
												<a href="${rootPath}/head/job/resumeUpdate.htm?id=${listTemp.id}" target="_blank">
													${fn:substring(listTemp.name,0,subStrLen) }
												</a>
											</td>
											<td title="${listTemp.makeTime }">
												${listTemp.makeTime}
												<br/>
												${listTemp.trueTime }
											</td>
											<td>${listTemp.onTimeStr }</td>
											<td>${listTemp.upNum }</td>
											<td>${listTemp.downNum }</td>
											<td>${listTemp.finishStr }</td>
											<td>${listTemp.statusStr }</td>
											<td>${listTemp.pubTime }</td>
											<td>
												<c:if test="${listTemp.statusEnum == 'STATUS_DRAFT' || listTemp.statusEnum == 'STATUS_AUDITNO'}">
													<a href="${rootPath}/head/job/resumeUpdate.htm?operType=update&id=${listTemp.id}" target="_blank">
														修改
													</a>
													<br/>
													<a href="${rootPath}/head/job/resumeUpdate.htm?operType=file&id=${listTemp.id}&fileRelaType=5" target="_blank">
														上传附件
													</a>
													<br/>
													<a href="${rootPath}/head/system/sysFileList.htm?relaId=${listTemp.id}&fileRelaType=5&operType=update" target="_blank">
														查看附件
													</a>
												</c:if>
											</td>
										</tr>
									</c:forEach>							
								</tbody>
							</table>
						</form>
					</div>
				</div>
				<div class="col-">
					<form id="pageForm" action="${rootPath }/head/job/resumeList.htm"
						method="post" class="form-inline">
						<!-- 设置一些参数 -->
						<input type="hidden" name="keyword" value="${requestScope.keyword }">
						<input type="hidden" name="st" value="${requestScope.st }">
						<input type="hidden" name="ed" value="${requestScope.ed }">

						<input type="hidden" name="searchType" value="${requestScope.searchType }">
						<input type="hidden" name="status" value="${requestScope.status }">
						<input type="hidden" name="onTime" value="${requestScope.onTime }">
						<input type="hidden" name="finish" value="${requestScope.finish }">
						
	
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-end">
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '1', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>首页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.prePage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>上一页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.nextPage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>下一页</span>
									</a>
								</li>
								<li class="page-item">
									<a href="#" class="page-link" onclick="return pageFormSubmit('pageForm', 'currentPage', '${pageInfoUtil.totalPage}', 'pageSize','${pageInfoUtil.pageSize}')">
										<span>尾页</span>
									</a>
								</li>
								<li class="page-item">共${pageInfoUtil.totalRecord }条,共${pageInfoUtil.totalPage}页</li>
								<li class="page-item">
									第<input type="text" id="currentPage" name="currentPage" class="form-control" value="${pageInfoUtil.currentPage }"  size="5" maxlength="5"/>页
								</li>
								<li class="page-item">
									<input type="text" id="pageSize" name="pageSize" class="form-control" value="${pageInfoUtil.pageSize }" size="5" maxlength="5"/>
								</li>
								<li class="page-item"><input type="submit" value="GO" class="form-control btn btn-primary"/></li>
							</ul>
						</nav>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
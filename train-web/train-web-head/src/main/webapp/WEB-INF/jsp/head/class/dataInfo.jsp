<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<c:set value="${requestScope.response.data.one}" var="one"/>
<jsp:useBean class="com.wangsh.train.cla.pojo.AClassData" id="pojo"/>
<!DOCTYPE HTML>
<html>
	<head>
		<%@ include file="/common/include/title.jsp"%>
		<title>查看课程资料 - 培训机构</title>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<span id="cateTempId" cateId="${param.cateTempId }"></span>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="col-sm-10 col-sm-offset-1 form-signin-heading text-center" id="header_title">查看课程资料</h2>
					<a href="${rootPath }/head/class/dataUpdateSubmit.htm?id=${one.id}&operType=upView" class="href_info" onclick="return confirm('确认要操作吗?')">
						顶
					</a>
					<a href="${rootPath }/head/class/dataUpdateSubmit.htm?id=${one.id}&operType=downView" class="href_info" onclick="return confirm('确认要操作吗?')">
						踩
					</a>
					<form id="form" class="form-horizontal" action="${rootPath }/head/demo/dynastyUpdateSubmit.htm" method="post">
						<input type="hidden" name="id" value="${one.id }">
						<input type="hidden" name="operType" value="${param.operType }">
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">技术:</label>
							${one.tech.name }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">名称:</label>
							${one.name }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">浏览次数:</label>
							${one.viewNum }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">顶数量:</label>
							${one.upNum }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">踩数量:</label>
							${one.downNum }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">视频路径:</label>
							<a href="${one.videoPath}" class="href_info" target="_blank">
								${one.videoPath }
							</a>
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">视频解码:</label>
							${one.videoCode }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">时长:</label>
							${one.timeLong }
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">创建时间:</label>
							${one.createTime}
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">更新时间:</label>
							${one.updateTime}
						</div>
						<div class="form-group">
							<label for="pubTimeStr" class="col-sm-2 control-label">发布时间:</label>
							${one.putTime}
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">内容:</label>
							<div class="col-sm-8">
								${one.content}
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
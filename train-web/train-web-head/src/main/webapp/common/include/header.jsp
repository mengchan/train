<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 网页头部开始 -->
<nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
	<a class="navbar-brand" href="${rootPath }/">培训机构</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="${rootPath }/">
					首页
					<span class="sr-only">(current)</span>
				</a>
			</li>
			<c:if test="${sessionScope.users != null }">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" 
						id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">个人笔记
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="${rootPath }/head/users/usersCateList.htm?cateTempId=${requestScope.sys_map.usersCate_template_id_stuDay.vals}" target="_blank">学习日报</a>
						<a class="dropdown-item" href="${rootPath }/head/users/usersCateList.htm?cateTempId=${requestScope.sys_map.usersCate_template_id_week.vals}" target="_blank">学习周报</a>
						<a class="dropdown-item" href="${rootPath }/head/users/usersCateList.htm?cateTempId=${requestScope.sys_map.usersCate_template_id_month.vals}" target="_blank">学习月报</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="${rootPath }/head/class/classList.htm"   target="_blank"
						id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">班级
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="${rootPath }/head/class/classList.htm" target="_blank">班级列表</a>
						<a class="dropdown-item" href="${rootPath }/head/class/codeStatList.htm" target="_blank">代码统计列表</a>
						<a class="dropdown-item" href="${rootPath }/head/class/dataList.htm" target="_blank">课程资料列表</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="${rootPath }/head/ques/askList.htm" 
						id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">题库
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="${rootPath }/head/ques/askList.htm" target="_blank">问题列表(全部)</a>
						<a class="dropdown-item" href="${rootPath }/head/ques/askList.htm?searchType=self" target="_blank">问题列表(自己)</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="${rootPath }/head/ques/examDescList.htm" target="_blank">试卷列表</a>
						<%-- <a class="dropdown-item" href="${rootPath }/head/ques/examScoreDescList.htm" target="_blank">成绩列表(全部)</a> --%>
						<a class="dropdown-item" href="${rootPath }/head/ques/examScoreDescList.htm?searchType=self" target="_blank">成绩列表(自己)</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="${rootPath }/head/class/classList.htm"  target="_blank"
						id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">工作
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="${rootPath }/head/job/jobCompanyList.htm" target="_blank">公司列表(全部)</a>
						<a class="dropdown-item" href="${rootPath }/head/job/jobCompanyList.htm?searchType=self" target="_blank">公司列表(自己)</a>
						<a class="dropdown-item" href="${rootPath }/head/job/comPositionList.htm?searchType=self" target="_blank">职位列表(自己)</a>
						<a class="dropdown-item" href="${rootPath }/head/job/interviewList.htm?searchType=self" target="_blank">面试列表(自己)</a>
						<a class="dropdown-item" href="${rootPath }/head/job/interviewList.htm?searchType=askSelf" target="_blank">面试列表(自己录入)</a>
						<a class="dropdown-item" href="${rootPath }/head/job/offerList.htm?searchType=self" target="_blank">offer列表(自己)</a>
						<a class="dropdown-item" href="${rootPath }/head/job/resumeList.htm" target="_blank">简历列表(全部)</a>
						<a class="dropdown-item" href="${rootPath }/head/job/resumeList.htm?searchType=self" target="_blank">简历列表(自己)</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="${rootPath }/head/class/classList.htm"  target="_blank"
						id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">系统模块
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="${rootPath }/head/system/techList.htm" target="_blank">技术列表</a>
					</div>
				</li>
				<!-- <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="${rootPath }/head/class/classList.htm" 
						id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">系统
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="${rootPath }/head/system/sysFileList.htm">文件列表</a>
					</div>
				</li> -->
			</c:if>
			<li class="nav-item">
				<a class="nav-link" href="#top">
					返回顶部
				</a>
			</li>
			<!-- <li class="nav-item"><a class="nav-link" href="#">登陆</a></li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> 下拉 
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="#">子菜单-1</a> <a
						class="dropdown-item" href="#">子菜单-2</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="#">子菜单-3</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link disabled" href="#">面板</a>
			</li> -->
		</ul>
		<!-- 小分辨率要藏起来 -->
		<form class="form-inline my-2 my-lg-0 hidden-sm">
			<input class="form-control mr-sm-2" type="search" placeholder="关键字"
				aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">搜索</button>
		</form>
		<c:choose>
			<c:when test="${sessionScope.users != null }">
				<ul class="navbar-nav">
					<!-- 小分辨率要藏起来 -->
					<li class="nav-item navbar-text">
						<a href="${rootPath }/head/users/main.htm">
							<img src="${rootPath}/img/no_pic.png" 
								class="img-fluid rounded mx-auto"  width="28" height="28">
						</a>
					</li>
					<li class="nav-item dropdown">
						<a
							class="nav-link dropdown-toggle" href="#" id="navbarDropdownRight"
							role="button" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false"> 配置 <span class="sr-only">(current)</span>
						</a>
						<div class="dropdown-menu header-right" aria-labelledby="navbarDropdownRight">
							<a class="dropdown-item" href="${requestScope.usersCenterHeadUrl}" target="_blank">用户中心</a>
							<a class="dropdown-item" href="${rootPath }/head/users/usersUpdate.htm">查看个人信息</a>
							<a class="dropdown-item" href="${rootPath }/head/users/usersUpdate.htm?operType=updateInfo">修改个人信息</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="${rootPath }/head/users/usersClassList.htm">班级历史</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">退出</a>
						</div>
					</li>
				</ul>
			</c:when>
			<c:otherwise>
				<ul class="navbar-nav">
					<!-- 小分辨率要藏起来 -->
					<li class="nav-item"><a class="nav-link" href="${rootPath }/login.htm">登陆</a></li>
					<li class="nav-item"><a class="nav-link" href="${rootPath }/register.htm">注册</a></li>
				</ul>
			</c:otherwise>
		</c:choose>
	</div>
</nav>

<!-- 退出登陆的对话框 -->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">提示信息</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				确认要退出吗? 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
				<a class="btn btn-primary" href="${rootPath }/head/users/logout.htm">退出</a>
			</div>
		</div>
	</div>
</div>

<!-- 顶部锚点 -->
<a name="top"></a>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<c:if test="${response != null && response != '' && response.info != null && response.info != '' }">
	<!-- 提示信息 -->
	<div class="container">
		<div class="col-sm-8 col-sm-offset-1">
			<div class="alert alert-warning alert-dismissible fade show"
				role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				${response.info }
			</div>
		</div>
	</div>
</c:if>
<%-- 清空消息 --%>
<c:set value="" var="response" scope="session"/>
<footer class="footer mt-5">
	<div class="container">
		<div class="row">
			<div class="col-sm-11 col-lg-6">
				<h4>
					<!-- <img src="http://static.bootcss.com/www/assets/img/logo.png"> -->
				</h4>
				<p>
					本网站所列开源项目的中文版文档全部由<a href="${rootPath }/">培训机构</a>顶力开发 CC BY 3.0协议发布。
				</p>
			</div>
			<div class="col-sm-11  col-lg-6">
				<div class="row">
					<div class="col-sm-3">
						<ul class="list-unstyled">
							<li class="mb-1">关于</li>
							<li class=""><a href="#" class="text-muted">关于我们</a></li>
							<li class=""><a href="#" class="text-muted">广告合作</a></li>
							<li class=""><a href="#" class="text-muted">友情链接</a></li>
							<li class=""><a href="#" class="text-muted">招聘</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<ul class="list-unstyled">
							<li class="mb-1">联系方式</li>
							<li class=""><a href="#" class="text-muted">新浪微博</a></li>
							<li class=""><a href="#" class="text-muted">电子邮件</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<ul class="list-unstyled">
							<li class="mb-1">旗下网站</li>
							<li class=""><a href="#" class="text-muted">Laravel中文网</a></li>
							<li class=""><a href="#" class="text-muted">Ghost中国</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<ul class="list-unstyled">
							<li class="mb-1">赞助商</li>
							<li class=""><a href="#" class="text-muted">UCloud</a></li>
							<li class=""><a href="#" class="text-muted">又拍云</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-11 col-md-offset-1 text-center">
				<ul class="list-inline">
					<li class="list-inline-item"><a href="#"
						class="text-secondary">京ICP备11008151号</a></li>
					<li class="list-inline-item">京公网安备11010802014853</li>
				</ul>
			</div>
		</div>
	</div>
</footer>
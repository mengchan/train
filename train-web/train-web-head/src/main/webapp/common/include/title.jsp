<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" type="text/css" href="${rootPath }/common/resource/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="${rootPath }/css/main.css" />
<link rel="stylesheet" type="text/css" href="${rootPath }/css/common.css" />

<script type="text/javascript" src="${rootPath }/common/resource/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="${rootPath }/common/resource/popper.min.js"></script>
<script type="text/javascript" src="${rootPath }/common/resource/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="${rootPath }/common/resource/util.js"></script>
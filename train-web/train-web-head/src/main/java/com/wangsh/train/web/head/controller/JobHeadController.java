package com.wangsh.train.web.head.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.job.pojo.AJobComPosition;
import com.wangsh.train.job.pojo.AJobComPositionEnum;
import com.wangsh.train.job.pojo.AJobCompany;
import com.wangsh.train.job.pojo.AJobCompanyEnum;
import com.wangsh.train.job.pojo.AJobInterview;
import com.wangsh.train.job.pojo.AJobInterviewEnum;
import com.wangsh.train.job.pojo.AJobOffer;
import com.wangsh.train.job.pojo.AJobOfferEnum;
import com.wangsh.train.job.pojo.AJobResume;
import com.wangsh.train.job.pojo.AJobResumeEnum;
import com.wangsh.train.job.service.IJobDbService;
import com.wangsh.train.system.pojo.ASysFileEnum;
import com.wangsh.train.system.pojo.ASysPro;
import com.wangsh.train.system.pojo.ASysProEnum;
import com.wangsh.train.system.service.ISystemDbService;
import com.wangsh.train.users.pojo.AUsers;

/**
 * 	工作后台操作
 * 	@author wangsh
 */
@Controller
@RequestMapping("/head/job/")
public class JobHeadController extends BaseController
{
	@Resource
	private IJobDbService jobDbService;
	@Resource
	private ISystemDbService systemDbService;
	
	/**
	 * 公司列表页面
	 * @return
	 */
	@RequestMapping("/jobCompanyList")
	public String jobCompanyList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyList--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		String regionId = request.getParameter("regionId");
		request.setAttribute("regionId", regionId);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		condMap.put("regionId",regionId);
		condMap.put("extend", "true");
		
		/* 只查询启用的问题 */
		String searchType = request.getParameter("searchType");
		request.setAttribute("searchType", searchType);
		if("self".equalsIgnoreCase(searchType))
		{
			/* 只查看自己的发布的问题 */
			condMap.put("createId", users.getId());
		}else
		{
			/* 其他用户只能查看审核通过的问题 */
			condMap.put("status", AJobCompanyEnum.STATUS_AUDITYES.getStatus());
		}
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobCompany> response = this.jobDbService.findCondListJobCompanyService(pageInfoUtil, condMap);
		response.clearInfo();
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/head/job/jobCompanyList";
	}
	
	/**
	 * 打开公司添加页面
	 * @return
	 */
	@RequestMapping("/jobCompanyInsert")
	public String jobCompanyInsert(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyInsert--");
		return "/head/job/jobCompanyInsert";
	}
	
	/**
	 * 打开公司添加页面
	 * @return
	 */
	@RequestMapping("/jobCompanyInsertSubmit")
	public String jobCompanyInsertSubmit(HttpServletRequest request,Model model, AJobCompany jobCompany)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyInsertSubmit--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		try
		{
			String pubTimeStr = request.getParameter("pubTimeStr");
			/* 为对象赋值 */
			jobCompany.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			jobCompany.setCreateId(users.getId());
			dbResponse = this.jobDbService.saveOneJobCompanyService(jobCompany);
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("添加一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		model.addAttribute("response", dbResponse.toJSON());
		return this.jobCompanyInsert(request,model);
	}
	
	/**
	 * 打开公司更新页面
	 * @return
	 */
	@RequestMapping("/jobCompanyUpdate")
	public String jobCompanyUpdate(HttpServletRequest request,String updateFlag,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyUpdate--");
		/* 从session中获取信息 */
		String id = request.getParameter("id");
		/* 准备请求的信息 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		ApiResponse<AJobCompany> apiResponse = this.jobDbService.findOneJobCompanyService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 根据不同的请求跳转不同的页面 */
			return "/head/job/jobCompanyUpdate";
		}else if("file".equalsIgnoreCase(operType))
		{
			/* 上传文件 */
			model.addAttribute("name", "公司的Logo");
			model.addAttribute("url", "/head/job/jobCompanyFileUpdateSubmit.htm?operType=" + operType + "&dataId=" + id);
			return this.uploadFile;
		}
		return "/head/job/jobCompanyInfo";
	}
	
	/**
	 * 打开公司更新页面
	 * @return
	 */
	@RequestMapping("/jobCompanyUpdateSubmit")
	public String jobCompanyUpdateSubmit(HttpServletRequest request,MultipartFile file,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--jobCompanyUpdateSubmit--");
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		AUsers users = (AUsers) this.findObjfromSession(request, "users") ; 
		try
		{
			/* 从session中获取信息 */
			String id = request.getParameter("id");
			/* 准备请求的信息 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", id);
			ApiResponse<AJobCompany> apiResponse = this.jobDbService.findOneJobCompanyService(condMap);
			AJobCompany jobCompany = apiResponse.getDataOneJava() ; 
			model.addAttribute("response", apiResponse.toJSON());
			
			Map<String, Object> paramsMap = new HashMap<String,Object>();
			boolean flag = false ; 
			String operType = request.getParameter("operType");
			if("update".equalsIgnoreCase(operType))
			{
				/* 根据不同的请求跳转不同的页面 */
				String name = request.getParameter("name");
				String website = request.getParameter("website");
				String address = request.getParameter("address");
				String contact = request.getParameter("contact");
				String phone = request.getParameter("phone");
				String content = request.getParameter("content");
				String regionId = request.getParameter("regionId");
				String status = request.getParameter("status");
				
				String pubTimeStr = request.getParameter("pubTimeStr");
				jobCompany.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
				
				jobCompany.setName(name);
				jobCompany.setWebsite(website);
				jobCompany.setAddress(address);
				jobCompany.setContact(contact);
				jobCompany.setPhone(phone);
				jobCompany.setContent(content);
				jobCompany.setStatus(Byte.valueOf(status));
				jobCompany.setRegionId(Integer.valueOf(regionId));
				
				flag = true ; 
			}else if("upView".equalsIgnoreCase(operType))
			{
				jobCompany.setUpNum(jobCompany.getUpNum() + 1);
				
				flag = true ; 
			}else if("downView".equalsIgnoreCase(operType))
			{
				jobCompany.setDownNum(jobCompany.getDownNum() + 1);
				
				flag = true ; 
			}
			
			if(flag)
			{
				dbResponse = this.jobDbService.updateOneJobCompanyService(jobCompany,paramsMap);
			}
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return this.jobCompanyUpdate(request, this.cleanInfoFlag, model);
	}
	
	/**
	 * 更新代码统计提交操作
	 * @return
	 */
	@RequestMapping(value = "/jobCompanyFileUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String jobCompanyFileUpdateSubmit(MultipartFile file,
			HttpServletRequest request,Model model)
	{
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		AUsers users = (AUsers) this.findObjfromSession(request, "users") ; 
		try
		{
			String id = request.getParameter("dataId");
			/* 从session中获取信息 */
			/* 准备请求的信息 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", id);
			ApiResponse<AJobCompany> apiResponse = this.jobDbService.findOneJobCompanyService(condMap);
			AJobCompany jobCompany = apiResponse.getDataOneJava() ; 
			model.addAttribute("response", apiResponse.toJSON());
			
			if(users.getId() == jobCompany.getCreateId())
			{
				Map<String, Object> paramsMap = new HashMap<String,Object>();
				boolean flag = false ; 
				String operType = request.getParameter("operType");
				if("file".equalsIgnoreCase(operType))
				{
					try
					{
						if(file != null)
						{
							ConstatFinalUtil.SYS_LOGGER.info("name:{},oriName:{}" , file.getName() , file.getOriginalFilename());
							paramsMap.put("fileName", file.getOriginalFilename());
							paramsMap.put("inputStream", file.getInputStream());
							paramsMap.put("fileSize", file.getSize());
							
							flag = true ; 
						}
					} catch (IOException e)
					{
						ConstatFinalUtil.SYS_LOGGER.error("上传文件失败了;id:{}",jobCompany.getId());
					}
				}
				
				if(flag)
				{
					dbResponse = this.jobDbService.updateOneJobCompanyService(jobCompany,paramsMap);
					ConstatFinalUtil.SYS_LOGGER.info("--代码统计添加返回结果:{}--",dbResponse.toJSON());
				}
			}else
			{
				dbResponse.setCode(ApiResponseEnum.INFO_AUTH_NO.getStatus());
				dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(ApiResponseEnum.INFO_AUTH_NO.getStatus() + ""), Collections.EMPTY_MAP);
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return dbResponse.toJSON().toJSONString();
	}
	
	/*====公司职位表开始====*/
	/**
	 * 公司职位列表页面
	 * @return
	 */
	@RequestMapping("/comPositionList")
	public String comPositionList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionList--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 只查询启用的问题 */
		String searchType = request.getParameter("searchType");
		request.setAttribute("searchType", searchType);
		if("self".equalsIgnoreCase(searchType))
		{
			/* 只查看自己的发布的问题 */
			condMap.put("createId", users.getId());
		}else
		{
			/* 其他用户只能查看审核通过的问题 */
			condMap.put("status", AJobComPositionEnum.STATUS_AUDITYES.getStatus());
		}
		
		/* 增加一个companyId */
		String companyId = request.getParameter("companyId");
		if(companyId == null)
		{
			companyId = "" ; 
		}
		condMap.put("companyId", companyId);
		request.setAttribute("companyId", companyId);
		
		/* 增加一个regionId */
		String regionId = request.getParameter("regionId");
		if(regionId == null)
		{
			regionId = "" ; 
		}
		condMap.put("regionId", regionId);
		request.setAttribute("regionId", regionId);
		
		/* 增加一个cateId */
		String cateId = request.getParameter("cateId");
		if(cateId == null)
		{
			cateId = "" ; 
		}
		condMap.put("cateId", cateId);
		request.setAttribute("cateId", cateId);
		
		/* 增加一个educId */
		String educId = request.getParameter("educId");
		if(educId == null)
		{
			educId = "" ; 
		}
		condMap.put("educId", educId);
		request.setAttribute("educId", educId);
		
		condMap.put("extend", "true");
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobComPosition> apiResponse = this.jobDbService.findCondListComPositionService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		
		/* 职位类型 */
		condMap.clear();
		condMap.put("operType", ASysProEnum.OPERTYPE_EQUAL.getStatus());
		condMap.put("proType", ASysProEnum.PROTYPE_POSITION.getStatus());
		ApiResponse<ASysPro> cateApiResponse = this.systemDbService.findCondListSysProService(null, condMap);
		request.setAttribute("cateApiResponse", cateApiResponse.toJSON());
		
		/* 学历 */
		condMap.clear();
		condMap.put("operType", ASysProEnum.OPERTYPE_EQUAL.getStatus());
		condMap.put("proType", ASysProEnum.PROTYPE_EDUCE.getStatus());
		ApiResponse<ASysPro> educeApiResponse = this.systemDbService.findCondListSysProService(null, condMap);
		request.setAttribute("educeApiResponse", educeApiResponse.toJSON());
		return "/head/job/comPositionList";
	}

	/**
	 * 打开添加公司职位页面
	 * @param updateFlag: true:表示 清除信息
	 * @return
	 */
	@RequestMapping("/comPositionInsert")
	public String comPositionInsert(HttpServletRequest request,String updateFlag,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionInsert--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		/* 职位类型 */
		condMap.clear();
		condMap.put("operType", ASysProEnum.OPERTYPE_EQUAL.getStatus());
		condMap.put("proType", ASysProEnum.PROTYPE_POSITION.getStatus());
		ApiResponse<ASysPro> cateApiResponse = this.systemDbService.findCondListSysProService(null, condMap);
		request.setAttribute("cateApiResponse", cateApiResponse.toJSON());
		
		/* 学历 */
		condMap.clear();
		condMap.put("operType", ASysProEnum.OPERTYPE_EQUAL.getStatus());
		condMap.put("proType", ASysProEnum.PROTYPE_EDUCE.getStatus());
		ApiResponse<ASysPro> educeApiResponse = this.systemDbService.findCondListSysProService(null, condMap);
		request.setAttribute("educeApiResponse", educeApiResponse.toJSON());
		
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
		}
		return "/head/job/comPositionInsert";
	}
	
	/**
	 * 添加公司职位提交操作
	 * @return
	 */
	@RequestMapping(value = "/comPositionInsertSubmit")
	public String comPositionInsertSubmit(HttpServletRequest request,String pubTimeStr , AJobComPosition comPosition,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionInsertSubmit--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		comPosition.setCreateId(users.getId());
		/* 为对象赋值 */
		comPosition.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		/* 保存结果 */
		ApiResponse apiResponse = this.jobDbService.saveOneComPositionService(comPosition);
		ConstatFinalUtil.SYS_LOGGER.info("--公司职位添加返回结果:{}--",apiResponse.toJSON());
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return this.comPositionInsert(request,this.cleanInfoFlag,model);
	}
	
	/**
	 * 打开更新公司职位页面
	 * @return
	 */
	@RequestMapping("/comPositionUpdate")
	public String comPositionUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AJobComPosition> apiResponse = this.jobDbService.findOneComPositionService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 职位类型 */
			condMap.clear();
			condMap.put("operType", ASysProEnum.OPERTYPE_EQUAL.getStatus());
			condMap.put("proType", ASysProEnum.PROTYPE_POSITION.getStatus());
			ApiResponse<ASysPro> cateApiResponse = this.systemDbService.findCondListSysProService(null, condMap);
			request.setAttribute("cateApiResponse", cateApiResponse.toJSON());
			
			/* 学历 */
			condMap.clear();
			condMap.put("operType", ASysProEnum.OPERTYPE_EQUAL.getStatus());
			condMap.put("proType", ASysProEnum.PROTYPE_EDUCE.getStatus());
			ApiResponse<ASysPro> educeApiResponse = this.systemDbService.findCondListSysProService(null, condMap);
			request.setAttribute("educeApiResponse", educeApiResponse.toJSON());
			
			return "/head/job/comPositionUpdate";
		}
		return "/head/job/comPositionInfo";
	}
	
	/**
	 * 添加公司职位提交操作
	 * @return
	 */
	@RequestMapping(value = "/comPositionUpdateSubmit")
	public String comPositionUpdateSubmit(HttpServletRequest request,String id,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionUpdateSubmit--");
		ApiResponse<Object> apiResponseRes = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobComPosition> apiResponse = this.jobDbService.findOneComPositionService(condMap);
		/* 获取java对象 */
		AJobComPosition comPosition = apiResponse.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String regionId = request.getParameter("regionId");
			String cateId = request.getParameter("cateId");
			String educId = request.getParameter("educId");
			String name = request.getParameter("name");
			String wages = request.getParameter("wages");
			String experSt = request.getParameter("experSt");
			String experEd = request.getParameter("experEd");
			String address = request.getParameter("address");
			String content = request.getParameter("content");
			String status = request.getParameter("status");
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			/* 设置属性 */
			comPosition.setRegionId(Integer.valueOf(regionId));
			comPosition.setCateId(Integer.valueOf(cateId));
			comPosition.setEducId(Integer.valueOf(educId));
			comPosition.setName(name);
			comPosition.setWages(Integer.valueOf(wages));
			comPosition.setExperSt(Integer.valueOf(experSt));
			comPosition.setExperEd(Integer.valueOf(experEd));
			comPosition.setAddress(address);
			comPosition.setContent(content);
			comPosition.setStatus(Byte.valueOf(status));
			
			comPosition.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));

			flag = true ; 
		}else if("upView".equalsIgnoreCase(operType))
		{
			comPosition.setUpNum(comPosition.getUpNum() + 1);
			
			flag = true ; 
		}else if("downView".equalsIgnoreCase(operType))
		{
			comPosition.setDownNum(comPosition.getDownNum() + 1);
			
			flag = true ; 
		}
		
		if(flag)
		{
			/* 数据库操作 */
			apiResponseRes = jobDbService.updateOneComPositionService(comPosition);
			ConstatFinalUtil.SYS_LOGGER.info("--课程资料更新返回结果:{}--",apiResponseRes.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", apiResponseRes.toJSON());
		}
		return comPositionUpdate(request, id, this.cleanInfoFlag,model);
	}
	
	/**
	 * 公司职位批量操作
	 * @return
	 */
	@RequestMapping(value = "/comPositionBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String comPositionBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--comPositionBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					condMap.clear();
					condMap.put("id", id);
					ApiResponse<AJobComPosition> apiResponseDb = this.jobDbService.findOneComPositionService(condMap);
					AJobComPosition comPosition = apiResponseDb.getDataOneJava();
					/* 设置状态 */
					comPosition.setStatus(Byte.valueOf(status));
					comPosition.setPubTime(new Date());
					
					ApiResponse<Object> comPositionClassResponse = this.jobDbService.updateOneComPositionService(comPosition);
					if(comPositionClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*====公司职位表结束====*/
	
	/*====公司面试表开始====*/
	/**
	 * 公司面试列表页面
	 * @return
	 */
	@RequestMapping("/interviewList")
	public String interviewList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewList--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 只查询启用的问题 */
		String searchType = request.getParameter("searchType");
		request.setAttribute("searchType", searchType);
		if("self".equalsIgnoreCase(searchType))
		{
			/* 只查看自己的发布的问题 */
			condMap.put("createId", users.getId());
		}else if("askSelf".equalsIgnoreCase(searchType))
		{
			/* 只查看自己的发布的问题 */
			condMap.put("askCreateId", users.getId());
		}else
		{
			List<String> statusList = new ArrayList<String>();
			statusList.add(AJobInterviewEnum.STATUS_AUDITYES.getStatus() + "");
			statusList.add(AJobInterviewEnum.STATUS_INSERTED.getStatus() + "");
			statusList.add(AJobInterviewEnum.STATUS_INSERTING.getStatus() + "");
			statusList.add(AJobInterviewEnum.STATUS_CONFIRM.getStatus() + "");
			/* 其他用户只能查看审核通过的问题 */
			condMap.put("statusList", statusList);
		}
		
		/* 增加一个companyId */
		String companyId = request.getParameter("companyId");
		if(companyId == null)
		{
			companyId = "" ; 
		}
		condMap.put("companyId", companyId);
		request.setAttribute("companyId", companyId);
		
		/* 增加一个cateId */
		String simulation = request.getParameter("simulation");
		if(simulation == null)
		{
			simulation = "" ; 
		}
		condMap.put("simulation", simulation);
		request.setAttribute("simulation",simulation);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobInterview> apiResponse = this.jobDbService.findCondListInterviewService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		
		return "/head/job/interviewList";
	}

	/**
	 * 打开添加公司面试页面
	 * @param updateFlag: true:表示 清除信息
	 * @return
	 */
	@RequestMapping("/interviewInsert")
	public String interviewInsert(HttpServletRequest request,String updateFlag,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewInsert--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
		}
		return "/head/job/interviewInsert";
	}
	
	/**
	 * 添加公司面试提交操作
	 * @return
	 */
	@RequestMapping(value = "/interviewInsertSubmit")
	public String interviewInsertSubmit(HttpServletRequest request,String pubTimeStr , AJobInterview interview,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewInsertSubmit--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		interview.setCreateId(users.getId());
		/* 为对象赋值 */
		interview.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		
		String interTimeStr = request.getParameter("interTimeStr");
		interview.setInterTime(this.dateFormatUtil.strDateTime(interTimeStr));
		
		/* 保存结果 */
		ApiResponse apiResponse = this.jobDbService.saveOneInterviewService(interview);
		ConstatFinalUtil.SYS_LOGGER.info("--公司面试添加返回结果:{}--",apiResponse.toJSON());
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return this.interviewInsert(request,this.cleanInfoFlag,model);
	}
	
	/**
	 * 打开更新公司面试页面
	 * @return
	 */
	@RequestMapping("/interviewUpdate")
	public String interviewUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobInterview> apiResponse = this.jobDbService.findOneInterviewService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/job/interviewUpdate";
		}else if("file".equalsIgnoreCase(operType))
		{
			/* 上传文件 */
			model.addAttribute("name", "上传面试笔试照片");
			model.addAttribute("recName", "");
			
			model.addAttribute("operType", "insert");
			model.addAttribute("relaId", id);
			model.addAttribute("fileType", ASysFileEnum.FILETYPE_IMG.getStatus());
			model.addAttribute("fileRelaType", ASysFileEnum.FILERELATYPE_INTERVIEW.getStatus());
			model.addAttribute("url", "/head/system/sysFileFileUpdateSubmit.htm?relaId=" + id);
			return "/head/system/sysFileFileUpdate";
		}
		return "/head/job/interviewInfo";
	}
	
	/**
	 * 添加公司面试提交操作
	 * @return
	 */
	@RequestMapping(value = "/interviewUpdateSubmit")
	public String interviewUpdateSubmit(HttpServletRequest request,String id,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewUpdateSubmit--");
		ApiResponse<Object> apiResponseRes = new ApiResponse<Object>();
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobInterview> apiResponse = this.jobDbService.findOneInterviewService(condMap);
		/* 获取java对象 */
		AJobInterview interview = apiResponse.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String name = request.getParameter("name");
			String voicePath = request.getParameter("voicePath");
			String voiceCode = request.getParameter("voiceCode");
			String eval = request.getParameter("eval");
			String price = request.getParameter("price");
			String simulation = request.getParameter("simulation");
			String content = request.getParameter("content");
			String status = request.getParameter("status");
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			String interTimeStr = request.getParameter("interTimeStr");
			/* 设置属性 */
			interview.setName(name);
			interview.setVoicePath(voicePath);
			interview.setVoiceCode(voiceCode);
			interview.setEval(eval);
			interview.setPrice(Integer.valueOf(price));
			interview.setSimulation(Byte.valueOf(simulation));
			
			interview.setContent(content);
			interview.setStatus(Byte.valueOf(status));
			
			interview.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			interview.setInterTime(this.dateFormatUtil.strDateTime(interTimeStr));

			flag = true ; 
		}else if("inserting".equalsIgnoreCase(operType))
		{
			/* 将状态变为录入中 */
			interview.setAskCreateId(users.getId());
			interview.setStatus(AJobInterviewEnum.STATUS_INSERTING.getStatus());
			
			flag = true ; 
		}else if("inserted".equalsIgnoreCase(operType))
		{
			/* 将状态变为录入中 */
			interview.setStatus(AJobInterviewEnum.STATUS_INSERTED.getStatus());
			
			flag = true ; 
		}else if("upView".equalsIgnoreCase(operType))
		{
			interview.setUpNum(interview.getUpNum() + 1);
			
			flag = true ; 
		}else if("downView".equalsIgnoreCase(operType))
		{
			interview.setDownNum(interview.getDownNum() + 1);
			
			flag = true ; 
		}
		
		if(flag)
		{
			/* 数据库操作 */
			apiResponseRes = jobDbService.updateOneInterviewService(interview);
			ConstatFinalUtil.SYS_LOGGER.info("--课程资料更新返回结果:{}--",apiResponseRes.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", apiResponseRes.toJSON());
		}
		return interviewUpdate(request, id, this.cleanInfoFlag,model);
	}
	
	/**
	 * 公司面试批量操作
	 * @return
	 */
	@RequestMapping(value = "/interviewBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String interviewBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--interviewBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					condMap.clear();
					condMap.put("id", id);
					ApiResponse<AJobInterview> apiResponseDb = this.jobDbService.findOneInterviewService(condMap);
					AJobInterview interview = apiResponseDb.getDataOneJava();
					/* 设置状态 */
					interview.setStatus(Byte.valueOf(status));
					interview.setPubTime(new Date());
					
					ApiResponse<Object> interviewClassResponse = this.jobDbService.updateOneInterviewService(interview);
					if(interviewClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*====公司面试表结束====*/
	
	/*====公司offer表开始====*/
	/**
	 * 公司offer列表页面
	 * @return
	 */
	@RequestMapping("/offerList")
	public String offerList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerList--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 只查询启用的问题 */
		String searchType = request.getParameter("searchType");
		request.setAttribute("searchType", searchType);
		if("self".equalsIgnoreCase(searchType))
		{
			/* 只查看自己的发布的问题 */
			condMap.put("createId", users.getId());
		}else
		{
			/* 其他用户只能查看审核通过的问题 */
			condMap.put("status", AJobOfferEnum.STATUS_AUDITYES.getStatus());
		}
		
		/* 增加一个companyId */
		String companyId = request.getParameter("companyId");
		if(companyId == null)
		{
			companyId = "" ; 
		}
		condMap.put("companyId", companyId);
		request.setAttribute("companyId", companyId);
		
		/* 增加一个fiveRisks */
		String fiveRisks = request.getParameter("fiveRisks");
		if(fiveRisks == null)
		{
			fiveRisks = "" ; 
		}
		condMap.put("fiveRisks", fiveRisks);
		request.setAttribute("fiveRisks", fiveRisks);
		
		/* 增加一个accFund */
		String accFund = request.getParameter("accFund");
		if(accFund == null)
		{
			accFund = "" ; 
		}
		condMap.put("accFund", accFund);
		request.setAttribute("accFund", accFund);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobOffer> apiResponse = this.jobDbService.findCondListOfferService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		
		return "/head/job/offerList";
	}

	/**
	 * 打开添加公司offer页面
	 * @param updateFlag: true:表示 清除信息
	 * @return
	 */
	@RequestMapping("/offerInsert")
	public String offerInsert(HttpServletRequest request,String updateFlag,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerInsert--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
		}
		return "/head/job/offerInsert";
	}
	
	/**
	 * 添加公司offer提交操作
	 * @return
	 */
	@RequestMapping(value = "/offerInsertSubmit")
	public String offerInsertSubmit(HttpServletRequest request,String pubTimeStr , AJobOffer offer,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerInsertSubmit--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		offer.setCreateId(users.getId());
		/* 为对象赋值 */
		offer.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		/* 保存结果 */
		ApiResponse apiResponse = this.jobDbService.saveOneOfferService(offer);
		ConstatFinalUtil.SYS_LOGGER.info("--公司offer添加返回结果:{}--",apiResponse.toJSON());
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return this.offerInsert(request,this.cleanInfoFlag,model);
	}
	
	/**
	 * 打开更新公司offer页面
	 * @return
	 */
	@RequestMapping("/offerUpdate")
	public String offerUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobOffer> apiResponse = this.jobDbService.findOneOfferService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/job/offerUpdate";
		}else if("file".equalsIgnoreCase(operType))
		{
			/* 上传文件 */
			model.addAttribute("name", "上传offer截图");
			model.addAttribute("recName", "");
			
			model.addAttribute("operType", "insert");
			model.addAttribute("relaId", id);
			model.addAttribute("fileType", ASysFileEnum.FILETYPE_IMG.getStatus());
			model.addAttribute("fileRelaType", ASysFileEnum.FILERELATYPE_OFFER.getStatus());
			model.addAttribute("url", "/head/system/sysFileFileUpdateSubmit.htm?relaId=" + id);
			return "/head/system/sysFileFileUpdate";
		}
		return "/head/job/offerInfo";
	}
	
	/**
	 * 添加公司offer提交操作
	 * @return
	 */
	@RequestMapping(value = "/offerUpdateSubmit")
	public String offerUpdateSubmit(HttpServletRequest request,String id,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerUpdateSubmit--");
		ApiResponse<Object> apiResponseRes = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobOffer> apiResponse = this.jobDbService.findOneOfferService(condMap);
		/* 获取java对象 */
		AJobOffer offer = apiResponse.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String name = request.getParameter("name");
			String wages = request.getParameter("wages");
			String fiveRisks = request.getParameter("fiveRisks");
			String accFund = request.getParameter("accFund");
			String interCount = request.getParameter("interCount");
			String content = request.getParameter("content");
			String status = request.getParameter("status");
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			/* 设置属性 */
			offer.setName(name);
			offer.setWages(Integer.valueOf(wages));
			offer.setFiveRisks(Byte.valueOf(fiveRisks));
			offer.setAccFund(Byte.valueOf(accFund));
			offer.setInterCount(Integer.valueOf(interCount));
			offer.setContent(content);
			offer.setStatus(Byte.valueOf(status));
			
			offer.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));

			flag = true ; 
		}
		
		if(flag)
		{
			/* 数据库操作 */
			apiResponseRes = jobDbService.updateOneOfferService(offer);
			ConstatFinalUtil.SYS_LOGGER.info("--公司offer更新返回结果:{}--",apiResponseRes.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", apiResponseRes.toJSON());
		}
		return offerUpdate(request, id, this.cleanInfoFlag,model);
	}
	
	/**
	 * 公司offer批量操作
	 * @return
	 */
	@RequestMapping(value = "/offerBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String offerBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--offerBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					condMap.clear();
					condMap.put("id", id);
					ApiResponse<AJobOffer> apiResponseDb = this.jobDbService.findOneOfferService(condMap);
					AJobOffer offer = apiResponseDb.getDataOneJava();
					/* 设置状态 */
					offer.setStatus(Byte.valueOf(status));
					offer.setPubTime(new Date());
					
					ApiResponse<Object> offerClassResponse = this.jobDbService.updateOneOfferService(offer);
					if(offerClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*====公司offer表结束====*/
	
	/*====简历表开始====*/
	/**
	 * 简历列表页面
	 * @return
	 */
	@RequestMapping("/resumeList")
	public String resumeList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeList--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 只查询启用的问题 */
		String searchType = request.getParameter("searchType");
		request.setAttribute("searchType", searchType);
		if("self".equalsIgnoreCase(searchType))
		{
			/* 只查看自己的发布的问题 */
			condMap.put("createId", users.getId());
		}else
		{
			/* 其他用户只能查看审核通过的问题 */
			condMap.put("status", AJobResumeEnum.STATUS_AUDITYES.getStatus());
			/* 只看最终版本的简历 */
			condMap.put("finish", AJobResumeEnum.FINISH_YES.getStatus());
		}
		
		/* 增加一个finish */
		String finish = request.getParameter("finish");
		if(finish == null)
		{
			finish = "" ; 
		}
		condMap.put("finish", finish);
		request.setAttribute("finish", finish);
		
		/* 增加一个finish */
		String onTime = request.getParameter("onTime");
		if(onTime == null)
		{
			onTime = "" ; 
		}
		condMap.put("onTime", onTime);
		request.setAttribute("onTime", onTime);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AJobResume> apiResponse = this.jobDbService.findCondListResumeService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return "/head/job/resumeList";
	}

	/**
	 * 打开添加简历页面
	 * @param updateFlag: true:表示 清除信息
	 * @return
	 */
	@RequestMapping("/resumeInsert")
	public String resumeInsert(HttpServletRequest request,String updateFlag,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeInsert--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
		}
		return "/head/job/resumeInsert";
	}
	
	/**
	 * 添加简历提交操作
	 * @return
	 */
	@RequestMapping(value = "/resumeInsertSubmit")
	public String resumeInsertSubmit(HttpServletRequest request,String pubTimeStr , AJobResume resume,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeInsertSubmit--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		resume.setCreateId(users.getId());
		
		String makeTimeStr = request.getParameter("makeTimeStr");
		String trueTimeStr = request.getParameter("trueTimeStr");
		
		resume.setMakeTime(this.dateFormatUtil.strDateTime(makeTimeStr));
		resume.setTrueTime(this.dateFormatUtil.strDateTime(trueTimeStr));
		
		/* 为对象赋值 */
		resume.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		/* 保存结果 */
		ApiResponse apiResponse = this.jobDbService.saveOneResumeService(resume);
		ConstatFinalUtil.SYS_LOGGER.info("--简历添加返回结果:{}--",apiResponse.toJSON());
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return this.resumeInsert(request,this.cleanInfoFlag,model);
	}
	
	/**
	 * 打开更新简历页面
	 * @return
	 */
	@RequestMapping("/resumeUpdate")
	public String resumeUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AJobResume> apiResponse = this.jobDbService.findOneResumeService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/job/resumeUpdate";
		}else if("file".equalsIgnoreCase(operType))
		{
			/* 上传文件 */
			model.addAttribute("name", "上传简历附件");
			model.addAttribute("recName", "");
			
			model.addAttribute("operType", "insert");
			model.addAttribute("relaId", id);
			model.addAttribute("fileType", ASysFileEnum.FILETYPE_COMMON.getStatus());
			model.addAttribute("fileRelaType", ASysFileEnum.FILERELATYPE_RESUME.getStatus());
			model.addAttribute("url", "/head/system/sysFileFileUpdateSubmit.htm?relaId=" + id);
			return "/head/system/sysFileFileUpdate";
		}
		return "/head/job/resumeInfo";
	}
	
	/**
	 * 添加简历提交操作
	 * @return
	 */
	@RequestMapping(value = "/resumeUpdateSubmit")
	public String resumeUpdateSubmit(HttpServletRequest request,String id,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeUpdateSubmit--");
		ApiResponse<Object> apiResponseRes = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AJobResume> apiResponse = this.jobDbService.findOneResumeService(condMap);
		/* 获取java对象 */
		AJobResume resume = apiResponse.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String name = request.getParameter("name");
			String content = request.getParameter("content");
			String status = request.getParameter("status");
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			String makeTimeStr = request.getParameter("makeTimeStr");
			
			resume.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			resume.setMakeTime(this.dateFormatUtil.strDateTime(makeTimeStr));
			
			/* 设置属性 */
			resume.setName(name);
			resume.setContent(content);
			resume.setStatus(Byte.valueOf(status));
			
			flag = true ; 
		}else if("upView".equalsIgnoreCase(operType))
		{
			resume.setUpNum(resume.getUpNum() + 1);
			
			flag = true ; 
		}else if("downView".equalsIgnoreCase(operType))
		{
			resume.setDownNum(resume.getDownNum() + 1);
			
			flag = true ; 
		}
		
		if(flag)
		{
			/* 数据库操作 */
			apiResponseRes = jobDbService.updateOneResumeService(resume);
			ConstatFinalUtil.SYS_LOGGER.info("--课程资料更新返回结果:{}--",apiResponseRes.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", apiResponseRes.toJSON());
		}
		return resumeUpdate(request, id, this.cleanInfoFlag,model);
	}
	
	/**
	 * 简历批量操作
	 * @return
	 */
	@RequestMapping(value = "/resumeBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String resumeBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--resumeBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					condMap.clear();
					condMap.put("id", id);
					ApiResponse<AJobResume> apiResponseDb = this.jobDbService.findOneResumeService(condMap);
					AJobResume resume = apiResponseDb.getDataOneJava();
					/* 设置状态 */
					resume.setStatus(Byte.valueOf(status));
					resume.setPubTime(new Date());
					
					ApiResponse<Object> resumeClassResponse = this.jobDbService.updateOneResumeService(resume);
					if(resumeClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*====简历表结束====*/
}

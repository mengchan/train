package com.wangsh.train.web.head.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import com.wangsh.train.common.config.SpringMVCConfig;

/**
 * 自己项目的配置
 * @author Xh-Win10-Server
 *
 */
@Configuration
public class HeadMVCConfig extends SpringMVCConfig
{
	@Resource
	private HandlerInterceptor authHeadInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry)
	{
		registry.addInterceptor(authHeadInterceptor).addPathPatterns("/head/**");
	}
}

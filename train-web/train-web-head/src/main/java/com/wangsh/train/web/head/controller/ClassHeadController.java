package com.wangsh.train.web.head.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wangsh.train.cla.pojo.AClass;
import com.wangsh.train.cla.pojo.AClassData;
import com.wangsh.train.cla.pojo.AClassDataEnum;
import com.wangsh.train.cla.pojo.AClassEnum;
import com.wangsh.train.cla.pojo.ACodeDetail;
import com.wangsh.train.cla.pojo.ACodeStat;
import com.wangsh.train.cla.service.IClassDbService;
import com.wangsh.train.cla.service.IClassOperService;
import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.system.pojo.ASysTech;
import com.wangsh.train.system.pojo.ASysTechEnum;
import com.wangsh.train.system.service.ISystemDbService;
import com.wangsh.train.users.pojo.AUsers;

/**
 * 	管理员首页操作
 * 	@author wangsh
 */
@Controller
@RequestMapping("/head/class/")
public class ClassHeadController extends BaseController
{
	@Resource
	private IClassDbService classDbService;
	@Resource
	private IClassOperService classOperService;
	@Resource
	private ISystemDbService systemDbService;
	
	/**
	 * 班级列表页面
	 * @return
	 */
	@RequestMapping("/classList")
	public String classList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--classList--");
		String claStatus = request.getParameter("claStatus");
		if(claStatus == null)
		{
			claStatus = "" ; 
		}
		request.setAttribute("claStatus", claStatus);
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		condMap.put("claStatus", claStatus);
		/* 只查看启用的 */
		condMap.put("status", AClassEnum.STATUS_ENABLE.getStatus());
		condMap.put("extend", "true");
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AClass> response = this.classDbService.findCondListClassService(pageInfoUtil, condMap);
		response.clearInfo();
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/head/class/classList";
	}
	
	/**
	 * 打开更新班级页面
	 * @return
	 */
	@RequestMapping("/classUpdate")
	public String classUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--classUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AClass> response = this.classDbService.findOneClassService(condMap);
		response.clearInfo();
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/class/classUpdate";
		}
		return "/head/class/classInfo";
	}

	/**
	 * 代码统计列表页面
	 * @return
	 */
	@RequestMapping("/codeStatList")
	public String codeStatList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--codeStatList--");
		/* 只能查询自己的代码统计 */
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		String usersClaId = request.getParameter("usersClaId");
		request.setAttribute("usersClaId", usersClaId);
		
		String codeType = request.getParameter("codeType");
		if(codeType == null)
		{
			codeType = "" ; 
		}
		request.setAttribute("codeType", codeType);
		
		String orderType = request.getParameter("orderType");
		if(orderType == null)
		{
			orderType = "" ; 
		}
		request.setAttribute("orderType", orderType);
		condMap.put("orderType", orderType);
		
		condMap.put("codeType", codeType);
		condMap.put("usersId", users.getId());
		condMap.put("usersClaId", usersClaId);
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<ACodeStat> response = this.classDbService.findCondListCodeStatService(pageInfoUtil, condMap);
		response.clearInfo();
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/head/class/codeStatList";
	}

	/**
	 * 打开添加代码统计页面
	 * @return
	 */
	@RequestMapping("/codeStatInsert")
	public String codeStatInsert()
	{
		ConstatFinalUtil.SYS_LOGGER.info("--codeStatInsert--");
		return "/head/class/codeStatInsert";
	}
	
	/**
	 * 添加代码统计提交操作
	 * @return
	 */
	@RequestMapping(value = "/codeStatInsertSubmit")
	public String codeStatInsertSubmit(String pubTimeStr , ACodeStat codeStat,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--codeStatInsertSubmit--");
		/* 保存结果 */
		ApiResponse dbResponse = new ApiResponse<Object>();
		
		/* 只能查询自己的代码统计 */
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		try
		{
			codeStat.setUsersId(users.getId());
			codeStat.setUsersClaId(users.getUsersClassId());
			/* 为对象赋值 */
			codeStat.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			dbResponse = this.classDbService.saveOneCodeStatService(codeStat);
			ConstatFinalUtil.SYS_LOGGER.info("--代码统计添加返回结果:{}--",dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("添加一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		model.addAttribute("response", dbResponse.toJSON());
		return this.codeStatInsert();
	}
	
	/**
	 * 打开更新代码统计页面
	 * @return
	 */
	@RequestMapping("/codeStatUpdate")
	public String codeStatUpdate(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--codeStatUpdate--");
		String id = request.getParameter("id");
		String operType = request.getParameter("operType");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<ACodeStat> response = this.classDbService.findOneCodeStatService(condMap);
		ACodeStat codeStat = response.getDataOneJava() ;
		response.clearInfo();
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/class/codeStatUpdate";
		}else if("file".equalsIgnoreCase(operType))
		{
			/* 上传文件 */
			model.addAttribute("name", "上传代码统计");
			model.addAttribute("recName", codeStat.getName());
			model.addAttribute("url", "/head/class/codeStatFileUpdateSubmit.htm?operType=" + operType + "&codeStatId=" + id);
			return this.uploadFile;
		}
		return "/head/class/codeStatInfo";
	}
	
	/**
	 * 更新代码统计提交操作
	 * @return
	 */
	@RequestMapping(value = "/codeStatUpdateSubmit")
	public String codeStatUpdateSubmit(String id,MultipartFile file,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--codeStatUpdateSubmit--");
		/* 返回关系 */
		ApiResponse dbResponse = new ApiResponse<Object>();
		/* 只能查询自己的代码统计 */
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		try
		{
			/* 准备查询条件 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", id);
			/* 查询数据库 */
			ApiResponse<ACodeStat> response = this.classDbService.findOneCodeStatService(condMap);
			/* 获取java对象 */
			ACodeStat codeStat = response.getDataOneJava() ;
			
			Map<String, Object> paramsMap = new HashMap<String,Object>();
			boolean flag = false ; 
			String operType = request.getParameter("operType");
			if("update".equalsIgnoreCase(operType))
			{
				/* 接收参数 */
				String name = request.getParameter("name");
				String status = request.getParameter("status");
				String pubTimeStr = request.getParameter("pubTimeStr");
				String content = request.getParameter("content");
				/* 设置属性 */
				codeStat.setName(name);
				codeStat.setContent(content);
				codeStat.setStatus(Byte.valueOf(status));
				codeStat.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
				
				flag = true ; 
			}
			
			if(flag)
			{
				codeStat.setUsersClaId(users.getUsersClassId());
				
				/* 数据库操作 */
				dbResponse = classDbService.updateOneCodeStatService(codeStat,paramsMap);
				ConstatFinalUtil.SYS_LOGGER.info("--代码统计添加返回结果:{}--",dbResponse.toJSON());
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		model.addAttribute("response", dbResponse.toJSON());
		return "/head/class/codeStatUpdate";
	}
	
	/**
	 * 更新代码统计提交操作
	 * @return
	 */
	@RequestMapping(value = "/codeStatFileUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String codeStatFileUpdateSubmit(String codeStatId,MultipartFile file,
			HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--codeStatFileUpdateSubmit--");
		/* 返回关系 */
		ApiResponse dbResponse = new ApiResponse<Object>();
		/* 只能查询自己的代码统计 */
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		try
		{
			/* 准备查询条件 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", codeStatId);
			/* 查询数据库 */
			ApiResponse<ACodeStat> response = this.classDbService.findOneCodeStatService(condMap);
			/* 获取java对象 */
			ACodeStat codeStat = response.getDataOneJava() ;
			
			Map<String, Object> paramsMap = new HashMap<String,Object>();
			boolean flag = false ; 
			String operType = request.getParameter("operType");
			if("file".equalsIgnoreCase(operType))
			{
				try
				{
					if(file != null)
					{
						ConstatFinalUtil.SYS_LOGGER.info("name:{},oriName:{}" , file.getName() , file.getOriginalFilename());
						paramsMap.put("fileName", file.getOriginalFilename());
						paramsMap.put("inputStream", file.getInputStream());
						paramsMap.put("fileSize", file.getSize());
						
						flag = true ; 
					}
				} catch (IOException e)
				{
					ConstatFinalUtil.SYS_LOGGER.error("上传文件失败了;id:{}",codeStat.getId());
				}
			}
			
			if(flag)
			{
				codeStat.setUsersClaId(users.getUsersClassId());
				
				/* 数据库操作 */
				dbResponse = classDbService.updateOneCodeStatService(codeStat,paramsMap);
				ConstatFinalUtil.SYS_LOGGER.info("--代码统计添加返回结果:{}--",dbResponse.toJSON());
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return dbResponse.toJSON().toJSONString();
	}
	
	/**
	 * 代码统计明细列表页面
	 * @return
	 */
	@RequestMapping("/codeDetailList")
	public String codeDetailList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--codeDetailList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		String codeStatId = request.getParameter("codeStatId");
		request.setAttribute("codeStatId", codeStatId);
		condMap.put("codeStatId", codeStatId);
		
		String orderType = request.getParameter("orderType");
		if(orderType == null)
		{
			orderType = "" ; 
		}
		request.setAttribute("orderType", orderType);
		condMap.put("orderType", orderType);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<ACodeDetail> response = this.classDbService.findCondListCodeDetailService(pageInfoUtil, condMap);
		response.clearInfo();
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/head/class/codeDetailList";
	}
	
	/**
	 * 打开更新代码统计明细页面
	 * @return
	 */
	@RequestMapping("/codeDetailUpdate")
	public String codeDetailUpdate(String id , String operType , Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--codeDetailUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<ACodeDetail> response = this.classDbService.findOneCodeDetailService(condMap);
		response.clearInfo();
		/* 存储request */
		model.addAttribute("response", response.toJSON());
		
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/class/codeDetailUpdate";
		}
		return "/head/class/codeDetailInfo";
	}
	
	/*====课程资料表开始====*/
	/**
	 * 课程资料列表页面
	 * @return
	 */
	@RequestMapping("/dataList")
	public String dataList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dataList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 只查询启用的课程资料 */
		condMap.put("status", AClassDataEnum.STATUS_ENABLE.getStatus());
		
		/* 增加一个techId */
		String techId = request.getParameter("techId");
		if(techId == null)
		{
			techId = "" ; 
		}
		condMap.put("techId", techId);
		request.setAttribute("techId", techId);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AClassData> apiResponse = this.classDbService.findCondListDataService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		
		/* 查询所有启用的课程资料 */
		condMap.clear();
		condMap.put("status", ASysTechEnum.STATUS_ENABLE.getStatus());
		ApiResponse<ASysTech> sysTechResponse = this.systemDbService.findCondListSysTechService(null,condMap);
		request.setAttribute("sysTechResponse", sysTechResponse.toJSON());
		return "/head/class/dataList";
	}

	/**
	 * 打开更新课程资料页面
	 * @return
	 */
	@RequestMapping("/dataUpdate")
	public String dataUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dataUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AClassData> apiResponse = this.classDbService.findOneDataService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 存储request */
		request.setAttribute("response", apiResponse.toJSON());
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/class/dataUpdate";
		}
		
		AClassData data = apiResponse.getDataOneJava() ; 
		data.setViewNum(data.getViewNum() + 1);
		/* 数据库操作 */
		ApiResponse<Object> apiResponseRes = classDbService.updateOneDataService(data);
		ConstatFinalUtil.SYS_LOGGER.info("--课程资料更新返回结果:{}--",apiResponseRes.toJSON());
		return "/head/class/dataInfo";
	}
	
	/**
	 * 添加课程资料提交操作
	 * @return
	 */
	@RequestMapping(value = "/dataUpdateSubmit")
	public String dataUpdateSubmit(HttpServletRequest request,String id,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--dataUpdateSubmit--");
		ApiResponse<Object> apiResponseRes = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AClassData> apiResponse = this.classDbService.findOneDataService(condMap);
		/* 获取java对象 */
		AClassData data = apiResponse.getDataOneJava() ;
		
		boolean flag = false ; 
		String operType = request.getParameter("operType");
		if("upView".equalsIgnoreCase(operType))
		{
			data.setUpNum(data.getUpNum() + 1);
			
			flag = true ; 
		}else if("downView".equalsIgnoreCase(operType))
		{
			data.setDownNum(data.getDownNum() + 1);
			
			flag = true ; 
		}
		
		if(flag)
		{
			/* 数据库操作 */
			apiResponseRes = classDbService.updateOneDataService(data);
			ConstatFinalUtil.SYS_LOGGER.info("--课程资料更新返回结果:{}--",apiResponseRes.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", apiResponseRes.toJSON());
		}
		return dataUpdate(request, id, this.cleanInfoFlag,model);
	}
}

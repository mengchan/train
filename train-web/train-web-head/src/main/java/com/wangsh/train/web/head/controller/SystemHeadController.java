package com.wangsh.train.web.head.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.system.pojo.ASysFile;
import com.wangsh.train.system.pojo.ASysFileEnum;
import com.wangsh.train.system.pojo.ASysTech;
import com.wangsh.train.system.pojo.ASysTechEnum;
import com.wangsh.train.system.service.ISystemDbService;

/**
 * 	工作后台操作
 * 	@author wangsh
 */
@Controller
@RequestMapping("/head/system/")
public class SystemHeadController extends BaseController
{
	@Resource
	private ISystemDbService systemDbService;
	
	/**
	 * 系统文件列表页面
	 * @return
	 */
	@RequestMapping("/sysFileList")
	public String sysFileList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--sysFileList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		String relaId = request.getParameter("relaId");
		String fileType = request.getParameter("fileType");
		String fileRelaType = request.getParameter("fileRelaType");
		request.setAttribute("relaId", relaId);
		request.setAttribute("fileType", fileType);
		request.setAttribute("fileRelaType", fileRelaType);
		
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		condMap.put("fileType",fileType);
		condMap.put("fileRelaType", fileRelaType);
		condMap.put("relaId", relaId);
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<ASysFile> response = this.systemDbService.findCondListSysFileService(pageInfoUtil, condMap);
		response.clearInfo();
		/* 将结果存储到Request中 */
		model.addAttribute("response", response.toJSON());
		return "/head/system/sysFileList";
	}
	
	/**
	 * 打开系统文件添加页面
	 * @return
	 */
	@RequestMapping("/sysFileInsert")
	public String sysFileInsert(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--sysFileInsert--");
		return "/head/system/sysFileInsert";
	}
	
	/**
	 * 打开系统文件添加页面
	 * @return
	 */
	@RequestMapping(value = "/sysFileInsertSubmit")
	public String sysFileInsertSubmit(HttpServletRequest request,Model model, ASysFile sysFile)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--sysFileInsertSubmit--");
		
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		try
		{
			String pubTimeStr = request.getParameter("pubTimeStr");
			sysFile.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			
			dbResponse = this.systemDbService.saveOneSysFileService(sysFile);
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("添加一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return this.sysFileInsert(request,model);
	}
	
	/**
	 * 打开系统文件更新页面
	 * @return
	 */
	@RequestMapping("/sysFileUpdate")
	public String sysFileUpdate(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--sysFileUpdate--");
		/* 从session中获取信息 */
		String id = request.getParameter("id");
		/* 准备请求的信息 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		ApiResponse<ASysFile> apiResponse = this.systemDbService.findOneSysFileService(condMap);
		apiResponse.clearInfo();
		model.addAttribute("response", apiResponse.toJSON());
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 根据不同的请求跳转不同的页面 */
			return "/head/system/sysFileUpdate";
		}else if("file".equalsIgnoreCase(operType))
		{
			model.addAttribute("name", "上传系统文件");
			model.addAttribute("url", "/head/system/sysFileFileUpdateSubmit.htm?operType=" + operType + "&sysFileId=" + id);
			return "/head/system/sysFileFileUpdate";
		}
		return "/head/system/sysFileInfo";
	}
	
	/**
	 * 打开系统文件更新页面
	 * @return
	 */
	@RequestMapping(value = "/sysFileUpdateSubmit")
	public String sysFileUpdateSubmit(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--sysFileUpdateSubmit--");
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		try
		{
			/* 从session中获取信息 */
			String id = request.getParameter("id");
			/* 准备请求的信息 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", id);
			ApiResponse<ASysFile> apiResponse = this.systemDbService.findOneSysFileService(condMap);
			ASysFile sysFile = apiResponse.getDataOneJava() ; 
			model.addAttribute("response", apiResponse.toJSON());
			
			/* 接收参数 */
			String relaId = request.getParameter("relaId");
			String content = request.getParameter("content");
			
			String fileType = request.getParameter("fileType");
			String fileRelaType = request.getParameter("fileRelaType");
			String status = request.getParameter("status");
			
			sysFile.setRelaId(Integer.valueOf(relaId));
			sysFile.setContent(content);
			
			sysFile.setFileType(Byte.valueOf(fileType));
			sysFile.setFileRelaType(Byte.valueOf(fileRelaType));
			sysFile.setStatus(Byte.valueOf(status));
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			sysFile.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			
			/* 根据不同的请求跳转不同的页面 */
			dbResponse = this.systemDbService.updateOneSysFileService(sysFile);
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return "/head/system/sysFileUpdate";
	}
	
	/**
	 * 打开系统文件更新页面
	 * @return
	 */
	@RequestMapping(value = "/sysFileFileUpdateSubmit",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String sysFileFileUpdateSubmit(HttpServletRequest request,Model model,MultipartFile file)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--sysFileUpdateSubmit--");
		ApiResponse<Object> dbResponse = new ApiResponse<Object>();
		try
		{
			/* 从session中获取信息 */
			String sysFileId = request.getParameter("sysFileId");
			/* 准备请求的信息 */
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", sysFileId);
			ApiResponse<ASysFile> apiResponse = this.systemDbService.findOneSysFileService(condMap);
			ASysFile sysFile = apiResponse.getDataOneJava() ; 
			model.addAttribute("response", apiResponse.toJSON());
			
			Map<String, Object> paramsMap = new HashMap<String,Object>();
			try
			{
				if(file != null)
				{
					ConstatFinalUtil.SYS_LOGGER.info("name:{},oriName:{}" , file.getName() , file.getOriginalFilename());
					paramsMap.put("fileName", file.getOriginalFilename());
					paramsMap.put("inputStream", file.getInputStream());
					paramsMap.put("fileSize", file.getSize());
				}
			} catch (IOException e)
			{
				ConstatFinalUtil.SYS_LOGGER.error("上传文件失败了;");
			}
			
			String operType = request.getParameter("operType");
			if("insert".equalsIgnoreCase(operType))
			{
				/* 必须要先添加记录,然后再更新 */
				sysFile = new ASysFile();
				
				/* 接收参数 */
				String relaId = request.getParameter("relaId");
				
				String fileType = request.getParameter("fileType");
				String fileRelaType = request.getParameter("fileRelaType");
				
				sysFile.setRelaId(Integer.valueOf(relaId));
				
				sysFile.setFileType(Byte.valueOf(fileType));
				sysFile.setFileRelaType(Byte.valueOf(fileRelaType));
				sysFile.setContent("");
				sysFile.setStatus(ASysFileEnum.STATUS_ENABLE.getStatus());
				
				sysFile.setPubTime(new Date());
				dbResponse = this.systemDbService.saveOneSysFileService(sysFile);
				ConstatFinalUtil.SYS_LOGGER.error("添加记录返回结果:{}",dbResponse.toJSON().toJSONString());
			}
			
			/* 根据不同的请求跳转不同的页面 */
			dbResponse = this.systemDbService.updateOneSysFileService(sysFile,paramsMap);
			/* 存储结果 */
			model.addAttribute("response", dbResponse.toJSON());
			ConstatFinalUtil.SYS_LOGGER.error("更新记录返回结果:{}",dbResponse.toJSON().toJSONString());
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOGGER.error("更新一条记录失败了",e);
			dbResponse.setCode(ApiResponseEnum.STATUS_SERVER_ERROR.getStatus());
			Map<String, String> infoMap = new HashMap<String, String>();
			/* 报错信息的占位符 */
			infoMap.put("errinfo", e.toString());
			dbResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(dbResponse.getCode() + "") , infoMap);
		}
		return dbResponse.toJSON().toJSONString();
	}
	
	/*====技术表开始====*/
	/**
	 * 技术列表页面
	 * @return
	 */
	@RequestMapping("/techList")
	public String techList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--techList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 只查询启用的技术 */
		condMap.put("status", ASysTechEnum.STATUS_ENABLE.getStatus());
		condMap.put("extend", "true");
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<ASysTech> apiResponse = this.systemDbService.findCondListSysTechService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return "/head/system/techList";
	}
	
	/**
	 * 打开更新技术页面
	 * @return
	 */
	@RequestMapping("/techUpdate")
	public String techUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--techUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<ASysTech> apiResponse = this.systemDbService.findOneSysTechService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 存储request */
		request.setAttribute("response", apiResponse.toJSON());
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/system/techUpdate";
		}
		return "/head/system/techInfo";
	}
}

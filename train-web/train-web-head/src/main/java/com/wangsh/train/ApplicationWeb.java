package com.wangsh.train;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.DispatcherServlet;

@SpringBootApplication
public class ApplicationWeb {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationWeb.class, args);
	}
	
	/**
	 * 增加访问的前缀和后缀
	 */
//	@Bean
//	public ServletRegistrationBean<DispatcherServlet> servletRegistrationBean(DispatcherServlet servlet)
//	{
//		ServletRegistrationBean<DispatcherServlet> bean = new ServletRegistrationBean<DispatcherServlet>(servlet);
//		bean.addUrlMappings("*.htm");
//		return bean ; 
//	}
}

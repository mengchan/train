package com.wangsh.train.web.head.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.train.common.controller.BaseController;
import com.wangsh.train.common.pojo.ApiResponse;
import com.wangsh.train.common.pojo.ApiResponseEnum;
import com.wangsh.train.common.util.ConstatFinalUtil;
import com.wangsh.train.common.util.PageInfoUtil;
import com.wangsh.train.ques.pojo.AAnsSel;
import com.wangsh.train.ques.pojo.AAnsSelEnum;
import com.wangsh.train.ques.pojo.AAsk;
import com.wangsh.train.ques.pojo.AAskEnum;
import com.wangsh.train.ques.pojo.AExamDesc;
import com.wangsh.train.ques.pojo.AExamDescEnum;
import com.wangsh.train.ques.pojo.AExamDetail;
import com.wangsh.train.ques.pojo.AExamScoreDesc;
import com.wangsh.train.ques.pojo.AExamScoreDescEnum;
import com.wangsh.train.ques.pojo.AExamScoreDetail;
import com.wangsh.train.ques.service.IExamDbService;
import com.wangsh.train.ques.service.IQuesDbService;
import com.wangsh.train.system.pojo.ASysProEnum;
import com.wangsh.train.system.pojo.ASysTech;
import com.wangsh.train.system.service.ISystemDbService;
import com.wangsh.train.users.pojo.AUsers;

/**
 * 	管理员首页操作
 * 	@author wangsh
 */
@Controller
@RequestMapping("/head/ques/")
public class QuesHeadController extends BaseController
{
	@Resource
	private IQuesDbService quesDbService ; 
	@Resource
	private ISystemDbService systemDbService;
	@Resource
	private IExamDbService examDbService;
	
	/*====问题表开始====*/
	/**
	 * 问题列表页面
	 * @return
	 */
	@RequestMapping("/askList")
	public String askList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askList--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/* 只查询启用的问题 */
		String searchType = request.getParameter("searchType");
		request.setAttribute("searchType", searchType);
		if("self".equalsIgnoreCase(searchType))
		{
			/* 只查看自己的发布的问题 */
			condMap.put("usersId", users.getId());
		}else
		{
			/* 其他用户只能查看审核通过的问题 */
			condMap.put("status", AAskEnum.STATUS_AUDITYES.getStatus());
		}
		
		/*==添加额外的搜索条件==*/
		/*====来源开始====*/
		String souType = request.getParameter("souType");
		if(souType == null)
		{
			souType = "" ;
		}
		condMap.put("souType", souType);
		request.setAttribute("souType", souType);
		/*====来源结束====*/
		/*====类型开始====*/
		String askType = request.getParameter("askType");
		if(askType == null)
		{
			askType = "" ;
		}
		condMap.put("askType", askType);
		request.setAttribute("askType", askType);
		/*====类型结束====*/
		/*====技术开始====*/
		String techId = request.getParameter("techId");
		if(techId == null)
		{
			techId = "" ;
		}
		condMap.put("techId", techId);
		request.setAttribute("techId", techId);
		
		/*====interviewId开始====*/
		String interviewId = request.getParameter("interviewId");
		if(interviewId == null)
		{
			interviewId = "" ;
		}
		condMap.put("interviewId", interviewId);
		request.setAttribute("interviewId", interviewId);
		/*====interviewId结束====*/
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AAsk> apiResponse = this.quesDbService.findCondListAskService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		
		/* 查询所有的技术 */
		condMap.clear();
		condMap.put("status", ASysProEnum.STATUS_ENABLE.getStatus());
		ApiResponse<ASysTech> sysTechApiResponse = this.systemDbService.findCondListSysTechService(null, condMap);
		request.setAttribute("sysTechResponse", sysTechApiResponse.toJSON());
		return "/head/ques/askList";
	}

	/**
	 * 打开添加问题页面
	 * @param updateFlag: true:表示 清除信息
	 * @return
	 */
	@RequestMapping("/askInsert")
	public String askInsert(HttpServletRequest request,String updateFlag,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askInsert--");
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
		}
		
		/* 查询所有的技术 */
		condMap.clear();
		condMap.put("status", ASysProEnum.STATUS_ENABLE.getStatus());
		ApiResponse<ASysTech> sysTechApiResponse = this.systemDbService.findCondListSysTechService(null, condMap);
		request.setAttribute("sysTechResponse", sysTechApiResponse.toJSON());
		return "/head/ques/askInsert";
	}
	
	/**
	 * 添加问题提交操作
	 * @return
	 */
	@RequestMapping(value = "/askInsertSubmit")
	public String askInsertSubmit(HttpServletRequest request,String pubTimeStr , AAsk ask,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askInsertSubmit--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		/* 为对象赋值 */
		ask.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		
		ask.setUsersId(users.getId());
		ask.setUsersClaId(users.getClassId());
		
		/* 保存结果 */
		ApiResponse apiResponse = this.quesDbService.saveOneAskService(ask);
		ConstatFinalUtil.SYS_LOGGER.info("--问题添加返回结果:{}--",apiResponse.toJSON());
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return this.askInsert(request,this.cleanInfoFlag,model);
	}
	
	/**
	 * 打开更新问题页面
	 * @return
	 */
	@RequestMapping("/askUpdate")
	public String askUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AAsk> apiResponse = this.quesDbService.findOneAskService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 查询所有的技术 */
			condMap.clear();
			condMap.put("status", ASysProEnum.STATUS_ENABLE.getStatus());
			ApiResponse<ASysTech> sysTechApiResponse = this.systemDbService.findCondListSysTechService(null, condMap);
			request.setAttribute("sysTechResponse", sysTechApiResponse.toJSON());
			return "/head/ques/askUpdate";
		}
		return "/head/ques/askInfo";
	}
	
	/**
	 * 添加问题提交操作
	 * @return
	 */
	@RequestMapping(value = "/askUpdateSubmit")
	public String askUpdateSubmit(HttpServletRequest request,String id,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askUpdateSubmit--");
		ApiResponse<Object> apiResponseRes = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AAsk> apiResponse = this.quesDbService.findOneAskService(condMap);
		/* 获取java对象 */
		AAsk ask = apiResponse.getDataOneJava() ;
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String techId = request.getParameter("techId");
			String name = request.getParameter("name");
			String answer = request.getParameter("answer");
			String hardStar = request.getParameter("hardStar");
			String askType = request.getParameter("askType");
			String souType = request.getParameter("souType");
			String examContent = request.getParameter("examContent");
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			String content = request.getParameter("content");
			
			String status = request.getParameter("status");
			
			/* 设置属性 */
			ask.setTechId(Integer.valueOf(techId));
			ask.setName(name);
			ask.setAnswer(answer);
			ask.setHardStar(Byte.valueOf(hardStar));
			ask.setAskType(Byte.valueOf(askType));
			ask.setSouType(Byte.valueOf(souType));
			ask.setExamContent(examContent);
			ask.setStatus(Byte.valueOf(status));
			
			ask.setContent(content);
			
			ask.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			ask.setUpdateTime(new Date());
			/* 数据库操作 */
			ApiResponse dbRes = quesDbService.updateOneAskService(ask);
			ConstatFinalUtil.SYS_LOGGER.info("--问题更新返回结果:{}--",dbRes.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", dbRes.toJSON());
		}else
		{
			request.setAttribute("response", apiResponseRes.toJSON());
		}
		return askUpdate(request, id, this.cleanInfoFlag,model);
	}
	
	/**
	 * 问题批量操作
	 * @return
	 */
	@RequestMapping(value = "/askBatch" , produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String askBatch(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--askBatch--");
		Map<String, Object> condMap = new HashMap<String, Object>();
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 从session中获取信息 */
		String[] ids = request.getParameterValues("ids");
		
		/* 操作类型 */
		String operType = request.getParameter("operType");
		if(ids != null)
		{
			int totalNum = ids.length ; 
			int succedNum = 0 ; 
			int failedNum = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				condMap.clear();
				condMap.put("id", id);
				ApiResponse<AAsk> apiResponseDb = this.quesDbService.findOneAskService(condMap);
				AAsk ask = apiResponseDb.getDataOneJava();
				
				boolean updateFlag = false ; 
				if("status".equalsIgnoreCase(operType))
				{
					String status = request.getParameter("status");
					
					/* 设置状态 */
					ask.setStatus(Byte.valueOf(status));
					
					updateFlag = true ; 
				}else if("askType".equalsIgnoreCase(operType))
				{
					/* 设置问题类型 */
					String askType = request.getParameter("askType");
					ask.setAskType(Byte.valueOf(askType));
					updateFlag = true ; 
				}else if("teaId".equalsIgnoreCase(operType))
				{
					/* 设置问题类型 */
					String teaId = request.getParameter("teaId");
					ask.setTechId(Integer.valueOf(teaId));
					updateFlag = true ; 
				}
				
				if(updateFlag)
				{
					ApiResponse<Object> askClassResponse = this.quesDbService.updateOneAskService(ask);
					if(askClassResponse.getCode() == ApiResponseEnum.STATUS_SUCCESS.getStatus())
					{
						succedNum ++ ; 
					}else
					{
						failedNum ++ ; 
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("总条数:" + totalNum + ";");
			sb.append("成功条数:" + succedNum + ";");
			sb.append("失败条数:" + failedNum + ";");
			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
			apiResponse.setInfo(sb.toString(), Collections.EMPTY_MAP);
		}
		return apiResponse.toJSON().toJSONString();
	}
	/*====选项表开始====*/
	
	/**
	 * 打开添加选项页面
	 * @param updateFlag: true:表示 清除信息
	 * @return
	 */
	@RequestMapping("/ansSelInsert")
	public String ansSelInsert(HttpServletRequest request,String updateFlag,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--ansSelInsert--");
		return "/head/ques/ansSelInsert";
	}
	
	/**
	 * 添加选项提交操作
	 * @return
	 */
	@RequestMapping(value = "/ansSelInsertSubmit")
	public String ansSelInsertSubmit(HttpServletRequest request,String pubTimeStr , AAnsSel ansSel,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--ansSelInsertSubmit--");
		
		/* 为对象赋值 */
		ansSel.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
		
		ansSel.setStatus(AAnsSelEnum.STATUS_ENABLE.getStatus());
		
		/* 保存结果 */
		ApiResponse apiResponse = this.quesDbService.saveOneAnsSelService(ansSel);
		ConstatFinalUtil.SYS_LOGGER.info("--选项添加返回结果:{}--",apiResponse.toJSON());
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return this.ansSelInsert(request,this.cleanInfoFlag,model);
	}
	
	/**
	 * 打开更新选项页面
	 * @return
	 */
	@RequestMapping("/ansSelUpdate")
	public String ansSelUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--ansSelUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AAnsSel> apiResponse = this.quesDbService.findOneAnsSelService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/ques/ansSelUpdate";
		}
		return "/head/ques/ansSelInfo";
	}
	
	/**
	 * 添加选项提交操作
	 * @return
	 */
	@RequestMapping(value = "/ansSelUpdateSubmit")
	public String ansSelUpdateSubmit(HttpServletRequest request,String id,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--ansSelUpdateSubmit--");
		ApiResponse<Object> apiResponseRes = new ApiResponse<Object>();
		
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AAnsSel> apiResponse = this.quesDbService.findOneAnsSelService(condMap);
		/* 获取java对象 */
		AAnsSel ansSel = apiResponse.getDataOneJava() ;
		
		boolean flag = false ;
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String name = request.getParameter("name");
			String corrType = request.getParameter("corrType");
			String pubTimeStr = request.getParameter("pubTimeStr");
			
			/* 设置属性 */
			ansSel.setName(name);
			ansSel.setCorrType(Byte.valueOf(corrType));
			
			ansSel.setPubTime(this.dateFormatUtil.strDateTime(pubTimeStr));
			ansSel.setUpdateTime(new Date());
			flag = true ;
		}
		
		if(flag)
		{
			/* 数据库操作 */
			apiResponseRes = quesDbService.updateOneAnsSelService(ansSel);
			ConstatFinalUtil.SYS_LOGGER.info("--选项更新返回结果:{}--",apiResponseRes.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", apiResponseRes.toJSON());
		}
		return ansSelUpdate(request, id, this.cleanInfoFlag,model);
	}
	
	/**
	 * 考试列表页面
	 * @return
	 */
	@RequestMapping("/examDescList")
	public String examDescList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examDescList--");
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/*==添加额外的搜索条件==*/
		/*====试卷类型开始====*/
		String examType = request.getParameter("examType");
		if(examType == null)
		{
			examType = "" ;
		}
		condMap.put("examType", examType);
		request.setAttribute("examType", examType);
		/*====试卷类型结束====*/
		
		/*====classId开始====*/
		String classId = request.getParameter("classId");
		if(classId == null)
		{
			classId = "" ;
		}
		condMap.put("classId", classId);
		request.setAttribute("classId", classId);
		/*====classId结束====*/
		
		/* ==查看状态== */
		/* 准备状态 */
		List<String> statusList = new ArrayList<String>();
		/* 1:已发布,2;进行中,3:改成绩,4:完成 */
		statusList.add(AExamDescEnum.STATUS_ING.getStatus() + "");
		statusList.add(AExamDescEnum.STATUS_SCORE.getStatus() + "");
		statusList.add(AExamDescEnum.STATUS_FINISH.getStatus() + "");
		condMap.put("statusList", statusList);
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AExamDesc> apiResponse = this.examDbService.findCondListExamDescService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return "/head/ques/examDescList";
	}
	
	/**
	 * 打开更新考试页面
	 * @return
	 */
	@RequestMapping("/examDescUpdate")
	public String examDescUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examDescUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		/* 查询数据库 */
		ApiResponse<AExamDesc> apiResponse = this.examDbService.findOneExamDescService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 查询所有的技术 */
			condMap.clear();
			condMap.put("status", ASysProEnum.STATUS_ENABLE.getStatus());
			ApiResponse<ASysTech> sysTechApiResponse = this.systemDbService.findCondListSysTechService(null, condMap);
			request.setAttribute("sysTechResponse", sysTechApiResponse.toJSON());
			return "/head/ques/examDescUpdate";
		}
		return "/head/ques/examDescInfo";
	}
	
	/**
	 * 分数概要页面
	 * @return
	 */
	@RequestMapping("/examScoreDescList")
	public String examScoreDescList(HttpServletRequest request,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescList--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		/* 分页信息 */
		PageInfoUtil pageInfoUtil = this.proccedPageInfo(request);
		/* 搜索条件,生成公共的搜索条件 */
		Map<String, Object> condMap = this.proccedSearch(request);
		
		/*==添加额外的搜索条件==*/
		/* 只查询启用的问题 */
		String searchType = request.getParameter("searchType");
		request.setAttribute("searchType", searchType);
		if("self".equalsIgnoreCase(searchType))
		{
			/* 只查看自己的发布的问题 */
			condMap.put("usersId", users.getId());
		}else
		{
			/* ==查看状态== */
			/* 准备状态 */
			List<String> statusList = new ArrayList<String>();
			/* 1:已发布,2;进行中,3:改成绩,4:完成 */
			statusList.add(AExamScoreDescEnum.STATUS_COMMIT.getStatus() + "");
			statusList.add(AExamScoreDescEnum.STATUS_SCORE.getStatus() + "");
			statusList.add(AExamScoreDescEnum.STATUS_FINISH.getStatus() + "");
			condMap.put("statusList", statusList);
		}
		
		/*==添加额外的搜索条件==*/
		/*====usersClaId开始====*/
		String usersClaId = request.getParameter("usersClaId");
		if(usersClaId == null)
		{
			usersClaId = "" ;
		}
		condMap.put("usersClaId", usersClaId);
		request.setAttribute("usersClaId", usersClaId);
		/*====usersClaId结束====*/
		/*====examId开始====*/
		String examId = request.getParameter("examId");
		if(examId == null)
		{
			examId = "" ;
		}
		condMap.put("examId", examId);
		request.setAttribute("examId", examId);
		/*====examId结束====*/
		
		/*====classId开始====*/
		String classId = request.getParameter("classId");
		if(classId == null)
		{
			classId = "" ;
		}
		condMap.put("classId", classId);
		request.setAttribute("classId", classId);
		/*====classId结束====*/
		
		/*====orderBy开始====*/
		String orderBy = request.getParameter("orderBy");
		if(orderBy == null)
		{
			orderBy = "" ;
		}
		condMap.put("orderBy", orderBy);
		request.setAttribute("orderBy", orderBy);
		/*====orderBy结束====*/
		
		/* 查询数据库
		 * 所有的数据以JSON的形式返回
		 *  */
		ApiResponse<AExamScoreDesc> apiResponse = this.examDbService.findCondListExamScoreDescService(pageInfoUtil, condMap);
		/* 清除信息 */
		apiResponse.clearInfo();
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return "/head/ques/examScoreDescList";
	}
	
	/**
	 * 打开添加分数概要页面(开始做试卷了)
	 * @param updateFlag: true:表示 清除信息
	 * @return
	 */
	@RequestMapping("/examScoreDescInsert")
	public String examScoreDescInsert(HttpServletRequest request,String updateFlag,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescInsert--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		String examId = request.getParameter("examId");
		
		condMap.clear();
		condMap.put("id", examId);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AExamDesc> apiResponse = this.examDbService.findOneExamDescService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 故意打乱顺序 */
		AExamDesc examDesc = apiResponse.getDataOneJava();
		List<AExamDetail> selDetailList = examDesc.getSelDetailList();
		Collections.shuffle(selDetailList);
		examDesc.setSelDetailList(selDetailList);
		
		List<AExamDetail> multiSelDetailList = examDesc.getSelDetailList();
		Collections.shuffle(multiSelDetailList);
		examDesc.setSelDetailList(multiSelDetailList);
		
		List<AExamDetail> askDetailList = examDesc.getSelDetailList();
		Collections.shuffle(askDetailList);
		examDesc.setSelDetailList(askDetailList);
		
		List<AExamDetail> judeDetailList = examDesc.getSelDetailList();
		Collections.shuffle(judeDetailList);
		examDesc.setSelDetailList(judeDetailList);
		
		List<AExamDetail> proDetailList = examDesc.getSelDetailList();
		Collections.shuffle(proDetailList);
		examDesc.setSelDetailList(proDetailList);
		
		List<AExamDetail> interDetailList = examDesc.getSelDetailList();
		Collections.shuffle(interDetailList);
		examDesc.setSelDetailList(interDetailList);
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		return "/head/ques/examScoreDescInsert";
	}
	
	/**
	 * 添加分数概要提交操作
	 * @return
	 */
	@RequestMapping(value = "/examScoreDescInsertSubmit")
	public String examScoreDescInsertSubmit(HttpServletRequest request,String pubTimeStr , AExamScoreDesc examScoreDesc,Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescInsertSubmit--");
		AUsers users = (AUsers) this.findObjfromSession(request, "users");
		
		List<AExamScoreDetail> examScoreDetailList = examScoreDesc.getExamScoreDetailList() ; 
		/* 获取所有的参数 */
		Map<String, String[]> parameterMap = request.getParameterMap();
		for (Iterator iterator = parameterMap.entrySet().iterator(); iterator.hasNext();)
		{
			Entry<String, String[]> me = (Entry<String, String[]>) iterator.next();
			String key = me.getKey() ;
			String[] values = me.getValue() ; 
			
			if(key.startsWith("askId_"))
			{
				AExamScoreDetail examScoreDetail = new AExamScoreDetail();
				
				/* 是多选择的问题;单选,多选 */
				String[] keys = key.split("_");
				/* 问题id;askId */
				String askId = keys[1] ; 
				/* 选项的值;会有多选 */
				String val = values[0] ; 
				
				StringBuffer allValuesSb = new StringBuffer();
				for (int i = 0; i < values.length; i++)
				{
					String tempStr = values[i];
					allValuesSb.append(tempStr + ",");
				}
				examScoreDetail.setAskId(Integer.valueOf(askId));
				try
				{
					examScoreDetail.setAnswerId(Integer.valueOf(val));
				} catch (Exception e)
				{
				}
				examScoreDetail.setAnswer(allValuesSb.toString());
				
				/* 添加到集合中 */
				examScoreDetailList.add(examScoreDetail);
			}
		}
		
		/* 为对象赋值 */
		examScoreDesc.setPubTime(new Date());
		
		examScoreDesc.setUsersId(users.getId());
		examScoreDesc.setUsersClaId(users.getUsersClassId());
		
		/* 保存结果 */
		ApiResponse apiResponse = this.examDbService.saveOneExamScoreDescService(examScoreDesc);
		ConstatFinalUtil.SYS_LOGGER.info("--分数概要添加返回结果:{}--",apiResponse.toJSON());
		/* 将结果存储到Request中 */
		request.setAttribute("response", apiResponse.toJSON());
		return this.examScoreDescInsert(request,this.cleanInfoFlag,model);
	}
	
	/**
	 * 打开更新考试页面
	 * @return
	 */
	@RequestMapping("/examScoreDescUpdate")
	public String examScoreDescUpdate(HttpServletRequest request,String id,String updateFlag,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescUpdate--");
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AExamScoreDesc> apiResponse = this.examDbService.findOneExamScoreDescService(condMap);
		if(updateFlag == null || "".equalsIgnoreCase(updateFlag))
		{
			/* 清除信息 */
			apiResponse.clearInfo();
		}
		/* 保留原来的信息 */
		JSONObject responseJSON = this.souResponse(request, apiResponse.toJSON());
		request.setAttribute("response", responseJSON);
		
		/* 接收参数 */
		String operType = request.getParameter("operType");
		request.setAttribute("operType", operType);
		if("socreUpdate".equalsIgnoreCase(operType))
		{
			return "/head/ques/examScoreDescUpdate";
		}
		return "/head/ques/examScoreDescInfo";
	}
	
	/**
	 * 打开更新考试页面
	 * @return
	 */
	@RequestMapping("/examScoreDescUpdateSubmit")
	public String examScoreDescUpdateSubmit(HttpServletRequest request,String id,
			Model model)
	{
		ConstatFinalUtil.SYS_LOGGER.info("--examScoreDescUpdateSubmit--");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		/* 准备查询条件 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		condMap.put("extend", "true");
		/* 查询数据库 */
		ApiResponse<AExamScoreDesc> apiResponseRes = this.examDbService.findOneExamScoreDescService(condMap);
		AExamScoreDesc examScoreDesc = apiResponseRes.getDataOneJava() ; 
		
		boolean flag = false ;
		/* 接收参数 */
		String operType = request.getParameter("operType");
		if("socreUpdate".equalsIgnoreCase(operType))
		{
			List<AExamScoreDetail> examScoreDetailList = new ArrayList<AExamScoreDetail>() ; 
			/* 获取所有的参数 */
			Map<String, String[]> parameterMap = request.getParameterMap();
			for (Iterator iterator = parameterMap.entrySet().iterator(); iterator.hasNext();)
			{
				Entry<String, String[]> me = (Entry<String, String[]>) iterator.next();
				String key = me.getKey() ;
				String[] values = me.getValue() ; 
				
				if(key.startsWith("detailId_"))
				{
					AExamScoreDetail examScoreDetail = new AExamScoreDetail();
					
					/* 是多选择的问题;单选,多选 */
					String[] keys = key.split("_");
					/* 问题id;askId */
					String detailId = keys[1] ; 
					/* 选项的值;会有多选 */
					String val = values[0] ; 
					
					examScoreDetail.setId(Integer.valueOf(detailId));
					examScoreDetail.setScore(Integer.valueOf(val));
					
					/* 添加到集合中 */
					examScoreDetailList.add(examScoreDetail);
				}
			}
			/* 存储集合 */
			examScoreDesc.setExamScoreDetailList(examScoreDetailList);
			
			examScoreDesc.setStatus(AExamScoreDescEnum.STATUS_FINISH.getStatus());
			/* 为对象赋值 */
			examScoreDesc.setPubTime(new Date());
			
			flag = true ; 
		}
		if(flag)
		{
			/* 更新数据 */
			apiResponse = this.examDbService.updateOneExamScoreDescService(examScoreDesc);
			ConstatFinalUtil.SYS_LOGGER.info("--分数概要添加返回结果:{}--",apiResponse.toJSON());
			/* 将结果存储到Request中 */
			request.setAttribute("response", apiResponse.toJSON());
		}
		return "redirect:/head/ques/examScoreDescList.htm";
	}
}

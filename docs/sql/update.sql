ALTER TABLE `train`.`a_job_company` 
CHANGE COLUMN `up` `upNum` int NULL DEFAULT NULL COMMENT '顶的数量' AFTER `phone`,
CHANGE COLUMN `down` `downNum` int NULL DEFAULT NULL COMMENT '踩的数量' AFTER `upNum`;

ALTER TABLE `train`.`a_job_interview` 
CHANGE COLUMN `voiceUp` `upNum` int NULL DEFAULT NULL COMMENT '顶的数量' AFTER `voiceCode`,
CHANGE COLUMN `voiceDown` `downNum` int NULL DEFAULT NULL COMMENT '踩的数量' AFTER `upNum`;

ALTER TABLE `train`.`a_job_resume` 
CHANGE COLUMN `up` `upNum` int NULL DEFAULT NULL COMMENT '顶数量' AFTER `onTime`,
CHANGE COLUMN `down` `downNum` int NULL DEFAULT NULL COMMENT '踩数量' AFTER `upNum`;
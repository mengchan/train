# train

#### 介绍
培训相关的所有项目,需求是培训机构所有的需求

#### 软件架构
软件架构说明
1. 所有使用到的前端框架都木有提交到git上面;请大家自己下载,然后放到指定的目录就可以;
2. train-web-back使用的是H-ui.admin;直接将此包放到;/train-web-back/src/main/webapp/common/resource
3. train-web-head使用的是bootstrap,直接把包放到;/train-web-head/src/main/webapp/common/resource
4. 数据库的脚本文件统一放到了根项目下面的/docs/sql下面;

#### 安装教程

1. 请将每个web项目下面的/train-web-back/src/main/webapp/common/resource/sourceCode覆盖到开源框架中指定的位置中
2. 根据不同的文件夹替换不同的目录;
3. maven打包说明:
maven:
4. -Pprod clean package
5. -Pmy clean package

#### 使用说明

1. 数据库的脚本已经全部完成;/docs/sql/train_blank.zip培训项目的sql语句
2. xxxx
3. xxxx

#### 需求描述

1. 最终目的:把面试的题都收集起来;后续考试的时候就考试面试题
2. 需求描述:
	编程题就是上机题
3. 需要做的事情
	题库需要整理
	系统即将发生的bug要修改


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)